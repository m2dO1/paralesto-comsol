function model = conduction()
    import com.comsol.model.*
    import com.comsol.model.util.*
    model = ModelUtil.create('Model');
    current_folder = cd;
    model.modelPath(current_folder);
    model.label('conduction_50x50x50.mph');
    model.param.set('cdim', '2');
    model.component.create('comp1', false);
    model.component('comp1').geom.create('geom1', 3);
    model.component('comp1').curvedInterior(false);
    model.component('comp1').mesh.create('mesh1');
    
    model.component('comp1').geom('geom1').lengthUnit('cm');
    model.component('comp1').geom('geom1').repairTolType('relative');
    model.component('comp1').geom('geom1').create('blk1', 'Block');
    model.component('comp1').geom('geom1').feature('blk1').set('size', [10 10 10]);
    model.component('comp1').geom('geom1').create('blk2', 'Block');
    model.component('comp1').geom('geom1').feature('blk2').set('pos', [5 5 5]);
    model.component('comp1').geom('geom1').feature('blk2').set('base', 'center');
    model.component('comp1').geom('geom1').feature('blk2').set('size', {'10' 'cdim' '10'});
    model.component('comp1').geom('geom1').create('blk3', 'Block');
    model.component('comp1').geom('geom1').feature('blk3').set('pos', [5 5 5]);
    model.component('comp1').geom('geom1').feature('blk3').set('base', 'center');
    model.component('comp1').geom('geom1').feature('blk3').set('size', {'10' '10' 'cdim'});
    model.component('comp1').geom('geom1').feature('fin').set('repairtoltype', 'relative');
    model.component('comp1').geom('geom1').run;
    
    model.variable.create('var1');
    model.variable('var1').set('void_density', '1000');
    model.variable('var1').set('solid_density', '2710');
    model.variable('var1').set('Q_source', '2000');
    model.variable('var1').set('k_solid', '251');
    model.variable('var1').set('k_void', '0.598');
    model.variable('var1').set('Cp_void', '4184');
    model.variable('var1').set('Cp_solid', '903');
    
    model.component('comp1').cpl.create('intop1', 'Integration');
    model.component('comp1').cpl.create('aveop1', 'Average');
    model.component('comp1').cpl.create('aveop2', 'Average');
    model.component('comp1').cpl('intop1').selection.set([1 2 3 4 5 6 7 8 9]);
    model.component('comp1').cpl('aveop1').selection.geom('geom1', 2);
    model.component('comp1').cpl('aveop1').selection.set([14]);
    model.component('comp1').cpl('aveop2').selection.geom('geom1', 2);
    model.component('comp1').cpl('aveop2').selection.set([38]);
    
    model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
    model.component('comp1').physics('sens').selection.all;
    model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 3);
    model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
    model.component('comp1').physics('sens').feature('cvar1').selection.all;
    model.component('comp1').physics.create('ht2', 'HeatTransfer', 'geom1');
    model.component('comp1').physics('ht2').identifier('ht2');
    model.component('comp1').physics('ht2').field('temperature').field('T2');
    model.component('comp1').physics('ht2').feature('solid1').create('opq1', 'Opacity', 3);
    model.component('comp1').physics('ht2').create('hs1', 'HeatSource', 3);
    model.component('comp1').physics('ht2').feature('hs1').selection.set([5]);
    model.component('comp1').physics('ht2').create('temp1', 'TemperatureBoundary', 2);
    model.component('comp1').physics('ht2').feature('temp1').selection.set([2 5 8]);
    
    model.component('comp1').multiphysics.create('nitf1', 'NonIsothermalFlow', 3);
    
    model.component('comp1').mesh('mesh1').create('map1', 'Map');
    model.component('comp1').mesh('mesh1').create('swe1', 'Sweep');
    model.component('comp1').mesh('mesh1').feature('map1').selection.set([31 32 33]);
    model.component('comp1').mesh('mesh1').feature('map1').create('dis1', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('map1').create('dis2', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('map1').create('dis3', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('map1').create('dis4', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').selection.set([40]);
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').selection.set([64]);
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis3').selection.set([62]);
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis4').selection.set([63]);
    model.component('comp1').mesh('mesh1').feature('swe1').create('dis1', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('swe1').create('dis2', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('swe1').create('dis3', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').selection.set([7 8 9]);
    model.component('comp1').mesh('mesh1').feature('swe1').feature('dis2').selection.set([4 5 6]);
    model.component('comp1').mesh('mesh1').feature('swe1').feature('dis3').selection.set([1 2 3]);
    
    model.thermodynamics.label('Thermodynamics Package');
    
    model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
    model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
    model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
    model.component('comp1').physics('sens').feature('cvar1').set('valueType', 'real');
    model.component('comp1').physics('ht2').prop('PhysicalModelProperty').set('BackCompStateT', 0);
    model.component('comp1').physics('ht2').prop('ConsistentStabilization').set('glim', '(0.01[K])/ht2.helem');
    model.component('comp1').physics('ht2').feature('solid1').set('k_mat', 'userdef');
    model.component('comp1').physics('ht2').feature('solid1').set('k', {'k_void + (k_solid - k_void)*(ED)'; '0'; '0'; '0'; 'k_void + (k_solid - k_void)*(ED)'; '0'; '0'; '0'; 'k_void + (k_solid - k_void)*(ED)'});
    model.component('comp1').physics('ht2').feature('solid1').set('rho_mat', 'userdef');
    model.component('comp1').physics('ht2').feature('solid1').set('rho', 'void_density + (solid_density - void_density)*(ED)');
    model.component('comp1').physics('ht2').feature('solid1').set('Cp_mat', 'userdef');
    model.component('comp1').physics('ht2').feature('solid1').set('Cp', 'Cp_void + (Cp_solid - Cp_void) * (ED)');
    model.component('comp1').physics('ht2').feature('solid1').set('minput_strainreferencetemperature_src', 'userdef');
    model.component('comp1').physics('ht2').feature('solid1').label('Heat Transfer in Solids 1');
    model.component('comp1').physics('ht2').feature('solid1').feature('opq1').label('Opaque 1');
    model.component('comp1').physics('ht2').feature('dcont1').set('pairDisconnect', true);
    model.component('comp1').physics('ht2').feature('dcont1').label('Continuity');
    model.component('comp1').physics('ht2').feature('hs1').set('Q0', 'Q_source');
    
    model.component('comp1').multiphysics('nitf1').set('includeViscousDissipation', false);
    model.component('comp1').multiphysics('nitf1').set('Heat_physics', 'none');
    model.component('comp1').multiphysics('nitf1').label('Non-Isothermal Flow 1');
    
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').set('numelem', 50);
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').set('numelem', 20);
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis3').set('numelem', 20);
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis4').set('numelem', 10);
    model.component('comp1').mesh('mesh1').feature('swe1').set('facemethod', 'quadlegacy52');
    model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').set('numelem', 20);
    model.component('comp1').mesh('mesh1').feature('swe1').feature('dis2').set('numelem', 10);
    model.component('comp1').mesh('mesh1').feature('swe1').feature('dis3').set('numelem', 20);
    model.component('comp1').mesh('mesh1').run;
    
    model.study.create('std1');
    model.study('std1').create('sens', 'Sensitivity');
    model.study('std1').create('stat', 'Stationary');
    
    model.sol.create('sol1');
    model.sol('sol1').study('std1');
    model.sol('sol1').attach('std1');
    model.sol('sol1').create('st1', 'StudyStep');
    model.sol('sol1').create('v1', 'Variables');
    model.sol('sol1').create('s1', 'Stationary');
    model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
    model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
    model.sol('sol1').feature('s1').create('i1', 'Iterative');
    model.sol('sol1').feature('s1').create('d1', 'Direct');
    model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('so1', 'SOR');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('so1', 'SOR');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').create('d1', 'Direct');
    model.sol('sol1').feature('s1').feature.remove('fcDef');
    
    model.study('std1').feature('sens').set('gradientStep', 'stat');
    model.study('std1').feature('sens').set('optobj', {'comp1.intop1((ht2.kxx)*(T2x^2)+(ht2.kyy)*(T2y^2)+(ht2.kzz)*(T2z^2))'});
    model.study('std1').feature('sens').set('descr', {'Total net energy rate'});
    model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});
    
    model.sol('sol1').attach('std1');
    model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
    model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
    model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
    model.sol('sol1').feature('s1').feature('dDef').label('Direct 2');
    model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
    model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
    model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
    model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
    model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
    model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
    model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'i1');
    model.sol('sol1').feature('s1').feature('fc1').set('initstep', 0.01);
    model.sol('sol1').feature('s1').feature('fc1').set('minstep', 1.0E-6);
    model.sol('sol1').feature('s1').feature('fc1').set('maxiter', 50);
    model.sol('sol1').feature('s1').feature('fc1').set('termonres', false);
    model.sol('sol1').feature('s1').feature('i1').label('AMG, heat transfer variables (ht2)');
    model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', true);
    model.sol('sol1').feature('s1').feature('i1').set('rhob', 20);
    model.sol('sol1').feature('s1').feature('i1').feature('ilDef').label('Incomplete LU 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').label('Multigrid 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('maxcoarsedof', 50000);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('saamgcompwise', true);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').label('Presmoother 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('soDef').label('SOR 2');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').label('SOR 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').set('relax', 0.9);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').label('Postsmoother 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('soDef').label('SOR 2');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').label('SOR 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').set('relax', 0.9);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').label('Coarse Solver 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('dDef').label('Direct 2');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').label('Direct 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('linsolver', 'pardiso');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('pivotperturb', 1.0E-13);
    model.sol('sol1').feature('s1').feature('d1').label('Direct, heat transfer variables (ht2)');
    model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
    model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-13);
    model.sol('sol1').runAll;
end