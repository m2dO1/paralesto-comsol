function model = conjugate()
	% Model exported on Dec 6 2023, 08:51 by COMSOL 6.1.0.357.

	import com.comsol.model.*
	import com.comsol.model.util.*

	model = ModelUtil.create('Model');

	model.modelPath('/home/m2do/Documents/comsol-lsto/COMSOL-paper/example3/conjugate_avg_at_heat_source');

	model.label('conjugate_avg_at_heat_source.mph');

	model.param.set('cdim', '2');

	model.component.create('comp1', false);

	model.component('comp1').geom.create('geom1', 3);

	model.component('comp1').curvedInterior(false);

	model.component('comp1').mesh.create('mesh1');

	model.component('comp1').geom('geom1').lengthUnit('cm');
	model.component('comp1').geom('geom1').repairTolType('relative');
	model.component('comp1').geom('geom1').create('blk1', 'Block');
	model.component('comp1').geom('geom1').feature('blk1').set('size', [10 10 10]);
	model.component('comp1').geom('geom1').create('blk2', 'Block');
	model.component('comp1').geom('geom1').feature('blk2').set('pos', [5 5 5]);
	model.component('comp1').geom('geom1').feature('blk2').set('base', 'center');
	model.component('comp1').geom('geom1').feature('blk2').set('size', {'10' 'cdim' '10'});
	model.component('comp1').geom('geom1').create('blk3', 'Block');
	model.component('comp1').geom('geom1').feature('blk3').set('pos', [5 5 5]);
	model.component('comp1').geom('geom1').feature('blk3').set('base', 'center');
	model.component('comp1').geom('geom1').feature('blk3').set('size', {'10' '10' 'cdim'});
	model.component('comp1').geom('geom1').create('blk4', 'Block');
	model.component('comp1').geom('geom1').feature('blk4').set('pos', [5 5 5]);
	model.component('comp1').geom('geom1').feature('blk4').set('axis', [0 1 0]);
	model.component('comp1').geom('geom1').feature('blk4').set('base', 'center');
	model.component('comp1').geom('geom1').feature('blk4').set('size', {'10' 'cdim' '10'});
	model.component('comp1').geom('geom1').feature('fin').set('repairtoltype', 'relative');
	model.component('comp1').geom('geom1').run;

	model.variable.create('var1');
	model.variable('var1').set('void_density', '1000');
	model.variable('var1').set('solid_density', '2710');
	model.variable('var1').set('Q_source', '2000');
	model.variable('var1').set('k_solid', '251');
	model.variable('var1').set('k_void', '0.598');
	model.variable('var1').set('Cp_void', '4184');
	model.variable('var1').set('Cp_solid', '903');

	model.component('comp1').cpl.create('intop1', 'Integration');
	model.component('comp1').cpl.create('aveop1', 'Average');
	model.component('comp1').cpl.create('aveop2', 'Average');
	model.component('comp1').cpl.create('aveop3', 'Average');
	model.component('comp1').cpl.create('intop2', 'Integration');
	model.component('comp1').cpl('intop1').selection.set([1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27]);
	model.component('comp1').cpl('aveop1').selection.geom('geom1', 2);
	model.component('comp1').cpl('aveop1').selection.set([14]);
	model.component('comp1').cpl('aveop2').selection.geom('geom1', 2);
	model.component('comp1').cpl('aveop2').selection.set([104]);
	model.component('comp1').cpl('aveop3').selection.set([5 14 23]);
	model.component('comp1').cpl('intop2').selection.set([5 14 23]);

	model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
	model.component('comp1').physics('sens').selection.all;
	model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 3);
	model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
	model.component('comp1').physics('sens').feature('cvar1').selection.all;
	model.component('comp1').physics.create('spf', 'LaminarFlow', 'geom1');
	model.component('comp1').physics('spf').create('wall1', 'WallBC', 2);
	model.component('comp1').physics('spf').create('inl1', 'InletBoundary', 2);
	model.component('comp1').physics('spf').feature('inl1').selection.set([46]);
	model.component('comp1').physics('spf').create('out1', 'OutletBoundary', 2);
	model.component('comp1').physics('spf').feature('out1').selection.set([53]);
	model.component('comp1').physics('spf').create('vf1', 'VolumeForce', 3);
	model.component('comp1').physics('spf').feature('vf1').selection.all;
	model.component('comp1').physics.create('ht', 'HeatTransferInFluids', 'geom1');
	model.component('comp1').physics('ht').create('temp1', 'TemperatureBoundary', 2);
	model.component('comp1').physics('ht').feature('temp1').selection.set([46]);
	model.component('comp1').physics('ht').create('hs1', 'HeatSource', 3);
	model.component('comp1').physics('ht').feature('hs1').selection.set([5 14 23]);

	model.component('comp1').multiphysics.create('nitf1', 'NonIsothermalFlow', 3);
	model.component('comp1').multiphysics.create('nitf2', 'NonIsothermalFlow', 3);

	model.component('comp1').mesh('mesh1').create('map1', 'Map');
	model.component('comp1').mesh('mesh1').create('swe1', 'Sweep');
	model.component('comp1').mesh('mesh1').feature('map1').selection.set([31 32 33 64 65 66 97 98 99]);
	model.component('comp1').mesh('mesh1').feature('map1').create('dis3', 'Distribution');
	model.component('comp1').mesh('mesh1').feature('map1').create('dis5', 'Distribution');
	model.component('comp1').mesh('mesh1').feature('map1').feature('dis3').selection.set([34 35 38 40 115 120 142 144]);
	model.component('comp1').mesh('mesh1').feature('map1').feature('dis5').selection.set([36 75 77 79 80 143]);
	model.component('comp1').mesh('mesh1').feature('swe1').create('dis1', 'Distribution');
	model.component('comp1').mesh('mesh1').feature('swe1').create('dis2', 'Distribution');
	model.component('comp1').mesh('mesh1').feature('swe1').create('dis3', 'Distribution');
	model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').selection.set([7 8 9 25 26 27]);
	model.component('comp1').mesh('mesh1').feature('swe1').feature('dis2').selection.set([4 5 6 10 11 12 13 14 15 16 17 18 22 23 24]);
	model.component('comp1').mesh('mesh1').feature('swe1').feature('dis3').selection.set([1 2 3 19 20 21]);

	model.thermodynamics.label('Thermodynamics Package');

	model.component('comp1').view('view1').set('renderwireframe', true);

	model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
	model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
	model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
	model.component('comp1').physics('sens').feature('cvar1').set('valueType', 'real');
	model.component('comp1').physics('spf').prop('PhysicalModelProperty').set('Compressibility', 'WeaklyCompressible');
	model.component('comp1').physics('spf').prop('PhysicalModelProperty').set('BackCompState', 1);
	model.component('comp1').physics('spf').prop('AdvancedSettingProperty').set('UsePseudoTime', true);
	model.component('comp1').physics('spf').feature('fp1').set('rho_mat', 'userdef');
	model.component('comp1').physics('spf').feature('fp1').set('rho', 1000);
	model.component('comp1').physics('spf').feature('fp1').set('m_pow', 1);
	model.component('comp1').physics('spf').feature('fp1').set('mu_inf', 0);
	model.component('comp1').physics('spf').feature('fp1').set('n_car', 0);
	model.component('comp1').physics('spf').feature('fp1').set('lam_car', 0);
	model.component('comp1').physics('spf').feature('fp1').set('mu_mat', 'userdef');
	model.component('comp1').physics('spf').feature('fp1').set('mu', 0.01);
	model.component('comp1').physics('spf').feature('fp1').set('minput_temperature_src', 'userdef');
	model.component('comp1').physics('spf').feature('fp1').set('m_pow_mat', 'from_mat');
	model.component('comp1').physics('spf').feature('fp1').set('mu_inf_mat', 'from_mat');
	model.component('comp1').physics('spf').feature('fp1').set('lam_car_mat', 'from_mat');
	model.component('comp1').physics('spf').feature('fp1').set('n_car_mat', 'from_mat');
	model.component('comp1').physics('spf').feature('wall1').set('zeta', '-0.1[V]');
	model.component('comp1').physics('spf').feature('wall1').set('weakConstraints', false);
	model.component('comp1').physics('spf').feature('wall1').set('TranslationalVelocityOption', 'ZeroFixedWall');
	model.component('comp1').physics('spf').feature('wall1').label('Wall 1.1');
	model.component('comp1').physics('spf').feature('dcont1').set('pairDisconnect', true);
	model.component('comp1').physics('spf').feature('dcont1').label('Continuity');
	model.component('comp1').physics('spf').feature('inl1').set('U0in', 0.1);
	model.component('comp1').physics('spf').feature('inl1').set('constraintType', 'symmetricConstraint');
	model.component('comp1').physics('spf').feature('inl1').set('IT_list', 'user_defined');
	model.component('comp1').physics('spf').feature('inl1').set('LT_list', 'user_defined');
	model.component('comp1').physics('spf').feature('inl1').set('multipleInlets', false);
	model.component('comp1').physics('spf').feature('out1').set('constraintType', 'symmetricConstraint');
	model.component('comp1').physics('spf').feature('out1').set('multipleInlets', false);
	model.component('comp1').physics('spf').feature('vf1').set('F', {'-u*(1e4)* (1-ED)*1[kg/(s*m^3)]'; '-v*(1e4)* (1-ED)*1[kg/(s*m^3)]'; '-w*(1e4)* (1-ED)*1[kg/(s*m^3)] '});
	model.component('comp1').physics('spf').feature('grav1').label('Gravity');
	model.component('comp1').physics('ht').prop('PhysicalModelProperty').set('BackCompStateT', 0);
	model.component('comp1').physics('ht').prop('ConsistentStabilization').set('glim', '(0.01[K])/ht.helem');
	model.component('comp1').physics('ht').feature('fluid1').set('fluidType', 'gasLiquid');
	model.component('comp1').physics('ht').feature('fluid1').set('k_mat', 'userdef');
	model.component('comp1').physics('ht').feature('fluid1').set('k', {'0.598 + (251 - 0.598)*(1-ED)'; '0'; '0'; '0'; '0.598 + (251 - 0.598)*(1-ED)'; '0'; '0'; '0'; '0.598 + (251 - 0.598)*(1-ED)'});
	model.component('comp1').physics('ht').feature('fluid1').set('rho_mat', 'userdef');
	model.component('comp1').physics('ht').feature('fluid1').set('rho', '1000 + (2710 - 1000)*(1-ED)');
	model.component('comp1').physics('ht').feature('fluid1').set('Cp_mat', 'userdef');
	model.component('comp1').physics('ht').feature('fluid1').set('Cp', '4184 + (903 - 4184)*(1-ED)');
	model.component('comp1').physics('ht').feature('fluid1').set('minput_pressure_src', 'userdef');
	model.component('comp1').physics('ht').feature('fluid1').set('u_src', 'root.comp1.u');
	model.component('comp1').physics('ht').feature('fluid1').set('u', {'u'; 'v'; 'w'});
	model.component('comp1').physics('ht').feature('fluid1').set('gamma_not_IG_mat', 'userdef');
	model.component('comp1').physics('ht').feature('fluid1').set('heatcapacity_mat', 'userdef');
	model.component('comp1').physics('ht').feature('fluid1').set('heatcapacity', '4184 + (903 - 4184)*(1-ED)');
	model.component('comp1').physics('ht').feature('fluid1').label('Heat Transfer in Fluids 1');
	model.component('comp1').physics('ht').feature('dcont1').set('pairDisconnect', true);
	model.component('comp1').physics('ht').feature('dcont1').label('Continuity');
	model.component('comp1').physics('ht').feature('hs1').set('Q0', 2000);

	model.component('comp1').multiphysics('nitf1').set('includeViscousDissipation', false);
	model.component('comp1').multiphysics('nitf1').set('Fluid_physics', 'none');
	model.component('comp1').multiphysics('nitf1').set('Heat_physics', 'none');
	model.component('comp1').multiphysics('nitf1').label('Non-Isothermal Flow 1');
	model.component('comp1').multiphysics('nitf2').set('includeViscousDissipation', false);
	model.component('comp1').multiphysics('nitf2').set('Heat_physics', 'none');
	model.component('comp1').multiphysics('nitf2').set('SpecifyTref', 'FromFluidFlowInterface');
	model.component('comp1').multiphysics('nitf2').label('Non-Isothermal Flow 2');

	model.component('comp1').mesh('mesh1').feature('map1').feature('dis3').set('numelem', 10);
	model.component('comp1').mesh('mesh1').feature('map1').feature('dis5').set('numelem', 5);
	model.component('comp1').mesh('mesh1').feature('swe1').set('facemethod', 'quadlegacy52');
	model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').set('numelem', 10);
	model.component('comp1').mesh('mesh1').feature('swe1').feature('dis2').set('numelem', 5);
	model.component('comp1').mesh('mesh1').feature('swe1').feature('dis3').set('numelem', 10);
	model.component('comp1').mesh('mesh1').run;

	model.study.create('std1');
	model.study('std1').create('sens', 'Sensitivity');
	model.study('std1').create('stat', 'Stationary');

	model.sol.create('sol1');
	model.sol('sol1').study('std1');
	model.sol('sol1').attach('std1');
	model.sol('sol1').create('st1', 'StudyStep');
	model.sol('sol1').create('v1', 'Variables');
	model.sol('sol1').create('s1', 'Stationary');
	model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
	model.sol('sol1').feature('s1').create('se1', 'Segregated');
	model.sol('sol1').feature('s1').create('d1', 'Direct');
	model.sol('sol1').feature('s1').create('i1', 'Iterative');
	model.sol('sol1').feature('s1').create('i2', 'Iterative');
	model.sol('sol1').feature('s1').create('d2', 'Direct');
	model.sol('sol1').feature('s1').feature('se1').create('ss1', 'SegregatedStep');
	model.sol('sol1').feature('s1').feature('se1').create('ss2', 'SegregatedStep');
	model.sol('sol1').feature('s1').feature('se1').create('ll1', 'LowerLimit');
	model.sol('sol1').feature('s1').feature('se1').feature.remove('ssDef');
	model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('sc1', 'SCGS');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('sc1', 'SCGS');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').create('d1', 'Direct');
	model.sol('sol1').feature('s1').feature('i2').create('mg1', 'Multigrid');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('pr').create('so1', 'SOR');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('po').create('so1', 'SOR');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('cs').create('d1', 'Direct');
	model.sol('sol1').feature('s1').feature.remove('fcDef');

	model.study('std1').feature('sens').set('gradientStep', 'stat');
	model.study('std1').feature('sens').set('optobj', {'0.8*(1.0/10.7688 )*comp1.spf.inl1.pAverage' '0.2*(1.0/0.0188)*comp1.aveop3(T)'});
	model.study('std1').feature('sens').set('descr', {'Pressure average over feature selection' ''});
	model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat' 'stat'});

	model.sol('sol1').attach('std1');
	model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
	model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
	model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
	model.sol('sol1').feature('s1').feature('dDef').label('Direct 3');
	model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
	model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
	model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
	model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
	model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
	model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
	model.sol('sol1').feature('s1').feature('se1').label('Segregated 1.1');
	model.sol('sol1').feature('s1').feature('se1').set('maxsegiter', 200);
	model.sol('sol1').feature('s1').feature('se1').set('segtermonres', false);
	model.sol('sol1').feature('s1').feature('se1').set('segstabacc', 'segcflcmp');
	model.sol('sol1').feature('s1').feature('se1').feature('ss1').label('Temperature');
	model.sol('sol1').feature('s1').feature('se1').feature('ss1').set('segvar', {'comp1_T' 'comp1_ED'});
	model.sol('sol1').feature('s1').feature('se1').feature('ss1').set('linsolver', 'i1');
	model.sol('sol1').feature('s1').feature('se1').feature('ss1').set('subdamp', '0.8');
	model.sol('sol1').feature('s1').feature('se1').feature('ss2').label('Velocity u, Pressure p');
	model.sol('sol1').feature('s1').feature('se1').feature('ss2').set('segvar', {'comp1_p' 'comp1_u' 'comp1_ED'});
	model.sol('sol1').feature('s1').feature('se1').feature('ss2').set('linsolver', 'i1');
	model.sol('sol1').feature('s1').feature('se1').feature('ss2').set('subdamp', '0.5');
	model.sol('sol1').feature('s1').feature('se1').feature('ll1').label('Lower Limit 1.1');
	model.sol('sol1').feature('s1').feature('se1').feature('ll1').set('lowerlimit', 'comp1.T 0 ');
	model.sol('sol1').feature('s1').feature('d1').label('Direct, heat transfer variables (ht)');
	model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
	model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-13);
	model.sol('sol1').feature('s1').feature('i1').label('AMG, fluid flow variables (spf)');
	model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', true);
	model.sol('sol1').feature('s1').feature('i1').set('maxlinit', 1000);
	model.sol('sol1').feature('s1').feature('i1').set('rhob', 20);
	model.sol('sol1').feature('s1').feature('i1').feature('ilDef').label('Incomplete LU 1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').label('Multigrid 1.1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('maxcoarsedof', 80000);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('strconn', 0.02);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('saamgcompwise', true);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').label('Presmoother 1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('soDef').label('SOR 1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').label('SCGS 1.1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('linesweeptype', 'ssor');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('iter', 0);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('approxscgs', true);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('scgsdirectmaxsize', 1000);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').label('Postsmoother 1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('soDef').label('SOR 1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').label('SCGS 1.1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('linesweeptype', 'ssor');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('iter', 1);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('approxscgs', true);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('scgsdirectmaxsize', 1000);
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').label('Coarse Solver 1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('dDef').label('Direct 2');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').label('Direct 1.1');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('linsolver', 'pardiso');
	model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('pivotperturb', 1.0E-13);
	model.sol('sol1').feature('s1').feature('i2').label('AMG, heat transfer variables (ht)');
	model.sol('sol1').feature('s1').feature('i2').set('nlinnormuse', true);
	model.sol('sol1').feature('s1').feature('i2').set('rhob', 20);
	model.sol('sol1').feature('s1').feature('i2').feature('ilDef').label('Incomplete LU 1');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').label('Multigrid 1.1');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').set('prefun', 'saamg');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').set('maxcoarsedof', 50000);
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').set('saamgcompwise', true);
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').set('usesmooth', false);
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('pr').label('Presmoother 1');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('pr').feature('soDef').label('SOR 2');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('pr').feature('so1').label('SOR 1.1');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('pr').feature('so1').set('relax', 0.9);
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('po').label('Postsmoother 1');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('po').feature('soDef').label('SOR 2');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('po').feature('so1').label('SOR 1.1');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('po').feature('so1').set('relax', 0.9);
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('cs').label('Coarse Solver 1');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('cs').feature('dDef').label('Direct 2');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('cs').feature('d1').label('Direct 1.1');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('cs').feature('d1').set('linsolver', 'pardiso');
	model.sol('sol1').feature('s1').feature('i2').feature('mg1').feature('cs').feature('d1').set('pivotperturb', 1.0E-13);
	model.sol('sol1').feature('s1').feature('d2').label('Direct, fluid flow variables (spf)');
	model.sol('sol1').feature('s1').feature('d2').set('linsolver', 'pardiso');
	model.sol('sol1').feature('s1').feature('d2').set('pivotperturb', 1.0E-13);
	model.sol('sol1').runAll;
end
