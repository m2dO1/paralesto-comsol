clear 
close all
clc

% Copyright 2024 H Alicia Kim
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%% Import model as generated by COMSOL Multiphysics
model = model();

out_folder = 'conjugate'

%% Import the level-set modules
myMex = mexPackage(@MexLSM3D);
lsm   = myMex.LevelSetWrapper();

%% Material, BC and meshing, geometry
lsm.nelx = 25;
lsm.nely = 25;
lsm.nelz = 25;
Vdomain  = lsm.nelx * lsm.nely * lsm.nelz;

% Level-set parameters
lsm.move_limit = 0.15;
lsm.map_flag = 1; % 0: Least-squares, 1: Discrete adjoint
lsm.perturbation = 0.15;

% Initial voids
% ADD INITIAL VOIDS (OPTIONAL)

% Voids
lsm.CreateCuboidHoles(0.5*nelx, 0.5*nely, 0.5*nelz, nelx, 2.5, 2.5)

% Domains
% ADD FIXED DOMAINS (OPTIONAL)

% Initialize the level-set function
lsm.SetUp();


%% Map the coordinates between the level set function and COMSOL Multiphysics 
info = mphxmeshinfo(model);
ED_index = find(strcmp(info.dofs.dofnames, 'comp1.ED')) - 1;
ED_index_set = find(info.dofs.nameinds == ED_index);
ED_coords = info.dofs.coords(:, ED_index_set);
ED_T = ED_coords';
[physics_to_LSM_map, LSM_to_physics_map] = obtain_map_btw_physics_and_LSM_3D(ED_coords');

%% Prepare folders and files for the post-processing
pg1  = model.result.create('pg1', 'PlotGroup3D');
vol1 = pg1.feature.create('vol1', 'Volume');

% Define .txt files for optimization history
current_folder = fullfile(cd, out_folder);

% Make the directory for optimized mph model and convergence history
History_and_Optimized_Model_path = [current_folder '/HistoryAndOptimizedModelMph'];
if exist(History_and_Optimized_Model_path, 'dir') == 7
    rmdir(History_and_Optimized_Model_path,'s');
end
mkdir([current_folder '/HistoryAndOptimizedModelMph']);

% Path for convergence history
TOP_HISTORY = [current_folder '/HistoryAndOptimizedModelMph/History.txt'];
if exist(TOP_HISTORY, 'file') == 2
    delete(TOP_HISTORY);
end

% Make the directory for the STL files
STLfiles_path = [current_folder '/STLfiles'];
if exist(STLfiles_path, 'dir') == 7
    rmdir(STLfiles_path,'s');
end
mkdir([current_folder '/STLfiles']);

% Create figure
fig = figure('Color', [1, 1, 1]);
t_layout = tiledlayout(1, 2, 'TileSpacing','compact');

%% Set up for optimization loop
n_iterations = 0;
n_constraint = 1;
max_it       = 200;
volfrac      = 0.25*Vdomain; 

% Optimizer algorithm
% The optimization module is only available on Linux 
% MATLAB optimizer should be used on Windows (e.g., see the 3D code)
opt_algo_flag   = 0; % 0: Newton-Raphson, 1: MMA, 2: Simplex
is_3d = true; % flag to specify if the problem is 3D (true) or 2D (false)
myMexOpt = mexPackage(@MexOptimizer); 
opt = myMexOpt.OptimizerWrapper(n_constraint, volfrac, opt_algo_flag, is_3d);

%% Optimization loop
t_start = tic;
while n_iterations < max_it
    t_loop = tic;
    n_iterations = n_iterations + 1;
    fprintf('\n\n');
    disp(['iteration = ' num2str(n_iterations)])
    
    % ===================================================================
    % Step 1: Solve the model: Forward analysis
    % ===================================================================
    % Calculate the element densities
    ED_LSM = lsm.CalculateElementDensities(true);
    for i = 1:length(ED_LSM)
        if ED_LSM(i) < 0.0
            ED_LSM(i) = 0.0;
        else
            ED_LSM(i) = ED_LSM(i);
        end
    end
    
    % Sort the element densities from the level-set function to COMSOL
    ED_COMSOL = ED_LSM(LSM_to_physics_map);
    
    % Assign the sorted element densities to COMSOL
    U_sol = mphgetu(model, 'type', 'sol');
    U_sol(:) = 0; % reset solution
    U_sol(ED_index_set) = ED_COMSOL;
    model.sol('sol1').setU(U_sol);
    model.sol('sol1').createSolution;
    model.sol('sol1').feature('v1').set('initmethod','sol');
    model.sol('sol1').feature('v1').set('initsol','sol1');
    model.sol('sol1').feature('v1').set('notsolmethod','sol');
    model.sol('sol1').feature('v1').set('notsol','sol1');
    
    % Perform FEA with the assigned element densities
    model.sol('sol1').runAll;        
    
    % ===================================================================
    % Step 2: Calculate the objective function & its sensitivites
    % ===================================================================
    J1 = mphint2(model,'(1.0/0.0188)*comp1.aveop3(T)','volume','selection',[1]);
    J2 = mphglobal(model,'(1.0/10.7688 )*comp1.spf.inl1.pAverage');
    J = (0.2*J1) + (0.8*J2);
    Obj_History(n_iterations,: ) = [J];
    legend_str = {"J"};
    
    % Calculate the element sensitivities using the adjoint method with fsens()
    df_drho_COMSOL = mphgetu(model, 'type', 'fsens');
    df_drho_COMSOL = df_drho_COMSOL(ED_index_set);
    
    % Sort the element sensitivities from COMSOL to level-set
    df_drho_LSM = df_drho_COMSOL(physics_to_LSM_map);
    
    % ===================================================================
    % Step 3: Map the boundary point sensitivities
    % ===================================================================
    df_dbpt = lsm.MapSensitivities(df_drho_LSM, true);
    dg_dbpt = lsm.MapVolumeSensitivities();
    
    % ===================================================================
    % Step 4: Solve the sub-optimization problem (for design velocities)
    % ===================================================================      
    curr_cons_vals = lsm.GetVolume();
    Vol_History(n_iterations,:) = curr_cons_vals/Vdomain*100;
    lsm.GetLimits();

    opt.SetLimits(lsm.upper_lim, lsm.lower_lim);
    velocities = opt.Solve(df_dbpt, dg_dbpt, curr_cons_vals, false);

    % velocities = optimize(df_dbpt, dg_dbpt, volfrac, curr_cons_vals, ...
    %                       lsm.lower_lim, lsm.upper_lim);
    
    % ===================================================================
    % Step 5: Update the level-set function and write STL file
    % ===================================================================
    lsm.Update(velocities, false);
    lsm.WriteStl(n_iterations, STLfiles_path, 'levelset');
    
    % ===================================================================
    % Step 6: Post-processing
    % ===================================================================
    % Display each iteration data
    fprintf('Iter.:%3d Obj: %1.7f, Vol (%%): %1.3f \n', ...
            n_iterations,J,curr_cons_vals/Vdomain*100);
    
    % Save the history data
    TOP_HIS_fid = fopen(TOP_HISTORY,'a+');
    fprintf(TOP_HIS_fid,'%1.7f %1.3f \n',J,curr_cons_vals/Vdomain*100);
    fclose(TOP_HIS_fid);
    
    % Plot the element densities of optimal topology
    nexttile(t_layout, 1)
    surf1.set('expr','ED');
    surf1.set('colortablerev','on');
    surf1.set('colortable','Rainbow');
    surf1.feature('filt1').set('expr', 'ED > 0.5');
    pg1.run;
    mphplot(model,'pg1','rangenum',1);
    axis fill; axis off;
    set(gca, 'FontSize', 17)

    % Plot the optimization history
    nexttile(t_layout, 2)
    history_plot(Obj_History,Vol_History,legend_str);
    drawnow;

    % Elapsed time
    toc(t_loop);
end

% Total elapsed time
fprintf('\nEnd of the optimization loop.\n')
toc(t_start);

%% Save mph file after topology optimization for post-processing
mphsave(model,[current_folder '/HistoryAndOptimizedModelMph/Optimized_model.mph']);
savefig(fig, [current_folder, '/HistoryAndOptimizedModelMph/', out_folder]);

