# Exploring Plasticity Material Models in Level-Set Topology Optimization
This repository contains the code used to produce the results presented in the following conference paper:

M. Pozzi, A.T.R. Guibert, H.A. Kim, and F. Braghin. "Exploring Plasticity Material Models in Level-Set Topology Optimization". *ECCOMAS Congress 2024, Lisbon, Portugal*. DOI: [10.23967/eccomas.2024.035](https://www.scipedia.com/public/Pozzi_et_al_2024a).

## Instructions
To run the examples, clone [ParaLeSTO-COMSOL](https://gitlab.com/m2dO1/paralesto-comsol) repository and install all the required dependencies. See the documentation in the repository for more information. Then, run the `setup.m` script. This will compile the MEX files and add the necessary paths to the MATLAB environment.

In addition, to use elasto-plastic material models in COMSOL Multiphysics, the [Nonlinear Structural Materials Module](https://www.comsol.com/nonlinear-structural-materials-module) is required.

## Examples
This folder contains the following examples:
- `hook_2d`: 2d hook structure with elasto-plastic material model and linear isotropic hardening. Additionally, the same structure is optimized using the linear elastic material model.

- `hook_3d`: 3d hook structure with elasto-plastic material model and linear isotropic hardening. Additionally, the same structure is optimized using the linear elastic material model.

- `hook_3d_linear_hardening`: 3d hook structure with elasto-plastic material model and linear isotropic hardening. The scripts `main_yield.m` and `main_hardening.m` are used to run the optimization with different values for the yield criterion and the hardening modulus, respectively.

- `plastic_models_2d`: 2d hook structure with elasto-plastic material model and different isotropic hardening laws (linear and exponential).

- `plastic_models_3d`: 3d hook structure with elasto-plastic material model and different isotropic hardening laws (linear and exponential).

## Citation
To cite this package, please cite the following:

A.T.R. Guibert, J. Hyun, A. Neofytou, and H.A. Kim. "Facilitating multidisciplinary collaboration through a versatile level-set topology optimization framework via COMSOL multiphysics". *Structural and Multidisciplinary Optimization*, (2024). DOI: [10.1007/s00158-024-03877-w](https://doi.org/10.1007/s00158-024-03877-w).

M. Pozzi, A.T.R. Guibert, H.A. Kim, and F. Braghin. "Exploring Plasticity Material Models in Level-Set Topology Optimization". *ECCOMAS Congress 2024, Lisbon, Portugal*. DOI: [10.23967/eccomas.2024.035](https://www.scipedia.com/public/Pozzi_et_al_2024a).

## Contact
If you have any questions, create an issue or email m2dolab@ucsd.edu.
