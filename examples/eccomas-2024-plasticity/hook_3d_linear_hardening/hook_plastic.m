function out = model(yield, hardening)
%
% hook_plastic.m
%
% Model exported on May 20 2024, 11:39 by COMSOL 6.2.0.290.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('/home/matteo/Documents/plasticity/hook_3d');

model.label('hook_3d.mph');

model.param.set('lx', '1[m]');
model.param.set('ly', '1[m]');
model.param.set('lz', '0.2[m]');
model.param.set('nelx', '50');
model.param.set('nely', '50');
model.param.set('nelz', '10');
model.param.set('x1', '0.4');
model.param.set('y1', '0.3');
model.param.set('y2', '0.4');
model.param.set('young', '72[GPa]');
model.param.set('poisson', '0.3');
model.param.set('yield', yield);
model.param.set('disp', 'ly/50');
model.param.set('ED_min', '1e-6');
model.param.set('penal', '1');
model.param.set('hardening', hardening);

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component('comp1').func.create('an1', 'Analytic');
model.component('comp1').func('an1').set('expr', 'ED_min+(1-ED_min)*x');

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').geomRep('comsol');
model.component('comp1').geom('geom1').create('blk1', 'Block');
model.component('comp1').geom('geom1').feature('blk1').set('size', {'lx' 'ly' 'lz'});
model.component('comp1').geom('geom1').create('wp1', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp1').set('quickplane', 'yz');
model.component('comp1').geom('geom1').feature('wp1').set('quickx', 'x1*lx');
model.component('comp1').geom('geom1').feature('wp1').set('unite', true);
model.component('comp1').geom('geom1').create('parf1', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf1').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf1').selection('face').set('blk1(1)', 6);
model.component('comp1').geom('geom1').create('wp2', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp2').set('quickplane', 'zx');
model.component('comp1').geom('geom1').feature('wp2').set('quicky', 'y1*ly');
model.component('comp1').geom('geom1').feature('wp2').set('unite', true);
model.component('comp1').geom('geom1').create('parf2', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf2').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf2').selection('face').set('parf1(1)', 7);
model.component('comp1').geom('geom1').create('wp3', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp3').set('quickplane', 'zx');
model.component('comp1').geom('geom1').feature('wp3').set('quicky', 'y2*ly');
model.component('comp1').geom('geom1').feature('wp3').set('unite', true);
model.component('comp1').geom('geom1').create('parf3', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf3').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf3').selection('face').set('parf2(1)', 8);
model.component('comp1').geom('geom1').run;

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
model.component('comp1').material('mat1').propertyGroup.create('ElastoplasticModel', 'Elastoplastic material model');

model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 3);
model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);
model.component('comp1').physics.create('solid', 'SolidMechanics', 'geom1');
model.component('comp1').physics('solid').feature('lemm1').create('plsty1', 'Plasticity', 3);
model.component('comp1').physics('solid').create('fix1', 'Fixed', 2);
model.component('comp1').physics('solid').feature('fix1').selection.set([5]);
model.component('comp1').physics('solid').create('disp1', 'Displacement2', 2);
model.component('comp1').physics('solid').feature('disp1').selection.set([8]);

model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').create('swe1', 'Sweep');
model.component('comp1').mesh('mesh1').feature('map1').selection.set([7 8 9]);
model.component('comp1').mesh('mesh1').feature('map1').create('size1', 'Size');
model.component('comp1').mesh('mesh1').feature('swe1').create('dis1', 'Distribution');

model.component('comp1').material('mat1').propertyGroup('def').set('density', 'density');
model.component('comp1').material('mat1').propertyGroup('Enu').set('E', 'an1(ED)*young');
model.component('comp1').material('mat1').propertyGroup('Enu').set('nu', 'poisson');
model.component('comp1').material('mat1').propertyGroup('ElastoplasticModel').set('sigmags', 'an1(ED)*yield');
model.component('comp1').material('mat1').propertyGroup('ElastoplasticModel').set('sigmagh', 'an1(ED)*hardening*solid.epe');

model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
model.component('comp1').physics('solid').prop('ShapeProperty').set('order_displacement', 1);
model.component('comp1').physics('solid').feature('lemm1').set('geometricNonlinearity', 'linear');
model.component('comp1').physics('solid').feature('lemm1').set('CalculateDissipatedEnergy', true);
model.component('comp1').physics('solid').feature('lemm1').feature('plsty1').set('IsotropicHardeningModel', 'HardeningFunction');
model.component('comp1').physics('solid').feature('disp1').set('Direction', {'free'; 'prescribed'; 'free'});
model.component('comp1').physics('solid').feature('disp1').set('U0', {'0'; '-disp'; '0'});

model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('custom', 'on');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmax', 'lz/nelz');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmaxactive', true);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmin', 'lz/nelz');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hminactive', true);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').set('numelem', 'nelx');
model.component('comp1').mesh('mesh1').run;

model.study.create('std1');
model.study('std1').create('sens', 'Sensitivity');
model.study('std1').create('stat', 'Stationary');
model.study('std1').feature('stat').set('geometricNonlinearity', true);

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').create('d1', 'Direct');
model.sol('sol1').feature('s1').create('i1', 'Iterative');
model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('so1', 'SOR');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('so1', 'SOR');
model.sol('sol1').feature('s1').feature.remove('fcDef');

% model.result.create('pg1', 'PlotGroup3D');
% model.result.create('pg2', 'PlotGroup3D');
% model.result('pg1').create('slc1', 'Slice');
% model.result('pg2').create('vol1', 'Volume');
% model.result('pg2').feature('vol1').set('expr', 'solid.misesGp');
% model.result('pg2').feature('vol1').create('def', 'Deform');

model.study('std1').feature('sens').set('gradientStep', 'stat');
model.study('std1').feature('sens').set('optobj', {'-comp1.solid.Ws_tot'});
model.study('std1').feature('sens').set('descr', {'Total elastic strain energy'});
model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});

model.sol('sol1').attach('std1');
model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
model.sol('sol1').feature('s1').feature('dDef').label('Direct 2');
model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'd1');
model.sol('sol1').feature('s1').feature('fc1').set('maxiter', 100);
model.sol('sol1').feature('s1').feature('d1').label('Suggested Direct Solver (solid)');
model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-9);
model.sol('sol1').feature('s1').feature('d1').set('nliniterrefine', true);
model.sol('sol1').feature('s1').feature('i1').label('Suggested Iterative Solver (GMRES with SA AMG) (solid)');
model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', true);
model.sol('sol1').feature('s1').feature('i1').set('rhob', 40);
model.sol('sol1').feature('s1').feature('i1').feature('ilDef').label('Incomplete LU 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').label('Multigrid 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').label('Presmoother 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('soDef').label('SOR 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').label('SOR 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').set('relax', 0.8);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').label('Postsmoother 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('soDef').label('SOR 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').label('SOR 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').set('relax', 0.8);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').label('Coarse Solver 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('dDef').label('Direct 1');
model.sol('sol1').runAll;

% model.result('pg1').label('Sensitivity');
% model.result('pg1').feature('slc1').set('descr', 'fsens(ED)');
% model.result('pg1').feature('slc1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg1').feature('slc1').set('resolution', 'normal');
% model.result('pg2').label('Stress (solid)');
% model.result('pg2').set('frametype', 'spatial');
% model.result('pg2').feature('vol1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg2').feature('vol1').set('colortable', 'Prism');
% model.result('pg2').feature('vol1').set('resolution', 'custom');
% model.result('pg2').feature('vol1').set('refine', 2);
% model.result('pg2').feature('vol1').set('threshold', 'manual');
% model.result('pg2').feature('vol1').set('thresholdvalue', 0.2);
% model.result('pg2').feature('vol1').set('resolution', 'custom');
% model.result('pg2').feature('vol1').set('refine', 2);
% model.result('pg2').feature('vol1').feature('def').set('scaleactive', true);

out = model;
