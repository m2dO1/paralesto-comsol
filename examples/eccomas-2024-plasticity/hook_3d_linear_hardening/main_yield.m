clear; clc; close all;

max_it = 300;
yield = [100, 150, 200, 250]; % [MPa]
hardening = 573; % [MPa]

% Strings
hardening_str = num2str(hardening);

% Loop
Ws = zeros(size(yield));
Wp = zeros(size(yield));
t_start = tic;
for ii = 1:length(yield)
    % Convert to string
    yield_str = num2str(yield(ii));

    % Run optimization
    [Ws_temp, Wp_temp] = main_fun(yield_str, hardening_str, max_it);
    Ws(ii) = Ws_temp;
    Wp(ii) = Wp_temp;
    disp('Done.')

    % Clean
    clc; close all;
end

% Print end message
disp('All done.')
t_span = toc(t_start);
fprintf('Elapsed time is %f seconds.\n', t_span)

% Plot results
fig = figure();
hold on; grid on;
xlabel('Yield stress [MPa]', 'Interpreter', 'latex')
yyaxis left
plot(yield, Ws*1e-3, '.-', 'LineWidth', 2, 'MarkerSize', 20)
ylabel('Strain energy [kJ]', 'Interpreter', 'latex')
yyaxis right
plot(yield, Wp*1e-3, '.-', 'LineWidth', 2, 'MarkerSize', 20)
ylabel('Plastic dissipation [kJ]', 'Interpreter', 'latex')
set(gca, 'FontSize', 20, 'TickLabelInterpreter', 'latex')

savefig(fig, fullfile(cd, 'results_yield'));
save('results_yield.mat', 'Ws', 'Wp', 'yield', 'hardening', 't_span')














