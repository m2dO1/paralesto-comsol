clear; clc; close all;

max_it = 300;
plasticity_type = {'linear', 'voce'};
w_Ws_str = {'1.00', '0.50'};
w_Wp_str = {'0.00', '0.50'};

% Loop over plasticity types
Ws = zeros(length(plasticity_type), length(w_Ws_str));
Wp = zeros(length(plasticity_type), length(w_Ws_str));
t_start = tic;
for ii = 1:length(plasticity_type)

    % Loop over weights
    for jj = 1:length(w_Ws_str)

        % Run optimization
        [Ws_temp, Wp_temp] = main_fun(max_it, plasticity_type{ii}, w_Ws_str{jj}, w_Wp_str{jj});
        Ws(ii, jj) = Ws_temp;
        Wp(ii, jj) = Wp_temp;
        disp('Done.')

        % Clean
        clc; close all;
    end
end

% Print end message
disp('All done.')
t_span = toc(t_start);
fprintf('Elapsed time is %f seconds.\n', t_span)

% Data
w_Ws = zeros(size(w_Ws_str));
w_Wp = zeros(size(w_Wp_str));
for ii = 1:length(w_Ws_str)
    w_Ws(ii) = str2double(w_Ws_str{ii});
    w_Wp(ii) = str2double(w_Wp_str{ii});
end

% Plot
fig = figure();
tiledlayout('flow', 'TileSpacing', 'compact')
for ii = 1:length(plasticity_type)
    nexttile
    hold on; grid on;
    yyaxis left
    plot(w_Ws, Ws(ii, :), '.-', 'LineWidth', 2)
    title(plasticity_type{ii}, 'Interpreter', 'latex')
    xlabel('Weight', 'Interpreter', 'latex')
    ylabel('$W_s$ [J]', 'Interpreter', 'latex')
    yyaxis right
    plot(w_Ws, Wp(ii, :), '.-', 'LineWidth', 2)
    ylabel('$W_p$ [J]', 'Interpreter', 'latex')
    set(gca, 'FontSize', 20, 'TickLabelInterpreter', 'latex')
end

savefig(fig, fullfile(cd, 'results'));
save('results.mat', 'Ws', 'Wp', 'w_Ws', 'w_Wp', 't_span', 'plasticity_type')

% Plot
figure
tiledlayout('flow', 'TileSpacing', 'compact')
hold on; grid on;
for ii = 1:size(Ws, 1)
    nexttile
    plot(-Ws(ii, :)*1e-3, Wp(ii, :)*1e-3, 'LineWidth', 2)
    xlabel('$W_s$ [kJ]', 'Interpreter', 'latex')
    ylabel('$W_p$ [kJ]', 'Interpreter', 'latex')
    legend(plasticity_type{ii}, 'Interpreter', 'latex')
    set(gca, 'FontSize', 20, 'TickLabelInterpreter', 'latex')
end









