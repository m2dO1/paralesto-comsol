function out = model(hardening_function)
%
% hook_plastic.m
%
% Model exported on May 20 2024, 09:18 by COMSOL 6.2.0.290.

if nargin == 0
    hardening_function = 'hardening*solid.epe';
end

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('/home/matteo/Documents/plasticity/hook_2d');

model.label('hook_elastic.mph');

model.param.set('lx', '1[m]');
model.param.set('ly', '1[m]');
model.param.set('lz', '0.01[m]');
model.param.set('nelx', '100');
model.param.set('nely', '100');
model.param.set('x1', '0.4');
model.param.set('y1', '0.3');
model.param.set('y2', '0.4');
model.param.set('young', '72[GPa]');
model.param.set('poisson', '0.3');
model.param.set('yield', '125[MPa]');
model.param.set('disp', 'ly/50');
model.param.set('ED_min', '1e-6');
model.param.set('penal', '1');
model.param.set('hardening', '573[MPa]');
model.param.set('ludwik_k', '573[MPa]');
model.param.set('ludwik_n', '0.55');
model.param.set('voce_sigma', '555[MPa]');
model.param.set('voce_beta', '15.5');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 2);

model.component('comp1').func.create('an1', 'Analytic');
model.component('comp1').func('an1').set('expr', 'ED_min+(1-ED_min)*x');

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').create('r1', 'Rectangle');
model.component('comp1').geom('geom1').feature('r1').set('size', {'lx' 'ly'});
model.component('comp1').geom('geom1').create('ls1', 'LineSegment');
model.component('comp1').geom('geom1').feature('ls1').set('specify1', 'coord');
model.component('comp1').geom('geom1').feature('ls1').set('coord1', {'lx' 'ly*y1'});
model.component('comp1').geom('geom1').feature('ls1').set('specify2', 'coord');
model.component('comp1').geom('geom1').feature('ls1').set('coord2', {'lx' 'ly*y2'});
model.component('comp1').geom('geom1').feature('ls1').setAttribute('construction', 'on');
model.component('comp1').geom('geom1').create('pare1', 'PartitionEdges');
model.component('comp1').geom('geom1').feature('pare1').set('position', 'projection');
model.component('comp1').geom('geom1').feature('pare1').selection('edge').set('r1(1)', 2);
model.component('comp1').geom('geom1').feature('pare1').selection('vertexproj').set('ls1(1)', 2);
model.component('comp1').geom('geom1').create('pare2', 'PartitionEdges');
model.component('comp1').geom('geom1').feature('pare2').set('position', 'projection');
model.component('comp1').geom('geom1').feature('pare2').selection('edge').set('pare1(1)', 4);
model.component('comp1').geom('geom1').feature('pare2').selection('vertexproj').set('ls1(1)', 1);
model.component('comp1').geom('geom1').create('ls2', 'LineSegment');
model.component('comp1').geom('geom1').feature('ls2').set('specify2', 'coord');
model.component('comp1').geom('geom1').feature('ls2').set('coord2', {'x1*lx' 'ly'});
model.component('comp1').geom('geom1').feature('ls2').selection('vertex1').set('pare2(1)', 2);
model.component('comp1').geom('geom1').create('pare3', 'PartitionEdges');
model.component('comp1').geom('geom1').feature('pare3').set('position', 'projection');
model.component('comp1').geom('geom1').feature('pare3').selection('edge').set('pare2(1)', 3);
model.component('comp1').geom('geom1').feature('pare3').selection('vertexproj').set('ls2(1)', 2);
model.component('comp1').geom('geom1').run;
model.component('comp1').geom('geom1').run('fin');

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
model.component('comp1').material('mat1').propertyGroup.create('ElastoplasticModel', 'Elastoplastic material model');

model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 2);
model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);
model.component('comp1').physics.create('solid', 'SolidMechanics', 'geom1');
model.component('comp1').physics('solid').feature('lemm1').create('plsty1', 'Plasticity', 2);
model.component('comp1').physics('solid').create('fix1', 'Fixed', 1);
model.component('comp1').physics('solid').feature('fix1').selection.set([3]);
model.component('comp1').physics('solid').create('disp1', 'Displacement1', 1);
model.component('comp1').physics('solid').feature('disp1').selection.set([6]);

model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').feature('map1').create('size1', 'Size');

model.component('comp1').view('view1').axis.set('xmin', -0.023703038692474365);
model.component('comp1').view('view1').axis.set('xmax', 1.051185131072998);
model.component('comp1').view('view1').axis.set('ymin', -0.15076126158237457);
model.component('comp1').view('view1').axis.set('ymax', 1.239583134651184);

model.component('comp1').material('mat1').propertyGroup('def').set('density', '1');
model.component('comp1').material('mat1').propertyGroup('Enu').set('E', 'an1(ED)*young');
model.component('comp1').material('mat1').propertyGroup('Enu').set('nu', 'poisson');
model.component('comp1').material('mat1').propertyGroup('ElastoplasticModel').set('sigmags', 'an1(ED)*yield');
model.component('comp1').material('mat1').propertyGroup('ElastoplasticModel').set('sigmagh', ['an1(ED)*', hardening_function]);

model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
model.component('comp1').physics('solid').prop('ShapeProperty').set('order_displacement', 1);
model.component('comp1').physics('solid').prop('d').set('d', 'lz');
% model.component('comp1').physics('solid').feature('lemm1').set('E_mat', 'userdef');
% model.component('comp1').physics('solid').feature('lemm1').set('E', 'an1(ED)*young');
model.component('comp1').physics('solid').feature('lemm1').set('geometricNonlinearity', 'linear');
model.component('comp1').physics('solid').feature('lemm1').set('CalculateDissipatedEnergy', true);
% model.component('comp1').physics('solid').feature('lemm1').feature('plsty1').set('sigmags', 'an1(ED)*yield');
model.component('comp1').physics('solid').feature('lemm1').feature('plsty1').set('IsotropicHardeningModel', 'HardeningFunction');
% model.component('comp1').physics('solid').feature('lemm1').feature('plsty1').set('sigmagh', 'hardening*solid.epe');
model.component('comp1').physics('solid').feature('disp1').set('Direction', {'free'; 'prescribed'; 'free'});
model.component('comp1').physics('solid').feature('disp1').set('U0', {'0'; '-disp'; '0'});

model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('custom', 'on');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmax', 'lx/nelx');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmaxactive', true);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmin', 'lx/nelx');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hminactive', true);
model.component('comp1').mesh('mesh1').run;

model.component('comp1').physics('solid').feature('lemm1').feature('plsty1').set('sigmagh_mat', 'from_mat');

model.study.create('std1');
model.study('std1').create('sens', 'Sensitivity');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').feature.remove('fcDef');

% model.result.create('pg1', 'PlotGroup2D');
% model.result.create('pg2', 'PlotGroup2D');
% model.result('pg1').create('surf1', 'Surface');
% model.result('pg2').create('surf1', 'Surface');
% model.result('pg2').feature('surf1').set('expr', 'solid.misesGp');
% model.result('pg2').feature('surf1').create('def', 'Deform');

model.study('std1').feature('sens').set('gradientStep', 'stat');
model.study('std1').feature('sens').set('optobj', {'-comp1.solid.Ws_tot'});
model.study('std1').feature('sens').set('descr', {'Total elastic strain energy'});
model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});

model.sol('sol1').attach('std1');
model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
model.sol('sol1').feature('s1').feature('dDef').label('Direct 1');
model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
model.sol('sol1').feature('s1').feature('fc1').set('maxiter', 100);
model.sol('sol1').runAll;

% model.result('pg1').label('Sensitivity');
% model.result('pg1').feature('surf1').set('descr', 'fsens(ED)');
% model.result('pg1').feature('surf1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg1').feature('surf1').set('resolution', 'normal');
% model.result('pg2').label('Stress (solid)');
% model.result('pg2').set('frametype', 'spatial');
% model.result('pg2').feature('surf1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg2').feature('surf1').set('colortable', 'Prism');
% model.result('pg2').feature('surf1').set('threshold', 'manual');
% model.result('pg2').feature('surf1').set('thresholdvalue', 0.2);
% model.result('pg2').feature('surf1').set('resolution', 'normal');
% model.result('pg2').feature('surf1').feature('def').set('scale', 3.162712700336352);
% model.result('pg2').feature('surf1').feature('def').set('scaleactive', false);

out = model;
