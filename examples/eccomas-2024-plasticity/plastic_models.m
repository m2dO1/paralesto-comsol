clear; clc; close all;

E = 72e3;
s0 = 125;

H = 573; %E / (1 - 1/100);
linear = @(e) s0 + H * e;

k = H; % 980;
n = 0.55;
ludwik = @(e) s0 + k * e.^n;

s = 555;
b = 15.5;
voce = @(e) s0 + s * (1 - exp(-b*e));

ep = linspace(0, 0.01, 1001);
figure()
hold on; grid on;
plot(ep, linear(ep), 'LineWidth', 2)
plot(ep, ludwik(ep), 'LineWidth', 2)
plot(ep, voce(ep), 'LineWidth', 2)
xlabel('Plastic strain [-]', 'Interpreter', 'latex')
ylabel('Hardening Law [MPa]', 'Interpreter', 'latex')
legend('Linear', 'Ludwik', 'Voce', 'Interpreter', 'latex')
set(gca, 'FontSize', 20, 'TickLabelInterpreter', 'latex')
axis square












