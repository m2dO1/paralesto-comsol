clear; clc; close all;

max_it = 300;
plasticity_type = {'linear', 'voce'};
w_Ws = {'1.00', '0.75', '0.50', '0.25'};
w_Wp = {'0.00', '0.25', '0.50', '0.75'};

% Loop over plasticity types
Ws = zeros(length(plasticity_type), length(w_Ws));
Wp = zeros(length(plasticity_type), length(w_Ws));
t_start = tic;
for ii = 1:length(plasticity_type)

    % Loop over weights
    for jj = 1:length(w_Ws)

        % Run optimization
        [Ws_temp, Wp_temp] = main_fun(max_it, plasticity_type{ii}, w_Ws{jj}, w_Wp{jj});
        Ws(ii, jj) = Ws_temp;
        Wp(ii, jj) = Wp_temp;
        disp('Done.')

        % Clean
        clc; close all;
    end
end

% Print end message
disp('All done.')
t_end = toc(t_start);
fprintf('Elapsed time is %f seconds.\n', t_end)

% Data
w_Ws_num = zeros(size(w_Ws));
w_Wp_num = zeros(size(w_Wp));
for ii = 1:length(w_Ws)
    w_Ws_num(ii) = str2double(w_Ws{ii});
    w_Wp_num(ii) = str2double(w_Wp{ii});
end

% Plot
fig = figure();
tiledlayout('flow', 'TileSpacing', 'compact')
for ii = 1:length(plasticity_type)
    nexttile
    hold on; grid on;
    yyaxis left
    plot(w_Ws_num, Ws(ii, :), '.-', 'LineWidth', 2)
    title(plasticity_type{ii}, 'Interpreter', 'latex')
    xlabel('Weight', 'Interpreter', 'latex')
    ylabel('$W_s$ [J]', 'Interpreter', 'latex')
    yyaxis right
    plot(w_Ws_num, Wp(ii, :), '.-', 'LineWidth', 2)
    ylabel('$W_p$ [J]', 'Interpreter', 'latex')
    set(gca, 'FontSize', 20, 'TickLabelInterpreter', 'latex')
end

savefig(fig, fullfile(cd, 'results'));
save('results.mat', 'Ws', 'Wp', 'w_Ws_num', 'w_Wp_num', 't_end')















