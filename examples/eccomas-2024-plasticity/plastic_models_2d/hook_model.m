function out = model(hardening_function, w_Ws, w_Wp)
%
% hook_model.m
%
% Model exported on May 6 2024, 13:45 by COMSOL 6.2.0.339.

if nargin == 0
    hardening_function = 'an1(ED)*hardening*solid.epe';
    w_Ws = '1.00';
    w_Wp = '0.00';
elseif nargin == 1
    w_Ws = '1.00';
    w_Wp = '0.00';
end
hardening_function = ['an1(ED)*', hardening_function];

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('/home/matteo/Documents/plasticity/plastic_models_2d');

model.label('hook_model.mph');

model.param.set('lx', '1[m]');
model.param.set('ly', '1[m]');
model.param.set('lz', '0.01[m]');
model.param.set('nelx', '100');
model.param.set('nely', '100');
model.param.set('x1', '0.4');
model.param.set('y1', '0.3');
model.param.set('y2', '0.4');
model.param.set('young', '72[GPa]');
model.param.set('poisson', '0.3');
model.param.set('yield', '125[MPa]');
model.param.set('disp', 'ly/50');
model.param.set('ED_min', '1e-6');
model.param.set('penal', '1');
model.param.set('w_Ws', w_Ws, 'Weight for the strain energy');
model.param.set('w_Wp', w_Wp, 'Weight for the plastic dissipation');
model.param.set('hardening', '573[MPa]');
model.param.set('ludwik_k', '573[MPa]');
model.param.set('ludwik_n', '0.55');
model.param.set('voce_sigma', '555[MPa]');
model.param.set('voce_beta', '15.5');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 2);

model.component('comp1').func.create('an1', 'Analytic');
model.component('comp1').func('an1').set('expr', 'ED_min+(1-ED_min)*x');

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').create('r1', 'Rectangle');
model.component('comp1').geom('geom1').feature('r1').set('size', {'lx' 'ly'});
model.component('comp1').geom('geom1').create('pt1', 'Point');
model.component('comp1').geom('geom1').feature('pt1').set('p', {'x1*lx' 'ly'});
model.component('comp1').geom('geom1').feature('pt1').setAttribute('construction', 'on');
model.component('comp1').geom('geom1').create('pare1', 'PartitionEdges');
model.component('comp1').geom('geom1').feature('pare1').set('position', 'projection');
model.component('comp1').geom('geom1').feature('pare1').selection('edge').set('r1(1)', 3);
model.component('comp1').geom('geom1').feature('pare1').selection('vertexproj').set('pt1(1)', 1);
model.component('comp1').geom('geom1').create('ls1', 'LineSegment');
model.component('comp1').geom('geom1').feature('ls1').set('specify1', 'coord');
model.component('comp1').geom('geom1').feature('ls1').set('coord1', {'lx' 'y1*ly'});
model.component('comp1').geom('geom1').feature('ls1').set('specify2', 'coord');
model.component('comp1').geom('geom1').feature('ls1').set('coord2', {'lx' 'y2*ly'});
model.component('comp1').geom('geom1').feature('ls1').setAttribute('construction', 'on');
model.component('comp1').geom('geom1').create('pare2', 'PartitionEdges');
model.component('comp1').geom('geom1').feature('pare2').set('position', 'projection');
model.component('comp1').geom('geom1').feature('pare2').selection('edge').set('pare1(1)', 5);
model.component('comp1').geom('geom1').feature('pare2').selection('vertexproj').set('ls1(1)', 2);
model.component('comp1').geom('geom1').create('pare3', 'PartitionEdges');
model.component('comp1').geom('geom1').feature('pare3').set('position', 'projection');
model.component('comp1').geom('geom1').feature('pare3').selection('edge').set('pare2(1)', 5);
model.component('comp1').geom('geom1').feature('pare3').selection('vertexproj').set('ls1(1)', 1);
model.component('comp1').geom('geom1').run;

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
model.component('comp1').material('mat1').propertyGroup.create('ElastoplasticModel', 'Elastoplastic material model');

model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 2);
model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);
model.component('comp1').physics.create('solid', 'SolidMechanics', 'geom1');
model.component('comp1').physics('solid').feature('lemm1').create('plsty1', 'Plasticity', 2);
model.component('comp1').physics('solid').create('fix1', 'Fixed', 1);
model.component('comp1').physics('solid').feature('fix1').selection.set([3]);
model.component('comp1').physics('solid').create('disp1', 'Displacement1', 1);
model.component('comp1').physics('solid').feature('disp1').selection.set([6]);

model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').feature('map1').create('dis1', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map1').create('dis2', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').selection.set([2]);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').selection.set([1]);

model.component('comp1').view('view1').axis.set('xmin', -0.09074071049690247);
model.component('comp1').view('view1').axis.set('xmax', 1.09074068069458);
model.component('comp1').view('view1').axis.set('ymin', -0.13161222636699677);
model.component('comp1').view('view1').axis.set('ymax', 1.1286346912384033);

model.component('comp1').material('mat1').propertyGroup('def').set('density', '1');
model.component('comp1').material('mat1').propertyGroup('Enu').set('E', 'an1(ED)*young');
model.component('comp1').material('mat1').propertyGroup('Enu').set('nu', 'poisson');
model.component('comp1').material('mat1').propertyGroup('ElastoplasticModel').set('sigmags', 'an1(ED)*yield');
model.component('comp1').material('mat1').propertyGroup('ElastoplasticModel').set('sigmagh', hardening_function);

model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
model.component('comp1').physics('solid').prop('ShapeProperty').set('order_displacement', 1);
model.component('comp1').physics('solid').prop('d').set('d', 'lz');
model.component('comp1').physics('solid').feature('lemm1').set('geometricNonlinearity', 'linear');
model.component('comp1').physics('solid').feature('lemm1').set('CalculateDissipatedEnergy', true);
model.component('comp1').physics('solid').feature('lemm1').feature('plsty1').set('IsotropicHardeningModel', 'HardeningFunction');
model.component('comp1').physics('solid').feature('lemm1').feature('plsty1').set('sigmagh', hardening_function);
model.component('comp1').physics('solid').feature('disp1').set('Direction', {'free'; 'prescribed'; 'free'});
model.component('comp1').physics('solid').feature('disp1').set('U0', {'0'; '-disp'; '0'});

model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').set('numelem', 'nelx');
model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').set('equidistant', true);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').set('numelem', 'nely');
model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').set('equidistant', true);
model.component('comp1').mesh('mesh1').run;

model.component('comp1').physics('solid').feature('lemm1').feature('plsty1').set('sigmagh_mat', 'from_mat');

model.study.create('std1');
model.study('std1').create('sens', 'Sensitivity');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').feature.remove('fcDef');

% model.result.create('pg1', 'PlotGroup2D');
% model.result.create('pg2', 'PlotGroup2D');
% model.result('pg1').create('surf1', 'Surface');
% model.result('pg2').create('surf1', 'Surface');
% model.result('pg2').feature('surf1').set('expr', 'solid.misesGp');
% model.result('pg2').feature('surf1').create('def', 'Deform');

model.study('std1').feature('sens').set('gradientStep', 'stat');
model.study('std1').feature('sens').set('optobj', {'-w_Ws*comp1.solid.Ws_tot' 'w_Wp*comp1.solid.Wp_tot'});
model.study('std1').feature('sens').set('descr', {'Total elastic strain energy' 'Total plastic dissipation'});
model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat' 'stat'});

model.sol('sol1').attach('std1');
model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
model.sol('sol1').feature('s1').feature('dDef').label('Direct 1');
model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
model.sol('sol1').runAll;

% model.result('pg1').label('Sensitivity');
% model.result('pg1').feature('surf1').set('descr', 'fsens(ED)');
% model.result('pg1').feature('surf1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg1').feature('surf1').set('resolution', 'normal');
% model.result('pg2').label('Stress (solid)');
% model.result('pg2').set('frametype', 'spatial');
% model.result('pg2').feature('surf1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg2').feature('surf1').set('colortable', 'Prism');
% model.result('pg2').feature('surf1').set('threshold', 'manual');
% model.result('pg2').feature('surf1').set('thresholdvalue', 0.2);
% model.result('pg2').feature('surf1').set('resolution', 'normal');
% model.result('pg2').feature('surf1').feature('def').set('scale', 3.3313985493581235);
% model.result('pg2').feature('surf1').feature('def').set('scaleactive', false);

out = model;
