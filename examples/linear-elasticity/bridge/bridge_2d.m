function out = model
%
% bridge_2d.m
%
% Model exported on Mar 13 2024, 10:01 by COMSOL 6.2.0.290.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('/home/matteo/Documents/paralesto-comsol/New Folder');

model.label('bridge_2d.mph');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 2);

model.component('comp1').func.create('an1', 'Analytic');
model.component('comp1').func('an1').set('expr', '1e-6+(1-1e-6)*(x^1)');

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').create('r1', 'Rectangle');
model.component('comp1').geom('geom1').feature('r1').set('size', [1 0.5]);
model.component('comp1').geom('geom1').create('ls1', 'LineSegment');
model.component('comp1').geom('geom1').feature('ls1').set('specify1', 'coord');
model.component('comp1').geom('geom1').feature('ls1').set('coord1', [0.1 0]);
model.component('comp1').geom('geom1').feature('ls1').set('specify2', 'coord');
model.component('comp1').geom('geom1').feature('ls1').set('coord2', [0.9 0]);
model.component('comp1').geom('geom1').feature('ls1').setAttribute('construction', 'on');
model.component('comp1').geom('geom1').create('pare1', 'PartitionEdges');
model.component('comp1').geom('geom1').feature('pare1').set('position', 'projection');
model.component('comp1').geom('geom1').feature('pare1').selection('edge').set('r1(1)', 1);
model.component('comp1').geom('geom1').feature('pare1').selection('vertexproj').set('ls1(1)', [1 2]);
model.component('comp1').geom('geom1').run;

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');

model.component('comp1').physics.create('solid', 'SolidMechanics', 'geom1');
model.component('comp1').physics('solid').create('fix1', 'Fixed', 1);
model.component('comp1').physics('solid').feature('fix1').selection.set([2 5]);
model.component('comp1').physics('solid').create('bndl1', 'BoundaryLoad', 1);
model.component('comp1').physics('solid').feature('bndl1').selection.set([3]);
model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 2);
model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);

model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').feature('map1').create('size1', 'Size');

model.component('comp1').view('view1').axis.set('xmin', -0.14806774258613586);
model.component('comp1').view('view1').axis.set('xmax', 1.1480677127838135);
model.component('comp1').view('view1').axis.set('ymin', -0.33690333366394043);
model.component('comp1').view('view1').axis.set('ymax', 0.8369033336639404);

model.component('comp1').material('mat1').propertyGroup('def').set('density', '1');
model.component('comp1').material('mat1').propertyGroup('Enu').set('E', 'an1(ED)');
model.component('comp1').material('mat1').propertyGroup('Enu').set('nu', '0.3');

model.component('comp1').physics('solid').prop('ShapeProperty').set('order_displacement', 1);
model.component('comp1').physics('solid').prop('Type2D').set('Type2D', 'PlaneStress');
model.component('comp1').physics('solid').prop('d').set('d', 0.01);
model.component('comp1').physics('solid').feature('lemm1').set('geometricNonlinearity', 'linear');
model.component('comp1').physics('solid').feature('bndl1').set('FperArea', [0; -1; 0]);
model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
model.component('comp1').physics('sens').feature('cvar1').set('order', 0);

model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('custom', 'on');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmax', 0.01);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmaxactive', true);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmin', 0.01);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hminactive', true);
model.component('comp1').mesh('mesh1').run;

model.study.create('std1');
model.study('std1').create('sens', 'Sensitivity');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').feature.remove('fcDef');

% model.result.create('pg1', 'PlotGroup2D');
% model.result.create('pg2', 'PlotGroup2D');
% model.result('pg1').create('surf1', 'Surface');
% model.result('pg1').feature('surf1').set('expr', 'solid.misesGp');
% model.result('pg1').feature('surf1').create('def', 'Deform');
% model.result('pg2').create('surf1', 'Surface');
% model.result('pg2').feature('surf1').set('expr', 'fsens(ED)');

model.study('std1').feature('sens').set('gradientStep', 'stat');
model.study('std1').feature('sens').set('optobj', {'comp1.solid.Ws_tot'});
model.study('std1').feature('sens').set('descr', {'Total elastic strain energy'});
model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});

model.sol('sol1').attach('std1');
model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
model.sol('sol1').feature('s1').feature('dDef').label('Direct 1');
model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
model.sol('sol1').runAll;

% model.result('pg1').label('Stress (solid)');
% model.result('pg1').set('frametype', 'spatial');
% model.result('pg1').feature('surf1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg1').feature('surf1').set('colortable', 'Prism');
% model.result('pg1').feature('surf1').set('threshold', 'manual');
% model.result('pg1').feature('surf1').set('thresholdvalue', 0.2);
% model.result('pg1').feature('surf1').set('resolution', 'normal');
% model.result('pg1').feature('surf1').feature('def').set('scale', 0.05125496290534176);
% model.result('pg1').feature('surf1').feature('def').set('scaleactive', false);
% model.result('pg2').label('Sensitivity');
% model.result('pg2').feature('surf1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg2').feature('surf1').set('resolution', 'normal');

out = model;
