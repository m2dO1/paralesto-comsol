function out = model
%
% bridge_3d.m
%
% Model exported on Mar 13 2024, 10:22 by COMSOL 6.2.0.290.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('/home/matteo/Documents/paralesto-comsol/examples/linear-elasticity/bridge');

model.label('bridge_3d.mph');

model.param.set('ED_min', '1e-6');
model.param.set('penal', '1');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component('comp1').func.create('an1', 'Analytic');
model.component('comp1').func('an1').set('expr', 'ED_min+(1-ED_min)*(x^penal)');

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').create('blk1', 'Block');
model.component('comp1').geom('geom1').feature('blk1').set('size', [1 0.2 0.5]);
model.component('comp1').geom('geom1').create('wp1', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp1').set('quickplane', 'yz');
model.component('comp1').geom('geom1').feature('wp1').set('quickx', 0.1);
model.component('comp1').geom('geom1').feature('wp1').set('unite', true);
model.component('comp1').geom('geom1').create('parf1', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf1').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf1').selection('face').set('blk1(1)', 1);
model.component('comp1').geom('geom1').create('wp2', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp2').set('quickplane', 'yz');
model.component('comp1').geom('geom1').feature('wp2').set('quickx', 0.9);
model.component('comp1').geom('geom1').feature('wp2').set('unite', true);
model.component('comp1').geom('geom1').create('parf2', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf2').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf2').selection('face').set('parf1(1)', 6);
model.component('comp1').geom('geom1').run;

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');

model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 3);
model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);
model.component('comp1').physics.create('solid', 'SolidMechanics', 'geom1');
model.component('comp1').physics('solid').create('fix1', 'Fixed', 2);
model.component('comp1').physics('solid').feature('fix1').selection.set([3 7]);
model.component('comp1').physics('solid').create('bndl1', 'BoundaryLoad', 2);
model.component('comp1').physics('solid').feature('bndl1').selection.set([4]);

model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').create('swe1', 'Sweep');
model.component('comp1').mesh('mesh1').feature('map1').selection.set([3 6 7]);
model.component('comp1').mesh('mesh1').feature('map1').create('size1', 'Size');
model.component('comp1').mesh('mesh1').feature('swe1').create('dis1', 'Distribution');

model.component('comp1').material('mat1').propertyGroup('def').set('density', '1');
model.component('comp1').material('mat1').propertyGroup('Enu').set('E', 'an1(ED)*1e3');
model.component('comp1').material('mat1').propertyGroup('Enu').set('nu', '0.3');

model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
model.component('comp1').physics('solid').prop('ShapeProperty').set('order_displacement', 1);
model.component('comp1').physics('solid').feature('lemm1').set('geometricNonlinearity', 'linear');
model.component('comp1').physics('solid').feature('bndl1').set('FperArea', [0; 0; -1]);

model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('custom', 'on');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmax', 0.02);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmaxactive', true);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmin', 0.02);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hminactive', true);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').set('numelem', 25);
model.component('comp1').mesh('mesh1').run;

model.study.create('std1');
model.study('std1').create('sens', 'Sensitivity');
model.study('std1').create('stat', 'Stationary');
model.study('std1').feature('stat').set('geometricNonlinearity', true);

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').create('d1', 'Direct');
model.sol('sol1').feature('s1').create('i1', 'Iterative');
model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('so1', 'SOR');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('so1', 'SOR');
model.sol('sol1').feature('s1').feature.remove('fcDef');

% model.result.create('pg1', 'PlotGroup3D');
% model.result.create('pg2', 'PlotGroup3D');
% model.result('pg1').create('slc1', 'Slice');
% model.result('pg2').create('vol1', 'Volume');
% model.result('pg2').feature('vol1').set('expr', 'solid.misesGp');
% model.result('pg2').feature('vol1').create('def', 'Deform');

model.study('std1').feature('sens').set('gradientStep', 'stat');
model.study('std1').feature('sens').set('optobj', {'comp1.solid.Ws_tot'});
model.study('std1').feature('sens').set('descr', {'Total elastic strain energy'});
model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});

model.sol('sol1').attach('std1');
model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
model.sol('sol1').feature('s1').feature('dDef').label('Direct 2');
model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'd1');
model.sol('sol1').feature('s1').feature('d1').label('Suggested Direct Solver (solid)');
model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-9);
model.sol('sol1').feature('s1').feature('d1').set('nliniterrefine', true);
model.sol('sol1').feature('s1').feature('i1').label('Suggested Iterative Solver (GMRES with SA AMG) (solid)');
model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', true);
model.sol('sol1').feature('s1').feature('i1').set('rhob', 40);
model.sol('sol1').feature('s1').feature('i1').feature('ilDef').label('Incomplete LU 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').label('Multigrid 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').label('Presmoother 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('soDef').label('SOR 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').label('SOR 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').set('relax', 0.8);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').label('Postsmoother 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('soDef').label('SOR 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').label('SOR 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').set('relax', 0.8);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').label('Coarse Solver 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('dDef').label('Direct 1');
model.sol('sol1').runAll;

% model.result('pg1').label('Sensitivity');
% model.result('pg1').feature('slc1').set('descr', 'fsens(ED)');
% model.result('pg1').feature('slc1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg1').feature('slc1').set('resolution', 'normal');
% model.result('pg2').label('Stress (solid)');
% model.result('pg2').set('frametype', 'spatial');
% model.result('pg2').feature('vol1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg2').feature('vol1').set('colortable', 'Prism');
% model.result('pg2').feature('vol1').set('resolution', 'custom');
% model.result('pg2').feature('vol1').set('refine', 2);
% model.result('pg2').feature('vol1').set('threshold', 'manual');
% model.result('pg2').feature('vol1').set('thresholdvalue', 0.2);
% model.result('pg2').feature('vol1').set('resolution', 'custom');
% model.result('pg2').feature('vol1').set('refine', 2);
% model.result('pg2').feature('vol1').feature('def').set('scale', 39.70701994929478);
% model.result('pg2').feature('vol1').feature('def').set('scaleactive', false);

out = model;
