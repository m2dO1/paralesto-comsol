function out = model
%
% linear_elasticity_mbb.m
%
% Model exported on Jan 18 2024, 08:43 by COMSOL 6.2.0.290.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('/home/m2do/Documents/paralesto-comsol/examples/linear-elasticity/mbb-beam');

model.label('linear_elasticity_mbb.mph');

model.param.set('lx', '1 [m]');
model.param.set('ly', '0.2 [m]');
model.param.set('lz', '0.5 [m]');
model.param.set('nelx', '50');
model.param.set('nely', '10');
model.param.set('nelz', '25');
model.param.set('young', '1 [Pa]');
model.param.set('poisson', '0.3');
model.param.set('x_load', '0.9');
model.param.set('load', '-1.0 [Pa]');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').create('blk1', 'Block');
model.component('comp1').geom('geom1').feature('blk1').set('size', {'lx' 'ly' 'lz'});
model.component('comp1').geom('geom1').create('wp1', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp1').set('quickplane', 'yz');
model.component('comp1').geom('geom1').feature('wp1').set('quickx', 'x_load * lx');
model.component('comp1').geom('geom1').feature('wp1').set('unite', true);
model.component('comp1').geom('geom1').create('parf1', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf1').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf1').selection('face').set('blk1(1)', 4);
model.component('comp1').geom('geom1').run;

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');

model.component('comp1').cpl.create('intop1', 'Integration');
model.component('comp1').cpl('intop1').selection.set([1]);

model.component('comp1').physics.create('solid', 'SolidMechanics', 'geom1');
model.component('comp1').physics('solid').create('fix1', 'Fixed', 2);
model.component('comp1').physics('solid').feature('fix1').selection.set([1]);
model.component('comp1').physics('solid').create('roll1', 'Roller', 2);
model.component('comp1').physics('solid').feature('roll1').selection.set([7]);
model.component('comp1').physics('solid').create('bndl1', 'BoundaryLoad', 2);
model.component('comp1').physics('solid').feature('bndl1').selection.set([6]);
model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 3);
model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);

model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').create('map2', 'Map');
model.component('comp1').mesh('mesh1').create('swe1', 'Sweep');
model.component('comp1').mesh('mesh1').feature('map1').selection.set([4]);
model.component('comp1').mesh('mesh1').feature('map1').create('dis1', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map1').create('dis2', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').selection.set([5]);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').selection.set([4]);
model.component('comp1').mesh('mesh1').feature('map2').selection.set([6]);
model.component('comp1').mesh('mesh1').feature('map2').create('dis1', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map2').create('dis2', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map2').feature('dis1').selection.set([10]);
model.component('comp1').mesh('mesh1').feature('map2').feature('dis2').selection.set([14]);
model.component('comp1').mesh('mesh1').feature('swe1').create('dis1', 'Distribution');

model.component('comp1').material('mat1').propertyGroup('def').set('density', '1');
model.component('comp1').material('mat1').propertyGroup('Enu').set('E', 'young');
model.component('comp1').material('mat1').propertyGroup('Enu').set('nu', 'poisson');

model.component('comp1').cpl('intop1').set('intorder', 2);

model.component('comp1').physics('solid').prop('ShapeProperty').set('order_displacement', 1);
model.component('comp1').physics('solid').feature('lemm1').set('E_mat', 'userdef');
model.component('comp1').physics('solid').feature('lemm1').set('E', 'ED * young');
model.component('comp1').physics('solid').feature('lemm1').set('geometricNonlinearity', 'linear');
model.component('comp1').physics('solid').feature('bndl1').set('FperArea', {'0'; '0'; 'load'});
model.component('comp1').physics('sens').feature('cvar1').set('initialValue', '1.0');
model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
model.component('comp1').physics('sens').feature('cvar1').set('order', 0);

model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').set('numelem', 'nelx * x_load');
model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').set('numelem', 'nely');
model.component('comp1').mesh('mesh1').feature('map2').feature('dis1').set('numelem', 'nelx * (1 - x_load)');
model.component('comp1').mesh('mesh1').feature('map2').feature('dis2').set('numelem', 'nely');
model.component('comp1').mesh('mesh1').feature('swe1').selection('sourceface').set([4 6]);
model.component('comp1').mesh('mesh1').feature('swe1').selection('targetface').set([3]);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').set('numelem', 'nelz');
model.component('comp1').mesh('mesh1').run;

model.study.create('std1');
model.study('std1').create('sens', 'Sensitivity');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').create('d1', 'Direct');
model.sol('sol1').feature('s1').create('i1', 'Iterative');
model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('so1', 'SOR');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('so1', 'SOR');
model.sol('sol1').feature('s1').feature.remove('fcDef');

% model.result.create('pg1', 'PlotGroup3D');
% model.result.create('pg2', 'PlotGroup3D');
% model.result('pg1').create('vol1', 'Volume');
% model.result('pg1').feature('vol1').set('expr', 'solid.misesGp');
% model.result('pg1').feature('vol1').create('def', 'Deform');
% model.result('pg2').create('slc1', 'Slice');
% model.result('pg2').feature('slc1').set('expr', 'fsens(ED)');

model.study('std1').feature('sens').set('gradientStep', 'stat');
model.study('std1').feature('sens').set('optobj', {'comp1.intop1(comp1.solid.Ws)'});
model.study('std1').feature('sens').set('descr', {'Total elastic strain energy'});
model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});

model.sol('sol1').attach('std1');
model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
model.sol('sol1').feature('s1').feature('dDef').label('Direct 2');
model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'd1');
model.sol('sol1').feature('s1').feature('d1').label('Suggested Direct Solver (solid)');
model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-9);
model.sol('sol1').feature('s1').feature('i1').label('Suggested Iterative Solver (GMRES with SA AMG) (solid)');
model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', true);
model.sol('sol1').feature('s1').feature('i1').feature('ilDef').label('Incomplete LU 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').label('Multigrid 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').label('Presmoother 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('soDef').label('SOR 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').label('SOR 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').set('relax', 0.8);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').label('Postsmoother 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('soDef').label('SOR 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').label('SOR 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').set('relax', 0.8);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').label('Coarse Solver 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('dDef').label('Direct 1');
model.sol('sol1').runAll;

% model.result('pg1').label('Stress (solid)');
% model.result('pg1').set('frametype', 'spatial');
% model.result('pg1').feature('vol1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg1').feature('vol1').set('colortable', 'Prism');
% model.result('pg1').feature('vol1').set('resolution', 'custom');
% model.result('pg1').feature('vol1').set('refine', 2);
% model.result('pg1').feature('vol1').set('threshold', 'manual');
% model.result('pg1').feature('vol1').set('thresholdvalue', 0.2);
% model.result('pg1').feature('vol1').set('resolution', 'custom');
% model.result('pg1').feature('vol1').set('refine', 2);
% model.result('pg1').feature('vol1').feature('def').set('scale', 0.0387119124065733);
% model.result('pg1').feature('vol1').feature('def').set('scaleactive', false);
% model.result('pg2').label('Sensitivity');
% model.result('pg2').feature('slc1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x-coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y-coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z-coordinate'});
% model.result('pg2').feature('slc1').set('resolution', 'normal');

out = model;
