function out = model
%
% laminar.m
%
% Model exported on Jan 31 2024, 15:50 by COMSOL 6.2.0.339.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('C:\Users\Alex Guibert\Desktop\UCSD\Research\LSTO with COMSOL\paralesto-comsol-windows\examples\laminar-flow');

model.label('laminar.mph');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').create('blk1', 'Block');
model.component('comp1').geom('geom1').feature('blk1').set('size', {'0.45e-1' '0.25e-1' '0.45e-1'});
model.component('comp1').geom('geom1').create('blk2', 'Block');
model.component('comp1').geom('geom1').feature('blk2').set('pos', {'0.45e-1' '0' '0'});
model.component('comp1').geom('geom1').feature('blk2').set('size', {'0.1e-1' '0.25e-1' '0.45e-1'});
model.component('comp1').geom('geom1').create('blk7', 'Block');
model.component('comp1').geom('geom1').feature('blk7').set('pos', {'0' '0.35e-1' '0'});
model.component('comp1').geom('geom1').feature('blk7').set('size', {'0.45e-1' '0.25e-1' '0.45e-1'});
model.component('comp1').geom('geom1').create('blk8', 'Block');
model.component('comp1').geom('geom1').feature('blk8').set('pos', {'0.45e-1' '0.35e-1' '0'});
model.component('comp1').geom('geom1').feature('blk8').set('size', {'0.1e-1' '0.25e-1' '0.45e-1'});
model.component('comp1').geom('geom1').create('blk13', 'Block');
model.component('comp1').geom('geom1').feature('blk13').set('pos', {'0' '0.25e-1' '0'});
model.component('comp1').geom('geom1').feature('blk13').set('size', {'0.45e-1' '0.1e-1' '0.45e-1'});
model.component('comp1').geom('geom1').create('blk14', 'Block');
model.component('comp1').geom('geom1').feature('blk14').set('pos', {'0.45e-1' '0.25e-1' '0'});
model.component('comp1').geom('geom1').feature('blk14').set('size', {'0.1e-1' '0.1e-1' '0.45e-1'});
model.component('comp1').geom('geom1').create('blk19', 'Block');
model.component('comp1').geom('geom1').feature('blk19').set('pos', {'0' '0' '0.45e-1'});
model.component('comp1').geom('geom1').feature('blk19').set('size', {'0.45e-1' '0.25e-1' '0.1e-1'});
model.component('comp1').geom('geom1').create('blk20', 'Block');
model.component('comp1').geom('geom1').feature('blk20').set('pos', {'0.45e-1' '0' '0.45e-1'});
model.component('comp1').geom('geom1').feature('blk20').set('size', {'0.1e-1' '0.25e-1' '0.1e-1'});
model.component('comp1').geom('geom1').create('blk22', 'Block');
model.component('comp1').geom('geom1').feature('blk22').set('pos', {'0' '0.35e-1' '0.45e-1'});
model.component('comp1').geom('geom1').feature('blk22').set('size', {'0.45e-1' '0.25e-1' '0.1e-1'});
model.component('comp1').geom('geom1').create('blk23', 'Block');
model.component('comp1').geom('geom1').feature('blk23').set('pos', {'0.45e-1' '0.35e-1' '0.45e-1'});
model.component('comp1').geom('geom1').feature('blk23').set('size', {'0.1e-1' '0.25e-1' '0.1e-1'});
model.component('comp1').geom('geom1').create('blk25', 'Block');
model.component('comp1').geom('geom1').feature('blk25').set('pos', {'0' '0.25e-1' '0.45e-1'});
model.component('comp1').geom('geom1').feature('blk25').set('size', {'0.45e-1' '0.1e-1' '0.1e-1'});
model.component('comp1').geom('geom1').create('blk26', 'Block');
model.component('comp1').geom('geom1').feature('blk26').set('pos', {'0.45e-1' '0.25e-1' '0.45e-1'});
model.component('comp1').geom('geom1').feature('blk26').set('size', {'0.1e-1' '0.1e-1' '0.1e-1'});
model.component('comp1').geom('geom1').run;

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup('def').func.create('eta', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('Cp', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('rho', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('k', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('cs', 'Interpolation');
model.component('comp1').material('mat1').propertyGroup('def').func.create('an1', 'Analytic');
model.component('comp1').material('mat1').propertyGroup('def').func.create('an2', 'Analytic');
model.component('comp1').material('mat1').propertyGroup('def').func.create('an3', 'Analytic');

model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 3);
model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
model.component('comp1').physics('sens').feature('cvar1').selection.all;
model.component('comp1').physics.create('spf', 'LaminarFlow', 'geom1');
model.component('comp1').physics('spf').create('inl1', 'InletBoundary', 2);
model.component('comp1').physics('spf').feature('inl1').selection.set([11]);
model.component('comp1').physics('spf').create('out1', 'OutletBoundary', 2);
model.component('comp1').physics('spf').feature('out1').selection.set([33]);
model.component('comp1').physics('spf').create('vf1', 'VolumeForce', 3);
model.component('comp1').physics('spf').feature('vf1').selection.all;

model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').create('dis1', 'Distribution');
model.component('comp1').mesh('mesh1').create('dis2', 'Distribution');
model.component('comp1').mesh('mesh1').create('swe1', 'Sweep');
model.component('comp1').mesh('mesh1').feature('map1').selection.set([7 14 21 30 37 44]);
model.component('comp1').mesh('mesh1').feature('map1').create('dis1', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map1').create('dis2', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map1').create('dis3', 'Distribution');
model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').selection.set([7 23 36 52 63 73]);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').selection.set([8 16 24 29]);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis3').selection.set([15 37 44 45 53 58 68]);
model.component('comp1').mesh('mesh1').feature('dis1').selection.geom('geom1', 1);
model.component('comp1').mesh('mesh1').feature('dis1').selection.set([1]);
model.component('comp1').mesh('mesh1').feature('dis2').selection.geom('geom1', 1);
model.component('comp1').mesh('mesh1').feature('dis2').selection.set([4]);
model.component('comp1').mesh('mesh1').feature('swe1').create('dis1', 'Distribution');
model.component('comp1').mesh('mesh1').feature('swe1').create('dis2', 'Distribution');
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').selection.set([8]);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis2').selection.set([7]);

model.component('comp1').material('mat1').label('Water, liquid');
model.component('comp1').material('mat1').set('family', 'water');
model.component('comp1').material('mat1').propertyGroup('def').func('eta').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('eta').set('pieces', {'273.15' '413.15' '1.3799566804-0.021224019151*T^1+1.3604562827E-4*T^2-4.6454090319E-7*T^3+8.9042735735E-10*T^4-9.0790692686E-13*T^5+3.8457331488E-16*T^6'; '413.15' '553.75' '0.00401235783-2.10746715E-5*T^1+3.85772275E-8*T^2-2.39730284E-11*T^3'});
model.component('comp1').material('mat1').propertyGroup('def').func('eta').set('argunit', 'K');
model.component('comp1').material('mat1').propertyGroup('def').func('eta').set('fununit', 'Pa*s');
model.component('comp1').material('mat1').propertyGroup('def').func('Cp').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('Cp').set('pieces', {'273.15' '553.75' '12010.1471-80.4072879*T^1+0.309866854*T^2-5.38186884E-4*T^3+3.62536437E-7*T^4'});
model.component('comp1').material('mat1').propertyGroup('def').func('Cp').set('argunit', 'K');
model.component('comp1').material('mat1').propertyGroup('def').func('Cp').set('fununit', 'J/(kg*K)');
model.component('comp1').material('mat1').propertyGroup('def').func('rho').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('rho').set('smooth', 'contd1');
model.component('comp1').material('mat1').propertyGroup('def').func('rho').set('pieces', {'273.15' '293.15' '0.000063092789034*T^3-0.060367639882855*T^2+18.9229382407066*T-950.704055329848'; '293.15' '373.15' '0.000010335053319*T^3-0.013395065634452*T^2+4.969288832655160*T+432.257114008512'});
model.component('comp1').material('mat1').propertyGroup('def').func('rho').set('argunit', 'K');
model.component('comp1').material('mat1').propertyGroup('def').func('rho').set('fununit', 'kg/m^3');
model.component('comp1').material('mat1').propertyGroup('def').func('k').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('k').set('pieces', {'273.15' '553.75' '-0.869083936+0.00894880345*T^1-1.58366345E-5*T^2+7.97543259E-9*T^3'});
model.component('comp1').material('mat1').propertyGroup('def').func('k').set('argunit', 'K');
model.component('comp1').material('mat1').propertyGroup('def').func('k').set('fununit', 'W/(m*K)');
model.component('comp1').material('mat1').propertyGroup('def').func('cs').set('table', {'273' '1403';  ...
'278' '1427';  ...
'283' '1447';  ...
'293' '1481';  ...
'303' '1507';  ...
'313' '1526';  ...
'323' '1541';  ...
'333' '1552';  ...
'343' '1555';  ...
'353' '1555';  ...
'363' '1550';  ...
'373' '1543'});
model.component('comp1').material('mat1').propertyGroup('def').func('cs').set('interp', 'piecewisecubic');
model.component('comp1').material('mat1').propertyGroup('def').func('cs').set('fununit', {'m/s'});
model.component('comp1').material('mat1').propertyGroup('def').func('cs').set('argunit', {'K'});
model.component('comp1').material('mat1').propertyGroup('def').func('an1').set('funcname', 'alpha_p');
model.component('comp1').material('mat1').propertyGroup('def').func('an1').set('expr', '-1/rho(T)*d(rho(T),T)');
model.component('comp1').material('mat1').propertyGroup('def').func('an1').set('args', {'T'});
model.component('comp1').material('mat1').propertyGroup('def').func('an1').set('fununit', '1/K');
model.component('comp1').material('mat1').propertyGroup('def').func('an1').set('argunit', {'K'});
model.component('comp1').material('mat1').propertyGroup('def').func('an1').set('plotfixedvalue', {'273.15'});
model.component('comp1').material('mat1').propertyGroup('def').func('an1').set('plotargs', {'T' '273.15' '373.15'});
model.component('comp1').material('mat1').propertyGroup('def').func('an2').set('funcname', 'gamma_w');
model.component('comp1').material('mat1').propertyGroup('def').func('an2').set('expr', '1+(T/Cp(T))*(alpha_p(T)*cs(T))^2');
model.component('comp1').material('mat1').propertyGroup('def').func('an2').set('args', {'T'});
model.component('comp1').material('mat1').propertyGroup('def').func('an2').set('fununit', '1');
model.component('comp1').material('mat1').propertyGroup('def').func('an2').set('argunit', {'K'});
model.component('comp1').material('mat1').propertyGroup('def').func('an2').set('plotfixedvalue', {'273.15'});
model.component('comp1').material('mat1').propertyGroup('def').func('an2').set('plotargs', {'T' '273.15' '373.15'});
model.component('comp1').material('mat1').propertyGroup('def').func('an3').set('funcname', 'muB');
model.component('comp1').material('mat1').propertyGroup('def').func('an3').set('expr', '2.79*eta(T)');
model.component('comp1').material('mat1').propertyGroup('def').func('an3').set('args', {'T'});
model.component('comp1').material('mat1').propertyGroup('def').func('an3').set('fununit', 'Pa*s');
model.component('comp1').material('mat1').propertyGroup('def').func('an3').set('argunit', {'K'});
model.component('comp1').material('mat1').propertyGroup('def').func('an3').set('plotfixedvalue', {'273.15'});
model.component('comp1').material('mat1').propertyGroup('def').func('an3').set('plotargs', {'T' '273.15' '553.75'});
model.component('comp1').material('mat1').propertyGroup('def').set('thermalexpansioncoefficient', '');
model.component('comp1').material('mat1').propertyGroup('def').set('bulkviscosity', '');
model.component('comp1').material('mat1').propertyGroup('def').set('thermalexpansioncoefficient', {'alpha_p(T)' '0' '0' '0' 'alpha_p(T)' '0' '0' '0' 'alpha_p(T)'});
model.component('comp1').material('mat1').propertyGroup('def').set('bulkviscosity', 'muB(T)');
model.component('comp1').material('mat1').propertyGroup('def').set('dynamicviscosity', 'eta(T)');
model.component('comp1').material('mat1').propertyGroup('def').set('ratioofspecificheat', 'gamma_w(T)');
model.component('comp1').material('mat1').propertyGroup('def').set('electricconductivity', {'5.5e-6[S/m]' '0' '0' '0' '5.5e-6[S/m]' '0' '0' '0' '5.5e-6[S/m]'});
model.component('comp1').material('mat1').propertyGroup('def').set('heatcapacity', 'Cp(T)');
model.component('comp1').material('mat1').propertyGroup('def').set('density', 'rho(T)');
model.component('comp1').material('mat1').propertyGroup('def').set('thermalconductivity', {'k(T)' '0' '0' '0' 'k(T)' '0' '0' '0' 'k(T)'});
model.component('comp1').material('mat1').propertyGroup('def').set('soundspeed', 'cs(T)');
model.component('comp1').material('mat1').propertyGroup('def').addInput('temperature');

model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
model.component('comp1').physics('spf').feature('inl1').set('U0in', 0.001);
model.component('comp1').physics('spf').feature('vf1').set('F', {'10*(1-ED)/(0.01+ED)'; '10*(1-ED)/(0.01+ED)'; '10*(1-ED)/(0.01+ED)'});

model.component('comp1').mesh('mesh1').feature('size').set('custom', 'on');
model.component('comp1').mesh('mesh1').feature('size').set('hmax', 0.0025);
model.component('comp1').mesh('mesh1').feature('size').set('hmin', 0.0025);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').set('numelem', 9);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis3').set('numelem', 2);
model.component('comp1').mesh('mesh1').feature('dis1').set('numelem', 9);
model.component('comp1').mesh('mesh1').feature('dis2').set('numelem', 2);
model.component('comp1').mesh('mesh1').feature('swe1').selection('sourceface').set([7 14 21 30 37 44]);
model.component('comp1').mesh('mesh1').feature('swe1').selection('targetface').set([3 10 17 26 33 40]);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').set('numelem', 2);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis2').set('numelem', 9);
model.component('comp1').mesh('mesh1').run;

model.study.create('std1');
model.study('std1').create('sens', 'Sensitivity');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').create('d1', 'Direct');
model.sol('sol1').feature('s1').create('i1', 'Iterative');
model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('sc1', 'SCGS');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('sc1', 'SCGS');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').create('d1', 'Direct');
model.sol('sol1').feature('s1').feature.remove('fcDef');

model.study('std1').feature('sens').set('gradientStep', 'stat');
model.study('std1').feature('sens').set('optobj', {'comp1.spf.inl1.pAverage'});
model.study('std1').feature('sens').set('descr', {'Pressure average over feature selection'});
model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});

% model.result('pg1').run;

model.component('comp1').mesh('mesh1').feature('dis1').set('numelem', 18);
model.component('comp1').mesh('mesh1').feature('dis2').set('numelem', 4);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').set('numelem', 10);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').set('numelem', 18);
model.component('comp1').mesh('mesh1').feature('map1').feature('dis3').set('numelem', 4);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').set('numelem', 4);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis2').set('numelem', 18);
model.component('comp1').mesh('mesh1').run;
model.component('comp1').mesh('mesh1').stat.selection.geom(3);
model.component('comp1').mesh('mesh1').stat.selection.set([1 2 3 4 5 6 7 8 9 10 11 12]);

model.component('comp1').physics('spf').feature('vf1').set('F', {'-u*(a_fluid + (a_solid-a_fluid)*sf*(1-ED)/(sf+ED))' '-v*(a_fluid + (a_solid-a_fluid)*sf*(1-ED)/(sf+ED))' '-w*(a_fluid + (a_solid-a_fluid)*sf*(1-ED)/(sf+ED))'});

model.param.set('a_solid', '1e5[kg/s/m^3]');
model.param.set('a_fluid', '0.0[kg/s/m^3]');
model.param.set('sf', '0.01');

model.component('comp1').mesh('mesh1').stat.selection.geom(3);
model.component('comp1').mesh('mesh1').stat.selection.set([1 2 3 4 5 6 7 8 9 10 11 12]);

model.sol('sol1').study('std1');
model.sol('sol1').feature.remove('s1');
model.sol('sol1').feature.remove('v1');
model.sol('sol1').feature.remove('st1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').feature('st1').set('study', 'std1');
model.sol('sol1').feature('st1').set('studystep', 'stat');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').feature('v1').set('control', 'stat');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').feature('fc1').set('dtech', 'auto');
model.sol('sol1').feature('s1').feature('fc1').set('initstep', 0.01);
model.sol('sol1').feature('s1').feature('fc1').set('minstep', 1.0E-4);
model.sol('sol1').feature('s1').feature('fc1').set('maxiter', 100);
model.sol('sol1').feature('s1').create('d1', 'Direct');
model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-13);
model.sol('sol1').feature('s1').feature('d1').label('Direct, fluid flow variables (spf)');
model.sol('sol1').feature('s1').create('i1', 'Iterative');
model.sol('sol1').feature('s1').feature('i1').set('linsolver', 'gmres');
model.sol('sol1').feature('s1').feature('i1').set('prefuntype', 'left');
model.sol('sol1').feature('s1').feature('i1').set('itrestart', 50);
model.sol('sol1').feature('s1').feature('i1').set('rhob', 20);
model.sol('sol1').feature('s1').feature('i1').set('maxlinit', 1000);
model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', 'on');
model.sol('sol1').feature('s1').feature('i1').label('AMG, fluid flow variables (spf)');
model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('mgcycle', 'v');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('maxcoarsedof', 80000);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('strconn', 0.02);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('nullspace', 'constant');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('saamgcompwise', true);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('loweramg', true);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('compactaggregation', false);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('sc1', 'SCGS');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('linesweeptype', 'ssor');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('iter', 0);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('scgsrelax', 0.7);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('scgsmethod', 'lines');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('scgsvertexrelax', 0.7);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('relax', 0.5);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('scgssolv', 'stored');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('approxscgs', true);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('scgsdirectmaxsize', 1000);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('sc1', 'SCGS');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('linesweeptype', 'ssor');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('iter', 1);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('scgsrelax', 0.7);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('scgsmethod', 'lines');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('scgsvertexrelax', 0.7);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('relax', 0.5);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('scgssolv', 'stored');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('approxscgs', true);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('scgsdirectmaxsize', 1000);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').create('d1', 'Direct');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('linsolver', 'pardiso');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('pivotperturb', 1.0E-13);
model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'd1');
model.sol('sol1').feature('s1').feature('fc1').set('dtech', 'auto');
model.sol('sol1').feature('s1').feature('fc1').set('initstep', 0.01);
model.sol('sol1').feature('s1').feature('fc1').set('minstep', 1.0E-4);
model.sol('sol1').feature('s1').feature('fc1').set('maxiter', 100);
model.sol('sol1').feature('s1').feature.remove('fcDef');
model.sol('sol1').attach('std1');
model.sol('sol1').runAll;

% model.result('pg1').run;

out = model;
