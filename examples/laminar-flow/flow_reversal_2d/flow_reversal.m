function out = model
    %
    % FlowReversal.m
    %
    % Model exported on Jul 2 2024, 14:52 by COMSOL 6.2.0.339.
    
    import com.comsol.model.*
    import com.comsol.model.util.*
    
    model = ModelUtil.create('Model');
    
    model.modelPath('C:\Users\m2do\Desktop\ParaLeSTO Turbulence Project\examples\Robin\Physics_by_Physics\2)_NavierStokeFlow_PipeBend');
    
    model.label('FlowReversal.mph');
    
    model.param.set('Lx', '5 [m]');
    model.param.set('Ly', '1 [m]');
    model.param.set('Re', '1');
    model.param.set('Mu_f', '(1/Re) [kg/m/s]');
    model.param.set('Rho_f', '1 [kg/m^3]');
    model.param.set('Alpha_min', '0.0001');
    model.param.set('Alpha_max', '10^7 [kg/m/s]');
    model.param.set('Uinlet', '(Re * (Mu_f/Rho_f)) / Ly');
    model.param.set('q_phi', '0.01');
    model.param.set('Pref', '196.60');
    model.param.label('Constants');
    
    model.component.create('comp1', true);
    
    model.component('comp1').geom.create('geom1', 2);
    
    model.result.table.create('tbl1', 'Table');
    model.result.table.create('tbl2', 'Table');
    
    model.func.create('an1', 'Analytic');
    model.func('an1').label('Brinkman penalization function');
    model.func('an1').set('funcname', 'alpha_int');
    model.func('an1').set('expr', '(Alpha + (Alpha_min - Alpha) * GAMMA * (1 + q_phi)/(GAMMA + q_phi))');
    model.func('an1').set('args', {'GAMMA' 'Alpha' 'q_phi'});
    model.func('an1').set('argunit', {'1' 'kg/s/m^3' ''});
    model.func('an1').set('plotaxis', {'on' 'on' 'on'});
    model.func('an1').set('plotfixedvalue', {'0' '0' '0'});
    model.func('an1').set('plotargs', {'GAMMA' '0' '1'; 'Alpha' '0' '1'; 'q_phi' '0' '1'});
    
    model.component('comp1').mesh.create('mesh1');
    
    model.component('comp1').geom('geom1').create('r1', 'Rectangle');
    model.component('comp1').geom('geom1').feature('r1').set('size', {'Lx' 'Ly'});
    model.component('comp1').geom('geom1').create('pt1', 'Point');
    model.component('comp1').geom('geom1').feature('pt1').set('p', {'Lx/2' 'Ly/2'});
    model.component('comp1').geom('geom1').run;
    model.component('comp1').geom('geom1').run('fin');
    
    model.component('comp1').variable.create('var2');
    model.component('comp1').variable('var2').set('U_inlet_para', 'Uinlet * (1 - ( (y-Ly/2)/(Ly/2) )^2)', 'Parabolic Inlet Velocity Profile (U_max at y=1.3m)');
    model.component('comp1').variable.create('var3');
    model.component('comp1').variable('var3').set('Obj_Func', '-comp1.point1');
    model.component('comp1').variable('var3').set('Inlet', 'spf.intop((AveInlet(p) + 0.5*Rho_f*(u^2)))', 'Normalized Energy Flux (inlet)');
    model.component('comp1').variable('var3').set('Outlet', 'spf.intop((AveOutlet(p) + 0.5*Rho_f*(u^2)))', 'Normalized Energy Flux (outlet)');
    model.component('comp1').variable('var3').set('Constraint_Func', 'Inlet - Outlet', 'Total Energy Flux difference (contributions from pressure and kinetic energy)');
    
    model.component('comp1').cpl.create('aveop1', 'Average');
    model.component('comp1').cpl.create('aveop2', 'Average');
    model.component('comp1').cpl('aveop1').selection.geom('geom1', 1);
    model.component('comp1').cpl('aveop1').selection.set([1]);
    model.component('comp1').cpl('aveop2').selection.geom('geom1', 1);
    model.component('comp1').cpl('aveop2').selection.set([4]);
    
    model.component('comp1').physics.create('spf', 'LaminarFlow', 'geom1');
    model.component('comp1').physics('spf').create('inl1', 'InletBoundary', 1);
    model.component('comp1').physics('spf').feature('inl1').selection.set([1]);
    model.component('comp1').physics('spf').create('out1', 'OutletBoundary', 1);
    model.component('comp1').physics('spf').feature('out1').selection.set([4]);
    model.component('comp1').physics('spf').create('vf1', 'VolumeForce', 2);
    model.component('comp1').physics('spf').feature('vf1').selection.set([1]);
    model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
    model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 2);
    model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'GAMMA');
    model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);
    
    model.component('comp1').mesh('mesh1').create('map1', 'Map');
    model.component('comp1').mesh('mesh1').feature('map1').create('dis1', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('map1').create('dis2', 'Distribution');
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').selection.set([1]);
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').selection.set([2]);
    
    model.component('comp1').probe.create('point1', 'Point');
    model.component('comp1').probe('point1').selection.set([3]);
    
    model.result.table('tbl1').label('Probe Table 1');
    model.result.table('tbl2').comments('Global Evaluation 1');
    
    model.component('comp1').variable('var2').label('Parabolic Velocity');
    model.component('comp1').variable('var3').label('Objective function');
    
    model.component('comp1').view('view1').set('showlabels', true);
    model.component('comp1').view('view1').axis.set('xmin', -0.6137096881866455);
    model.component('comp1').view('view1').axis.set('xmax', 5.613709449768066);
    model.component('comp1').view('view1').axis.set('ymin', -2.9508063793182373);
    model.component('comp1').view('view1').axis.set('ymax', 3.9508063793182373);
    
    model.component('comp1').cpl('aveop1').set('opname', 'AveInlet');
    model.component('comp1').cpl('aveop2').set('opname', 'AveOutlet');
    
    model.component('comp1').physics('spf').feature('fp1').set('rho_mat', 'userdef');
    model.component('comp1').physics('spf').feature('fp1').set('rho', 'Rho_f');
    model.component('comp1').physics('spf').feature('fp1').set('mu_mat', 'userdef');
    model.component('comp1').physics('spf').feature('fp1').set('mu', 'Mu_f');
    model.component('comp1').physics('spf').feature('inl1').set('U0in', 'U_inlet_para');
    model.component('comp1').physics('spf').feature('vf1').set('F', {'-u * alpha_int(GAMMA, Alpha_max, q_phi) [N*s/(m^4)]'; '-v * alpha_int(GAMMA, Alpha_max, q_phi) [N*s/(m^4)]'; '0'});
    model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
    model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
    model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
    
    % Meshing the domain (To be changed if your lsm.nel meshing change)
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis1').set('numelem', 50);
    model.component('comp1').mesh('mesh1').feature('map1').feature('dis2').set('numelem', 250);
    model.component('comp1').mesh('mesh1').run;
    
    model.component('comp1').probe('point1').set('expr', '-u');
    model.component('comp1').probe('point1').set('descr', '-u');
    model.component('comp1').probe('point1').set('table', 'tbl1');
    model.component('comp1').probe('point1').set('window', 'window1');
    
    model.study.create('std1');
    model.study('std1').create('sens', 'Sensitivity');
    model.study('std1').create('stat', 'Stationary');
    
    model.sol.create('sol1');
    model.sol('sol1').study('std1');
    model.sol('sol1').attach('std1');
    model.sol('sol1').create('st1', 'StudyStep');
    model.sol('sol1').create('v1', 'Variables');
    model.sol('sol1').create('s1', 'Stationary');
    model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
    model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
    model.sol('sol1').feature('s1').create('d1', 'Direct');
    model.sol('sol1').feature('s1').create('i1', 'Iterative');
    model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('sc1', 'SCGS');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('sc1', 'SCGS');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').create('d1', 'Direct');
    model.sol('sol1').feature('s1').feature.remove('fcDef');
    
%     model.result.dataset.create('dset2', 'Solution');
%     model.result.dataset.create('avh1', 'Average');
%     model.result.dataset('dset2').set('probetag', 'point1');
%     model.result.dataset('avh1').set('probetag', 'point1');
%     model.result.dataset('avh1').set('data', 'dset2');
%     model.result.dataset('avh1').selection.geom('geom1', 0);
%     model.result.dataset('avh1').selection.set([3]);
%     model.result.numerical.create('pev1', 'EvalPoint');
%     model.result.numerical.create('gev1', 'EvalGlobal');
%     model.result.numerical('pev1').set('probetag', 'point1');
%     model.result.create('pg1', 'PlotGroup1D');
%     model.result.create('pg2', 'PlotGroup2D');
%     model.result.create('pg3', 'PlotGroup2D');
%     model.result.create('pg4', 'PlotGroup2D');
%     model.result('pg1').set('probetag', 'window1_default');
%     model.result('pg1').create('tblp1', 'Table');
%     model.result('pg1').feature('tblp1').set('probetag', 'point1');
%     model.result('pg2').create('surf1', 'Surface');
%     model.result('pg3').create('con1', 'Contour');
%     model.result('pg3').feature('con1').set('expr', 'p');
%     model.result('pg4').create('surf1', 'Surface');
%     model.result('pg4').feature('surf1').set('expr', 'fsens(GAMMA)');
%     
%     model.component('comp1').probe('point1').genResult([]);
%     
%     model.result('pg5').tag('pg1');
    

    % Obj Function 
    model.study('std1').feature('sens').set('gradientStep', 'stat');
    model.study('std1').feature('sens').set('optobj', {'comp1.Obj_Func'});
    model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});
    
    model.sol('sol1').attach('std1');
    model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
    model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
    model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
    model.sol('sol1').feature('s1').feature('dDef').label('Direct 2');
    model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
    model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
    model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
    model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
    model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
    model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
    model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
    model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'd1');
    model.sol('sol1').feature('s1').feature('fc1').set('initstep', 0.01);
    model.sol('sol1').feature('s1').feature('fc1').set('maxiter', 200);
    model.sol('sol1').feature('s1').feature('d1').label('Direct, fluid flow variables (spf)');
    model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
    model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-13);
    model.sol('sol1').feature('s1').feature('i1').label('AMG, fluid flow variables (spf)');
    model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', true);
    model.sol('sol1').feature('s1').feature('i1').set('maxlinit', 1000);
    model.sol('sol1').feature('s1').feature('i1').set('rhob', 20);
    model.sol('sol1').feature('s1').feature('i1').feature('ilDef').label('Incomplete LU 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').label('Multigrid 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('maxcoarsedof', 80000);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('strconn', 0.02);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('saamgcompwise', true);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').label('Presmoother 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('soDef').label('SOR 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').label('SCGS 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('linesweeptype', 'ssor');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('iter', 0);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('approxscgs', true);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('scgsdirectmaxsize', 1000);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').label('Postsmoother 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('soDef').label('SOR 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').label('SCGS 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('linesweeptype', 'ssor');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('iter', 1);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('approxscgs', true);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('scgsdirectmaxsize', 1000);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').label('Coarse Solver 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('dDef').label('Direct 2');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').label('Direct 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('linsolver', 'pardiso');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('pivotperturb', 1.0E-13);
    model.sol('sol1').runAll;
    
%     model.result.dataset('dset2').label('Probe Solution 2');
%     model.result.numerical('gev1').set('table', 'tbl2');
%     model.result.numerical('gev1').set('expr', {'comp1.Constraint_Func'});
%     model.result.numerical('gev1').set('unit', {'N'});
%     model.result.numerical('gev1').set('descr', {'Total Energy Flux difference (contributions from pressure and kinetic energy)'});
%     model.result.numerical('gev1').setResult;
%     model.result('pg1').label('Probe Plot Group 1');
%     model.result('pg1').set('xlabel', '-u (m/s), Point Probe 1');
%     model.result('pg1').set('ylabel', '-u (m/s), Point Probe 1');
%     model.result('pg1').set('windowtitle', 'Probe Plot 1');
%     model.result('pg1').set('xlabelactive', false);
%     model.result('pg1').set('ylabelactive', false);
%     model.result('pg2').label('Velocity (spf)');
%     model.result('pg2').set('frametype', 'spatial');
%     model.result('pg2').feature('surf1').label('Surface');
%     model.result('pg2').feature('surf1').set('smooth', 'internal');
%     model.result('pg2').feature('surf1').set('resolution', 'normal');
%     model.result('pg3').label('Pressure (spf)');
%     model.result('pg3').set('frametype', 'spatial');
%     model.result('pg3').feature('con1').label('Contour');
%     model.result('pg3').feature('con1').set('number', 40);
%     model.result('pg3').feature('con1').set('levelrounding', false);
%     model.result('pg3').feature('con1').set('smooth', 'internal');
%     model.result('pg3').feature('con1').set('resolution', 'normal');
%     model.result('pg4').label('Sensitivity');
%     model.result('pg4').feature('surf1').set('resolution', 'normal');
    
    out = model;
