function out = model
    %
    % double_pipe.m
    %
    % Model exported on Jun 27 2024, 16:06 by COMSOL 6.2.0.339.
    
    import com.comsol.model.*
    import com.comsol.model.util.*
    
    model = ModelUtil.create('Model');
    
    model.modelPath('C:\Users\m2do\Desktop\ParaLeSTO Turbulence Project\examples\Robin\Physics_by_Physics\2)_StokeFlow_PipeBend');
    
    model.label('double_pipe.mph');
    
    model.param.set('Alpha_max', '2.5*(1/(0.01^2)) [kg/s/m^3]');
    model.param.set('Alpha_min', '2.5 * (1/(100^2))     [kg/s/m^3]');
    model.param.set('q_phi', '0.01');
    model.param.set('delta', '1');
    model.param.group.create('par2');
    model.param('par2').set('Re', '1');
    model.param('par2').set('D', '1/6 [m]');
    model.param('par2').set('u_inlet', '1 [m/s]');
    model.param('par2').set('Mu_f', '1 [Pa*s]');
    model.param('par2').set('Rho_f', '(Re * Mu_f) / (u_inlet * D)');
    model.param.label('Constants');
    model.param('par2').label('Fluid');
    
    model.component.create('comp1', true);
    
    model.component('comp1').geom.create('geom1', 2);
    
    model.func.create('an1', 'Analytic');
    model.func('an1').label('Brinkman penalization function');
    model.func('an1').set('funcname', 'alpha_int');
    model.func('an1').set('expr', '(Alpha + (Alpha_min - Alpha) * GAMMA * (1 + q_phi)/(GAMMA + q_phi))');
    model.func('an1').set('args', {'GAMMA' 'Alpha' 'q_phi'});
    model.func('an1').set('argunit', {'1' 'kg/s/m^3' ''});
    model.func('an1').set('plotaxis', {'on' 'on' 'on'});
    model.func('an1').set('plotfixedvalue', {'0' '0' '0'});
    model.func('an1').set('plotargs', {'GAMMA' '0' '1'; 'Alpha' '0' '1'; 'q_phi' '0' '1'});
    
    model.component('comp1').mesh.create('mesh1');
    
    model.component('comp1').geom('geom1').create('r1', 'Rectangle');
    model.component('comp1').geom('geom1').feature('r1').set('size', {'delta' '1'});
    model.component('comp1').geom('geom1').create('ls1', 'LineSegment');
    model.component('comp1').geom('geom1').feature('ls1').set('specify1', 'coord');
    model.component('comp1').geom('geom1').feature('ls1').set('coord1', {'0' '1/6'});
    model.component('comp1').geom('geom1').feature('ls1').set('specify2', 'coord');
    model.component('comp1').geom('geom1').feature('ls1').set('coord2', {'0' '1/3'});
    model.component('comp1').geom('geom1').create('ls3', 'LineSegment');
    model.component('comp1').geom('geom1').feature('ls3').set('specify1', 'coord');
    model.component('comp1').geom('geom1').feature('ls3').set('coord1', {'0' '5/6'});
    model.component('comp1').geom('geom1').feature('ls3').set('specify2', 'coord');
    model.component('comp1').geom('geom1').feature('ls3').set('coord2', {'0' '2/3'});
    model.component('comp1').geom('geom1').create('ls4', 'LineSegment');
    model.component('comp1').geom('geom1').feature('ls4').set('specify1', 'coord');
    model.component('comp1').geom('geom1').feature('ls4').set('coord1', {'delta' '1/6'});
    model.component('comp1').geom('geom1').feature('ls4').set('specify2', 'coord');
    model.component('comp1').geom('geom1').feature('ls4').set('coord2', {'delta' '1/3'});
    model.component('comp1').geom('geom1').create('ls5', 'LineSegment');
    model.component('comp1').geom('geom1').feature('ls5').set('specify1', 'coord');
    model.component('comp1').geom('geom1').feature('ls5').set('coord1', {'delta' '5/6'});
    model.component('comp1').geom('geom1').feature('ls5').set('specify2', 'coord');
    model.component('comp1').geom('geom1').feature('ls5').set('coord2', {'delta' '2/3'});
    model.component('comp1').geom('geom1').run;
    model.component('comp1').geom('geom1').run('fin');
    
    model.component('comp1').variable.create('var2');
    model.component('comp1').variable('var2').set('U_inlet_para_up', 'u_inlet * (1 - ( (y-(3/4))/(D/2) )^2)', 'Parabolic Inlet Velocity Profile (U_max at y=1.3m)');
    model.component('comp1').variable('var2').set('U_inlet_para_down', 'u_inlet * (1 - ( (y-(1/4))/(D/2) )^2)', 'Parabolic Inlet Velocity Profile (U_max at y=1.3m)');
    model.component('comp1').variable.create('var3');
    model.component('comp1').variable('var3').set('Inlet', 'spf.intop((AvePressureInlet(p) + 0.5*Rho_f*(u^2)))', 'Normalized Energy Flux (inlet)');
    model.component('comp1').variable('var3').set('Outlet', 'spf.intop((AvePressureOutlet(p) + 0.5*Rho_f*(u^2)))', 'Normalized Energy Flux (outlet)');
    model.component('comp1').variable('var3').set('Obj_Func_1', 'Inlet - Outlet', 'Total Energy Flux difference (contributions from pressure and kinetic energy)');
    
    model.component('comp1').cpl.create('aveop1', 'Average');
    model.component('comp1').cpl.create('aveop2', 'Average');
    model.component('comp1').cpl('aveop1').selection.geom('geom1', 1);
    model.component('comp1').cpl('aveop1').selection.set([3 5]);
    model.component('comp1').cpl('aveop2').selection.geom('geom1', 1);
    model.component('comp1').cpl('aveop2').selection.set([9 11]);
    
    model.component('comp1').physics.create('spf', 'LaminarFlow', 'geom1');
    model.component('comp1').physics('spf').create('inl1', 'InletBoundary', 1);
    model.component('comp1').physics('spf').feature('inl1').selection.set([3 5]);
    model.component('comp1').physics('spf').create('inl2', 'InletBoundary', 1);
    model.component('comp1').physics('spf').feature('inl2').selection.set([3]);
    model.component('comp1').physics('spf').create('out1', 'OutletBoundary', 1);
    model.component('comp1').physics('spf').feature('out1').selection.set([9 11]);
    model.component('comp1').physics('spf').create('vf1', 'VolumeForce', 2);
    model.component('comp1').physics('spf').feature('vf1').selection.set([1]);
    model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
    model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 2);
    model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'GAMMA');
    model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);
    
    model.component('comp1').mesh('mesh1').create('map1', 'Map');
    model.component('comp1').mesh('mesh1').feature('map1').create('size1', 'Size');
    
    model.component('comp1').variable('var2').label('Parabolic Velocity');
    model.component('comp1').variable('var3').label('Objective function');
    
    model.component('comp1').view('view1').set('showlabels', true);
    model.component('comp1').view('view1').axis.set('xmin', -0.2205871045589447);
    model.component('comp1').view('view1').axis.set('xmax', 1.2726656198501587);
    model.component('comp1').view('view1').axis.set('ymin', -0.37699267268180847);
    model.component('comp1').view('view1').axis.set('ymax', 1.2779226303100586);
    
    model.component('comp1').cpl('aveop1').set('opname', 'AvePressureInlet');
    model.component('comp1').cpl('aveop2').set('opname', 'AvePressureOutlet');
    
    model.component('comp1').physics('spf').feature('fp1').set('rho_mat', 'userdef');
    model.component('comp1').physics('spf').feature('fp1').set('rho', 'Rho_f');
    model.component('comp1').physics('spf').feature('fp1').set('mu_mat', 'userdef');
    model.component('comp1').physics('spf').feature('fp1').set('mu', 'Mu_f');
    model.component('comp1').physics('spf').feature('inl1').set('U0in', 'U_inlet_para_up');
    model.component('comp1').physics('spf').feature('inl2').set('U0in', 'U_inlet_para_down');
    model.component('comp1').physics('spf').feature('vf1').set('F', {'-u * alpha_int(GAMMA, Alpha_max, q_phi) [N*s/(m^4)]'; '-v * alpha_int(GAMMA, Alpha_max, q_phi) [N*s/(m^4)]'; '0'});
    model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
    model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
    model.component('comp1').physics('sens').feature('cvar1').set('order', 0);
    
    % Meshing the domain (Has to be changed if your lsm.nel meshing change)
    model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('custom', 'on');
    model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmax', '1/120');
    model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmaxactive', true);
    model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmin', '1/120');
    model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hminactive', true);
    model.component('comp1').mesh('mesh1').run;
    
    model.study.create('std1');
    model.study('std1').create('sens', 'Sensitivity');
    model.study('std1').create('stat', 'Stationary');
    
    model.sol.create('sol1');
    model.sol('sol1').study('std1');
    model.sol('sol1').attach('std1');
    model.sol('sol1').create('st1', 'StudyStep');
    model.sol('sol1').create('v1', 'Variables');
    model.sol('sol1').create('s1', 'Stationary');
    model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
    model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
    model.sol('sol1').feature('s1').create('d1', 'Direct');
    model.sol('sol1').feature('s1').create('i1', 'Iterative');
    model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('sc1', 'SCGS');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('sc1', 'SCGS');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').create('d1', 'Direct');
    model.sol('sol1').feature('s1').feature.remove('fcDef');
    
%     model.result.create('pg1', 'PlotGroup2D');
%     model.result.create('pg2', 'PlotGroup2D');
%     model.result.create('pg3', 'PlotGroup2D');
%     model.result.create('pg4', 'PlotGroup2D');
%     model.result('pg1').create('surf1', 'Surface');
%     model.result('pg2').create('con1', 'Contour');
%     model.result('pg2').feature('con1').set('expr', 'p');
%     model.result('pg3').create('surf1', 'Surface');
%     model.result('pg3').feature('surf1').set('expr', 'fsens(GAMMA)');
%     model.result('pg4').create('str1', 'Streamline');
%     model.result('pg4').feature('str1').selection.set([3 5]);
    
    % Obj Function 
    model.study('std1').feature('sens').set('gradientStep', 'stat');
    model.study('std1').feature('sens').set('optobj', {'comp1.Obj_Func_1'});
    model.study('std1').feature('sens').set('descr', {'Total Energy Flux difference (contributions from pressure and kinetic energy)'});
    model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});
    
    model.sol('sol1').attach('std1');
    model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
    model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
    model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
    model.sol('sol1').feature('s1').feature('dDef').label('Direct 2');
    model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
    model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
    model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
    model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
    model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
    model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
    model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
    model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'd1');
    model.sol('sol1').feature('s1').feature('fc1').set('initstep', 0.01);
    model.sol('sol1').feature('s1').feature('fc1').set('maxiter', 100);
    model.sol('sol1').feature('s1').feature('d1').label('Direct, fluid flow variables (spf)');
    model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
    model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-13);
    model.sol('sol1').feature('s1').feature('i1').label('AMG, fluid flow variables (spf)');
    model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', true);
    model.sol('sol1').feature('s1').feature('i1').set('maxlinit', 1000);
    model.sol('sol1').feature('s1').feature('i1').set('rhob', 20);
    model.sol('sol1').feature('s1').feature('i1').feature('ilDef').label('Incomplete LU 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').label('Multigrid 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('maxcoarsedof', 80000);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('strconn', 0.02);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('saamgcompwise', true);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').label('Presmoother 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('soDef').label('SOR 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').label('SCGS 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('linesweeptype', 'ssor');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('iter', 0);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('approxscgs', true);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('sc1').set('scgsdirectmaxsize', 1000);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').label('Postsmoother 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('soDef').label('SOR 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').label('SCGS 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('linesweeptype', 'ssor');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('iter', 1);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('approxscgs', true);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('sc1').set('scgsdirectmaxsize', 1000);
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').label('Coarse Solver 1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('dDef').label('Direct 2');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').label('Direct 1.1');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('linsolver', 'pardiso');
    model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('pivotperturb', 1.0E-13);
    model.sol('sol1').runAll;
    
%     model.result('pg1').label('Velocity (spf)');
%     model.result('pg1').set('frametype', 'spatial');
%     model.result('pg1').feature('surf1').label('Surface');
%     model.result('pg1').feature('surf1').set('smooth', 'internal');
%     model.result('pg1').feature('surf1').set('resolution', 'normal');
%     model.result('pg2').label('Pressure (spf)');
%     model.result('pg2').set('frametype', 'spatial');
%     model.result('pg2').feature('con1').label('Contour');
%     model.result('pg2').feature('con1').set('number', 40);
%     model.result('pg2').feature('con1').set('levelrounding', false);
%     model.result('pg2').feature('con1').set('smooth', 'internal');
%     model.result('pg2').feature('con1').set('resolution', 'normal');
%     model.result('pg3').label('Sensitivity');
%     model.result('pg3').feature('surf1').set('colortabletype', 'discrete');
%     model.result('pg3').feature('surf1').set('bandcount', 20);
%     model.result('pg3').feature('surf1').set('resolution', 'normal');
%     model.result('pg4').feature('str1').set('posmethod', 'start');
%     model.result('pg4').feature('str1').set('number', 100);
%     model.result('pg4').feature('str1').set('resolution', 'normal');
    
    out = model;
