clear; close all; clc;

% Copyright 2024 H Alicia Kim
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

%% Import model as generated by COMSOL Multiphysics
model = agave_2d();
out_folder = 'agave_2d';  

%% Import the level-set modules
myMex = mexPackage(@MexLSM2D);
lsm   = myMex.LevelSetWrapper();

%% Material, BC and meshing, geometry
lsm.nelx = 100;
lsm.nely = 200;
Vdomain  = lsm.nelx * lsm.nely;

% Level-set parameters
lsm.move_limit = 0.15;
lsm.map_flag = 1; % 0: Least-squares, 1: Discrete adjoint
lsm.perturbation = 0.15;


% Initial voids 
lsm.CreateInitialCircleVoids(20, 40, 8);
lsm.CreateInitialCircleVoids(20, 80, 8);
lsm.CreateInitialCircleVoids(20, 120, 8);
lsm.CreateInitialCircleVoids(20, 160, 8);

lsm.CreateInitialCircleVoids(40, 40, 8);
lsm.CreateInitialCircleVoids(40, 80, 8);
lsm.CreateInitialCircleVoids(40, 120, 8);
lsm.CreateInitialCircleVoids(40, 160, 8);

lsm.CreateInitialCircleVoids(60, 40, 8);
lsm.CreateInitialCircleVoids(60, 80, 8);
lsm.CreateInitialCircleVoids(60, 120, 8);
lsm.CreateInitialCircleVoids(60, 160, 8);

lsm.CreateInitialCircleVoids(80, 40, 8);
lsm.CreateInitialCircleVoids(80, 80, 8);
lsm.CreateInitialCircleVoids(80, 120, 8);
lsm.CreateInitialCircleVoids(80, 160, 8);

% Voids
% ADD VOIDS (OPTIONAL)  

% Domains 
% ADD DOMAINS (OPTIONAL)  

% Initialize the level-set function
lsm.SetUp();


%% Map the coordinates between the level set function and COMSOL Multiphysics 
info = mphxmeshinfo(model);
ED_index = find(strcmp(info.dofs.dofnames, 'comp1.ED')) - 1;
ED_index_set = find(info.dofs.nameinds == ED_index);
ED_coords = info.dofs.coords(:, ED_index_set);
ED_T = ED_coords';
[physics_to_LSM_map, LSM_to_physics_map] = obtain_map_btw_physics_and_LSM_2D(ED_coords');


%% Prepare plot, folders and files for the post-processing
% Sym
model.result.dataset.create('mir1', 'Mirror2D');
model.result.dataset('mir1').set('genpoints', [0.5 0; 0.5 1]);

pg2 = model.result.create('pg2', 'PlotGroup2D');
pg2.set('showlegendsmaxmin', true);
pg2.set('showlegendsunit', true);
model.result('pg2').set('data', 'mir1');
    surf2 = pg2.feature.create('surf1', 'Surface');
    surf2.create('filt1', 'Filter');
    surf2.feature('filt1').set('expr', 'ED > 0.5');
    surf2.set('colortabletype', 'discrete');
    surf2.set('bandcount', 50);
    surf2.set('resolution', 'normal');


% Define .txt files for optimization history
current_folder = fullfile(cd, out_folder);

% Make the directory for optimized mph model and convergence history
History_and_Optimized_Model_path = [current_folder '/HistoryAndOptimizedModelMph'];
if exist(History_and_Optimized_Model_path, 'dir') == 7
    rmdir(History_and_Optimized_Model_path,'s');
end
mkdir([current_folder '/HistoryAndOptimizedModelMph']);

% Path for convergence history
TOP_HISTORY = [current_folder '/HistoryAndOptimizedModelMph/History.txt'];
if exist(TOP_HISTORY, 'file') == 2
    delete(TOP_HISTORY);
end

% Make the directory for the STL files
STLfiles_path = [current_folder '/STLfiles'];
if exist(STLfiles_path, 'dir') == 7
    rmdir(STLfiles_path,'s');
end
mkdir([current_folder '/STLfiles']);

% Create figure
fig = figure('Color', [1, 1, 1]);
t_layout = tiledlayout(2, 4, 'TileSpacing', 'compact', 'Padding', 'compact');



%% Set up for optimization loop
n_iterations = 0;
n_constraint = 1;
max_it       = 400;
volfrac      = 0.5 * Vdomain;

%% Optimization loop
t_start = tic;
while n_iterations < max_it
    t_loop = tic;
    n_iterations = n_iterations + 1;
    fprintf('\n\n');
    disp(['iteration = ' num2str(n_iterations)])
    
    % ===================================================================
    % Step 1: Solve the model: Forward analysis
    % ===================================================================
    % Calculate the element densities
    ED_LSM = lsm.CalculateElementDensities(false);
    for i = 1:length(ED_LSM)
        if ED_LSM(i) < 1e-3
            ED_LSM(i) = 1e-3;
        else
            ED_LSM(i) = ED_LSM(i);
        end
    end
    % Sort the element densities from the level-set function to COMSOL
    ED_COMSOL = ED_LSM(LSM_to_physics_map);
    
    % Assign the sorted element densities to COMSOL
    U_sol = mphgetu(model, 'type', 'sol');
    U_sol(:) = 0; % reset solution
    U_sol(ED_index_set) = ED_COMSOL;

    model.sol('sol1').setU(U_sol);
    model.sol('sol1').createSolution;
    model.sol('sol1').feature('v1').set('initmethod','sol');
    model.sol('sol1').feature('v1').set('initsol','sol1');
    model.sol('sol1').feature('v1').set('notsolmethod','sol');
    model.sol('sol1').feature('v1').set('notsol','sol1');
    
    % Perform FEA with the assigned element densities
    model.sol('sol1').runAll;   
    
    % ===================================================================
    % Step 2: Calculate the objective function & its sensitivites
    % =================================================================== 
    J = mphglobal(model, 'comp1.intop1((ht.kxx)*(Tx^2)+(ht.kyy)*(Ty^2))');
    Obj_History(n_iterations, :) = [J];
    legend_str = {"J"};
    
    % Calculate the element sensitivities using the adjoint method with fsens()
    df_drho_COMSOL = mphgetu(model, 'type', 'fsens');
    df_drho_COMSOL = df_drho_COMSOL(ED_index_set);
    
    % Sort the element sensitivities from COMSOL to level-set
    df_drho_LSM = df_drho_COMSOL(physics_to_LSM_map);
    
    % ===================================================================
    % Step 3: Map the boundary point sensitivities
    % ===================================================================
    df_dbpt = lsm.MapSensitivities(df_drho_LSM, false);
    dg_dbpt = lsm.MapVolumeSensitivities();
    
    % ===================================================================
    % Step 4: Solve the sub-optimization problem (for design velocities)
    % ===================================================================       
    curr_cons_vals = lsm.GetVolume();
    Vol_History(n_iterations,:) = curr_cons_vals/Vdomain*100;
    lsm.GetLimits();

    velocities = optimize(df_dbpt, dg_dbpt, volfrac, curr_cons_vals, ...
                          lsm.lower_lim, lsm.upper_lim);
    
    % ===================================================================
    % Step 5: Update the level-set function and write STL file
    % ===================================================================
    lsm.Update(velocities, false);
    lsm.WriteVtk(n_iterations, STLfiles_path, 'levelset');
    
    % ===================================================================
    % Step 6: Post-processing
    % ===================================================================
    % Plot the temperature field
    nexttile(t_layout, 1, [2 2])
    pg2.run;
    mphplot(model,'pg2','rangenum',1);
    axis fill; axis off;
    set(gca, 'FontSize', 15)

    % Plot the optimization history
    nexttile(t_layout, 3, [2 2])
    history_plot(Obj_History,Vol_History,legend_str);
    drawnow;

    % Save the history data
    TOP_HIS_fid = fopen(TOP_HISTORY,'a+');
    fprintf(TOP_HIS_fid,'%1.7f %1.3f \n',J,curr_cons_vals/Vdomain*100);
    fclose(TOP_HIS_fid);

    % Display each iteration data
    fprintf('Obj (J): %1.7f, Vol (%%): %1.3f \n', J, curr_cons_vals/Vdomain*100);
end

% Total elapsed time
fprintf('\nEnd of the optimization loop.\n')
toc(t_start);

%% Save mph file after topology optimization for post-processing
mphsave(model,[current_folder '/HistoryAndOptimizedModelMph/Optimized_model.mph']);
savefig(fig, [current_folder, '/HistoryAndOptimizedModelMph/', out_folder]);
