function out = model
%
% agave.m
%
% Model exported on Mar 14 2024, 11:50 by COMSOL 6.2.0.339.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('C:\Users\matte\Desktop\paralesto-comsol\examples\agave');

model.label('agave.mph');

model.param.set('ED_min', '1e-6');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component('comp1').func.create('an1', 'Analytic');
model.component('comp1').func('an1').set('expr', 'ED_min+(1-ED_min)*x');

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').create('blk1', 'Block');
model.component('comp1').geom('geom1').create('wp1', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp1').set('quickplane', 'yz');
model.component('comp1').geom('geom1').feature('wp1').set('quickx', 0.2);
model.component('comp1').geom('geom1').feature('wp1').set('unite', true);
model.component('comp1').geom('geom1').create('parf1', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf1').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf1').selection('face').set('blk1(1)', 1);
model.component('comp1').geom('geom1').create('wp2', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp2').set('quickplane', 'zx');
model.component('comp1').geom('geom1').feature('wp2').set('quicky', 0.2);
model.component('comp1').geom('geom1').feature('wp2').set('unite', true);
model.component('comp1').geom('geom1').create('parf2', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf2').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf2').selection('face').set('parf1(1)', 3);
model.component('comp1').geom('geom1').run;

model.component('comp1').material.create('mat1', 'Common');

model.component('comp1').cpl.create('intop1', 'Integration');
model.component('comp1').cpl('intop1').selection.set([1]);

model.component('comp1').physics.create('ht', 'HeatTransfer', 'geom1');
model.component('comp1').physics('ht').create('temp1', 'TemperatureBoundary', 2);
model.component('comp1').physics('ht').feature('temp1').selection.set([3]);
model.component('comp1').physics('ht').create('hs1', 'HeatSource', 3);
model.component('comp1').physics('ht').feature('hs1').selection.set([1]);
model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('cvar1', 'ControlVariableField', 3);
model.component('comp1').physics('sens').feature('cvar1').set('fieldVariableName', 'ED');
model.component('comp1').physics('sens').feature('cvar1').selection.set([1]);

model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').create('swe1', 'Sweep');
model.component('comp1').mesh('mesh1').feature('map1').selection.set([3 5 7]);
model.component('comp1').mesh('mesh1').feature('map1').create('size1', 'Size');
model.component('comp1').mesh('mesh1').feature('swe1').create('dis1', 'Distribution');

model.component('comp1').material('mat1').propertyGroup('def').set('thermalconductivity', {'an1(ED)' '0' '0' '0' 'an1(ED)' '0' '0' '0' 'an1(ED)'});
model.component('comp1').material('mat1').propertyGroup('def').set('density', '1');
model.component('comp1').material('mat1').propertyGroup('def').set('heatcapacity', '1');

model.component('comp1').physics('ht').prop('ShapeProperty').set('order_temperature', 1);
model.component('comp1').physics('ht').feature('hs1').set('Q0', 1);
model.component('comp1').physics('sens').feature('cvar1').set('initialValue', 1);
model.component('comp1').physics('sens').feature('cvar1').set('shapeFunctionType', 'shdisc');
model.component('comp1').physics('sens').feature('cvar1').set('order', 0);

model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('custom', 'on');
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmax', 0.05);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmaxactive', true);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hmin', 0.05);
model.component('comp1').mesh('mesh1').feature('map1').feature('size1').set('hminactive', true);
model.component('comp1').mesh('mesh1').feature('swe1').selection('sourceface').set([3 5 7]);
model.component('comp1').mesh('mesh1').feature('swe1').selection('targetface').set([4]);
model.component('comp1').mesh('mesh1').feature('swe1').feature('dis1').set('numelem', 20);
model.component('comp1').mesh('mesh1').run;

model.study.create('std1');
model.study('std1').create('sens', 'Sensitivity');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('sn1', 'Sensitivity');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').create('d1', 'Direct');
model.sol('sol1').feature('s1').create('i1', 'Iterative');
model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').create('so1', 'SOR');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').create('so1', 'SOR');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').create('d1', 'Direct');
model.sol('sol1').feature('s1').feature.remove('fcDef');

% model.result.create('pg1', 'PlotGroup3D');
% model.result.create('pg2', 'PlotGroup3D');
% model.result('pg1').create('vol1', 'Volume');
% model.result('pg2').create('slc1', 'Slice');
% model.result('pg2').feature('slc1').set('expr', 'fsens(ED)');

model.study('std1').feature('sens').set('gradientStep', 'stat');
model.study('std1').feature('sens').set('optobj', {'comp1.intop1(T)'});
model.study('std1').feature('sens').set('descr', {'Weighted average temperature'});
model.study('std1').feature('sens').set('optobjEvaluateFor', {'stat'});

model.sol('sol1').attach('std1');
model.sol('sol1').feature('st1').label('Compile Equations: Stationary');
model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
model.sol('sol1').feature('s1').label('Stationary Solver 1.1');
model.sol('sol1').feature('s1').feature('dDef').label('Direct 2');
model.sol('sol1').feature('s1').feature('aDef').label('Advanced 1');
model.sol('sol1').feature('s1').feature('sn1').label('Sensitivity 1.1');
model.sol('sol1').feature('s1').feature('sn1').set('control', 'sens');
model.sol('sol1').feature('s1').feature('sn1').set('sensfunc', 'all_obj_contrib');
model.sol('sol1').feature('s1').feature('sn1').set('sensmethod', 'adjoint');
model.sol('sol1').feature('s1').feature('fc1').label('Fully Coupled 1.1');
model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'd1');
model.sol('sol1').feature('s1').feature('fc1').set('initstep', 0.01);
model.sol('sol1').feature('s1').feature('fc1').set('minstep', 1.0E-6);
model.sol('sol1').feature('s1').feature('fc1').set('maxiter', 50);
model.sol('sol1').feature('s1').feature('fc1').set('termonres', false);
model.sol('sol1').feature('s1').feature('d1').label('Direct, heat transfer variables (ht)');
model.sol('sol1').feature('s1').feature('d1').set('linsolver', 'pardiso');
model.sol('sol1').feature('s1').feature('d1').set('pivotperturb', 1.0E-13);
model.sol('sol1').feature('s1').feature('i1').label('AMG, heat transfer variables (ht)');
model.sol('sol1').feature('s1').feature('i1').set('nlinnormuse', true);
model.sol('sol1').feature('s1').feature('i1').set('rhob', 20);
model.sol('sol1').feature('s1').feature('i1').feature('ilDef').label('Incomplete LU 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').label('Multigrid 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('maxcoarsedof', 50000);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('saamgcompwise', true);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('usesmooth', false);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').label('Presmoother 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('soDef').label('SOR 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').label('SOR 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('pr').feature('so1').set('relax', 0.9);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').label('Postsmoother 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('soDef').label('SOR 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').label('SOR 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('po').feature('so1').set('relax', 0.9);
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').label('Coarse Solver 1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('dDef').label('Direct 2');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').label('Direct 1.1');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('linsolver', 'pardiso');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').feature('cs').feature('d1').set('pivotperturb', 1.0E-13);
model.sol('sol1').runAll;

% model.result('pg1').label('Temperature (ht)');
% model.result('pg1').feature('vol1').set('colortable', 'HeatCameraLight');
% model.result('pg1').feature('vol1').set('smooth', 'internal');
% model.result('pg1').feature('vol1').set('resolution', 'normal');
% model.result('pg2').label('Sensitivity');
% model.result('pg2').feature('slc1').set('resolution', 'normal');

out = model;
