clear; clc; close all;

% Copyright 2024 H Alicia Kim
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% MATLAB root path
matlab_root = matlabroot;

% Lines to be written to the mex_include.h file
lines = {'#pragma once', ...
         ['#include "', fullfile(matlab_root, 'extern', 'include', 'matrix.h'), '"'], ...
         ['#include "', fullfile(matlab_root, 'extern', 'include', 'mex.h'), '"'] };

% Write lines to the mex_include.h file
writelines(lines, string(fullfile('mexLSTO', 'mex', 'mex_include.h')))
clear; clc;

% Change directory
cd mexLSTO

% Check platform and compile
if isunix
    % Compile LSM 3D, LSM 2D, and Optimizer
    fprintf('Installing ParaLeSTO-COMSOL on Linux platform.\n');
    compileMexLSM3D();
    compileMexLSM2D();
    compileMexOptimizer();
elseif ispc
    % Compile LSM 3D and LSM 2D
    fprintf('Installing ParaLeSTO-COMSOL on Windows platform.\n');
    compileMexLSM3D();
    compileMexLSM2D();
else
    error('Platform not supported.');
end

fprintf('\nRemember to use COMSOL LiveLink for MATLAB.\n');

% Back to the original directory
cd ..

% Add folders and subfolders
addpath(genpath("mexLSTO"));
addpath(genpath("helpers-matlab-files"));

% Print signature
fprintf('\n\n')
disp("  _____                _           _____ _______ ____  ")
disp(" |  __ \   ParaLeSTO  | |         / ____|__   __/ __ \ ")
disp(" | |__) |_ _ _ __ __ _| |     ___| (___    | | | |  | |")
disp(" |  ___/ _` | '__/ _` | |    / _ \\___ \   | | | |  | |")
disp(" | |  | (_| | | | (_| | |___|  __/____) |  | | | |__| |")
disp(" |_|   \__,_|_|  \__,_|______\___|_____/   |_|  \____/ ")
disp("                                                       ")
fprintf('\n\n')










%% Useful function

function compileMexLSM3D()
    if isfile(['MexLSM3D.', mexext]) == 0
        fprintf('\nCompiling MexLSM3D.\n');
        mex CXXFLAGS="-std=c++14 -fPIC -w" ...
            -I. -Imex -Ilsm/include ...
            mex/mex_package_ALL.cpp ...
            lsm/src/grid_math.cpp ...
            lsm/src/lsm_3d.cpp ...
            lsm/src/boundary.cpp ...
            lsm/src/level_set_wrapper.cpp ...
            lsm/src/level_set_wrapper_mex.cpp ...
            -output MexLSM3D
    else
        fprintf("\nMexLSM3D already compiled.\n");
    end
end

function compileMexLSM2D()
    if isfile(['MexLSM2D.', mexext]) == 0
        fprintf('\nCompiling MexLSM2D.\n');
        mex CXXFLAGS="-std=c++14 -fPIC -w" ...
            -I. -Imex -Ilsm_2d/include ...
            mex/mex_package_ALL.cpp ...
            lsm_2d/src/hole.cpp ...
            lsm_2d/src/heap.cpp ...
            lsm_2d/src/fast_marching_method.cpp ...
            lsm_2d/src/mesh.cpp ...
            lsm_2d/src/level_set.cpp ...
            lsm_2d/src/boundary.cpp ...
            lsm_2d/src/input_output.cpp ...
            lsm_2d/src/level_set_wrapper.cpp ...
            lsm_2d/src/level_set_wrapper_mex.cpp ...
            -output MexLSM2D
    else
        fprintf("\nMexLSM2D already compiled.\n");
    end
end

function compileMexOptimizer()
    if isfile(['MexOptimizer.', mexext]) == 0
        fprintf('\nCompiling MexOptimizer.\n');
        mex CXXFLAGS="-std=c++14 -fPIC -w" ...
            -I. -Imex -Iopt/include ...
            -lnlopt -lglpk ...
            mex/mex_package_ALL.cpp ...
            opt/src/optimize.cpp ...
            opt/src/optimizer_wrapper.cpp ...
            opt/src/optimizer_wrapper_mex.cpp ...
            -output MexOptimizer
    else
        fprintf("\nMexOptimizer already compiled.\n");
    end
end
