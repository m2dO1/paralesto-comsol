classdef mexClassType < mexClass
  %mexClassType A representation of a C++ class type.
  %   mexClassType is a representation of the C++ class type, meaning it
  %   can carry info about the type's static members, other type members,
  %   and a constructor that can be called for instantiation into an
  %   object.  The user can create an instance of mexClassType by passing
  %   it a C++ object (i.e. mexClass object) as an input argument.  This is
  %   equivalent in C++ to using decltype() on an object.
  
  properties (Access=private)
    mex_class_handle__
  end
  
  methods
    function self = mexClassType(mex_function, class_params, mex_class_handle)
      % If mex_function is really a mexClass object, then remaining
      % arguments are ignored.
      if isa(mex_function, 'mexClass')
        mex_class_obj = mex_function;
        mex_function = mex_class_obj.mex_function__;
        class_params = mex_function(mex_class_obj.commands__.GetObjectType, mex_class_obj.HANDLE_);
        mex_class_handle = str2func(class(mex_class_obj));
      end
      self = self@mexClass(mex_function, class_params);
      self.mex_class_handle__ = mex_class_handle;
    end
    
    % Overload () operator to instantiate mexClass objects
    function varargout = subsref(self, s)
      switch s(1).type
        case '()'
          varargin = s(1).subs(1:end);
          varargout = {self.mex_class_handle__(self.mex_function__,  self.mex_function__(self.commands__.CreateMexClassObject, self.HANDLE_, varargin{:}))};
        case '.'
          if length(s) > 1
            [y{1:nargout}] = self.(s(1).subs)(s(2).subs{:});
            varargout = y;
          else
            varargout = {self.(s(1).subs)};
          end
      end
    end
  end
  
end

