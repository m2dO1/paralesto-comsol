classdef mexPackage < mexClass
  %mexPackage Extracts the C++ items defined by the mexPackage definition
  %in the mex library.
  %   Detailed explanation goes here

  methods
    function self = mexPackage(mex_function)
      if ~isa(mex_function, 'function_handle'), error('Expecting a mex function handle.'); end
      commands = mex_function();
      self = self@mexClass(mex_function, mex_function(commands.GetPackageClass));
    end
  end
  
end

