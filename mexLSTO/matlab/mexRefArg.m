classdef mexRefArg < mexClass
  %mexRefArg Helper class to enable pass-by-reference of pure data types.
  %   mexRefArg is a wrapper for Matlab non-class type data that allows
  %   them to be passed by reference to function calls inside the
  %   mexPackage library.  It has only a single property 'value' which is
  %   the current value of the data it is referencing.  mexRefArg is not
  %   called directly by the user.  All mexPackage libraries automatically
  %   expose mexRefArg as a mexClassType.
  
  methods
    function self = mexRefArg(mex_function, class_params)
      self = self@mexClass(mex_function, class_params);
    end
  end
  
end

