classdef mexClassSingleton < handle
  %mexClassSingleton Singleton pattern implementation for mexClass

  properties (SetAccess = private)
    mex
  end
  
  methods (Static, Access = protected)
    function mex_obj = MexObject(always_reload_mex_lib, mex_handle, class_name, varargin)
      persistent mex_obj_;
      if nargin
        delete(mex_obj_);
        if always_reload_mex_lib
          eval(['clear ' func2str(mex_handle)]);
          mexClass.DontDestroy(mex_handle);
        end
         mex_obj_ = mexPackage(mex_handle).(class_name)(varargin{:});
      end
      mex_obj = mex_obj_;
    end
  end

  properties (Access = private)
    always_reload_mex_lib = false
  end
  
  methods
    function self = mexClassSingleton(always_reload_mex_lib)
      if nargin
        self.always_reload_mex_lib = always_reload_mex_lib;
      end
    end

    function obj = get.mex(self)
      obj = self.MexObject();
    end
  end
  
  methods (Access = protected)
    function CreateMexObject(self, mex_handle, class_name, varargin)
      self.mex = self.MexObject(self.always_reload_mex_lib, mex_handle, class_name, varargin{:});
    end
  end
    
end

