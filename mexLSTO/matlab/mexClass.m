classdef mexClass < dynamicprops & matlab.mixin.CustomDisplay
  %mexClass A representation of a C++ object.
  %   Detailed explanation goes here  
  
  properties (SetAccess=protected, Hidden)
    HANDLE_
  end
  
  properties (Access=protected)
    ordered_member_names__
    id__
    mex_function__
    commands__
    class_params_length__
  end
  
  methods
    function self = mexClass(mex_function, class_params)
      self.mex_function__ = mex_function;
      self.class_params_length__ = length(class_params);
      self.commands__ = mex_function();
      [self.id__, matlab_class, self.HANDLE_, method_names_list, property_names_list, fixed_mex_class_names_list, dynamic_mex_class_names_list, type_member_names_list, self.ordered_member_names__] = class_params{:};

      for n = 1:length(method_names_list)
        method_name = method_names_list{n};
        prop = self.addprop(method_name);
        self.(method_name) = @(varargin) self.CallMethod(method_name, varargin{:});
        prop.SetMethod = @(self, val) error('Functions cannot be assigned.');
      end

      for n = 1:length(property_names_list)
        property_name = property_names_list{n};
        prop = self.addprop(property_name);
        prop.Dependent = 1;
        prop.GetMethod = @(self) self.mex_function__(self.commands__.GetProperty, self.HANDLE_, property_name);
        prop.SetMethod = @(self, val) self.mex_function__(self.commands__.SetProperty, self.HANDLE_, property_name, val);
      end

      for n = 1:length(fixed_mex_class_names_list)
        mex_class_name = fixed_mex_class_names_list{n};
        prop = self.addprop(mex_class_name);
        self.(mex_class_name) = mexClass(self.mex_function__, self.mex_function__(self.commands__.GetProperty, self.HANDLE_, mex_class_name));
        prop.SetMethod = @(self, val) error('Use Set() method to assign a mexClass property.');
      end

      for n = 1:length(dynamic_mex_class_names_list)
        mex_class_array_name = dynamic_mex_class_names_list{n};
        prop = self.addprop(mex_class_array_name);
        prop.Dependent = 1;
        prop.GetMethod = @(self) self.mexExtractObjects(self.mex_function__(self.commands__.GetProperty, self.HANDLE_, mex_class_array_name));
        prop.SetMethod = @(self, val) error('Assignment operator not supported for this propery.');
      end

      for n = 1:length(type_member_names_list)
        type_member_name = type_member_names_list{n};
        prop = self.addprop(type_member_name);
        self.(type_member_name) = mexClassType(self.mex_function__, self.mex_function__(self.commands__.GetProperty, self.HANDLE_, type_member_name), @mexClass);
        prop.SetMethod = @(self, val) error('Types cannot be assigned.');
      end
    end
    
    function delete(self)
      dont_destroy_list = self.DontDestroy();
      if exist(func2str(self.mex_function__), 'file') && ~any(ismember(dont_destroy_list, func2str(self.mex_function__)))
        self.mex_function__(self.commands__.FreeMexClassObject, self.HANDLE_, self.id__);
      end
    end
  end
  
  methods (Static)
    % Maintains a static list of mex_functions whose mexClass's should
    % never call the destructor, as they are being managed externally.
    function list = DontDestroy(mex_function__)
      persistent dont_destroy_list
      if isempty(dont_destroy_list)
        dont_destroy_list = {};
      end
      if nargin
        if ~any(ismember(dont_destroy_list, func2str(mex_function__)))
          dont_destroy_list{end+1} = func2str(mex_function__);
        end
      end
      list = dont_destroy_list;
    end
  end
  
  methods (Access = protected)
    % Preserves the display order of properties as declared in C++
    function pg = getPropertyGroups(self)
      if ~isscalar(self)
        pg = getPropertyGroups@matlab.mixin.CustomDisplay(self);
      elseif ~isempty(self.ordered_member_names__)
        pg = matlab.mixin.util.PropertyGroup(self.ordered_member_names__);
      else
        pg = [];
      end
    end
  end
    
  methods (Access = private)
    function [ new_value ] = mexExtractObjects(self, value)
      %%% Searches for cell arrays
      %%% which may indicate a mexClass object.  If it is, it converts the
      %%% cell array to a mexClass object.
      new_value = value;
      if iscell(new_value) && ~isempty(new_value)
        if length(new_value) == self.class_params_length__ && isa(new_value{1}, 'uint64') && new_value{1} == self.id__
          mex_class_handle = str2func(new_value{2});
          new_value = mex_class_handle(self.mex_function__, new_value);
        else
          for n = 1:numel(new_value)
            new_value{n} = self.mexExtractObjects(new_value{n});
          end
        end
      end
    end

    % Calls the method and parses the output for mexClass objects
    function varargout = CallMethod(self, method_name, varargin)
      [y{1:nargout}] = self.mex_function__(self.commands__.CallMethod, self.HANDLE_, method_name, varargin{:});
      if ~isempty(y)
        varargout = self.mexExtractObjects(y);
      end
    end
  end
end

