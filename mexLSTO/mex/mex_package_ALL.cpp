// Single compilation unit for mexPackage library

#include "mex_gateway_function.cc"
#include "mex_class_members.cc"
#include "mex_class_members_adder.cc"
#include "mex_class_instance.cc"
#include "mex_class_objects.cc"
#include "demangle.cc"
#include "mex_data_ALL.cpp"
