#pragma once

#include "mx_traits.h"
#include <blitz/array.h>

// Adds mxConvert() support for Blitz arrays


template <typename Dummy>
struct mx_array_traits_blitz_config_template {
  // Specifies whether to use the Matlab storage order (column major) or use C
  // storage order (row major).  In general, C storage order should always be
  // preferred as it is more efficient when copying data from Matlab to C and
  // vice versa.
  static bool use_matlab_storage_order;
};

using mx_array_traits_blitz_config = mx_array_traits_blitz_config_template<void>;

template <typename Dummy>
#ifdef MX_TRAITS_BLITZ_USE_MATLAB_STORAGE_ORDER
bool mx_array_traits_blitz_config_template<Dummy>::use_matlab_storage_order = true;
#else
bool mx_array_traits_blitz_config_template<Dummy>::use_matlab_storage_order = false;
#endif


template <typename T, int N_rank>
struct mx_array_traits<blitz::Array<T, N_rank>, void> : mx_array_traits_blitz_config {
  using value_type = T;

  struct read_iterator {
    // This constructor assumes that the input array is in C order, not Fortran
    // order.
    read_iterator(const blitz::Array<T, N_rank>& a)
        : a_transposed_(blitz::ColumnMajorArray<N_rank>())
    {
      if (use_matlab_storage_order) {
        if (a.numElements() > 0) {
          a_transposed_.resize(a.shape());
          a_transposed_ = a;
        }
        iter_obj_ = a_transposed_.begin();
      } else {
        iter_obj_ = a.begin();
      }
    }

    blitz::Array<T, N_rank> a_transposed_;
    typename blitz::Array<T, N_rank>::const_iterator iter_obj_;

    auto operator*() -> decltype(*iter_obj_) {
      return *iter_obj_;
    }

    read_iterator& operator++() {
      ++iter_obj_;
      return *this;
    }
  };

  static std::vector<std::size_t> size(const blitz::Array<T, N_rank>& a) {
    const auto& shape = a.shape();
    std::vector<std::size_t> v(shape.begin(), shape.end());

    if (use_matlab_storage_order) {
      if (N_rank == 1) {
        v.push_back(1);   // make a 1xN Matlab matrix
      } else {
        std::reverse(v.begin(), v.end());
      }
    }

    return v;
  }

  // This will only get called if converting from Matlab to C
  static std::vector<std::size_t> size(blitz::Array<T, N_rank>& a) {
    const auto& shape = a.shape();
    std::vector<std::size_t> v(shape.begin(), shape.end());
    return v;
  }

  static auto begin(blitz::Array<T, N_rank>& a) -> decltype(a.begin()) {return a.begin();}

  static read_iterator begin(const blitz::Array<T, N_rank>& a) {return read_iterator(a);}
};


template <typename T>
struct mx_factory_traits<blitz::Array<T, 1>, MX_USE_IF(mx_is_element_type<T>::value)> {
  static blitz::Array<T, 1> make(const mxShape& shape) {
    return blitz::Array<T, 1>(shape.num_elements());
  }
};

// Factory traits class for N_rank > 1 and the element type must not be another array
template <typename T, int N_rank>
struct mx_factory_traits<blitz::Array<T, N_rank>, MX_USE_IF((N_rank > 1) && mx_is_element_type<T>::value)>
    : mx_array_traits_blitz_config
{
  static blitz::Array<T, N_rank> make(const mxShape& shape) {
    assert(N_rank >= shape.rank());
    blitz::TinyVector<int, N_rank> tv(1);

    if (use_matlab_storage_order) {
      auto reversed_shape = shape.reverse();
      auto dst = tv.begin();
      for (auto dim : reversed_shape) {
        *dst++ = dim;
      }

      // Note, returning a ColumnMajorArray type can be dangerous because if the
      // user gets an iterator to iterator over the entire multi-dimensional
      // array, it will go in Fortran order, not C order.  But I'm too lazy to
      // come up with a fool-proof solution, since people shouldn't be using
      // Fortran orders in general.
      return blitz::Array<T, N_rank>(tv, blitz::ColumnMajorArray<N_rank>());
    } else {
      auto dst = tv.begin() + (N_rank - shape.rank());
      for (auto dim : shape) {
        *dst++ = dim;
      }
      return blitz::Array<T, N_rank>(tv);
    }
  }
};

template <typename T, int N_rank>
struct mx_copy_traits<blitz::Array<T, N_rank>, void> {
  static void copy(const blitz::Array<T, N_rank>& src, blitz::Array<T, N_rank>& dst) {
    dst.reference(src.copy());
  }
};


#ifdef USE_MEX_CLASS_FOR_CEREAL
#include "cereal/cereal.hpp"
#include "cereal_wrapper.h"
/*
namespace cereal {

  template <class Archive, class T, int N>
  void CEREAL_SAVE_FUNCTION_NAME(Archive& ar, const blitz::TinyVector<T, N>& array) {
    for (const auto& i : array) {
      ar(i);
    }
  }

  template <class Archive, class T, int N>
  void CEREAL_LOAD_FUNCTION_NAME(Archive& ar, blitz::TinyVector<T, N>& array) {
    for (auto& i : array) {
      ar(i);
    }
  }

  template <class Archive, class T, int N_rank>
  void CEREAL_SAVE_FUNCTION_NAME(Archive& ar, const blitz::Array<T, N_rank>& array) {
    ar(cereal::make_nvp("shape", array.shape()));
    for (const auto& i : array) {
      ar(i);
    }
  }

  template <class Archive, class T, int N_rank>
  void CEREAL_LOAD_FUNCTION_NAME(Archive& ar, blitz::Array<T, N_rank>& array) {
    blitz::TinyVector<int, N_rank> shape;
    ar(shape);

    array.resize(shape);
    for (auto& i : array) {
      ar(i);
    }
  }
 
} // namespace cereal
*/
#endif

