#pragma once

#ifndef USE_MEX_CLASS_FOR_CEREAL
#include "mex_class_members_adder.h"
#include "mx_struct_field_adder.h"
#include <type_traits>


/*==============================================================================
This file contains macros for writing mexClass definitions.  It is safe to
include this header file and write the mexClass definitions even in code that
will not be compiled as a mex library as there is no mex library specific code
being included here.  Therefore, it is not necessary to use #ifdef
MATLAB_MEX_FILE to protect this code.
==============================================================================*/

#define MEX_CLASS(...) \
  inline void AddMexClassMembers(const __VA_ARGS__* self, mex_pack::MembersAdderBase&& def=mex_pack::MembersAdderBase{})

#define MEX_CLASS_EXTENDED(...) \
  inline void AddMexClassMembersExtended(const __VA_ARGS__* self, mex_pack::MembersAdderBase&& def=mex_pack::MembersAdderBase{})

#define MEX_STRUCT(...) \
  inline void ConvertMxStruct(__VA_ARGS__* self, mx_type_match<__VA_ARGS__>, MxStructFieldAdderBase def=MxStructFieldAdderBase{})

#define mex_method(name) method(#name, &mx_decay_t<decltype(*self)>::name)

#define mex_property(member) property(#member, &mx_decay_t<decltype(*self)>::member)

#define GET_MEX_PROPERTY_MACRO(_1, _2, NAME, ...) NAME
#define MEX_EXPAND(x) x

#define MEX_PROPERTY_REF1(member) property_ref_(#member, self ? &(self->member) : nullptr)
#define MEX_PROPERTY_REF2(name, member) property_ref_(name, self ? &(self->member) : nullptr)

#define mex_property_ref(...) MEX_EXPAND(GET_MEX_PROPERTY_MACRO(__VA_ARGS__, MEX_PROPERTY_REF2, MEX_PROPERTY_REF1)(__VA_ARGS__))

#define MEX_READ_ONLY_PROPERTY_NONADDRESSABLE1(member) bind_instance().property(#member, std::function<mx_decay_t<decltype(self->member)>(const mx_decay_t<decltype(*self)>&)>([](const mx_decay_t<decltype(*self)>& obj) {return obj.member;}))
#define MEX_READ_ONLY_PROPERTY_NONADDRESSABLE2(name, member) bind_instance().property(name, std::function<mx_decay_t<decltype(self->member)>(const mx_decay_t<decltype(*self)>&)>([](const mx_decay_t<decltype(*self)>& obj) {return obj.member;}))

#define mex_read_only_property_nonaddressable(...) MEX_EXPAND(GET_MEX_PROPERTY_MACRO(__VA_ARGS__, MEX_READ_ONLY_PROPERTY_NONADDRESSABLE2, MEX_READ_ONLY_PROPERTY_NONADDRESSABLE1)(__VA_ARGS__))

#define MEX_PROPERTY_NONADDRESSABLE1(member) bind_instance().property(#member, std::function<mx_decay_t<decltype(self->member)>(const mx_decay_t<decltype(*self)>&)>([](const mx_decay_t<decltype(*self)>& obj) {return obj.member;}), std::function<void(mx_decay_t<decltype(*self)>&, mx_decay_t<decltype(self->member)>)>([](mx_decay_t<decltype(*self)>& obj, mx_decay_t<decltype(self->member)> val) {obj.member = val;}))
#define MEX_PROPERTY_NONADDRESSABLE2(name, member) bind_instance().property(name, std::function<mx_decay_t<decltype(self->member)>(const mx_decay_t<decltype(*self)>&)>([](const mx_decay_t<decltype(*self)>& obj) {return obj.member;}), std::function<void(mx_decay_t<decltype(*self)>&, mx_decay_t<decltype(self->member))>([](mx_decay_t<decltype(*self)>& obj, mx_decay_t<decltype(self->member)> val) {obj.member = val;}))

#define mex_property_nonaddressable(...) MEX_EXPAND(GET_MEX_PROPERTY_MACRO(__VA_ARGS__, MEX_PROPERTY_NONADDRESSABLE2, MEX_PROPERTY_NONADDRESSABLE1)(__VA_ARGS__))

#endif  // #ifndef USE_MEX_CLASS_FOR_CEREAL


#ifdef USE_MEX_CLASS_FOR_CEREAL

#include "cereal/cereal_mex_class.h"

#endif

