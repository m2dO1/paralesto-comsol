#pragma once

#include "mex_class_instance.h"


namespace mex_pack {

/*==============================================================================
Class: ClassType

Description:  Exposes a mexClass type object to Matlab
==============================================================================*/

template <typename T>
class ClassType : public ClassMembers {
 public:
  ClassType();

  // Instantiate new object from Matlab
  ClassMembers* CreateNewObject(mexInputArgs prhs) const override;

  // Downcast the given object to the type specified by the given type object.
  // The result is a reference to the original object, equivalent to
  // dynamic_cast<T&>() in C++.
  // T must be a polymorphic type (i.e. has at least one virtual method)
  static const T* Downcast(ClassMembers& input_members_obj) {
    const T* downcasted_obj_ptr = nullptr;
    if (std::is_polymorphic<T>::value) {
      AddMexClassMembers(static_cast<const T*>(nullptr), MembersAdder<T>(input_members_obj.type_info_, input_members_obj.GetObject(), &downcasted_obj_ptr));
    }
    return downcasted_obj_ptr;
  }

 private:
  // Call Matlab format constructor if one exists
  static T* CallMatlabCtor(std::true_type, const mexInputArgs& prhs) {
    return new T(prhs);
  }
  static T* CallMatlabCtor(std::false_type, const mexInputArgs&) {
    return nullptr;
  }

  // Create new object with default constructor if one exists
  static T* CreateDefaultObject(std::true_type) {
    return new T;
  }
  static T* CreateDefaultObject(std::false_type) {
    return nullptr;
  }
};

} // namespace mex_pack

