#pragma once

#include "mx_struct.h"
#include <tuple>

// forward declaration
template <typename T> class MxStructFieldAdder;


class MxStructFieldAdderBase {
 protected:
  bool convert_to_c_ = false;
  const mxConstStruct* mx_read_proxy_ = nullptr;
  mxStruct* mx_write_proxy_ = nullptr;

 public:
  MxStructFieldAdderBase() = default;

  MxStructFieldAdderBase(bool convert_to_c, const mxConstStruct* mx_read_proxy, mxStruct* mx_write_proxy)
      : convert_to_c_(convert_to_c)
      , mx_read_proxy_(mx_read_proxy)
      , mx_write_proxy_(mx_write_proxy)
  {}

  template <typename T>
  MxStructFieldAdder<T> operator()(T* p_obj) {
    return MxStructFieldAdder<T>(p_obj, *this);
  }

  // May be useful in MEX_STRUCT definition for run-time control
  bool convert_to_c() const {return convert_to_c_;}

  // Only provided for backwards compatibility with MembersAdder class
  template <typename T>
  T* nonconst_cast(T* obj) const {
    return obj;
  }
};


// MxStructFieldAdder is the equivalent of the MembersAdder class for mexClass types.
// It allows for mexClass-like definitions but for pure data types.
// It implements the conversion routines needed for mx_element_traits.

template <typename T>
class MxStructFieldAdder : public MxStructFieldAdderBase {
  T* p_obj_;

 public:
  MxStructFieldAdder(T* p_obj, const MxStructFieldAdderBase& params)
      : MxStructFieldAdderBase(params)
      , p_obj_(p_obj)
  {}

  MxStructFieldAdder<T> operator()() {return *this;}

  operator T*() {return p_obj_;};

  template <typename F>
  MxStructFieldAdder<T>& property(const char* field_name, F T::* field_obj) {
    if (convert_to_c_) {
      mx_read_proxy_->convert_field(field_name, &(p_obj_->*field_obj));
    } else {
      (*mx_write_proxy_)[field_name] = p_obj_->*field_obj;
    }
    return *this;
  }

  // Removed support for this as this doesn't work property if the reference points to another class member
  // that is also exposed to Matlab, as this causes a conflict because both conversions will be applied.
  //template <typename F>
  //MxStructFieldAdder<T>& property_ref_(const char* field_name, F* field_obj) {
  //  if (convert_to_c_) {
  //    mx_read_proxy_->convert_field(field_name, field_obj);
  //  } else {
  //    (*mx_write_proxy_)[field_name] = *field_obj;
  //  }
  //  return *this;
  //}
  
  template <typename F>
  MxStructFieldAdder<T>& property_bitfield_(const char* field_name, std::function<F()> getter, std::function<void(F)> setter) {
    if (convert_to_c_) {
      F tmp;
      mx_read_proxy_->convert_field(field_name, &tmp);
      setter(tmp);
    } else {
      (*mx_write_proxy_)[field_name] = getter();
    }
    return *this;
  }

  // Add base class
  template <typename BaseClass>
  MxStructFieldAdder<T>& base_class() {
    static_assert(std::is_base_of<BaseClass, T>::value, "Invalid base_class() specification.");
    ConvertMxStruct(dynamic_cast<BaseClass*>(p_obj_), mx_type_match<BaseClass>{}, *this);
    return *this;
  }
};

