#pragma once

#include "mex_output_args_decl.h"
#include "mx_struct.h"
#include "mex_include.h"
#include <algorithm>


/*==============================================================================
Class:  mexOutputArgs

Description:
------------
mexOutputArgs is a replacement for the mexFunction output arguments, nlhs and
plhs, that provides a safe mechanism for setting output arguments without
worrying about crashes if output argument is not present from Matlab.

Usage:
------
mexOutputArgs should be instantiated at the beginning of the mexFunction and
passed nlhs and plhs as constructor arguments like this:

    mexOutputArgs arg_out(nlhs, plhs);

The newly created object, arg_out, behaves somewhat like a pointer and initially
points to the first output argument.  One can then modify the argument it points
to by various incrementing operations, or one can have it return an input
argument at a different position using the [] operator.

Some examples are:

    arg_out[n]    <= refers to the nth argument while leaving arg_out unchanged

    ++arg_out     <= arg_out now points to the next argument

    *arg_out++    <= returns current argument and then points to the next argument

    arg_out += n  <= arg_out now points to the nth argument

Using the deferencing operator or the [] operator causes it to return an object
of type mexOutputArgs::Data, which functions as a proxy to the mxArray** type the
represents that output argument.  If the user tries to assign it a value without
the output argument being present from Matlab, it will flag an error.  Note that
mexOutputArgs always lets the first argument to be assigned, even if nlhs == 0,
as Matlab treats that as a special case to assign to the ans output.  Examples
of assignments are:

  arg_out[0] = c_obj;   // c_obj can be of any type supported by mxConvert()

  *arg_out++ = true;

  mxStruct out;
  arg_out[1] = out;

==============================================================================*/

class mexOutputArgsBase {
 public:
  mexOutputArgsBase(int nlhs, mxArray* plhs[])
      // if nlhs=0, then plhs[0] is still valid and goes to ans
      : nlhs_(std::max(nlhs, 1))
      , plhs_(plhs)
  {}

 protected:
  int nlhs_;
  mxArray** plhs_;
};

template <typename Converter>
class mexOutputArgsTemplate : public mexOutputArgsBase {
 public:
  class Data {
   public:
    Data(bool valid, mxArray*& mxa) : valid_(valid), mxa_(mxa) {}

    bool valid() const {return valid_;}

    operator mxArray*&() {
      if (!valid()) mexErrMsgTxt("No output argument to can be assigned to this.");
      return mxa_;
    }

    Data& operator=(mxArray* val) {
      if (valid()) mxa_ = val;
      return *this;
    }

    Data& operator=(const mxStruct& val) {
      return operator=(static_cast<mxArray*>(val));
    }

    // Use forwarding reference as some mxConvert() implementation do care
    // whether an lvalue or rvalue is being used.
    template <typename T, MX_OVERLOAD_IF(!std::is_convertible<T, mxArray*>::value)>
    Data& operator=(T&& val) {
      return operator=(Converter::convert(std::forward<T>(val)));
    }

    template <int CELL_DEPTH, typename T, MX_OVERLOAD_IF(!std::is_convertible<T, mxArray*>::value)>
    void cell_convert(T&& val) {
      operator=(Converter::template cell_convert<CELL_DEPTH>(std::forward<T>(val)));
    }

    template <int CELL_DEPTH>
    void cell_convert(mxArray* val) {
      operator=(val);
    }

   private:
    const bool valid_;
    mxArray*& mxa_;
  };

  mexOutputArgsTemplate(int nlhs, mxArray* plhs[])
      : mexOutputArgsBase(nlhs, plhs)
  {}

  mexOutputArgsTemplate(const mexOutputArgsBase& src)
      : mexOutputArgsBase(src)
  {}

  // Get an output argument from a specified position
  Data operator[](std::size_t pos) const {
    return Data((arg_pos_ + pos) < nlhs_, plhs_[arg_pos_ + pos]);
  }

  // Get output argument from current argument position
  Data operator*() const {return (*this)[0];}

  // Get number of arguments
  std::size_t size() const {return nlhs_;}

  mxArray** get() const {return plhs_ + arg_pos_;}

  // Arithmetic operators to change the default argument position
  mexOutputArgsTemplate<Converter>& operator++() {
    ++arg_pos_;
    return *this;
  }
  mexOutputArgsTemplate<Converter> operator++(int) {
    mexOutputArgsTemplate<Converter> tmp(*this);
    operator++();
    return tmp;
  }
  mexOutputArgsTemplate<Converter>& operator+=(int val) {
    arg_pos_ += val;
    return *this;
  }

 private:
  std::size_t arg_pos_ = 0;
};

