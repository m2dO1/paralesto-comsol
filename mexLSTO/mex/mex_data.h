/*==============================================================================
mex_data.h is an extension to the existing Matlab C/C++ Matrix API.  It is
primarily focused on providing generic type conversion tools between Matlab
types and arbitrary C types.

It currently adds the following:

  mxConvert() : Function template for converting between Matlab and C types.

  mxCellConvert() : Special implementation of mxConvert() for converting C
  objects to cell arrays.

  mxData : A replacement for const mxArray* types with support for implicit
  type conversions using mxConvert() underneath.

  mxStruct/mxConstStruct : Classes for easier handling of Matlab structures
  including generic type conversions for each structure field data to arbitrary
  C types based on mxConvert().

  mxShape : A class that provides a convenient way to access information about a
  Matlab array size.
  
  mexInputArgs : A class for safe and easy extraction of mex input arguments,
  replacing the need to refer to nrhs and prhs directly.

  mexOutputArgs : A class to replace nlhs and plhs for easier setting of mex
  output arguments.

==============================================================================*/

#include "mex_include.h"
#include "mx_convert.h"
#include "mx_convert_class.h"
#include "mx_cell_convert.h"
#include "mx_element_traits.tcc"
#include "mx_convert_tuple.tcc"
#include "mx_default_converter.tcc"
#include "mx_data.tcc"
#include "mx_struct.tcc"
#include "mx_shape.h"
#include "mex_input_args.tcc"
#include "mex_output_args.h"

