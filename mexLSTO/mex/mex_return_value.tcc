#pragma once
#include "mex_return_value.h"
#include "mex_ref_arg.h"


namespace mex_pack {

// Return address of input unless it is already a pointer
template <typename T, MX_OVERLOAD_IF(is_all_pointer<mx_decay_t<T>>::value)>
auto address_of(T&& v) -> decltype(std::forward<T>(v)) {
  return std::forward<T>(v);
}
template <typename T, MX_OVERLOAD_IF(!std::is_pointer<mx_decay_t<T>>::value)>
auto address_of(T&& v) -> decltype(&std::forward<T>(v)) {
  return &std::forward<T>(v);
}


template <typename T, MX_OVERLOAD_IF_DEF(!std::is_convertible<T, mxArray*>::value &&
                                         (is_pure_data_type_reference<T>::value ||
                                          (is_smart_pointer<T>::value && mx_is_pure_data_type<dereference_all_t<T>>::value)))>
mxArray* ReturnValueConverter::convert(T&& val) {
  if (is_null_pointer(val)) return mxCreateDoubleMatrix(0, 0, mxREAL);
  
  return mxConvert(RefArg(address_of(std::forward<T>(val))));
}

template <typename T, MX_OVERLOAD_IF_DEF(!std::is_convertible<T, mxArray*>::value &&
                                         !(is_pure_data_type_reference<T>::value ||
                                           (is_smart_pointer<T>::value && mx_is_pure_data_type<dereference_all_t<T>>::value)) &&
                                         !mx_class_prop<T>::is_cell)>
mxArray* ReturnValueConverter::convert(T&& val) {
  if (mex_pack::is_null_pointer(val)) return mxCreateDoubleMatrix(0, 0, mxREAL);

  return mxConvert(std::forward<T>(val));
}


template <int CELL_DEPTH, typename T, MX_OVERLOAD_IF_DEF(!std::is_convertible<T, mxArray*>::value &&
                                         (is_pure_data_type_reference<T>::value ||
                                          (is_smart_pointer<T>::value && mx_is_pure_data_type<dereference_all_t<T>>::value)))>
mxArray* ReturnValueConverter::cell_convert(T&& val) {
  if (is_null_pointer(val)) return mxCreateDoubleMatrix(0, 0, mxREAL);
  
  return mxConvert(RefArg(address_of(std::forward<T>(val))));
}

template <int CELL_DEPTH, typename T, MX_OVERLOAD_IF_DEF(!std::is_convertible<T, mxArray*>::value &&
                                         !(is_pure_data_type_reference<T>::value ||
                                           (is_smart_pointer<T>::value && mx_is_pure_data_type<dereference_all_t<T>>::value)) &&
                                         !mx_class_prop<T>::is_cell)>
mxArray* ReturnValueConverter::cell_convert(T&& val) {
  if (mex_pack::is_null_pointer(val)) return mxCreateDoubleMatrix(0, 0, mxREAL);

  return mxCellConvert<CELL_DEPTH>(std::forward<T>(val));
}

} // namespace mex_pack

