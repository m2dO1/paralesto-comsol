#pragma once

#include "mx_convert.h"
#include "mx_convert_tuple.h"
#include "mex_output_args.h"
#include "mex_type_traits.h"

/*
mexReturnValue is similar to mexOutputArgs, but uses a different converter
class, ReturnValueConveter instead of MxDefaultConverter, which allows for
support of pass-by-reference of pure data types in function call return values.
*/

namespace mex_pack {

/*
ReturnValueConverter is a converter class which intercepts pass-by-reference pure
data types and converts them first to a RefArg object before proceeding with the
normal conversion with mxConvert().  This is used to support pass-by-reference
of pure data types in the return value of function calls.
*/

struct ReturnValueConverter {
  // Convert std::tuple and arrays of std::tuple types
  template <typename T, MX_OVERLOAD_IF(mx_class_prop<T>::is_cell)>
  static mxArray* convert(T&& val) {
    return mxConvert<T, ReturnValueConverter>(std::forward<T>(val));
  }

  // Handles pure data type references (pointers, non-const references, smart pointers)
  template <typename T, MX_OVERLOAD_IF(!std::is_convertible<T, mxArray*>::value &&
                                       (is_pure_data_type_reference<T>::value ||
                                        (is_smart_pointer<T>::value && mx_is_pure_data_type<dereference_all_t<T>>::value)))>
  static mxArray* convert(T&& val);

  // Handles all other types
  template <typename T, MX_OVERLOAD_IF(!std::is_convertible<T, mxArray*>::value &&
                                       !(is_pure_data_type_reference<T>::value ||
                                         (is_smart_pointer<T>::value && mx_is_pure_data_type<dereference_all_t<T>>::value)) &&
                                       !mx_class_prop<T>::is_cell)>
  static mxArray* convert(T&& val);

  // Convert std::tuple and arrays of std::tuple types
  template <int CELL_DEPTH, typename T, MX_OVERLOAD_IF(mx_class_prop<T>::is_cell)>
  static mxArray* cell_convert(T&& val) {
    return mxConvert<T, ReturnValueConverter>(std::forward<T>(val));
  }

  // Handles pure data type references (pointers, non-const references, smart pointers)
  template <int CELL_DEPTH, typename T, MX_OVERLOAD_IF(!std::is_convertible<T, mxArray*>::value &&
                                       (is_pure_data_type_reference<T>::value ||
                                        (is_smart_pointer<T>::value && mx_is_pure_data_type<dereference_all_t<T>>::value)))>
  static mxArray* cell_convert(T&& val);

  // Handles all other types
  template <int CELL_DEPTH, typename T, MX_OVERLOAD_IF(!std::is_convertible<T, mxArray*>::value &&
                                       !(is_pure_data_type_reference<T>::value ||
                                         (is_smart_pointer<T>::value && mx_is_pure_data_type<dereference_all_t<T>>::value)) &&
                                       !mx_class_prop<T>::is_cell)>
  static mxArray* cell_convert(T&& val);
};


using mexReturnValue = mexOutputArgsTemplate<ReturnValueConverter>;

} // namespace mex_pack

