#pragma once

#include "mex_include.h"


/*=============================================================================
Description:
Proxy class for cell arrays.  Used by mxCellConvert() and mxConvert().
=============================================================================*/

template <typename C=MxDefaultConverter>
class mx_cell_proxy {
  mxArray* mxa_;
  mwIndex index_ = 0;
  mwIndex index_end_;

 public:
  using Converter = C;

  explicit mx_cell_proxy(mxArray* mxa) : mxa_(mxa) {
    index_end_ = mxGetNumberOfElements(mxa);
  }

  bool valid() const {return index_ != index_end_;}

  mx_cell_proxy& operator=(mxArray* val) {
    mxSetCell(mxa_, index_, val);
    return *this;
  }

  void next() {++index_;}
};

class mx_const_cell_proxy {
  const mxArray* mxa_;
  mwIndex index_ = 0;
  mwIndex index_end_;

 public:
  explicit mx_const_cell_proxy(const mxArray* mxa) : mxa_(mxa) {
    index_end_ = mxGetNumberOfElements(mxa);
  }

  bool valid() const {return index_ != index_end_;}

  void next() {++index_;}

  operator const mxArray*() const {return mxGetCell(mxa_, index_);}
};

