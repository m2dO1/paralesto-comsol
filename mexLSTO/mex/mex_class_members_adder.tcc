#pragma once

#include "mex_class_members_adder.h"
#include "mex_data.h"
#include "mex_class_objects.h"
#include "mex_class_type.h"
#include <iterator>


// Default implementation for extended mexClass definitions which does nothing
inline void AddMexClassMembersExtended(const void*, mex_pack::MembersAdderBase&&) {}


namespace mex_pack {

// An implementation of dynamic_cast for use specifically with downcasting pointers
// If the input type is not polymorphic, then it causes a run-time error
template <typename D, typename B, MX_OVERLOAD_IF(std::is_polymorphic<B>::value)>
D* downcast_pointer(B* ptr) {
  return dynamic_cast<D*>(ptr);
}
template <typename D, typename B, MX_OVERLOAD_IF(!std::is_polymorphic<B>::value)>
D* downcast_pointer(B*) {
  mexErrMsgTxt("Cannot downcast type that is not polymorphic.");
  return nullptr;
}


template <typename T>
MembersAdder<T> MembersAdderBase::operator()(const T* obj) {
  auto members_adder = dynamic_cast<MembersAdder<T>*>(this);
  if (members_adder) {
    // If dynamic_cast worked, then AddMexClassMembers() is being called to construct a new object only
    return *members_adder;
  } else {
    return MembersAdder<T>(obj, *this);
  }
}

template <typename T>
T* MembersAdderBase::nonconst_cast(const T* obj) const {
  if (attr_ && obj) {
    if (!(attr_->is_const_)) {
      return const_cast<T*>(obj);
    }
  }
  return nullptr;
}


template <typename C>
C* MembersAdderParams<C>::nonconst_obj() {
  if (is_const_object()) mexErrMsgTxt("Invalid const_cast during mexClass definition parsing.");
  return const_cast<C*>(obj_);
}


template <typename C, typename Attributes>
template <typename T>
MembersAdder<C>
MembersAdder<C, Attributes, ReadOnlySpecialization>::property(std::string name, T(C::* getter)() const) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data.cell_convert<Attributes::CELL_DEPTH>((p_obj->*getter)());}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename T>
MembersAdder<C>
MembersAdder<C, Attributes, ReadOnlySpecialization>::property(std::string name, T(C::* getter)()) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    // FIXME:  Need to make getter() return value a const type if a pointer or reference
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data.cell_convert<Attributes::CELL_DEPTH>((p_obj->*getter)());}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename T, MX_OVERLOAD_IF_DEF(!std::is_function<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, ReadOnlySpecialization>::property(std::string name, T C::* data_member) {
  if (attr_ && obj_) {
    auto& data_member_ref = obj_->*data_member;
    attr_->template AddPropertyGetter<T>(name, [&](mexGetterValue::Data data) {data.cell_convert<Attributes::CELL_DEPTH>(data_member_ref);}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename T>
MembersAdder<C>
MembersAdder<C, Attributes, ReadOnlySpecialization>::property_ref_(std::string name, T* data_member) {
  if (attr_ && obj_ && data_member) {
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data.cell_convert<Attributes::CELL_DEPTH>(const_cast<const T*>(data_member));}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename T, MX_OVERLOAD_IF_DEF(!std::is_function<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, ReadOnlySpecialization>::property(std::string name, T* static_member) {
  if (attr_) {
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data.cell_convert<Attributes::CELL_DEPTH>(const_cast<const T*>(static_member));}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}


template <typename C, typename Attributes, typename SpecializationAttribute>
MembersAdder<C, Attributes, SpecializationAttribute>::MembersAdder(MembersAdderParams<C>&& p)
    : MembersAdderParams<C>(std::move(p))
{}

template <typename C, typename Attributes, typename SpecializationAttribute>
MembersAdder<C, Attributes, SpecializationAttribute>::MembersAdder(const mexInputArgs* prhs, C** new_obj_ptr)
{
  prhs_ = prhs;
  output_obj_ptr_ = new_obj_ptr;
}

template <typename C, typename Attributes, typename SpecializationAttribute>
MembersAdder<C, Attributes, SpecializationAttribute>::MembersAdder(const std::type_index& input_type_info, const void* input_obj, const C** downcasted_obj_ptr)
{
  type_info_ = &input_type_info;
  downcasted_obj_ptr_ = downcasted_obj_ptr;
  obj_to_downcast_ = input_obj;
}

template <typename C, typename Attributes, typename SpecializationAttribute>
MembersAdder<C, Attributes, SpecializationAttribute>::MembersAdder(ClassMembers* attr)
{
  attr_ = attr;
}

template <typename C, typename Attributes, typename SpecializationAttribute>
MembersAdder<C, Attributes, SpecializationAttribute>::MembersAdder(const C* obj, const MembersAdderBase& params)
    : MembersAdderParams<C>(params)
{
  obj_ = obj;
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <int MIN_ARGS, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::ctor() {
  if (output_obj_ptr_ && prhs_) {
    if (!(*output_obj_ptr_)) {
      *output_obj_ptr_ = CreateFromArgTypes<C, Attributes, MIN_ARGS, Args...>(*prhs_);
    }
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <int MIN_ARGS, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::ctor_method(std::string name) {
  if (attr_) {
    attr_->AddMethod(name, [=](mexReturnValue plhs, mexInputArgs prhs) {
                              auto new_obj_ptr = CreateFromArgTypes<C, Attributes, MIN_ARGS, Args...>(prhs);
                              if (!new_obj_ptr) mexErrMsgTxt((std::string("Invalid number of input arguments.  Expecting ") + std::to_string(sizeof...(Args) - Attributes::N_DEFAULT_ARGS) + std::string(" to ") + std::to_string(Attributes::MAX_ARGS(sizeof...(Args))) + std::string(" input arguments.")).c_str());
                              *plhs = std::unique_ptr<C>(new_obj_ptr);
                           },
                     Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::method(std::string name, R(C::* method)(Args...) const) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddMethod<Attributes>(name, std::function<R(Args...)>([=](Args... args) -> R {return (p_obj->*method)(args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::method(std::string name, void(C::* method)(Args...) const) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddMethod<Attributes>(name, std::function<void(Args...)>([=](Args... args) {(p_obj->*method)(args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::method(std::string name, R(C::* method)(Args...)) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddMethod<Attributes>(name, std::function<R(Args...)>([=](Args... args) -> R {return (p_obj->*method)(args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::method(std::string name, void(C::* method)(Args...)) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddMethod<Attributes>(name, std::function<void(Args...)>([=](Args... args) {(p_obj->*method)(args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::method(std::string name, std::function<R(Args...)> func) {
  if (attr_) attr_->template AddMethod<Attributes>(name, func);
  
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::method(std::string name, R(* func)(Args...)) {
  return method(name, std::function<R(Args...)>(func));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T(C::* getter)() const) {
  return read_only().property(name, getter);
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(is_pure_data_type<T>::value && !((std::is_reference<T>::value || is_all_pointer<T>::value) && !std::is_const<dereference_all_t<T>>::value))>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T(C::* getter)()) {
  return read_only().property(name, getter);
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(is_pure_data_type<T>::value && (std::is_reference<T>::value || is_all_pointer<T>::value) && !std::is_const<dereference_all_t<T>>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T(C::* getter)()) {
  read_only().property(name, getter);

  if (attr_ && obj_ && !is_const_object() && !is_smart_pointer<T>::value) {
    auto* p_obj = nonconst_obj();
    attr_->AddPropertySetter(name, [=](mexInputArgs::Data data) {mxSafeExtract<mx_decay_t<decltype(dereference((p_obj->*getter)()))>>(data, &dereference((p_obj->*getter)()));});
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(is_pure_data_type<T>::value && !std::is_function<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T C::* data_member) {
  read_only().property(name, data_member);

  if (attr_ && obj_ && !is_const_object() && !is_smart_pointer<T>::value) {
    auto& data_member_ref = nonconst_obj()->*data_member;
    attr_->AddPropertySetter(name, [&](mexInputArgs::Data data) {mxSafeExtract<mx_decay_t<decltype(dereference(data_member_ref))>>(data, &dereference(data_member_ref));});
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(!std::is_function<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, const T C::* data_member) {
  return read_only().property(name, data_member);
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, const T* C::* data_member) {
  return read_only().property(name, data_member);
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property_ref_(std::string name, const T* data_member) {
  return read_only().property_ref_(name, data_member);
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(is_pure_data_type<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property_ref_(std::string name, T* data_member) {
  read_only().property_ref_(name, data_member);

  if (attr_ && obj_ && !is_const_object() && data_member) {
    attr_->AddPropertySetter(name, [=](mexInputArgs::Data data) {mxSafeExtract<T>(data, data_member);});
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T1, typename T2>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T1(C::* getter)() const, void(C::* setter)(T2)) {
  read_only().property(name, getter);

  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->AddPropertySetter(name, [=](mexInputArgs::Data data) {(p_obj->*setter)(data);});
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T1, typename T2>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T1(C::* getter)(), void(C::* setter)(T2)) {
  if (!is_const_object()) {
    property(name, getter);

    if (attr_ && obj_ && !is_const_object()) {
      auto* p_obj = nonconst_obj();
      attr_->AddPropertySetter(name, [=](mexInputArgs::Data data) {(p_obj->*setter)(data);});
    }
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(!is_pure_data_type<T>::value && !std::is_function<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T C::* data_member) {
  if (attr_ && obj_) {
    if (is_const_object()) {
      auto& data_member_ref = obj_->*data_member;
      attr_->template AddPropertyGetter<T>(name, [&](mexGetterValue::Data data) {data = data_member_ref;}, Attributes::HIDDEN);
    } else {
      auto& data_member_ref = nonconst_obj()->*data_member;
      attr_->template AddPropertyGetter<T>(name, [&](mexGetterValue::Data data) {data = data_member_ref;}, Attributes::HIDDEN);
    }
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(!is_pure_data_type<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T(C::* getter)()) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data = (p_obj->*getter)();}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(!is_pure_data_type<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property_ref_(std::string name, T* data_member) {
  if (attr_ && obj_ && data_member) {
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data = data_member;}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(is_pure_data_type<T>::value && !std::is_function<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T* static_member) {
  if (attr_) {
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data = static_member;}, Attributes::HIDDEN);
    attr_->AddPropertySetter(name, [=](mexInputArgs::Data data) {mxSafeExtract<T>(data, static_member);});
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T, MX_OVERLOAD_IF_DEF(!is_pure_data_type<T>::value && !std::is_function<T>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, T* static_member) {
  if (attr_) {
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data = static_member;}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::property(std::string name, const T* static_member) {
  if (attr_) {
    attr_->template AddPropertyGetter<T>(name, [=](mexGetterValue::Data data) {data = static_member;}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename BaseClass>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::base_class() {
  static_assert(std::is_base_of<BaseClass, C>::value, "Invalid base_class() specification.");
  if (attr_for_base_) {
    // Add base class attributes
    if (is_const_object()) {
      AddMexClassMembers(static_cast<const BaseClass*>(obj_), MembersAdderBase(attr_for_base_, true));
      AddMexClassMembers(static_cast<const BaseClass*>(obj_), MembersAdderBase(attr_for_base_, false));
      AddMexClassMembersExtended(static_cast<const BaseClass*>(obj_), MembersAdderBase(attr_for_base_, false));
    } else {
      AddMexClassMembers(const_cast<const BaseClass*>(static_cast<BaseClass*>(nonconst_obj())), MembersAdderBase(attr_for_base_, true));
      AddMexClassMembers(const_cast<const BaseClass*>(static_cast<BaseClass*>(nonconst_obj())), MembersAdderBase(attr_for_base_, false));
      AddMexClassMembersExtended(const_cast<const BaseClass*>(static_cast<BaseClass*>(nonconst_obj())), MembersAdderBase(attr_for_base_, false));
    }
  } else if (obj_ && type_info_ && upcasted_obj_ptr_) {
    // Perform upcasting
    if (!(*upcasted_obj_ptr_)) {
      if (is_const_object()) {
        if (*type_info_ == typeid(BaseClass)) {
          *upcasted_obj_ptr_ = static_cast<const BaseClass*>(obj_);
        } else {
          AddMexClassMembers(static_cast<const BaseClass*>(obj_), MembersAdderBase(*type_info_, upcasted_obj_ptr_));
        }
      } else {
        if (*type_info_ == typeid(BaseClass)) {
          *upcasted_obj_ptr_ = static_cast<BaseClass*>(nonconst_obj());
        } else {
          AddMexClassMembers(const_cast<const BaseClass*>(static_cast<BaseClass*>(nonconst_obj())), MembersAdderBase(*type_info_, upcasted_obj_ptr_));
        }
      }
    }
  } else if (obj_to_downcast_ && type_info_ && downcasted_obj_ptr_) {
    // Perform downcasting
    if (!(*downcasted_obj_ptr_)) {
      if (is_const_object()) {
        if (*type_info_ == typeid(BaseClass)) {
          *downcasted_obj_ptr_ = downcast_pointer<const C>(static_cast<const BaseClass*>(obj_to_downcast_));
        } else {
          const BaseClass* base_obj_ptr;
          AddMexClassMembers(static_cast<const BaseClass*>(nullptr), MembersAdder<BaseClass>(*type_info_, obj_to_downcast_, &base_obj_ptr));
          if (!base_obj_ptr) {
            *downcasted_obj_ptr_ = downcast_pointer<const C>(base_obj_ptr);
          }
        }
      } else {
        if (*type_info_ == typeid(BaseClass)) {
          *downcasted_obj_ptr_ = downcast_pointer<C>(static_cast<BaseClass*>(const_cast<void*>(obj_to_downcast_)));
        } else {
          const BaseClass* base_obj_ptr;
          AddMexClassMembers(static_cast<const BaseClass*>(nullptr), MembersAdder<BaseClass>(*type_info_, obj_to_downcast_, &base_obj_ptr));
          if (!base_obj_ptr) {
            *downcasted_obj_ptr_ = downcast_pointer<C>(const_cast<BaseClass*>(base_obj_ptr));
          }
        }
      }
    }
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename Type,  MX_OVERLOAD_IF_DEF(!is_pure_data_type<Type>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, SpecializationAttribute>::type_member(std::string name) {
  if (attr_) {
    using DecayedType = mx_decay_t<dereference_t<Type>>;
    attr_->template AddTypeMember<DecayedType>(name, [=](mexGetterValue::Data data) {data = Objects::GetMexClassParams(Objects::Add(new ClassType<DecayedType>));}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes, typename SpecializationAttribute>
template <typename T>
void MembersAdder<C, Attributes, SpecializationAttribute>::package_(std::string name) {
  if (attr_) {
    attr_->template AddTypeMember<T>(name, [=](mex_pack::mexGetterValue::Data data) {data = mex_pack::Objects::GetMexClassParams(mex_pack::Objects::Add(new T()));});
  }
}

template <typename C, typename Attributes>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, std::function<R(const C&, Args...)> func) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddMethod<Attributes>(name, std::function<R(Args...)>([=](Args... args) -> R {return func(*p_obj, args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, std::function<void(const C&, Args...)> func) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddMethod<Attributes>(name, std::function<void(Args...)>([=](Args... args) {func(*p_obj, args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, R(* func)(const C&, Args...)) {
  return method(name, std::function<R(const C&, Args...)>(func));
}

template <typename C, typename Attributes>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, std::function<R(C&, Args...)> func) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddMethod<Attributes>(name, std::function<R(Args...)>([=](Args... args) -> R {return func(*const_cast<C*>(p_obj), args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, std::function<void(C&, Args...)> func) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddMethod<Attributes>(name, std::function<void(Args...)>([=](Args... args) {func(*const_cast<C*>(p_obj), args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, R(* func)(C&, Args...)) {
  return method(name, std::function<R(C&, Args...)>(func));
}

template <typename C, typename Attributes>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, std::function<R(const C*, Args...)> func) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddMethod<Attributes>(name, std::function<R(Args...)>([=](Args... args) -> R {return func(*p_obj, args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, std::function<void(const C*, Args...)> func) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddMethod<Attributes>(name, std::function<void(Args...)>([=](Args... args) {func(*p_obj, args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, R(* func)(const C*, Args...)) {
  return method(name, std::function<R(const C*, Args...)>(func));
}

template <typename C, typename Attributes>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, std::function<R(C*, Args...)> func) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddMethod<Attributes>(name, std::function<R(Args...)>([=](Args... args) -> R {return func(const_cast<C*>(p_obj), args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, std::function<void(C*, Args...)> func) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddMethod<Attributes>(name, std::function<void(Args...)>([=](Args... args) {func(const_cast<C*>(p_obj), args...);}));
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename R, typename... Args>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::method(std::string name, R(* func)(C*, Args...)) {
  return method(name, std::function<R(C*, Args...)>(func));
}

template <typename C, typename Attributes>
template <typename R, MX_OVERLOAD_IF_DEF(is_pure_data_type<R>::value && !((std::is_reference<R>::value || is_all_pointer<R>::value) && !std::is_const<dereference_all_t<R>>::value))>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::property(std::string name, std::function<R(const C&)> func) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddPropertyGetter<R>(name, [=](mexGetterValue::Data data) {data.cell_convert<Attributes::CELL_DEPTH>(func(*p_obj));}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename R, MX_OVERLOAD_IF_DEF(is_pure_data_type<R>::value && (std::is_reference<R>::value || is_all_pointer<R>::value) && !std::is_const<dereference_all_t<R>>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::property(std::string name, std::function<R(C&)> func) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddPropertyGetter<R>(name, [=](mexGetterValue::Data data) {data.cell_convert<Attributes::CELL_DEPTH>(func(*p_obj));}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename R>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::property(std::string name, std::function<R(const C&)> getter_func, std::function<void(C&, R)> setter_func) {
  property(name, getter_func);
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->AddPropertySetter(name, [=](mexInputArgs::Data data) {setter_func(*p_obj, data);});
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename R, MX_OVERLOAD_IF_DEF(!is_pure_data_type<R>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::property(std::string name, std::function<R(const C&)> func) {
  if (attr_ && obj_) {
    auto* p_obj = obj_;
    attr_->template AddPropertyGetter<R>(name, [=](mexGetterValue::Data data) {data = func(*p_obj);}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

template <typename C, typename Attributes>
template <typename R, MX_OVERLOAD_IF_DEF(!is_pure_data_type<R>::value)>
MembersAdder<C>
MembersAdder<C, Attributes, BindInstanceSpecialization>::property(std::string name, std::function<R(C&)> func) {
  if (attr_ && obj_ && !is_const_object()) {
    auto* p_obj = nonconst_obj();
    attr_->template AddPropertyGetter<R>(name, [=](mexGetterValue::Data data) {data = func(*p_obj);}, Attributes::HIDDEN);
  }
  return MembersAdder<C>(std::move(*this));
}

} // namespace mex_pack

