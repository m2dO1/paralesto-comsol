#pragma once

#include "mx_element_traits.h"
#include "mx_convert_tuple.tcc"
#include "mx_struct_field_adder.h"


template <typename T>
template <typename C_iter>
void mx_element_traits<T, MX_USE_IF(is_mx_struct_type<T>::value)>::convert_to_c(const mxConstStruct& mx_proxy, C_iter c_obj) {
  ConvertMxStruct(&(*c_obj), mx_type_match<T>{}, MxStructFieldAdderBase(true, &mx_proxy, nullptr));
}

template <typename T>
void mx_element_traits<T, MX_USE_IF(is_mx_struct_type<T>::value)>::convert_to_matlab(const T& c_obj, mxStruct& mx_proxy) {
  ConvertMxStruct(const_cast<T*>(&c_obj), mx_type_match<T>{}, MxStructFieldAdderBase(false, nullptr, &mx_proxy));
}


template <typename C_iter>
void mx_element_traits<std::string, void>::convert_to_c(const mx_const_cell_proxy& mx_proxy, C_iter c_obj) {
  auto& c_obj_ref = *c_obj;
  mxExtract(static_cast<const mxArray*>(mx_proxy), &c_obj_ref);
}

template <typename U>
void mx_element_traits<std::string, void>::convert_to_matlab(const std::string& c_obj, U& mx_proxy) {
  mx_proxy = mxCreateString(c_obj.c_str());
}

template <typename U>
void mx_element_traits<const char*, void>::convert_to_matlab(const char* c_obj, U& mx_proxy) {
  mx_proxy = mxCreateString(c_obj);
}

template <typename ... Types>
template <typename T, typename U>
void mx_element_traits<std::tuple<Types...>, void>::convert_to_matlab(T&& c_obj, U& mx_proxy) {
  mx_proxy = mxConvertTuple<typename U::Converter>(std::forward<T>(c_obj));
}

template <typename T1, typename T2>
template <typename T, typename U>
void mx_element_traits<std::pair<T1, T2>, void>::convert_to_matlab(T&& c_obj, U& mx_proxy) {
  mx_proxy = mxConvertPair<typename U::Converter>(std::forward<T>(c_obj));
}

