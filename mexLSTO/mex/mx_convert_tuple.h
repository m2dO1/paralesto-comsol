#pragma once

#include "mx_default_converter.h"

/*
mxConvertTuple handles the conversion of std::tuple types to cell arrays in
Matlab.  It can handle nested std::tuple's, and the tuple elements can be any
pure data type of mexClass type.  It is a passed a converter class that it uses
to handle the underlying conversions of tuple elements to mxArray* types.
*/

template <std::size_t I, typename Converter>
struct MxConvertTupleElements {
  template <typename T>
  static void convert(mxArray* pm, T&& c_obj);
};

// Specialization for the 0'th element
template <typename Converter>
struct MxConvertTupleElements<0, Converter> {
  template <typename T>
  static void convert(mxArray* pm, T&& c_obj);
};

template <typename Converter=MxDefaultConverter, typename T>
mxArray* mxConvertTuple(T&& c_obj);

// Add support for std::pair
template <typename Converter=MxDefaultConverter, typename T>
mxArray* mxConvertPair(T&& c_obj);

