#pragma once


// forward declarations
template <typename Converter> class mexOutputArgsTemplate;

struct MxDefaultConverter;


// Define mexOutputArgs
using mexOutputArgs = mexOutputArgsTemplate<MxDefaultConverter>;

