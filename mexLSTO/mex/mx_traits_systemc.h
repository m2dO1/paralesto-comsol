#pragma once

// Add support for SystemC types
// Still work in progress as there are more SC data types to be supported in the future.

#include "mx_traits.h"
#include "matrix.h"
#include "mex.h"


template <typename T>
struct mx_element_traits<sc_signal<T>, void> : public mx_element_traits<T, void> {
  template <typename U, typename C_iter>
  static void convert_to_c(const U& mx_proxy, C_iter c_obj) {
    T c_data;
    mx_element_traits<T, void>::convert_to_c(mx_proxy, &c_data);
    c_obj->write(c_data);
  }

  template <typename U>
  static void convert_to_matlab(const sc_signal<T>& c_obj, U& mx_proxy) {
    mx_element_traits<T, void>::convert_to_matlab(c_obj.read(), mx_proxy);
  }
};

template <typename T>
struct mx_copy_traits<sc_signal<T>, void> {
  static void copy(const T& src, sc_signal<T>& dst) {
    dst = src;
  }
};

template <int W>
struct mx_element_traits<sc_int<W>, void> : public mx_element_traits_real<sc_int<W> > {
};

template <int W>
struct mx_element_traits<sc_uint<W>, void> : public mx_element_traits_real<sc_uint<W> > {
};


// Map sc_bigint/sc_biguint to an array of bits in Matlab
template <typename T>
struct mx_array_traits_sc_bigint {
  using value_type = double;

  static std::vector<std::size_t> size(const T& a) {return {std::size_t(a.length())};}

  struct read_iterator {
    read_iterator(const T& a) : a_(a) {}

    const T& a_;
    unsigned index_ = 0;

    double operator*() const {
      mxAssert(index_ < a_.length(), "Matlab array length less than sc_bigint size.");
      return a_[index_];
    }

    read_iterator& operator++() {
      ++index_;
      return *this;
    }
  };

  struct write_iterator {
    write_iterator(T& a) : a_(a) {}

    T& a_;
    unsigned index_ = 0;

    auto operator*() -> decltype(a_[index_]) {
      mxAssert(index_ < a_.length(), "Matlab array length less than sc_bigint size.");
      return a_[index_];
    }

    write_iterator& operator++() {
      ++index_;
      return *this;
    };
  };

  static read_iterator begin(const T& a) {return read_iterator(a);}

  static write_iterator begin(T& a) {return write_iterator(a);}
};

template <int W>
struct mx_array_traits<sc_bigint<W>, void>
    : mx_array_traits_sc_bigint<sc_bigint<W>>
{};

template <int W>
struct mx_array_traits<sc_biguint<W>, void>
    : mx_array_traits_sc_bigint<sc_biguint<W>>
{};


// Adds support for converting Matlab strings to sc_module_name
template <typename T, MX_OVERLOAD_IF(std::is_same<T, sc_module_name>::value)>
T mxExtract(const mxArray* pm) {
  if (mxIsChar(pm)) {
    auto* c_str = mxArrayToString(pm);
    assert(c_str);
    T c_obj(c_str);
    mxFree(c_str);
    return c_obj;
  } else {
    mexErrMsgTxt("Unsupported mxArray type.");
    return T("");
  }
}

