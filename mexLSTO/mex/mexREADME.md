# mexPackage
Seamless integration of C/C++ into Matlab.

## Official Location
https://github.qualcomm.com/brianwon/mexPackage

## Introduction
The mexPackage library provides a framework for exposing C++ classes and functions directly into Matlab, easily and seamlessly, such that they behave like native Matlab classes and functions.  Almost all attributes of a C++ class can be exposed to Matlab, including:
- constructors
- base classes
- data members
- methods
- static members and methods
- type members

Exposing C++ classes does not involve writing any Matlab or C++ wrapping code.  Instead the user only needs to write an interface description that describes the class attributes it wishes to expose, and any functions it also wishes to expose.  This interface description is actual C++ code that is embedded alongside the existing C++ code.

In Matlab, the C++ classes and functions appear and behave like native Matlab classes and functions.  C++ classes can be instantiated to create C++ objects in Matlab, allowing access to its properties and methods.   C++ functions can be like any other Matlab function, and type conversions of the input and output arguments are handled automatically.

The custom C++ classes that the user exposes to Matlab appear as custom Matlab classes, but mexPackage also supports the automatic conversion of many C++ data structures into Matlab built in data types.  Multidimensional arrays using STL containers and other array structures are automatically handled.  mexPackage is also extensible, allowing the user to add additional custom C++ data structures that can be converted to Matlab built in data.

mexPackage is not a language parser (like SWIG for example).  It is more akin to the BoostPython library which mexPackage borrows many concepts from.  Ultimately, it is a library of C++ and Matlab files that is built upon the Matlab mex library framework.  Users still compile the C++ code to be exposed in Matlab as a normal mex library, but mexPackage assumes complete control over the mex interface on both the Matlab and C++ side.

