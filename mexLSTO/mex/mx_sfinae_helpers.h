#pragma once

#include <type_traits>


// If C++17, use std::void_t instead
template<typename...> struct mx_make_void {using type = void;};
template<typename... Ts> using mx_void_t = typename mx_make_void<Ts...>::type;

// A version of std::decay that doesn't decay array types as we typically want that type preserved.
// If C++20, use std::remove_cvref_t instead
template <typename T>
using mx_decay_t = typename std::remove_cv<typename std::remove_reference<T>::type>::type;

// If T is a pass-by-value type, i.e. is not a pointer or a non-const reference,
// then return the underlying type, otherwise leave it alone.
template <typename T>
using mx_value_t = typename std::conditional<std::is_const<typename std::remove_reference<T>::type>::value,
                                             mx_decay_t<T>,
                                             typename std::remove_volatile<T>::type>::type;

// A convenience macro for using SFINAE-based template specialization
#define MX_USE_IF(...) typename std::enable_if<(__VA_ARGS__)>::type

// A convenience macro for using SFINAE-based function overloading
#define MX_OVERLOAD_IF(...) typename std::enable_if<(__VA_ARGS__)>::type* = nullptr
#define MX_OVERLOAD_IF_DEF(...) typename std::enable_if<(__VA_ARGS__)>::type*

// This can be used as a function argument tag to ensure that a function call
// matches the expected type exactly, and not from a derived type.
template <typename T>
struct mx_type_match {};

