#include "mex_class_objects.h"
#include "mex_package_class.h"
#include "mex_data.h"
#include "mx_array_persistent_ptr.h"
#include "mex_class_instance.h"
#include "mex.h"


using mex_pack::Objects;

unsigned package_handle;
bool package_handle_initialized = false;


void GetPackageClass(mexOutputArgs& plhs, mexInputArgs&) {
  // package_handle should only be initialized once after the mex library loads
  if (!package_handle_initialized) {
    package_handle = Objects::Add(new MainMexPackage);
    package_handle_initialized = true;
  }
  *plhs = Objects::GetMexClassParams(package_handle);
}

void CreateMexClassObject(mexOutputArgs& plhs, mexInputArgs& prhs) {
  unsigned type_handle = *prhs++;
  auto& type_obj = Objects::Get(type_handle);
  auto object_handle = Objects::Add(type_obj.CreateNewObject(prhs));
  *plhs = Objects::GetMexClassParams(object_handle);
}

void FreeMexClassObject(mexOutputArgs&, mexInputArgs& prhs) {
  unsigned object_handle = *prhs++;
  // Only allow package handle to be deleted when the mex library unloads
  if (object_handle != package_handle) {
    unsigned long long unique_id = *prhs;
    Objects::Remove(object_handle, unique_id);
  }
}

void GetProperty(mexOutputArgs& plhs, mexInputArgs& prhs) {
  unsigned object_handle = *prhs++;
  auto& obj = Objects::Get(object_handle);
  std::string member = *prhs++;

  mex_pack::mexGetterValue getter_value(plhs);
  if (!obj.GetProperty(member, *getter_value)) {
    mexErrMsgTxt("mexClass property is not readable.");
  }
}

void SetProperty(mexOutputArgs& plhs, mexInputArgs& prhs) {
  unsigned object_handle = *prhs++;
  auto& obj = Objects::Get(object_handle);
  std::string member = *prhs++;

  if (obj.SetProperty(member, *prhs)) {
    *plhs = 0;    // This makes mexClass's subsref() happy
  } else {
    mexErrMsgTxt("mexClass property is not settable.");
  }
}

void CallMethod(mexOutputArgs& plhs, mexInputArgs& prhs) {
  unsigned object_handle = *prhs++;
  auto& obj = Objects::Get(object_handle);
  std::string member = *prhs++;

  auto success = obj.CallMethod(member, plhs, prhs);
  if (!success) {
    mexErrMsgTxt("mexClass method is not accessible.");
  }
}

void GetObjectType(mexOutputArgs& plhs, mexInputArgs& prhs) {
  unsigned object_handle = *prhs++;
  auto& obj = Objects::Get(object_handle);
  auto type_handle = Objects::Add(obj.GetType());
  *plhs = Objects::GetMexClassParams(type_handle);
}


#define DEFINE_COMMANDS(use_macro) \
  use_macro( GetPackageClass ) \
  use_macro( CreateMexClassObject ) \
  use_macro( FreeMexClassObject ) \
  use_macro( GetProperty ) \
  use_macro( SetProperty ) \
  use_macro( CallMethod ) \
  use_macro( GetObjectType )

#define COMMA_DELIMIT(x) x,
#define ADD_STRUCT_FIELD(x) commands[#x] = command_index++;


// Define array of function pointers representing each command
void (* CommandFunctions[])(mexOutputArgs&, mexInputArgs&) = {
  DEFINE_COMMANDS(COMMA_DELIMIT)
};


void mexFunction(int nlhs, mxArray* plhs_[], int nrhs, const mxArray* prhs_[]) {
  if (nrhs == 0) {
    // No input arguments means return a Matlab struct of all available commands
    static mxArrayPersistentPtr p_commands;

    if (!p_commands) {
      mxStruct commands;
      unsigned command_index = 0;
      DEFINE_COMMANDS(ADD_STRUCT_FIELD)
      p_commands.reset(static_cast<mxArray*>(commands));
    }
    plhs_[0] = p_commands.copy();

  } else {
    mexInputArgs prhs(nrhs, prhs_);
    mexOutputArgs plhs(nlhs, plhs_);

    auto command_index = unsigned(*prhs++);

    if (command_index > std::extent<decltype(CommandFunctions)>::value) {
      mexErrMsgTxt((std::string("Invalid command: ") + std::to_string(command_index)).c_str());
    }

    CommandFunctions[command_index](plhs, prhs);
  }
}

