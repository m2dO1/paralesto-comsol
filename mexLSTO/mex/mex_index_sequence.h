#pragma once

#include <utility>


namespace mex_pack {

#ifdef __cpp_lib_integer_sequence

template <std::size_t... I>
using mex_index_sequence = std::index_sequence<I...>;

template <std::size_t N>
using mex_make_index_sequence = std::make_index_sequence<N>;

#else
// For C++11, define local implementation for std::index_sequence & std::make_index_sequence

// The following implementation was lifted from Stack Overflow
// https://stackoverflow.com/questions/17424477/implementation-c14-make-integer-sequence

template <size_t... Ints>
struct mex_index_sequence
{
    using type = mex_index_sequence;
    using value_type = size_t;
    static constexpr std::size_t size() noexcept { return sizeof...(Ints); }
};

// --------------------------------------------------------------

template <class Sequence1, class Sequence2>
struct _merge_and_renumber;

template <size_t... I1, size_t... I2>
struct _merge_and_renumber<mex_index_sequence<I1...>, mex_index_sequence<I2...>>
  : mex_index_sequence<I1..., (sizeof...(I1)+I2)...>
{ };

// --------------------------------------------------------------

template <size_t N>
struct mex_make_index_sequence
  : _merge_and_renumber<typename mex_make_index_sequence<N/2>::type,
                        typename mex_make_index_sequence<N - N/2>::type>
{ };

template<> struct mex_make_index_sequence<0> : mex_index_sequence<> { };
template<> struct mex_make_index_sequence<1> : mex_index_sequence<0> { };

#endif

} // namespace mex_pack

