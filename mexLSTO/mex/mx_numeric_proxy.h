#pragma once

struct mxArray_tag;
using mxArray = mxArray_tag;

/*=============================================================================
Description:
The following proxy classes provide access to the Matlab array elements for
numerical and logical types contained in the mxArray object.  Structure elements
use mxConstStruct and mxStruct as the element proxy types.  These proxy objects
provide methods, real() and imag() for reading and writing the numerical values
in the mxArray object.
All proxy classes provide a way to increment the current array element location
to the next one with the next() method.
=============================================================================*/

template <typename Traits>
class mx_numeric_proxy {
  using real_t = typename Traits::real_type;
  using complex_t = typename Traits::complex_type;

 public:
  // Not meant to be called directly.  Use mx_make_numeric_proxy() function
  // instead so that the proper traits class can be passed in.
  explicit mx_numeric_proxy(const mxArray* pm);

  bool valid() const;

  void next();

  auto real() const -> real_t;
  auto imag() const -> real_t;

  template <typename U> void real(U value);
  template <typename U> void imag(U value);

 private:
  bool is_complex_ = false;

  real_t*    p_real_ = nullptr;
  real_t*    p_end_real_ = nullptr;
  // For some reason this #if check isn't working.  Might be a mex script issue.
//#if MX_HAS_INTERLEAVED_COMPLEX
  complex_t* p_complex_ = nullptr;
  complex_t* p_end_complex_ = nullptr;
//#else
  real_t*    p_imag_ = nullptr;
//#endif
};

