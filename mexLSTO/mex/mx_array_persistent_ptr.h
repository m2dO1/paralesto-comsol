#pragma once

#include "mex_include.h"


// Wrapper for a persistent mxArray* (preserves itself between mexFunction()
// calls), and automatically handles clean up when the mex library unloads.

class mxArrayPersistentPtr {
  mxArray* pm_ = nullptr;

 public:
  mxArrayPersistentPtr() = default;

  mxArrayPersistentPtr(mxArray* pm) : pm_(pm) {
    if (pm_ != nullptr) mexMakeArrayPersistent(pm_);
  }

  // const mxArray* usually come from prhs, and they must be copied
  mxArrayPersistentPtr(const mxArray* pm) {
    if (pm != nullptr) {
      pm_ = mxDuplicateArray(pm);
      mexMakeArrayPersistent(pm_);
    }
  }

  ~mxArrayPersistentPtr() {
    if (pm_ != nullptr) mxDestroyArray(pm_);
  }

  void reset(mxArray* pm) {
    if (pm_ != nullptr) mxDestroyArray(pm_);
    pm_ = pm;
    if (pm_ != nullptr) mexMakeArrayPersistent(pm_);
  }

  void reset(const mxArray* pm) {
    if (pm_ != nullptr) mxDestroyArray(pm_);
    if (pm != nullptr) {
      pm_ = mxDuplicateArray(pm);
      mexMakeArrayPersistent(pm_);
    } else {
      pm_ = nullptr;
    }
  }

  operator bool() const {return (pm_ != nullptr);}

  // If assigning to phls, must use a copy.
  mxArray* copy() const {return mxDuplicateArray(pm_);}

  operator mxArray*() {return pm_;}

  operator const mxArray*() const {return pm_;}
};

