#pragma once

#include "mx_convert.h"
#include "mx_traits.h"
#include "mx_cell_proxy.h"
#include <algorithm>
#include <iterator>
#include <vector>


// This is a template specialization of mx_c_traits that is only used by
// mxCellConvert().
// Once the nesting count reaches 0, call mxConvert() to assign the cell array
// element value.
template <typename T>
struct mx_c_traits<T, 0, void> {
  // Dummy placeholder values
  static constexpr bool is_complex = false;
  static constexpr mxClassID_ elem_class_id = mxClassID_::mxDOUBLE_CLASS;

  static void shape(const T&, std::vector<mwSize>* s, bool first_dim=true) {
    if (first_dim) s->push_back(1);
  }

  template <typename U>
  static void mx_read_from_c(U&& c_obj, mx_cell_proxy<>& mx_elem_proxy) {
    assert(mx_elem_proxy.valid());  // Check if Matlab array is large enough to convert C data
    mx_elem_proxy = mxConvert(c_obj);
    mx_elem_proxy.next();
  }
};


/*=============================================================================
Function:  mxCellConvert()

Description:
------------
mxCellConvert() is a special implementation of mxConvert() that converts C
objects into Matlab cell arrays.  This is useful when the C objects contains
nested arrays of irregular size, which is cannot be handled by normal Matlab
numerical arrays.

Usage:
------
The template parameter NESTING_DEPTH specifies how many levels of nested arrays
there are in the C object before it reaches cell array element type.  Setting
NESTING_DEPTH = 0 means a single element cell array will be created.  Everything
else works the same as a normal mxConvert() call when converting from C to
Matlab.

Example:
--------
Suppose x is an object of type std::array<std::vector<std::deque<double>>>,
where the inner most array of type std::deque<double> can have different sizes,
but the std::vector and std::array have fixed sizes.  Then NESTING_DEPTH = 2
since that covers the outer two fixed size arrays.  One can then convert this to
a 2 dimensional cell array of 1D arrays of doubles by doing this:

  plhs[0] = mxCellConvert<2>(x);

Notes
--------
Input is a forwarding reference, to allow for const and non-const references of
mexClass array types, which is necessary to expose the mexClass object as
read-only or not.  Rvalue inputs are not considered as this information is
discarded once the element is accessed, which is always by lvalue reference.

=============================================================================*/

template <std::size_t NESTING_DEPTH, typename T, MX_OVERLOAD_IF(NESTING_DEPTH > 0)>
mxArray* mxCellConvert(T&& c_obj) {
  // Reverse the C dimension order since Matlab uses a different storage order
  std::vector<mwSize> dims;
  mx_c_traits<T, NESTING_DEPTH>::shape(c_obj, &dims);
  std::reverse(dims.begin(), dims.end());

  auto* pm = mxCreateCellArray(dims.size(), dims.data());

  mx_cell_proxy<> mx_elem_proxy(pm);

  mx_c_traits<mx_decay_t<T>, NESTING_DEPTH>::mx_read_from_c(c_obj, mx_elem_proxy);

  return pm;
}

// Bypass if NESTING_DEPTH == 0
template <std::size_t NESTING_DEPTH, typename T, MX_OVERLOAD_IF(NESTING_DEPTH == 0)>
mxArray* mxCellConvert(T&& c_obj) {
  return mxConvert(std::forward<T>(c_obj));
}

