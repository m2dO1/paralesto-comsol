#pragma once
#include "mex_ref_arg.h"


namespace mex_pack {

template <typename T>
mxArray* RefHolder<T>::convert() const {
  return mxConvert(data_ref_);
}

template <typename T>
void RefHolder<T>::set(const mxArray* pm) {
  data_ref_ = mxExtract<T>(pm);
}


template <typename T>
mxArray* ConstRefHolder<T>::convert() const {
  return mxConvert(data_ref_);
}

template <typename T>
void ConstRefHolder<T>::set(const mxArray*) {
  mexErrMsgTxt("Cannot set const reference.");
}


template <typename T>
DataHolder<T>::DataHolder(const mxArray* pm)
    : RefHolder<T>(data_)
    , data_(mxExtract<T>(pm))
{}

template <typename T>
mxArray* SmartPtrConstHolder<T>::convert() const {
  return mxConvert(*smart_ptr_);
}

template <typename T>
void SmartPtrConstHolder<T>::set(const mxArray*) {
  mexErrMsgTxt("Cannot set const reference.");
}

template <typename T>
void SmartPtrHolder<T>::set(const mxArray* pm) {
  *SmartPtrConstHolder<T>::smart_ptr_ = mxExtract<typename T::element_type>(pm);
}

ClassMembers* RefArg::GetTypeObject() {
  return new ClassType<RefArg>();
}

template <typename T, MX_OVERLOAD_IF_DEF(!std::is_const<T>::value)>
T& RefArg::extract() {
  if (is_const_) mexErrMsgTxt("Cannot convert const reference to non-const reference.");

  if (!ref_data) {
    if (pm_)
      ref_data.reset(new DataHolder<T>(pm_));
    else
      ref_data.reset(new DataHolder<T>());
  }
  auto data_ptr = dynamic_cast<RefHolder<T>*>(ref_data.get());
  if (!data_ptr) mexErrMsgTxt("mexRefArg type doesn't match.");
  return data_ptr->get();
}

// Get const reference data
template <typename T, MX_OVERLOAD_IF_DEF(std::is_const<T>::value)>
T& RefArg::extract() const {
  using T_nonconst = typename std::remove_const<T>::type;

  if (!ref_data) {
    if (pm_)
      ref_data.reset(new DataHolder<T_nonconst>(pm_));
    else
      ref_data.reset(new DataHolder<T_nonconst>());
  }
  if (is_const_) {
    auto data_ptr = dynamic_cast<ConstRefHolder<T_nonconst>*>(ref_data.get());
    if (!data_ptr) mexErrMsgTxt("mexRefArg type doesn't match.");
    return data_ptr->get();
  } else {
    auto data_ptr = dynamic_cast<RefHolder<T_nonconst>*>(ref_data.get());
    if (!data_ptr) mexErrMsgTxt("mexRefArg type doesn't match.");
    return data_ptr->get();
  }
}

} // namespace mex_pack

