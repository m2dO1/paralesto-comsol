#include "mex_input_args.h"


mexInputArgs::mexInputArgs(int nrhs, const mxArray* prhs[]) : nrhs_(nrhs), prhs_(prhs) {}

// Get an input argument from a specified position
mexInputArgs::Data mexInputArgs::operator[](std::size_t pos) const {
  if (pos < nrhs_) {
    return Data(prhs_[arg_pos_ + pos]);
  } else {
    return Data();
  }
}

// Arithmetic operators to change the default argument position
mexInputArgs& mexInputArgs::operator++() {
  ++arg_pos_;
  --nrhs_;
  return *this;
}
mexInputArgs mexInputArgs::operator++(int) {
  mexInputArgs tmp(*this);
  operator++();
  return tmp;
}
mexInputArgs& mexInputArgs::operator+=(int val) {
  arg_pos_ += val;
  nrhs_ -= val;
  return *this;
}

