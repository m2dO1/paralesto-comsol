#pragma once

#include "mex_class_members.h"
#include <ctime>
#include <deque>
#include <memory>
#include <string>
#include <iostream>


namespace mex_pack {

// Manages the pool of mexClass objects
class Objects {
  static std::deque<std::unique_ptr<ClassMembers>> handles_;
  static std::deque<unsigned> deleted_handles_;

  class UniqueID {
   private:
    const unsigned long long unique_id_;

   public:
    UniqueID() : unique_id_(static_cast<unsigned long long>(std::time(nullptr))) {}

    unsigned long long value() const {return unique_id_;}
  };

  // Generate a unique ID when the mex library loads.  This will be used to
  // ignore calls to DESTROY_MEX_CLASS if the ID does not match, which can
  // happen if the mex library was unloaded before the Matlab mexClass
  // objects, making their object handles stale.
  static const UniqueID unique_id_;

  static bool CheckValid(unsigned handle, bool just_report=false);

 public:
  static unsigned Add(ClassMembers* obj) ;

  static void Remove(unsigned handle, unsigned long long unique_id=0);

  static ClassMembers& Get(unsigned handle) {
    CheckValid(handle);
    return *handles_[handle];
  }

  // Returns a cell array that contains all the properties of the mexClass object so that it can be created in Matlab
  static mxArray* GetMexClassParams(unsigned handle, std::string matlab_class="mexClass");
};

} // namespace mex_pack

