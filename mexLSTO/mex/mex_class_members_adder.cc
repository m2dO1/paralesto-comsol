#include "mex_class_members_adder.h"


namespace mex_pack {

bool MembersAdderBase::is_const_object() const {
  if (attr_)
    return attr_->is_const_;
  else
    return false;
}

} // namespace mex_pack

