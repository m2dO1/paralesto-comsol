#pragma once

#include "mx_numeric_proxy.h"
#include "mx_class_id.h"
#include "mex_include.h"
#include <type_traits>
#include <utility>


#if MX_HAS_INTERLEAVED_COMPLEX

template <mxClassID_ ID>
struct mx_numeric_proxy_traits;

template <>
struct mx_numeric_proxy_traits<mxClassID_::mxLOGICAL_CLASS> {
  using real_type = mxLogical;

  struct complex_type {
    mxLogical real, imag;
  };

  static mxLogical* get_real_data(const mxArray* pm) {return mxGetLogicals(pm);}

  static complex_type* get_complex_data(const mxArray*) {
    static complex_type dummy{};
    return &dummy;
  }
};

#define MX_NUMERIC_PROXY_TRAITS(class_id, get_real_func, get_complex_func) \
  template <> \
  struct mx_numeric_proxy_traits<class_id> { \
    using real_type = typename std::remove_pointer<decltype(get_real_func(std::declval<const mxArray*>()))>::type; \
    using complex_type = typename std::remove_pointer<decltype(get_complex_func(std::declval<const mxArray*>()))>::type; \
    static real_type* get_real_data(const mxArray* pm) {return get_real_func(pm);} \
    static complex_type* get_complex_data(const mxArray* pm) {return get_complex_func(pm);} \
  };

MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxDOUBLE_CLASS, mxGetDoubles, mxGetComplexDoubles)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxSINGLE_CLASS, mxGetSingles, mxGetComplexSingles)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxINT8_CLASS,   mxGetInt8s, mxGetComplexInt8s)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxUINT8_CLASS,  mxGetUint8s, mxGetComplexUint8s)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxINT16_CLASS,  mxGetInt16s, mxGetComplexInt16s)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxUINT16_CLASS, mxGetUint16s, mxGetComplexUint16s)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxINT32_CLASS,  mxGetInt32s, mxGetComplexInt32s)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxUINT32_CLASS, mxGetUint32s, mxGetComplexUint32s)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxINT64_CLASS,  mxGetInt64s, mxGetComplexInt64s)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxUINT64_CLASS, mxGetUint64s, mxGetComplexUint64s)

#undef MX_NUMERIC_PROXY_TRAITS


template <typename Traits>
mx_numeric_proxy<Traits>::mx_numeric_proxy(const mxArray* pm)
    : is_complex_(mxIsComplex(pm))
{
  if (is_complex_) {
    p_complex_ = Traits::get_complex_data(pm);
    p_end_complex_ = p_complex_ + mxGetNumberOfElements(pm);
  } else {
    p_real_ = Traits::get_real_data(pm);
    p_end_real_ = p_real_ + mxGetNumberOfElements(pm);
  }
}

template <typename Traits>
bool mx_numeric_proxy<Traits>::valid() const {
  if (is_complex_)
    return p_complex_ != p_end_complex_;
  else
    return p_real_ != p_end_real_;
}

template <typename Traits>
void mx_numeric_proxy<Traits>::next() {
  if (is_complex_)
    ++p_complex_;
  else
    ++p_real_;
}

template <typename Traits>
auto mx_numeric_proxy<Traits>::real() const -> real_t {
  if (is_complex_)
    return p_complex_->real;
  else
    return *p_real_;
}

template <typename Traits>
auto mx_numeric_proxy<Traits>::imag() const -> real_t {
  if (is_complex_)
    return p_complex_->imag;
  else
    return real_t(0);
}

template <typename Traits>
template <typename U>
void mx_numeric_proxy<Traits>::real(U value) {
  if (is_complex_)
    p_complex_->real = static_cast<real_t>(value);
  else
    *p_real_ = static_cast<real_t>(value);
}

template <typename Traits>
template <typename U>
void mx_numeric_proxy<Traits>::imag(U value) {
  if (is_complex_) p_complex_->imag = static_cast<real_t>(value);
}

#else

#include <cstdint>

template <mxClassID_ ID>
struct mx_numeric_proxy_traits;

template <>
struct mx_numeric_proxy_traits<mxClassID_::mxLOGICAL_CLASS> {
  using real_type = mxLogical;
  using complex_type = mxLogical;

  static mxLogical* get_real_data(const mxArray* pm) {return mxGetLogicals(pm);}
  static mxLogical* get_imag_data(const mxArray* pm) {return mxGetLogicals(pm);}
};

#define MX_NUMERIC_PROXY_TRAITS(class_id, real_t) \
  template <> \
  struct mx_numeric_proxy_traits<class_id> { \
    using real_type = real_t; \
    using complex_type = real_t; \
    static real_t* get_real_data(const mxArray* pm) {return reinterpret_cast<real_type*>(mxGetData(pm));} \
    static real_t* get_imag_data(const mxArray* pm) {return reinterpret_cast<real_type*>(mxGetImagData(pm));} \
  };

MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxDOUBLE_CLASS, double)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxSINGLE_CLASS, float)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxINT8_CLASS, int8_t)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxUINT8_CLASS, uint8_t)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxINT16_CLASS, int16_t)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxUINT16_CLASS, uint16_t)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxINT32_CLASS, int32_t)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxUINT32_CLASS, uint32_t)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxINT64_CLASS, int64_t)
MX_NUMERIC_PROXY_TRAITS(mxClassID_::mxUINT64_CLASS, uint64_t)

#undef MX_NUMERIC_PROXY_TRAITS


template <typename Traits>
mx_numeric_proxy<Traits>::mx_numeric_proxy(const mxArray* pm)
    : is_complex_(mxIsComplex(pm))
{
  p_real_ = Traits::get_real_data(pm);
  p_end_real_ = p_real_ + mxGetNumberOfElements(pm);
  if (is_complex_) p_imag_ = Traits::get_imag_data(pm);
}

template <typename Traits>
bool mx_numeric_proxy<Traits>::valid() const {
  return p_real_ != p_end_real_;
}

template <typename Traits>
void mx_numeric_proxy<Traits>::next() {
  ++p_real_;
  if (is_complex_) ++p_imag_;
}

template <typename Traits>
auto mx_numeric_proxy<Traits>::real() const -> real_t {
  return *p_real_;
}

template <typename Traits>
auto mx_numeric_proxy<Traits>::imag() const -> real_t {
  return is_complex_ ? *p_imag_ : real_t(0);
}

template <typename Traits>
template <typename U>
void mx_numeric_proxy<Traits>::real(U value) {
  *p_real_ = static_cast<real_t>(value);
}

template <typename Traits>
template <typename U>
void mx_numeric_proxy<Traits>::imag(U value) {
  if (is_complex_) *p_imag_ = static_cast<real_t>(value);
}

#endif


// Used to create mx_numeric_proxy objects
template <mxClassID_ ID>
mx_numeric_proxy<mx_numeric_proxy_traits<ID>> mx_make_numeric_proxy(const mxArray* pm) {
  return mx_numeric_proxy<mx_numeric_proxy_traits<ID>>(pm);
}

