#pragma once

#include "mex_package_class.h"
#include "mex_class_members_adder.tcc"
#include "mx_convert_class.tcc"
#include "mex_ref_arg.tcc"
#include "mex_return_value.tcc"
#include "mex_getter_value.tcc"
#include "mex_class_members.tcc"


// MEX_PACKAGE is used to define the constructor for the top level package
#define MEX_PACKAGE MainMexPackage::MainMexPackage() : MainMexPackageBase()

