#pragma once

#include "mx_convert_utils.h"
#include "mx_shape.h"
#include "mx_struct.h"
#include "mx_convert_tuple.h"
#include "mx_cell_proxy.h"
#include "mx_numeric_proxy.tcc"
#include "mex_include.h"
#include <array>
#include <memory>
#include <string>
#include <tuple>
#include <type_traits>
#include <vector>


// Maps mxClassID_ enum class to actual mxClassID values
static constexpr std::array<mxClassID, int(mxClassID_::mxFUNCTION_CLASS)+1> mxClassID_map = {{
  mxUNKNOWN_CLASS,
  mxCELL_CLASS,
  mxSTRUCT_CLASS,
  mxLOGICAL_CLASS,
  mxCHAR_CLASS,
  mxVOID_CLASS,
  mxDOUBLE_CLASS,
  mxSINGLE_CLASS,
  mxINT8_CLASS,
  mxUINT8_CLASS,
  mxINT16_CLASS,
  mxUINT16_CLASS,
  mxINT32_CLASS,
  mxUINT32_CLASS,
  mxINT64_CLASS,
  mxUINT64_CLASS,
  mxFUNCTION_CLASS
}};


/*==============================================================================
Function:  mxExtract(), mxConvert()

Description:
------------
mxExtract()/mxConvert() is a function template that converts between Matlab data and C
objects of almost any type.  Support for various C data types is handled through
traits class specialization for that data type.  This means that support for
additional C data types can be done without modifying this library or the C data
type definition, since the traits class specialization is defined externally to
both.  mxExtract()/mxConvert() comes with built-in support for STL sequence containers, C
arrays, and all arithmetic data types.

Usage:  (Converting from Matlab to C)
-------------------------------------
There are two ways to call mxExtract() to convert Matlab data to C.  The first
is to pass the Matlab data (as an mxArray*) and an existing C object to write
the converted data to. Since this C object is being passed in, no template
arguments need to be specified since it can be automatically inteferred from the
function arguments.  An example would be:

  bool value;
  mxExtract(prhs[0], &value);

It is assumed that the C object being passed in is already appropriately sized
to hold the converted Matlab data.  mxExtract() will not resize the C object for
you!  So if you attempt to do this:

  std::vector<int> values;
  mxExtract(prhs[0], &values);

expect it to do nothing.  (Actually you wil get an assertion failure because
mxExtract() does not allow any zero length arrays.)
An example of how to fix this would be:

  std::vector<int> values(mxGetNumberOfElements(prhs[0]);
  mxExtract(prhs[0], &values);

The second way to call mxExtract() is to only pass in the Matlab data and have
it create the C object for you (and correctly sized) and return that newly
created object.  Examples would be:

  auto value = mxExtract<bool>(prhs[0]);

  auto values = mxExtract<std::vector<int>>(prhs[0]);

Note, in order to use this second form of mxExtract() with dynamically sized
array types (statically sized arrays are always fine), there must exist an
mx_factory_traits<> traits class implementation for that C array type.  By
default, one is already provided for STL sequence containers like std::vector
and std::deque.

Usage:  (Converting from C to Matlab)
-------------------------------------
Call mxConvert() with just a single argument containing the C object to convert
(and no template arguments are needed since it is inferred), and it will return
an mxArray* containing the Matlab data.  Examples are:

  plhs[0] = mxConvert(value);
  plhs[1] = mxConvert(values);

Note that if you are assigning to an mxStruct field, you do not need mxConvet()
is automatically called when needed, so you can simply do this:

  mxStruct out(&plhs[0]);
  out["value"] = value;
  out["values"] = values;

Matlab Array Element Types:
---------------------------
mxConvert() supports numerical, logical, and structure element types. With
numerical types, mxConvert() supports all integer types (up to 64 bits) and of
course the floating point types as well.

When converting from Matlab to C, mxConvert() tries to support any element type
when doing the conversion, but ultimately it is up to the mx_element_traits
class implementation for that C data type to ensure that it can support it.  For
the most part, numerical and logical element types are convertible amongst
themselves, but structure elements are different, and the C data type must be
expecting this or it will not.

When converting from C to Matlab, the mx_element_traits class implementation for
that C data type specifies the Matlab element type it should be converted to.
In general mxConvert() tries to convert C elements to Matlab double types
(because Matlab works most natively with that type), except for C bool types
which get conveted to Matlab logical types, and 64-bit integers which get
conveted to Matlab int64/uint64 types.  All other integer types get converted to
Matlab double types.

String Support
--------------
mxExtract()/mxConvert() has special support for strings as well.  When converting from
Matlab strings to C, you must convert it to an std::string type.  When
converting a C string to Matlab, you can use either std::string or const char*
types.  Examples are:

  std::string name;
  mxExtract<std::string>(prhs[0], &name);

  auto name = mxExtract<std::string>(prhs[0]);

  plhs[0] = mxConvert(name);
  plhs[0] = mxconvert("Hello World");

Overriding mxExtract()/mxConvert()
----------------------
The user is allowed to override the mxExtract()/mxConvert() if he/she needs to completely
customize its implementation for a particular type.  Just be aware of C++
function overload resolution rules, which are quite complex, to be sure that the
override for the new type doesn't break mxExtract()/mxConvert() for all other types using
the normal implementation.

==============================================================================*/

// Converts Matlab data to C with a provided C object.
// This is called for C numerical/logical data types
template <typename T, MX_OVERLOAD_IF(mx_class_prop<T>::is_numerical || mx_class_prop<T>::is_logical)>
void mxExtract(const mxArray* pm, T* c_obj) {
  if (mxGetNumberOfElements(pm) > 0) {
    if (mxIsNumeric(pm)) {
      auto class_id = mxGetClassID(pm);

      #define MX_WRITE_TO_C(CLASS_ID) {\
        auto mx_elem_proxy = mx_make_numeric_proxy<CLASS_ID>(pm); \
        mx_c_traits<T>::mx_write_to_c(mx_elem_proxy, c_obj); \
      }

      switch (class_id) {
        case mxINT8_CLASS:   MX_WRITE_TO_C(mxClassID_::mxINT8_CLASS); break;
        case mxUINT8_CLASS:  MX_WRITE_TO_C(mxClassID_::mxUINT8_CLASS); break;
        case mxINT16_CLASS:  MX_WRITE_TO_C(mxClassID_::mxINT16_CLASS); break;
        case mxUINT16_CLASS: MX_WRITE_TO_C(mxClassID_::mxUINT16_CLASS); break;
        case mxINT32_CLASS:  MX_WRITE_TO_C(mxClassID_::mxINT32_CLASS); break;
        case mxUINT32_CLASS: MX_WRITE_TO_C(mxClassID_::mxUINT32_CLASS); break;
        case mxINT64_CLASS:  MX_WRITE_TO_C(mxClassID_::mxINT64_CLASS); break;
        case mxUINT64_CLASS: MX_WRITE_TO_C(mxClassID_::mxUINT64_CLASS); break;
        case mxSINGLE_CLASS: MX_WRITE_TO_C(mxClassID_::mxSINGLE_CLASS); break;
        case mxDOUBLE_CLASS: MX_WRITE_TO_C(mxClassID_::mxDOUBLE_CLASS); break;
        default: mexErrMsgTxt("Unsupported class ID.");
      }
      #undef MX_WRITE_TO_C

    } else if (mxIsLogical(pm)) {
      auto mx_elem_proxy = mx_make_numeric_proxy<mxClassID_::mxLOGICAL_CLASS>(pm);
      mx_c_traits<T>::mx_write_to_c(mx_elem_proxy, c_obj);

    } else {
      mexErrMsgTxt((std::string("Expecting a Matlab numerical/logical type to convert to C++ type: ") + typeid(T).name()).c_str());
    }
  }
}

// This is called for C string array types
template <typename T, MX_OVERLOAD_IF(mx_class_prop<T>::is_string && mx_is_array_type<T>::value)>
void mxExtract(const mxArray* pm, T* c_obj) {
  if (mxGetNumberOfElements(pm) > 0) {
    if (mxIsCell(pm)) {
      mx_const_cell_proxy mx_elem_proxy(pm);
      mx_c_traits<T>::mx_write_to_c(mx_elem_proxy, c_obj);
    } else {
      mexErrMsgTxt((std::string("Expecting a Matlab cell array type to convert to C++ string array: ") + typeid(T).name()).c_str());
    }
  }
}

// This is called for C string types
template <typename T, MX_OVERLOAD_IF(mx_class_prop<T>::is_string && !mx_is_array_type<T>::value)>
void mxExtract(const mxArray* pm, T* c_obj) {
  if (mxIsChar(pm)) {
    auto* c_str = mxArrayToString(pm);
    assert(c_str);
    *c_obj = c_str;
    mxFree(c_str);
  } else {
    mexErrMsgTxt("Expecting a Matlab string type to convert to std::string: ");
  }
}

// Converts Matlab data to C with a provided C object.
// This is called for C structure array data types
template <typename T, MX_OVERLOAD_IF(mx_class_prop<T>::is_struct)>
void mxExtract(const mxArray* pm, T* c_obj) {
  if (mxGetNumberOfElements(pm) > 0) {
    if (mxIsStruct(pm)) {
      mxConstStruct mx_elem_proxy(pm);
      mx_c_traits<T>::mx_write_to_c(mx_elem_proxy, c_obj);

    } else {
      mexErrMsgTxt((std::string("Expecting a Matlab struct type to convert to C++ type: ") + typeid(T).name()).c_str());
    }
  }
}

// Overload for native mex_data types
template <typename T, MX_OVERLOAD_IF(std::is_convertible<T, const mxArray*>::value)>
T mxExtract(const mxArray* pm) {
  return T(pm);
}

// Converts Matlab data to C and automatically creates a new C object and returns it.
template <typename T, MX_OVERLOAD_IF((mx_class_prop<T>::is_numerical ||
                                      mx_class_prop<T>::is_logical ||
                                      mx_class_prop<T>::is_struct ||
                                      mx_class_prop<T>::is_string
                                     ) &&
                                     !(std::is_reference<T>::value && !std::is_const<typename std::remove_reference<T>::type>::value))>
mx_decay_t<T> mxExtract(const mxArray* pm) {
  // Reverse the C dimension order since Matlab uses a different storage order
  auto c_obj = mx_factory_traits<mx_decay_t<T>>::make(mxShape(pm).reverse());

  mxExtract(pm, &c_obj);

  return c_obj;
}

// A version of mxExtract that extracts the Matlab data into a newly created
// temporary C++ object and then copies it into the provided C++ object.
// This is a safer implementation for dynamically sized arrays but with the
// downside that mx_factory_traits and mx_copy_traits must be defined for
// that type.
template <typename T>
void mxExtractAndCopy(const mxArray* pm, T* c_obj) {
  mx_copy_traits<T>::copy(mxExtract<T>(pm), *c_obj);
}

// mxSafeExtract tries to intelligent decide whether or not to directly extract
// the Matlab data directly into the C object, or to create a new copy object,
// extract the Matlab data into that, and then copy into the given C object.
template <typename T, typename U, MX_OVERLOAD_IF(mx_is_array_type<T>::value)>
void mxSafeExtract(U pm, T* c_obj) {
  mx_copy_traits<T>::copy(pm, *c_obj);
}
template <typename T, typename U, MX_OVERLOAD_IF(!mx_is_array_type<T>::value)>
void mxSafeExtract(U pm, T* c_obj) {
  mxExtract(pm, c_obj);
}

// Convert C objects to Matlab data.
// This is called for C numerical/logical data types
template <typename T, MX_OVERLOAD_IF(mx_class_prop<T>::is_numerical || mx_class_prop<T>::is_logical)>
mxArray* mxConvert(const T& c_obj) {
  // Reverse the C dimension order since Matlab uses a different storage order
  std::vector<mwSize> dims;
  mx_c_traits<T>::shape(c_obj, &dims);
  std::reverse(dims.begin(), dims.end());

  if (mx_c_traits<T>::elem_class_id == mxClassID_::mxLOGICAL_CLASS) {
    auto* pm = mxCreateLogicalArray(dims.size(), dims.data());

    auto mx_elem_proxy = mx_make_numeric_proxy<mxClassID_::mxLOGICAL_CLASS>(pm);
    mx_c_traits<T>::mx_read_from_c(c_obj, mx_elem_proxy);

    return pm;

  } else {
    mxComplexity complex_flag = mx_c_traits<T>::is_complex ? mxCOMPLEX : mxREAL;
    auto* pm = mxCreateNumericArray(dims.size(), dims.data(), mxClassID_map[int(mx_c_traits<T>::elem_class_id)], complex_flag);

    auto mx_elem_proxy = mx_make_numeric_proxy<mx_c_traits<T>::elem_class_id>(pm);
    mx_c_traits<T>::mx_read_from_c(c_obj, mx_elem_proxy);

    return pm;
  }
}

// Convert C objects to Matlab data.
// This is called for C structure array data types
template <typename T, MX_OVERLOAD_IF(mx_class_prop<T>::is_struct)>
mxArray* mxConvert(const T& c_obj) {
  std::vector<mwSize> dims;
  mx_c_traits<T>::shape(c_obj, &dims);
  std::reverse(dims.begin(), dims.end());

  auto* pm = mxCreateStructArray(dims.size(), dims.data(), 0, nullptr);

  mxStruct mx_elem_proxy(pm);
  mx_c_traits<T>::mx_read_from_c(c_obj, mx_elem_proxy);

  return pm;
}

// Convert const char* string to Matlab string
inline mxArray* mxConvert(const char* c_obj) {
  return mxCreateString(c_obj);
}

// Convert all array types that must use Matlab cell arrays (std:string, std::tuple, etc.)
template <typename T, typename Converter=MxDefaultConverter, MX_OVERLOAD_IF(mx_class_prop<mx_decay_t<T>>::use_cell_array && mx_is_array_type<T>::value)>
mxArray* mxConvert(T&& c_obj) {
  std::vector<mwSize> dims;
  mx_c_traits<mx_decay_t<T>>::shape(c_obj, &dims);
  std::reverse(dims.begin(), dims.end());

  auto* pm = mxCreateCellArray(dims.size(), dims.data());

  mx_cell_proxy<Converter> mx_elem_proxy(pm);
  mx_c_traits<mx_decay_t<T>>::mx_read_from_c(std::forward<T>(c_obj), mx_elem_proxy);

  return pm;
}

// Convert all element types that would use Matlab cell arrays if they were part of an array (std:string, std::tuple, etc.)
template <typename T, typename C=MxDefaultConverter, MX_OVERLOAD_IF(mx_class_prop<mx_decay_t<T>>::use_cell_array && !mx_is_array_type<T>::value)>
mxArray* mxConvert(T&& c_obj) {
  struct mxArrayPtrWrap {
    mxArray* pm_ = nullptr;
    mxArrayPtrWrap& operator=(mxArray* pm) {
      pm_ = pm;
      return *this;
    }
    operator mxArray*() {return pm_;}
  };

  mxArrayPtrWrap mx_proxy;
  mx_element_traits<mx_decay_t<T>>::convert_to_matlab(std::forward<T>(c_obj), mx_proxy);
  return mx_proxy;
}

