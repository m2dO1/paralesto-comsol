#include "mx_struct.h"
#include "mx_data.h"
#include "matrix.h"
#include <cassert>
#include <sstream>


std::deque<std::string> extract_variadic_macro_arg_string(const std::string& va_arg_string) {
  std::stringstream ss(va_arg_string);
  std::deque<std::string> va_arg_string_array;
  va_arg_string_array.clear();

  while(ss.good()) {
    std::string arg_string;
    std::getline(ss, arg_string, ',');

    // Strip leading spaces
    while (std::isspace(*arg_string.begin())) {
      arg_string.erase(arg_string.begin());
    }
    // Strip trailing spaces
    while (std::isspace(*arg_string.rbegin())) {
      arg_string.erase(arg_string.length()-1);
    }

    va_arg_string_array.push_back(arg_string);
  }
  return va_arg_string_array;
}


mxStructBase::mxStructBase(const mxArray* mxa, int field_num, std::size_t array_idx)
    : field_num_(field_num)
    , array_idx_(array_idx)
{
  if (mxa) array_end_idx_ = array_idx + mxGetNumberOfElements(mxa);
}

std::string mxStructBase::next_field_name() const {
  std::string field_name = field_names_.front();
  field_names_.pop_front();
  return field_name;
}

mxConstStruct::mxConstStruct(const mxArray* mxa, int field_num, std::size_t array_idx)
    : mxStructBase(mxa, field_num, array_idx), mxa_(mxa)
{}

mxConstStruct::mxConstStruct(const mxArray* mxa, std::size_t array_idx) : mxConstStruct(mxa, -1, array_idx) {
  // Disable check because it could be a custom class, which seems impossible to detect.
  //assert(mxIsStruct(mxa));
}

mxConstStruct::mxConstStruct(const mxData& mxd) : mxConstStruct(static_cast<const mxArray*>(mxd), -1, 0) {
}

mxConstStruct mxConstStruct::operator[](const char* field_name) const {
  auto* field_mxa = mxGetField(mxa_, array_idx_, field_name);
  if (field_mxa) {
    return mxConstStruct(field_mxa, mxGetFieldNumber(mxa_, field_name), 0);
  } else {
    field_mxa = mxGetProperty(mxa_, array_idx_, field_name);
    assert(field_mxa);
    return mxConstStruct(field_mxa);
  }
}

bool mxConstStruct::is_field(const char* field_name) const {
  if (mxGetField(mxa_, array_idx_, field_name)) {
    return true;
  } else if (mxGetProperty(mxa_, array_idx_, field_name)) {
    return true;
  } else {
    return false;
  }
}


int mxConstStruct::number_of_fields() const {
  assert(mxIsStruct(mxa_));
  return mxGetNumberOfFields(mxa_);
}

const char* mxConstStruct::field_name(int field_num) const {
  assert(mxIsStruct(mxa_));
  return mxGetFieldNameByNumber(mxa_, field_num);
}


mxStruct::mxStruct(mxArray* mxa, mxArray* parent_mxa, int field_num, std::size_t array_idx)
    : mxStructBase(mxa, field_num, array_idx), mxa_(mxa), parent_mxa_(parent_mxa)
{}

mxStruct::mxStruct() : mxStruct(mxCreateStructMatrix(1, 1, 0, nullptr), nullptr) {}

mxStruct::mxStruct(mxArray* mxa, std::size_t array_idx) : mxStruct(mxa, nullptr, -1, array_idx) {
  assert(mxIsStruct(mxa));
}

mxStruct mxStruct::operator[](const char* field_name) {
  if (mxa_) {
    auto* field_mxa = mxGetField(mxa_, array_idx_, field_name);
    if (field_mxa) {
      return mxStruct(field_mxa, mxa_, mxGetFieldNumber(mxa_, field_name), array_idx_);
    } else {
      auto field_num = mxAddField(mxa_, field_name);
      assert(field_num != -1);
      return mxStruct(nullptr, mxa_, field_num, array_idx_);
    }
  } else {
    *this = mxCreateStructMatrix(1, 1, 0, nullptr);
    return (*this)[field_name];
  }
}

bool mxStruct::is_field(const char* field_name) const {
  assert(mxIsStruct(mxa_));
  return mxGetField(mxa_, array_idx_, field_name) != nullptr;
}

int mxStruct::number_of_fields() const {
  assert(mxIsStruct(mxa_));
  return mxGetNumberOfFields(mxa_);
}

const char* mxStruct::field_name(int field_num) const {
  assert(mxIsStruct(mxa_));
  return mxGetFieldNameByNumber(mxa_, field_num);
}

mxStruct& mxStruct::operator=(mxArray* rhs) {
  if (!parent_mxa_) {
    assert(mxIsStruct(rhs));
    mxa_ = rhs;
    array_idx_ = 0;
  } else {
    mxa_ = rhs;
    mxSetFieldByNumber(parent_mxa_, array_idx_, field_num_, rhs);
  }
  return *this;
}

mxStruct& mxStruct::operator=(const mxStruct& rhs) {
  if (!parent_mxa_) {
    mxa_ = rhs.mxa_;
    parent_mxa_ = rhs.parent_mxa_;
    field_num_ = rhs.field_num_;
    array_idx_ = rhs.array_idx_;
  } else {
    operator=(rhs.mxa_);
  }
  return *this;
}

