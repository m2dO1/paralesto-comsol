#pragma once

#include "mx_data.h"
#include "mx_convert.h"


template <typename T, MX_OVERLOAD_IF_DEF(!std::is_convertible<typename std::decay<T>::type, const mxArray*>::value)>
mxData::operator T() const {
  return mxExtract<T>(operator const mxArray*());
}

// In rare cases, it is convenient to force the conversion type via a run
// time argument, when implicit or even explicit conversion is not possible.
// The argument is only used to infer the conversion operation type.
template <typename T>
T mxData::conversion_type(T) const {
  return mxExtract<T>(operator const mxArray*());
}

