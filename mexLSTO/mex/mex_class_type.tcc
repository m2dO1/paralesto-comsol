#pragma once

#include "mex_class_type.h"
#include "exposure_attributes.h"


namespace mex_pack {

template <typename T>
ClassType<T>::ClassType() : ClassMembers(typeid(T)) {
  AddTypeNameProperty();
  AddMexClassMembers(static_cast<T*>(nullptr), MembersAdderBase(this, true));
  AddMexClassMembers(static_cast<T*>(nullptr), MembersAdderBase(this, false));
  AddMexClassMembersExtended(static_cast<T*>(nullptr), MembersAdderBase(this, false));
}

template <typename T>
ClassMembers* ClassType<T>::CreateNewObject(mexInputArgs prhs) const {
  auto* new_obj_ptr = CallMatlabCtor(bool_const<std::is_constructible<T, mexInputArgs>::value>{}, prhs);

  if (new_obj_ptr == nullptr) {
    // Then try calling constructors in mexClass definition, which can fail for two reasons:
    //    1. No constructors found to match argument format.
    //    2. AddMexClassMembers() only exists for base class of T
    AddMexClassMembers(static_cast<T*>(nullptr), MembersAdder<T>(&prhs, &new_obj_ptr));

    if (new_obj_ptr == nullptr && prhs.size() == 0) {
      // Finally call default constructor
      new_obj_ptr = CreateDefaultObject(bool_const<std::is_default_constructible<T>::value>{});
    }
  }

  if (new_obj_ptr == nullptr) mexErrMsgTxt("No constructor matching this argument format found.");

  return new ClassInstance<T>(std::unique_ptr<T>(new_obj_ptr));
}

} // namespace mex_pack

