#pragma once

/*==============================================================================
This file contains various traits classes that the mxConvert() function relies
on to convert between mxArray* and arbitrary C types.  Support for arithmetic
data types as elements, and typical array types including STL containers are
included.  But support for additional custom C types can be added by providing
additional template specializations of one or more of these traits classes.  Be
sure not to add these custom template specializations in this file!  Add them
outside, usually in a completely separate header file so that you're not
modifying the original C type source code as well.

There are two types of C types considered here.

1. Array types.  These are simply containers for one or more objects of another
type.  They can be single or multidimensional arrays.  Examples would be
std::vector, blitz::Array, and standard C arrays.

2.  Element types.  These are the types that are actually converted to or from
Matlab data types.  They can be C representations of numerical, logical, or
structure types.  Examples would be int, double, std::complex, sc_fix, and
custom structures.

Note:  Array types can contain other array types or element types.  Element
types can also contain other element types, like std::complex for example.
Structure element types can have fields of any type, array or element types or
other structure types.

All element types must implement a specialization of the mx_element_traits
class. All array types must implement a specialization of the mx_array_traits
class, and possibly a specialization of the mx_factory_traits class as well.
==============================================================================*/

#include "mx_element_traits.h"
#include "mx_array_traits.h"
#include "mx_struct_traits.h"
#include "mx_shape.h"
#include "mx_sfinae_helpers.h"
#include <iostream>


template <typename T, typename = void>
struct mx_is_pure_data_type : std::false_type {};

template <typename T>
struct mx_is_pure_data_type<T, MX_USE_IF(mx_is_element_type<T>::value)>
    : std:: true_type
{};

template <typename T>
struct mx_is_pure_data_type<T, MX_USE_IF(mx_is_array_type<T>::value)>
    : mx_is_pure_data_type<typename mx_array_traits<mx_decay_t<T>>::value_type>
{};


// Returns number of dimensions of array type (counting nested arrays)
template <typename A, typename = void>
struct mx_ndims {
  static constexpr std::size_t value = 1 + mx_ndims<typename mx_array_traits<mx_decay_t<A>>::value_type>::value;
};
template <typename T>
struct mx_ndims<T, MX_USE_IF(!mx_is_array_type<T>::value)> {
  static constexpr std::size_t value = 0;
};


/*==============================================================================
Class Template:  mx_copy_traits

Description:
mx_copy_traits provides a standardized object copying interface.  By default it
uses the copy assignment operator.
==============================================================================*/
template <typename T, typename = void>
struct mx_copy_traits {
  static void copy(const T& src, T& dst) {
    dst = src;
  }
};

// Specialization for all STL sequence containers (except for std::array)
template <template <typename, typename> class SEQC, typename T>
struct mx_copy_traits<SEQC<T, std::allocator<T>>, void> {
  static void copy(const SEQC<T, std::allocator<T>>& src, SEQC<T, std::allocator<T>>& dst) {
    // Avoid use of resize() method which invokes the copy constructor
    SEQC<T, std::allocator<T>> tmp(src.size());
    dst.swap(tmp);
    auto dst_ptr = dst.begin();
    for (const auto& src_elem : src) {
      mx_copy_traits<T>::copy(src_elem, *dst_ptr++);
    }
  }
};

// Specialization for std::vector<bool> since explicit element references aren't supported
template <>
struct mx_copy_traits<std::vector<bool>, void> {
  static void copy(const std::vector<bool>& src, std::vector<bool>& dst) {
    dst = src;
  }
};
template <>
struct mx_copy_traits<bool, void> {
  static void copy(bool src, bool& dst) {
    dst = src;
  }
  static void copy(bool src, std::vector<bool>::reference dst) {
    dst = src;
  }
};

// Specialization for std::array type
template <typename T, std::size_t N>
struct mx_copy_traits<std::array<T, N>, void> {
  static void copy(const std::array<T, N>& src, std::array<T, N>& dst) {
    auto dst_ptr = dst.begin();
    for (const auto& src_elem : src) {
      mx_copy_traits<T>::copy(src_elem, *dst_ptr++);
    }
  }
};


// Specialization for C arrays.
// C arrays must be handled differently because they lack value semantics.
// This is resolved by forcing conversion of the Matlab data to an equivalent
// std::array type first (which may be nested for multidimensions), before
// copying it to the destination C array.
template <typename T, std::size_t N>
struct mx_copy_traits<T[N], void> {
 private:
  // Converts C array type to equivalent std::array type (supports multidimensions)
  template <typename T2>
  struct carray_to_stdarray {
    using type = T2;
  };
  template <typename T2, std::size_t N2>
  struct carray_to_stdarray<T2[N2]> {
    using type = std::array<typename carray_to_stdarray<T2>::type, N2>;
  };

  using equiv_stdarray_t = std::array<typename carray_to_stdarray<T>::type, N>;

 public:
  static void copy(const equiv_stdarray_t& src, T (& dst)[N]) {
    auto dst_ptr = std::begin(dst);
    for (const auto& src_elem : src) {
      mx_copy_traits<T>::copy(src_elem, *dst_ptr++);
    }
  }
};


/*==============================================================================
Class Template:  mx_factory_traits

Description:
When using mxConvert to convert an mxArray* to a C object, and one is using the
form where it returns a newly created C object (instead of using one passed into
it), then mxConvert needs to know how to create the C object.  The
mx_factory_traits class provides this capability through its static make()
method.  By default, the default constructor is used, which should work fine for
most element types.  Implementations for all STL sequence containers are also
provided.

Note, if you never intends to use mxConvert to return new instances of your
custom class, then you don't need to provide an mx_factory_traits
implementation.

mx_factory_traits only has a single function, make(), which accepts a vector
input argument that indicates the size and shape of the array it should create,
which is based on the Matlab object's size and shape.  One does not have to
return the exact same shape, especially if the array type doesn't support the
same number of dimensions.  For example, it is common for all 1 dimensional C
array types to just size itself equal to the total number of elements,
regardless of the input shape.  If the desired shape has fewer number of
dimensions that the C type (which can be checked using the mx_dims<> class),
then singleton dimensions should be inserted first, as many needed so that
the desired shape applies to the last dimensions.
==============================================================================*/

// Use default constructor by default
template <typename T, typename = void>
struct mx_factory_traits {
  static T make(const mxShape&) {
    return {};
  }
};

// Specialization for std::array<T, N>
template <typename T, std::size_t N>
struct mx_factory_traits<std::array<T, N>, void> {
  static std::array<T, N> make(const mxShape& shape) {
    std::array<T, N> val{{}};

    if (N == 1 && mx_ndims<std::array<T, N>>::value > shape.ndims()) {
      val.fill(mx_factory_traits<T>::make(shape));
    } else if (!mx_is_element_type<T>::value) {
      if (N != shape.extent(0)) {
        std::cout << "Warning:  Assigning a Matlab array with " << shape.extent(0) << " elements to a C array with a fixed size of " << N << std::endl;
      }
      val.fill(mx_factory_traits<T>::make(shape.remove_first_dim()));
    } else if (N != shape.num_elements()) {
      std::cout << "Warning:  Assigning a Matlab array with " << shape.num_elements() << " elements to a C array with a fixed size of " << N << std::endl;
    }
    return val;
  }
};

// Base class implementation for STL sequence container-like classes.
// Dynamically sizes the array based on the provided shape, and recursively
// initializes nested arrays so that they are also prroperly sized.
template <typename A>
struct mx_factory_traits_seq_container {
  static A make(const mxShape& shape) {
    if (mx_is_element_type<typename mx_array_traits<A>::value_type>::value) {
      return A(shape.num_elements());
    } else if (mx_ndims<A>::value > shape.ndims()) {
      // This approach avoids use of copy constructor of element type unlike the resize() method
      A tmp(1);
      mx_copy_traits<typename mx_array_traits<A>::value_type>::copy(mx_factory_traits<typename mx_array_traits<A>::value_type>::make(shape), tmp[0]);
      return tmp;
    } else {
      // This approach avoids use of copy constructor of element type unlike the resize() method
      A tmp(shape.extent(0));
      for (auto& elem : tmp) {
        mx_copy_traits<typename mx_array_traits<A>::value_type>::copy(mx_factory_traits<typename mx_array_traits<A>::value_type>::make(shape.remove_first_dim()), elem);
      }
      return tmp;
    }
  }
};

// Specialization for all STL sequence containers except for std::array.
template <template <typename, typename> class SEQC, typename T>
struct mx_factory_traits<SEQC<T, std::allocator<T>>>
    : mx_factory_traits_seq_container<SEQC<T, std::allocator<T>>> {
};

// Specialization for std::vector<bool>
template <>
struct mx_factory_traits<std::vector<bool>, void> {
  static std::vector<bool> make(const mxShape& shape) {
    return std::vector<bool>(shape.num_elements());
  }
};


/*==============================================================================
Add support for std::reference_wrapper
==============================================================================*/
#include <functional>

template <typename T>
struct mx_element_traits<std::reference_wrapper<T>> : mx_element_traits<T> {};

template <typename T>
struct mx_array_traits<std::reference_wrapper<T>> : mx_array_traits<T> {};

