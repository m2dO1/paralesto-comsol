#pragma once

/*==============================================================================
DEPRECATED!!!

Use of mx_struct_traits and MX_STRUCT_TRAITS is being deprecated.  Use MX_STRUCT
style definitions instead, which support templates unlike MX_STRUCT_TRAITS.
See mexPackage documentation for more details.
==============================================================================*/

#include "mx_element_traits.h"
#include "mx_struct.h"


/*==============================================================================
Class Template:  mx_struct_traits

Description
mx_struct_traits makes it easier to define mx_element_traits implmentations for
struct types.  Instead of specializing mx_element_traits directly, one can
instead specialize mx_struct_traits, which has the same effect, plus it
automatically sets the class_id property to mxSTRUCT_CLASS.
==============================================================================*/

struct mx_not_struct_type {};   // identifier for non-struct types

template <typename T, typename = void>
struct mx_struct_traits : mx_not_struct_type {
};

template <typename T>
struct mx_element_traits<T, MX_USE_IF(!std::is_base_of<mx_not_struct_type, mx_struct_traits<T>>::value)>
    : mx_struct_traits<T>
{
  static constexpr bool is_complex = false;

  static constexpr mxClassID_ class_id = mxClassID_::mxSTRUCT_CLASS;
};


/*==============================================================================
Macro:  MX_STRUCT_TRAITS

Description:
This macro makes it easy to define an mx_element_traits specialization for any C
structure or class type that is not a template.
Simply provide the structure/class name as the first argument, followed by any
number of optional arguments listing the fields in the structure/classs that you
want to support conversion to and from Matlab.  You can list as many or as little
fields in the stucture/class that you want to support in the conversion.

Example:

  struct Foo {
    int value;
    std::vector<double> array_values;
  };

  MX_STRUCT_TRAITS(Foo, value, array_values)

Which then allows one to make conversions like:

  Foo f = mxData(prhs[0]);   // Matlab to C

  plhs[0] = mxConvert(f); // C to Matlab

  out["foo"] = f;         // C to Matlab struct field

==============================================================================*/

// Note:  Use of With struct allows members of c_obj to be referenced directly
// without explicit member access operators, which is is needed in order to work
// with the variadic macro argument.  One would not normally implement a
// specialization of mx_element_traits for a struct type in this manner
// otherwise.
#define MX_STRUCT_TRAITS(StructType, ...) \
  template <> \
  struct mx_struct_traits<StructType, void> { \
    template <typename C_iter> \
    static void convert_to_c(const mxConstStruct& mx_proxy, C_iter c_obj) { \
      struct With : StructType { \
        void convert(const mxConstStruct& mx_proxy) { \
          mx_proxy.convert_fields(#__VA_ARGS__, __VA_ARGS__); \
        } \
      }; \
      static_cast<With&>(static_cast<StructType&>(*c_obj)).convert(mx_proxy); \
    } \
  \
    static void convert_to_matlab(const StructType& c_obj, mxStruct& mx_proxy) { \
      struct With : StructType { \
        void convert(mxStruct& mx_proxy) const { \
          mx_proxy.assign_fields(#__VA_ARGS__, __VA_ARGS__); \
        } \
      }; \
      static_cast<const With&>(c_obj).convert(mx_proxy); \
    } \
 };

