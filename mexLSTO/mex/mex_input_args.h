#pragma once

#include "mx_data.h"
// #include "C:\Program Files\MATLAB\R2023b\extern\include\matrix.h"
// #include "C:\Program Files\MATLAB\R2023b\extern\include\mex.h"
#include "mex_include.h"


/*==============================================================================
Class:  mexInputArgs

Description:
------------
mexInputArgs is a replacement for the mexFunction input arguments, nrhs and prhs,
that provides a safe mechanism for retrieving input arguments without worrying
about crashes if the argument is not present from Matlab, and provides
additional features like default argument values and implicit type conversions
much like mxData.

Usage:
------
mexInputArgs should be instantiated at the beginning of the mexFunction and
passed nrhs and prhs as constructor arguments like this:

    mexInputArgs arg_in(nrhs, prhs);

The newly created object, arg_in, behaves somewhat like a pointer and initially
points to the first input argument.  One can then modify the argument it points
to by various incrementing operations, or one can have it return an input
argument at a different position using the [] operator.

Some examples are:

    arg_in[n]    <= refers to the nth argument while leaving arg_in unchanged

    ++arg_in     <= arg_in now points to the next argument

    *arg_in++    <= returns current argument and then points to the next argument

    arg_in += n  <= arg_in now points to the nth argument

Using the deferencing operator or the [] operator causes it to return an object
of type mexInputArgs::Data, which is class derived from mxData but with some
additional features, including the ability to set a default value in case the
argument wasn't specified from Matlab.  mexInputArgs::Data supports the same
implicit type conversion feature as mxData, so one can use it like this:

    // Will give error if 4th argument is not present
    bool value = arg_in[3];

    // Defaults to true if argument does not exist
    bool value = arg_in[3].with_default(true);

    // Will give error if 1st argument is not present
    std::deque<std::vector<double>> values = arg_in[0];

    // Defaults to empty array if not specified from Matlab
    std::deque<std::vector<double>> values = arg_in[0].with_default();

If a type conversion is attempted on an input argument that is not specified
from Matlab and a default value is not specified, then an error message will be
generated.

==============================================================================*/


class mexInputArgs {
 public:
  // Enhanced version of mxData specifically for use with mexInputArgs.
  class Data : public mxData {
   public:
    Data() : mxData(nullptr) {}
    explicit Data(const mxArray* mxa) : mxData(mxa) {}

    bool valid() const {return mxa_ != nullptr;}

    operator const mxArray*() const override {
      if (!valid()) mexErrMsgTxt("Not enough input arguments specified.");
      return mxa_;
    }

    template <typename T>
    mxData& with_default(const T& val = T());
  };

  mexInputArgs(int nrhs, const mxArray* prhs[]);

  // Get an input argument from a specified position
  Data operator[](std::size_t pos) const;

  // Get input argument from current argument position
  Data operator*() const {return (*this)[0];}

  // Get number of arguments
  std::size_t size() const {return nrhs_;}

  const mxArray** get() const {return prhs_ + arg_pos_;}

  // Arithmetic operators to change the default argument position
  mexInputArgs& operator++();
  mexInputArgs operator++(int);
  mexInputArgs& operator+=(int val);

 private:
  int nrhs_{};
  const mxArray** prhs_ = nullptr;
  std::size_t arg_pos_ = 0;
};

