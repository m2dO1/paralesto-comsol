#pragma once

#include "mex_class_members.h"
#include "mex_index_sequence.h"


namespace mex_pack {

template <bool IS_FACTORY, typename T, MX_OVERLOAD_IF(IS_FACTORY)>
std::unique_ptr<T> make_managed_value(T& value) {
  return std::unique_ptr<T>(&value);
}
template <bool IS_FACTORY, typename T, MX_OVERLOAD_IF(IS_FACTORY)>
std::unique_ptr<T> make_managed_value(T* value) {
  return std::unique_ptr<T>(value);
}
template <bool IS_FACTORY, typename T>
auto make_managed_value(T&& value) -> decltype(std::forward<T>(value)) {
  return std::forward<T>(value);
}


// With input arguments, no default arguments, with return value
template <typename Attributes, typename R, typename... Args, std::size_t... I, typename... DefaultArgs, MX_OVERLOAD_IF(!(std::is_same<R, void>::value || Attributes::VOID_RESULT) && sizeof...(I) == sizeof...(Args))>
void call_func(const std::function<R(Args...)>& func, const mexReturnValue& plhs, const mexInputArgs& prhs, mex_index_sequence<I...>, const std::tuple<DefaultArgs...>&, mex_index_sequence<>) {
  if (prhs.size() == sizeof...(I))
    (*plhs).cell_convert<Attributes::CELL_DEPTH>(make_managed_value<Attributes::FACTORY>(func(mxExtract<typename extract_arg<I, Args...>::type>(prhs[I])...)));
  else
    mexErrMsgTxt((std::string("Invalid number of input arguments.  Expecting ") + std::to_string(sizeof...(I)) + std::string(" input arguments.")).c_str());
}

// With input arguments, no default arguments, with no return value
template <typename Attributes, typename R, typename... Args, std::size_t... I, typename... DefaultArgs, MX_OVERLOAD_IF((std::is_same<R, void>::value || Attributes::VOID_RESULT) && sizeof...(I) == sizeof...(Args))>
void call_func(const std::function<R(Args...)>& func, const mexReturnValue&, const mexInputArgs& prhs, mex_index_sequence<I...>, const std::tuple<DefaultArgs...>&, mex_index_sequence<>) {
  if (prhs.size() == sizeof...(I))
    func(mxExtract<typename extract_arg<I, Args...>::type>(prhs[I])...);
  else
    mexErrMsgTxt((std::string("Invalid number of input arguments.  Expecting ") + std::to_string(sizeof...(I)) + std::string(" input arguments.")).c_str());
}

// No input arguments, with default arguments, with return value
template <typename Attributes, typename R, typename... Args, typename... DefaultArgs, std::size_t... Idef, MX_OVERLOAD_IF(!(std::is_same<R, void>::value || Attributes::VOID_RESULT) && sizeof...(Idef) > 0)>
void call_func(const std::function<R(Args...)>& func, const mexReturnValue& plhs, const mexInputArgs& prhs, mex_index_sequence<>, const std::tuple<DefaultArgs...>& default_args, mex_index_sequence<Idef...>) {
  if (prhs.size() == 0) {
    (*plhs).cell_convert<Attributes::CELL_DEPTH>(make_managed_value<Attributes::FACTORY>(func(std::get<Idef>(default_args)...)));
  } else {
    constexpr std::size_t NEW_N_INPUT_ARGS = 1;
    call_func<Attributes>(func, plhs, prhs, mex_make_index_sequence<NEW_N_INPUT_ARGS>(), *Attributes::default_args, mex_make_index_sequence<sizeof...(Args) - NEW_N_INPUT_ARGS>());
  }
}

// No input arguments, with default arguments, with no return value
template <typename Attributes, typename R, typename... Args, typename... DefaultArgs, std::size_t... Idef, MX_OVERLOAD_IF((std::is_same<R, void>::value || Attributes::VOID_RESULT) && sizeof...(Idef) > 0)>
void call_func(const std::function<R(Args...)>& func, const mexReturnValue& plhs, const mexInputArgs& prhs, mex_index_sequence<>, const std::tuple<DefaultArgs...>& default_args, mex_index_sequence<Idef...>) {
  if (prhs.size() == 0) {
    func(std::get<Idef>(default_args)...);
  } else {
    constexpr std::size_t NEW_N_INPUT_ARGS = 1;
    call_func<Attributes>(func, plhs, prhs, mex_make_index_sequence<NEW_N_INPUT_ARGS>(), *Attributes::default_args, mex_make_index_sequence<sizeof...(Args) - NEW_N_INPUT_ARGS>());
  }
}

// With input and default arguments with return value
template <typename Attributes, typename R, typename... Args, std::size_t... I, typename... DefaultArgs, std::size_t... Idef, MX_OVERLOAD_IF(!(std::is_same<R, void>::value || Attributes::VOID_RESULT) && (sizeof...(I) <= Attributes::MAX_ARGS(sizeof...(Args)) && sizeof...(I) < sizeof...(Args) && sizeof...(I) > 0))>
void call_func(const std::function<R(Args...)>& func, const mexReturnValue& plhs, const mexInputArgs& prhs, mex_index_sequence<I...>, const std::tuple<DefaultArgs...>& default_args, mex_index_sequence<Idef...>) {
  if (prhs.size() == sizeof...(I)) {
    (*plhs).cell_convert<Attributes::CELL_DEPTH>(make_managed_value<Attributes::FACTORY>(func(mxExtract<typename extract_arg<I, Args...>::type>(prhs[I])..., std::get<Idef + Attributes::N_DEFAULT_ARGS + sizeof...(I) - sizeof...(Args)>(default_args)...)));
  } else {
    constexpr std::size_t NEW_N_INPUT_ARGS = sizeof...(I) + 1;
    call_func<Attributes>(func, plhs, prhs, mex_make_index_sequence<NEW_N_INPUT_ARGS>(), *Attributes::default_args, mex_make_index_sequence<sizeof...(Args) - NEW_N_INPUT_ARGS>());
  }
}

// With input and default arguments with no return value
template <typename Attributes, typename R, typename... Args, std::size_t... I, typename... DefaultArgs, std::size_t... Idef, MX_OVERLOAD_IF((std::is_same<R, void>::value || Attributes::VOID_RESULT) && (sizeof...(I) <= Attributes::MAX_ARGS(sizeof...(Args)) && sizeof...(I) < sizeof...(Args) && sizeof...(I) > 0))>
void call_func(const std::function<R(Args...)>& func, const mexReturnValue& plhs, const mexInputArgs& prhs, mex_index_sequence<I...>, const std::tuple<DefaultArgs...>& default_args, mex_index_sequence<Idef...>) {
  if (prhs.size() == sizeof...(I)) {
    func(mxExtract<typename extract_arg<I, Args...>::type>(prhs[I])..., std::get<Idef + Attributes::N_DEFAULT_ARGS + sizeof...(I) - sizeof...(Args)>(default_args)...);
  } else {
    constexpr std::size_t NEW_N_INPUT_ARGS = sizeof...(I) + 1;
    call_func<Attributes>(func, plhs, prhs, mex_make_index_sequence<NEW_N_INPUT_ARGS>(), *Attributes::default_args, mex_make_index_sequence<sizeof...(Args) - NEW_N_INPUT_ARGS>());
  }
}

// With input and default arguments, but number of input arguments exceeds max allowed
template <typename Attributes, typename R, typename... Args, std::size_t... I, typename... DefaultArgs, std::size_t... Idef, MX_OVERLOAD_IF(sizeof...(I) > Attributes::MAX_ARGS(sizeof...(Args)) && sizeof...(I) < sizeof...(Args))>
void call_func(const std::function<R(Args...)>&, const mexReturnValue&, const mexInputArgs&, mex_index_sequence<I...>, const std::tuple<DefaultArgs...>&, mex_index_sequence<Idef...>) {
  mexErrMsgTxt((std::string("Invalid number of input arguments.  Expecting ") + std::to_string(sizeof...(Args) - Attributes::N_DEFAULT_ARGS) + std::string(" to ") + std::to_string(Attributes::MAX_ARGS(sizeof...(Args))) + std::string(" input arguments.")).c_str());
}


template <typename T>
void ClassMembers::AddPropertyGetter(std::string name, const std::function<void(mexGetterValue::Data)>& getter, bool hidden) {
  using TT = typename std::remove_cv<mex_pack::dereference_all_t<T>>::type;
  
  auto ptype = PropertyType::pure_data;
  if (!mex_pack::is_pure_data_type<TT>::value) {
    if (mx_is_array_type<TT>::value || mx_class_prop<TT>::is_cell || is_all_pointer<mx_decay_t<T>>::value) {
      ptype = PropertyType::dynamic_mex_class_object;
    } else {
      ptype = PropertyType::fixed_mex_class_object;
    }
  }

  property_getters_[name] = typename decltype(property_getters_)::mapped_type(getter, ptype);
  if (!hidden) AddPropertyName(name);
}

template <typename T>
void ClassMembers::AddTypeMember(std::string name, const std::function<void(mexGetterValue::Data)>& getter, bool hidden) {
  property_getters_[name] = typename decltype(property_getters_)::mapped_type(getter, PropertyType::type_member);
  if (!hidden) AddPropertyName(name);
}

template <typename Attributes>
void ClassMembers::AddMethod(const std::string& name, const std::function<void(mexOutputArgs, mexInputArgs)>& func) {
  AddMethod(name, [=](mexReturnValue plhs, mexInputArgs prhs) {
                    func(plhs, prhs);
                  },
            Attributes::HIDDEN);
}

template <typename Attributes>
void ClassMembers::AddMethod(const std::string& name, const std::function<void(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])>& func) {
  AddMethod(name, [=](mexReturnValue plhs, mexInputArgs prhs) {
                    func(plhs.size(), plhs.get(), prhs.size(), prhs.get());
                  },
            Attributes::HIDDEN);
}

template <typename Attributes, typename R, typename... Args>
void ClassMembers::AddMethod(const std::string& name, const std::function<R(Args...)>& func) {
  static_assert(Attributes::N_DEFAULT_ARGS <= sizeof...(Args), "Too many default args specified.");

  AddMethod(name, [=](mexReturnValue plhs, mexInputArgs prhs) {
                     call_func<Attributes>(func, plhs, prhs, mex_make_index_sequence<sizeof...(Args) - Attributes::N_DEFAULT_ARGS>(), *Attributes::default_args, mex_make_index_sequence<Attributes::N_DEFAULT_ARGS>());
                  },
            Attributes::HIDDEN);
}

} // namespace mex_pack

