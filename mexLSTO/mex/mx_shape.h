#pragma once

#include <algorithm>
#include <deque>

struct mxArray_tag;
using mxArray = mxArray_tag;
class mxData;


/*==============================================================================
Class:  mxShape

Description:
mxShape provides a convenient interface to access information about the Matlab
data's array size.  

==============================================================================*/

class mxShape {
  std::deque<std::size_t> shape_;

 public:
  mxShape(const mxArray* pm);

  mxShape(const mxData& md);

  // Reverses the dimension order
  mxShape reverse() const;

  auto begin() const -> decltype(shape_.cbegin()) {
    return shape_.cbegin();
  }

  auto end() const -> decltype(shape_.cend()) {
    return shape_.cend();
  }

  // Returns number of dimensions
  std::size_t rank() const {return shape_.size();}

  // Returns length of particular dimension, and 0 if it exceeds the number of
  // dimensions.
  std::size_t extent(std::size_t dim) const;

  // Returns the total number of elements by taking the product of all the
  // dimension lengths.
  std::size_t num_elements() const;

  // Matlab naming
  auto ndims() const -> decltype(rank()) {return rank();}
  auto rows() const -> decltype(extent(0)) {return extent(0);}
  auto cols() const -> decltype(extent(1)) {return extent(1);}
  auto numel() const -> decltype(num_elements()) {return num_elements();}

  // Returns a new shape with the first dimension removed
  mxShape remove_first_dim() const;
};

