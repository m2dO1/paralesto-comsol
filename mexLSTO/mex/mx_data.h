#pragma once

#include "mx_shape.h"
#include "mx_struct.h"


/*==============================================================================
Function:  mxData

Description:
------------
mxData can be thought of as a replacement for const mxArray* types.  It can be
used like const mxArray* objects with the Matlab C/C++ API, but it has
additional support for automatic type conversion using the mxExtract() function,
and some array shape properties from the mxShape class.  The way mxData handles
type conversion will seem simpler in many cases than using mxExtract(), because
it is done through a conversion operator that lets the compiler determine the
conversion type automatically, rather than having to explicitly specify it with
mxExtract().  The following are examples of equivalent Matlab to C conversions
using mxExtract() vs mxData.

  auto value1 = mxExtract<int>(prhs[0]);
  int  value2 = mxData(prhs[0]);

  auto             values1 = mxExtract<std::vector<int>>(prhs[0]);
  std::vector<int> values2 = mxData(prhs[0]);

  auto name1        = mxExtract<std::string>(prhs[0]);
  std::string name2 = mxData(prhs[0]);

mxData also supports a few of the mxShape methods.

==============================================================================*/

class mxData {
 public:
  explicit mxData(const mxArray* mxa) : mxa_(mxa) {}

  virtual ~mxData() = default;

  virtual operator const mxArray*() const {return mxa_;}

  operator mxConstStruct() const {
    return mxConstStruct(operator const mxArray*());
  }

  template <typename T, MX_OVERLOAD_IF(!std::is_convertible<typename std::decay<T>::type, const mxArray*>::value)>
  operator T() const;

  // In rare cases, it is convenient to force the conversion type via a run
  // time argument, when implicit or even explicit conversion is not possible.
  // The argument is only used to infer the conversion operation type.
  template <typename T>
  T conversion_type(T) const;

  // Returns number of dimensions
  std::size_t rank() const {return mxShape(mxa_).rank();}

  // Returns length of particular dimension, and 0 if it exceeds the number of
  // dimensions.
  std::size_t extent(std::size_t dim) const {return mxShape(mxa_).extent(dim);}

  // Returns the total number of elements by taking the product of all the
  // dimension lengths.
  std::size_t num_elements() const {return mxShape(mxa_).num_elements();}

  // Matlab naming
  auto ndims() const -> decltype(rank()) {return rank();}
  auto rows() const -> decltype(extent(0)) {return extent(0);}
  auto cols() const -> decltype(extent(1)) {return extent(1);}
  auto numel() const -> decltype(num_elements()) {return num_elements();}

 protected:
  const mxArray* mxa_;
};

