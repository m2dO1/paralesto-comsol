#pragma once

#include "mex_ref_arg.h"
#include "mx_convert_class.h"
#include "mex_class_instance.tcc"
#include "mex_class_type.tcc"
#include "mex_class_ctor.tcc"
#include "mex_class_objects.h"
#include "demangle.h"


// Default implementation that returns a non-void type to identify if it being used
int AddMexClassMembers(const void*) {return 0;}

namespace mex_pack {
  // Must be included only after all the mexClass definitions
  template <typename T>
  struct is_mex_class_type<T, bool_const<std::is_same<decltype(AddMexClassMembers(std::declval<mx_decay_t<T>*>())), void>::value>, false>
      : std::true_type
  {};

  template <typename T>
  struct is_mex_class_array_type<T, MX_USE_IF(mx_is_array_type<T>::value)> {
    static constexpr bool value = is_mex_class_type<typename mx_array_traits<mx_decay_t<T>>::value_type>::value ||
                                  is_mex_class_array_type<typename mx_array_traits<mx_decay_t<T>>::value_type>::value;
  };

  // If R is a pointer, then it will convert 'v' to a pointer by taking its address, otherwise it does nothing
  template <typename R, typename T, MX_OVERLOAD_IF(std::is_pointer<mx_decay_t<R>>::value)>
  auto make_ptr_or_ref(T&& v) -> decltype(&std::forward<T>(v)) {
    return &std::forward<T>(v);
  }
  template <typename R, typename T, MX_OVERLOAD_IF(!std::is_pointer<mx_decay_t<R>>::value)>
  auto make_ptr_or_ref(T&& v) -> decltype(std::forward<T>(v)) {
    return std::forward<T>(v);
  }

  template <typename T, MX_OVERLOAD_IF(std::is_pointer<T>::value)>
  T get_null_pointer() {
    return T(nullptr);
  }
  template <typename T, MX_OVERLOAD_IF(!std::is_pointer<T>::value)>
  T get_null_pointer() {
    mexErrMsgTxt("get_null_pointer():  Invalid call.");
    mx_decay_t<T>* dummy_ptr {nullptr};
    return *dummy_ptr;
  }
}


template <typename T, MX_OVERLOAD_IF_DEF(mex_pack::is_mex_class_type<mex_pack::dereference_all_t<T>>::value)>
mxArray* mxConvert(T&& c_obj) {
  auto handle = mex_pack::Objects::Add(new mex_pack::ClassInstance<typename std::decay<mex_pack::dereference_all_t<T>>::type>(std::forward<T>(c_obj)));
  if (std::is_same<T, mex_pack::RefArg>::value)
    return mex_pack::Objects::GetMexClassParams(handle, "mexRefArg");
  else
    return mex_pack::Objects::GetMexClassParams(handle);
}

template <typename T, MX_OVERLOAD_IF_DEF(mex_pack::is_mex_class_array_type<T>::value)>
mxArray* mxConvert(T&& c_obj) {
  static_assert(!std::is_rvalue_reference<decltype(c_obj)>::value, "Cannot pass mexClass arrays by value.");
  return mxCellConvert<mx_ndims<decltype(c_obj)>::value>(c_obj);
}

template <typename T, MX_OVERLOAD_IF_DEF(mex_pack::is_mex_class_type<mex_pack::dereference_t<T>>::value && (!std::is_const<mex_pack::dereference_t<T>>::value && (std::is_pointer<T>::value || std::is_reference<T>::value)))>
T mxExtract(const mxArray* pm) {
  // Empty array can be used as a null pointer
  if (std::is_pointer<T>::value) {
    if (mxIsEmpty(pm)) return mex_pack::get_null_pointer<T>();
  }

  auto mx_handle = mxGetProperty(pm, 0, "HANDLE_");
  if (mx_handle == 0) mexErrMsgTxt("Expecting a mexClass object but got something else.");
  auto handle = static_cast<unsigned>(mxGetScalar(mx_handle));
  auto& members_obj = mex_pack::Objects::Get(handle);
  if (members_obj.is_const_) {
    mexErrMsgTxt("Cannot convert const mexClass object to non-const mexClass object.");
  }

  using T_basic = typename std::decay<mex_pack::dereference_t<T>>::type;
  
  auto obj = static_cast<T_basic*>(const_cast<void*>((members_obj.GetUpcastableObject(typeid(T_basic)))));
  if (!obj) obj = const_cast<T_basic*>(mex_pack::ClassType<T_basic>::Downcast(members_obj));
  if (!obj) mexErrMsgTxt((std::string("Cannot convert from ") + mex_pack::demangle(members_obj.type_info_.name()) + " type to " + mex_pack::demangle(typeid(T_basic).name()) + " type.").c_str());
  return mex_pack::make_ptr_or_ref<T>(*obj);
}

template <typename T, MX_OVERLOAD_IF_DEF(mex_pack::is_mex_class_type<mex_pack::dereference_t<T>>::value && !(!std::is_const<mex_pack::dereference_t<T>>::value && (std::is_pointer<T>::value || std::is_reference<T>::value)))>
T mxExtract(const mxArray* pm) {
  // Empty array can be used as a null pointer
  if (std::is_pointer<T>::value) {
    if (mxIsEmpty(pm)) return mex_pack::get_null_pointer<T>();
  }

  auto mx_handle = mxGetProperty(pm, 0, "HANDLE_");
  if (mx_handle == 0) mexErrMsgTxt("Expecting a mexClass object but got something else.");
  auto handle = static_cast<unsigned>(mxGetScalar(mx_handle));
  auto& members_obj = mex_pack::Objects::Get(handle);

  using T_basic = typename std::decay<mex_pack::dereference_t<T>>::type;
  
  if (members_obj.is_const_) {
    auto obj = static_cast<const T_basic*>(members_obj.GetUpcastableObject(typeid(T_basic)));
    if (!obj) obj = mex_pack::ClassType<T_basic>::Downcast(members_obj);
    if (!obj) mexErrMsgTxt((std::string("Cannot convert from ") + mex_pack::demangle(members_obj.type_info_.name()) + " type to " + mex_pack::demangle(typeid(T_basic).name()) + " type.").c_str());
    return mex_pack::make_ptr_or_ref<T>(*obj);
  } else {
    auto obj = static_cast<T_basic*>(const_cast<void*>((members_obj.GetUpcastableObject(typeid(T_basic)))));
    if (!obj) obj = const_cast<T_basic*>(mex_pack::ClassType<T_basic>::Downcast(members_obj));
    if (!obj) mexErrMsgTxt((std::string("Cannot convert from ") + mex_pack::demangle(members_obj.type_info_.name()) + " type to " + mex_pack::demangle(typeid(T_basic).name()) + " type.").c_str());
    return mex_pack::make_ptr_or_ref<T>(*obj);
  }
}

template <typename T, MX_OVERLOAD_IF_DEF(mex_pack::is_pure_data_type_reference<T>::value)>
T mxExtract(const mxArray* pm) {
  // Empty array can be used as a null pointer
  if (std::is_pointer<T>::value) {
    if (mxIsEmpty(pm)) return mex_pack::get_null_pointer<T>();
  }

  auto* ref_arg = mxExtract<mex_pack::RefArg*>(pm);
  return mex_pack::make_ptr_or_ref<T>(ref_arg->extract<mex_pack::dereference_t<T>>());
}

