#pragma once

#include "mx_traits.h"
#include <algorithm>
#include <cassert>
#include <limits>
#include <memory>
#include <iterator>
#include <tuple>
#include <type_traits>
#include <vector>

/*=============================================================================
Class Template:  mx_c_traits

Description:
mx_c_traits is the interface by which mxExtract()/mxConvert() can access properties of the C
object it is trying to convert to or from.  The main purpose is to hide from
mxExtract()/mxConvert() whether or not the C object it is trying to use is an array or an
array element type.  mx_c_traits provides the same interface regardless.
The template argument NESTING_COUNT is only used by mxCellConvert().
=============================================================================*/

// Primary template for C array types
template <typename A, int NESTING_COUNT=std::numeric_limits<int>::max(), typename = void>
struct mx_c_traits {
 private:
  using array_traits = mx_array_traits<mx_decay_t<A>>;
  using array_value_traits = mx_c_traits<typename array_traits::value_type, NESTING_COUNT-1>;

 public:
  // Identifies if the array element is complex or not
  static constexpr bool is_complex = array_value_traits::is_complex;

  // Identifies the array element mxClassID value
  static constexpr mxClassID_ elem_class_id = array_value_traits::elem_class_id;

  // Identifies if the array element must be used in a Matlab cell array
  static constexpr bool use_cell_array = array_value_traits::use_cell_array;

  // Returns the total shape of the C object, which includes all the nested
  // arrays.
  static void shape(const A& c_obj, std::vector<mwSize>* s, bool=true) {
    auto c_obj_size = array_traits::size(c_obj);

    std::size_t n_elems = 1;
    for (auto extent : c_obj_size) n_elems *= extent;

    if (n_elems > 0) {
      std::copy(c_obj_size.begin(), c_obj_size.end(), std::back_inserter(*s));
      array_value_traits::shape(*array_traits::begin(c_obj), s, false);
    }
  }

  // Writes the Matlab data to the C object.  The Matlab array elements are read
  // linearly and simply copied to the C object in linear order.  Note that
  // Matlab data is natively stored in reverse dimension order from C data
  // types.
  template <typename U, typename A_iter>
  static void mx_write_to_c(U& mx_elem_proxy, A_iter c_obj) {
    std::size_t n_elems = 1;
    for (auto extent : array_traits::size(*c_obj)) n_elems *= extent;
    assert(n_elems > 0);  // Check that C object does not have a zero length size
    auto iter = array_traits::begin(*c_obj);

    for (std::size_t elem_idx=0; elem_idx < n_elems; ++elem_idx) {
      array_value_traits::mx_write_to_c(mx_elem_proxy, iter);
      ++iter;
    }
  }

  // Writes the C data to the Matlab object.  Like mx_write_to_c(), everything
  // is done in linear order from each side.
  template <typename U, typename V>
  static void mx_read_from_c(U&& c_obj, V& mx_elem_proxy) {
    std::size_t n_elems = 1;
    for (auto extent : array_traits::size(c_obj)) n_elems *= extent;
    auto iter = array_traits::begin(c_obj);

    for (std::size_t elem_idx=0; elem_idx < n_elems; ++elem_idx) {
      array_value_traits::mx_read_from_c(*iter, mx_elem_proxy);
      ++iter;
    }
  }
};

// Specialization for C element types
template <typename T, int NESTING_COUNT>
struct mx_c_traits<T, NESTING_COUNT, MX_USE_IF(mx_is_element_type<T>::value && NESTING_COUNT > 0)> {
  static constexpr bool is_complex = mx_element_traits<T>::is_complex;

  static constexpr mxClassID_ elem_class_id = mx_element_traits<mx_value_t<T>>::class_id;

  static constexpr bool use_cell_array = mx_element_use_cell_array<mx_element_traits<mx_value_t<T>>>::value;

  static void shape(const T&, std::vector<mwSize>* s, bool first_dim=true) {
    if (first_dim) s->push_back(1);
  }

  template <typename U, typename T_iter>
  static void mx_write_to_c(U& mx_elem_proxy, T_iter c_obj) {
    // If mx_elem_proxy.valid() is false, that means the C object is larger than
    // the Matlab data, which is okay.
    if (mx_elem_proxy.valid()) {
      mx_element_traits<T>::convert_to_c(static_cast<const U&>(mx_elem_proxy), c_obj);
      mx_elem_proxy.next();
    }
  }

  template <typename U, typename V>
  static void mx_read_from_c(U&& c_obj, V& mx_elem_proxy) {
    assert(mx_elem_proxy.valid());  // Check if Matlab array is large enough to convert C data
    mx_element_traits<T>::convert_to_matlab(c_obj, mx_elem_proxy);
    mx_elem_proxy.next();
  }
};


/*=============================================================================
Class Template:  mx_class_prop
Description:  Provides various properties of the provided C type based on its
class ID.
=============================================================================*/
template <typename T, bool = (mx_is_array_type<T>::value || mx_is_element_type<T>::value) && mx_is_pure_data_type<T>::value>
struct mx_class_prop {
  static constexpr bool is_numerical = false;
  static constexpr bool is_logical = false;
  static constexpr bool is_struct = false;
  static constexpr bool is_string = false;
  static constexpr bool is_cell = false;
  static constexpr bool use_cell_array = false;
};

template <typename T>
struct mx_class_prop<T, true> {
  static constexpr bool is_numerical = (mx_c_traits<T>::elem_class_id == mxClassID_::mxINT8_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxUINT8_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxINT16_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxUINT16_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxINT32_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxUINT32_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxINT64_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxUINT64_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxSINGLE_CLASS) ||
                                       (mx_c_traits<T>::elem_class_id == mxClassID_::mxDOUBLE_CLASS);

  static constexpr bool is_logical = (mx_c_traits<T>::elem_class_id == mxClassID_::mxLOGICAL_CLASS);

  static constexpr bool is_struct = (mx_c_traits<T>::elem_class_id == mxClassID_::mxSTRUCT_CLASS);

  static constexpr bool is_string = (mx_c_traits<T>::elem_class_id == mxClassID_::mxCHAR_CLASS);

  static constexpr bool is_cell = (mx_c_traits<T>::elem_class_id == mxClassID_::mxCELL_CLASS);

  static constexpr bool use_cell_array = mx_c_traits<T>::use_cell_array;
};

