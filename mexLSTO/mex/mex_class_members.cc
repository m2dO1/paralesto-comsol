#include "mex_class_members.h"


namespace mex_pack {

ClassMembers::~ClassMembers() {}

std::unordered_set<std::string> ClassMembers::GetMethodNames() const {
  std::unordered_set<std::string> method_names;
  for (const auto& elem : methods_) {
    method_names.insert(elem.first);
  }
  return method_names;
}

std::unordered_set<std::string> ClassMembers::GetPropertyNames(PropertyType ptype) const {
  std::unordered_set<std::string> property_names;

  for (const auto& elem : property_getters_) {
    if (elem.second.second == ptype) {
      property_names.insert(elem.first);
    }
  }
  for (const auto& elem : property_setters_) {
    if (elem.second.second == ptype) {
      property_names.insert(elem.first);
    }
  }
  return property_names;
}

bool ClassMembers::CallMethod(std::string name, mexOutputArgs plhs, mexInputArgs prhs) {
  if (methods_.count(name)) {
    methods_[name](plhs, prhs);
    return true;
  } else {
    return false;
  }
}

void ClassMembers::AddPropertySetter(std::string name, const std::function<void(mexInputArgs::Data)>& setter) {
  property_setters_[name] = typename decltype(property_setters_)::mapped_type(setter, PropertyType::pure_data);
  AddPropertyName(name);
}

bool ClassMembers::GetProperty(std::string name, mexGetterValue::Data out) {
  if (property_getters_.count(name)) {
    std::get<0>(property_getters_[name])(out);
    return true;
  } else {
    return false;
  }
}

bool ClassMembers::SetProperty(std::string name, mexInputArgs::Data in) {
  if (property_setters_.count(name)) {
    std::get<0>(property_setters_[name])(in);
    return true;
  } else {
    return false;
  }
}

void ClassMembers::AddMethod(std::string name, const std::function<void(mexReturnValue, mexInputArgs)>& method, bool hidden) {
  methods_[name] = method;
  if (!hidden) AddPropertyName(name);
}

void ClassMembers::AddPropertyName(std::string name) {
  if (std::find(ordered_member_names_.begin(), ordered_member_names_.end(), name) == ordered_member_names_.end()) {
    ordered_member_names_.push_back(name);
  }
}

} // namespace mex_pack

