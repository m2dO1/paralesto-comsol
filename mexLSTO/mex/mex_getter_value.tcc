#pragma once

#include "mex_getter_value.h"
#include "mex_type_traits.h"


namespace mex_pack {

template <typename T>
mxArray* GetterValueConverter::convert(T&& c_obj) {
  if (mex_pack::is_null_pointer(c_obj)) return mxCreateDoubleMatrix(0, 0, mxREAL);

  return mxConvert(dereference(std::forward<T>(c_obj)));
}

template <int CELL_DEPTH, typename T>
mxArray* GetterValueConverter::cell_convert(T&& c_obj) {
  if (mex_pack::is_null_pointer(c_obj)) return mxCreateDoubleMatrix(0, 0, mxREAL);

  return mxCellConvert<CELL_DEPTH>(dereference(std::forward<T>(c_obj)));
}

} // namespace mex_pack

