#pragma once

#include "mex_class.h"
#include <memory>


namespace mex_pack {

// Abstract interface to a managed object without knowing its type
class ManagedObjectIntf {
 public:
  virtual ~ManagedObjectIntf() = 0;
};

// Concrete class that holds the managed object
template <typename T>
class ManagedObjectHolder : public ManagedObjectIntf, public std::unique_ptr<T> {
 public:
  using std::unique_ptr<T>::unique_ptr;
};

// ManagedObject allows for sharing of a managed object without knowing its type.
// An ManagedObject object may be empty, meaning it doesn't manage anything
using ManagedObject = std::shared_ptr<ManagedObjectIntf>;

// Helper functions.

// Creates a managed object
template <typename T>
ManagedObject MakeManagedObject(T&& obj) {
  return ManagedObject(new ManagedObjectHolder<T>((new T(std::forward<T>(obj)))));
}

// Gets the managed object
template <typename T>
T& GetManagedObject(ManagedObject managed_obj);


/*==============================================================================
Class: ClassInstance

Description:  Exposes a mexClass instance object to Matlab
Contains a reference to the object for which ClassMembers will store its member
access functions, unless it is just a type, in which case there is no actual
object instance, and the member access functions just refer to any static
members if there are any.
Upon construction of ClassInstance, member access functions will be added to the
ClassMembers base class based on the mexClass definition.
==============================================================================*/

template <typename T>
class ClassInstance : public ClassMembers {
  // Constructor for unmanaged objects
  ClassInstance(const T& obj, bool is_const)
      : ClassMembers(typeid(T), is_const)
      , object_referenced_(obj)
  {
    Initialize();
  }

  void Initialize();

 public:
  // Constructors for pointers and references.  Will not manage them.
  ClassInstance(T* obj_ptr) : ClassInstance(*obj_ptr, false) {}
  ClassInstance(const T* obj_ptr) : ClassInstance(*obj_ptr, true) {}
  ClassInstance(T& obj) : ClassInstance(obj, false) {}
  ClassInstance(const T& obj) : ClassInstance(obj, true) {}

  // Constructors for values.  Will manage them.
  ClassInstance(T&& obj)
      : ClassMembers(typeid(T), false)
      , managed_obj_(MakeManagedObject(std::move(obj)))
      , object_referenced_(GetManagedObject<T>(managed_obj_))
  {
    Initialize();
  }

  // Constructor for smart pointers.  Will manage smart pointer.
  template <typename U, MX_OVERLOAD_IF(is_smart_pointer<U>::value)>
  ClassInstance(U&& smart_ptr)
      : ClassMembers(typeid(T), std::is_const<typename U::element_type>::value)
      , managed_obj_(MakeManagedObject(std::forward<U>(smart_ptr)))
      , object_referenced_(*GetManagedObject<U>(managed_obj_))
  {
    Initialize();
  }

  const void* GetObject() const override {
    return static_cast<const void*>(&object_referenced_);
  }

  const void* GetUpcastableObject(const std::type_index& type_info) const override {
    if (type_info == type_info_) {
      return static_cast<const void*>(&object_referenced_);
    } else {
      const void* upcasted_obj_ptr = nullptr;
      AddMexClassMembers(&object_referenced_, MembersAdderBase(type_info, &upcasted_obj_ptr));
      return upcasted_obj_ptr;
    }
  }

  // Get type object
  ClassMembers* GetType() const override;

 private:
  // If ClassInstance manages the object (i.e. at least partly responsible for its clean up),
  // then it is stored here.
  ManagedObject managed_obj_;

  // The object being referenced by ClassInstance
  const T& object_referenced_;
};

} // namespace mex_pack

