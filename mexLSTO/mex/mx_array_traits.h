#pragma once

#include "mx_element_traits.h"
#include "mx_sfinae_helpers.h"
#include <array>
#include <type_traits>
#include <vector>


/*==============================================================================
Class Template:  mx_array_traits

Description:
All C types that are intended to be used as an array type must have an
implementation of mx_array_traits defined for it.  mxConvert automatically
handles nesting of array types.  For example, one wants to use a nested arrays
of type std::deque<std::vector<T>>, you do not have define a specialization of
that entire type as long as a specialization for std::deque and std::vector are
already defined separately (which they are).

Properties:
  value_type : A type alias to the element type

Functions:
  size() : Returns the size of the array object as a vector with a length equal
  to the number of dimensions of the array type.  For example, a 1 dimensional
  array object, like std::deque, will return a size vector with a length of 1,
  and the value is equal to its current size.  Note that the size only indicates
  the size over this array type, not including any additional sizes that may
  result from nested arrays.  Note, size() can be overloaded for a const and
  non-const input argument to allow different implementations for computing the
  size when converting from C to Matlab vs Matlab to C.  This is rare, however,
  and normally a single size() function with a const input argument is all that
  is needed.

  begin() : Iterator function for writing.  This only need to be defined if you
  need to convert from Matlab to C.

  begin() const : Iterator function for reading.  This only neeed to be defined if
  you need to convert from C to Matlab.
==============================================================================*/

// Primary template.  Never to be used.
template <typename A, typename = void>
struct mx_array_traits;


// Metafunction that returns an indication of the type being an array type or not.
// It is designed to check mx_is_element_type<> first before moving to check if
// mx_array_traits<>::value_type exists due to ambiguity errors with some
// custom types.
template <typename T, bool = mx_is_element_type<mx_decay_t<T>>::value>
struct mx_is_array_type : std::false_type {};

template <typename T>
struct mx_is_array_type<T, false> {
  template <typename U, typename = void>
  struct mx_is_array_type_ : std::false_type {};

  template <typename U>
  struct mx_is_array_type_<U, mx_void_t<typename mx_array_traits<mx_decay_t<U>>::value_type>>
      : std::true_type
  {};

  static constexpr bool value = mx_is_array_type_<T>::value;
};


// Specialization for element types
template <typename T>
struct mx_array_traits<T, MX_USE_IF(mx_is_element_type<T>::value)> {
  using value_type = void;
};

// Base class implementation for STL sequence container-like classes
template <typename A>
struct mx_array_traits_seq_container {
  using value_type = typename A::value_type;

  static std::vector<std::size_t> size(const A& a) {return {a.size()};}

  static auto begin(A& a) -> decltype(a.begin()) {return a.begin();}

  static auto begin(const A& a) -> decltype(a.begin()) {return a.begin();}
};

// Implementation for STL sequence containers except for std::array
template <template <typename, typename> class SEQC, typename T>
struct mx_array_traits<SEQC<T, std::allocator<T>>, void>
    : mx_array_traits_seq_container<SEQC<T, std::allocator<T>>> {
};

// Implementation for std::array<T, N>
template <typename T, std::size_t N>
struct mx_array_traits<std::array<T, N>, void>
    : mx_array_traits_seq_container<std::array<T, N>> {
};

// Implementation for C arrays
template <typename T, std::size_t N>
struct mx_array_traits<T[N], void> {
  using value_type = T;

  static std::vector<std::size_t> size(const T (&)[N]) {return {N};}

  static auto begin(T (& a)[N]) -> decltype(std::begin(a)) {return std::begin(a);}

  static auto begin(const T (& a)[N]) -> decltype(std::begin(a)) {return std::begin(a);}
};

// Implementation for std::set & std::multiset (C++ to Matlab only)
template <template <typename, typename, typename> class SETC, typename T>
struct mx_array_traits<SETC<T, std::less<T>, std::allocator<T>>, void> {
  using A = SETC<T, std::less<T>, std::allocator<T>>;
  using value_type = typename A::value_type;

  static std::vector<std::size_t> size(const A& a) {return {a.size()};}

  static auto begin(const A& a) -> decltype(a.begin()) {return a.begin();}
};

// Implementation for std::unordered_set & std::unordered_multiset (C++ to Matlab only)
template <template <typename, typename, typename, typename> class UNORDERED_SETC, typename T>
struct mx_array_traits<UNORDERED_SETC<T, std::hash<T>, std::equal_to<T>, std::allocator<T>>, void> {
  using A = UNORDERED_SETC<T, std::hash<T>, std::equal_to<T>, std::allocator<T>>;
  using value_type = typename A::value_type;

  static std::vector<std::size_t> size(const A& a) {return {a.size()};}

  static auto begin(const A& a) -> decltype(a.begin()) {return a.begin();}
};

