#include "mx_shape.h"
#include "mx_data.h"
#include "matrix.h"


mxShape::mxShape(const mxArray* pm) {
  const auto* dims = mxGetDimensions(pm);
  auto n_dims = mxGetNumberOfDimensions(pm);
  shape_.resize(n_dims);
  std::copy_n(dims, n_dims, shape_.begin());
}

mxShape::mxShape(const mxData& md) : mxShape(md.operator const mxArray*()) {}

mxShape mxShape::reverse() const {
  mxShape new_shape(*this);
  std::reverse(new_shape.shape_.begin(), new_shape.shape_.end());
  return new_shape;
}

std::size_t mxShape::extent(std::size_t dim) const {
  if (dim < shape_.size()) {
    return shape_[dim];
  } else {
    return 0;
  }
}

std::size_t mxShape::num_elements() const {
  std::size_t n_elems = 1;
  for (auto extent : shape_) n_elems *= extent;
  return n_elems;
}

mxShape mxShape::remove_first_dim() const {
  mxShape new_shape(*this);
  new_shape.shape_.pop_front();
  return new_shape;
}

