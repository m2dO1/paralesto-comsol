/*
The following code is copied from here:

https://stackoverflow.com/questions/281818/unmangling-the-result-of-stdtype-infoname
*/

#pragma once
#include <string>
#include <typeinfo>


namespace mex_pack {

std::string demangle(const char* name);

} // namespace mex_pack

