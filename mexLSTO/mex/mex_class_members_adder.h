#pragma once

#include "mex_class_ctor.h"
#include "mex_output_args_decl.h"
#include "exposure_attributes.h"
#include "mex_type_traits.h"
#include <string>
#include <tuple>
#include <typeinfo>
#include <typeindex>

// forward declarations
class mexInputArgs;


namespace mex_pack {

// Returns an Attributes type with a specifc attribute set to the given value
template <typename Attributes, typename Type, template<Type, typename> class SetAttribute, Type CurrValue, Type NewValue>
using UpdateAttribute = typename std::conditional<CurrValue == NewValue, Attributes, SetAttribute<NewValue, Attributes>>::type;

// Used for partial template specialization
struct ReadOnlySpecialization {};
struct BindInstanceSpecialization {};

// Returns a unique type to specialize MembersAdder with depending on the attribute values.
template <typename Attributes>
using GetSpecializationAttribute = typename std::conditional<Attributes::BIND_INSTANCE, BindInstanceSpecialization, typename std::conditional<Attributes::READ_ONLY, ReadOnlySpecialization, void>::type>::type;


// forward declaration
class ClassMembers;

// forward declaration
template <typename C, typename Attributes=DefaultAttributes, typename SpecializationAttribute=GetSpecializationAttribute<Attributes>>
class MembersAdder;


class MembersAdderBase {
 protected:
  ClassMembers* attr_ = nullptr;
  ClassMembers* attr_for_base_ = nullptr;
  const std::type_index* type_info_ = nullptr;
  const void** upcasted_obj_ptr_ = nullptr;

 public:
  MembersAdderBase() = default;

  MembersAdderBase(ClassMembers* attr, bool add_base_classes)
      : attr_(add_base_classes ? nullptr : attr)
      , attr_for_base_(add_base_classes ? attr : nullptr)
  {}

  // Used for upcasting
  MembersAdderBase(const std::type_index& type_info, const void** upcasted_obj_ptr)
      : type_info_(&type_info)
      , upcasted_obj_ptr_(upcasted_obj_ptr)
  {}

  virtual ~MembersAdderBase() = default;

  // def(self) in the mexClass definition executes this function to return
  // the definition object for the type T.
  template <typename T>
  MembersAdder<T> operator()(const T* obj);

  bool is_const_object() const;

  // Can be used in mexClass definition to get a non-const pointer of self.
  // Returns nullptr if it is not const.
  template <typename T>
  T* nonconst_cast(const T* obj) const;
};


template <typename C>
class MembersAdderParams : public MembersAdderBase {
 protected:
  const C* obj_ = nullptr;
  const mexInputArgs* prhs_ = nullptr;
  C** output_obj_ptr_ = nullptr;
  const C** downcasted_obj_ptr_ = nullptr;
  const void* obj_to_downcast_ = nullptr;

  // This should only be called and used if you know for sure obj_ was originally non-const
  C* nonconst_obj();

  MembersAdderParams() = default;

  MembersAdderParams(const MembersAdderBase& params) : MembersAdderBase(params) {}
};


// Adds class members
template <typename C, typename Attributes, typename SpecializationAttribute>
class MembersAdder : public MembersAdderParams<C> {
  using MembersAdderParams<C>::prhs_;
  using MembersAdderParams<C>::output_obj_ptr_;
  using MembersAdderParams<C>::downcasted_obj_ptr_;
  using MembersAdderParams<C>::type_info_;
  using MembersAdderParams<C>::obj_to_downcast_;
  using MembersAdderParams<C>::attr_;
  using MembersAdderParams<C>::attr_for_base_;
  using MembersAdderParams<C>::obj_;
  using MembersAdderParams<C>::upcasted_obj_ptr_;
  using MembersAdderParams<C>::is_const_object;
  using MembersAdderParams<C>::nonconst_obj;

 public:
  // Move constructor
  MembersAdder(MembersAdderParams<C>&& p);

  // Used to call ctor()
  MembersAdder(const mexInputArgs* prhs, C** new_obj_ptr);

  // Used for downcasting
  MembersAdder(const std::type_index& input_type_info, const void* input_obj, const C** downcasted_obj_ptr);

  // Used for packages
  MembersAdder(ClassMembers* attr);

  // Used for instance objects
  MembersAdder(const C* obj, const MembersAdderBase& params);

  // Exposure attribute functions
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetReadOnlyAttribute, Attributes::READ_ONLY, true>> read_only() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetReadOnlyAttribute, Attributes::READ_ONLY, true>>(std::move(*this));
  }
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetHiddenAttribute, Attributes::HIDDEN, true>> hidden() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetHiddenAttribute, Attributes::HIDDEN, true>>(std::move(*this));
  }
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetFactoryAttribute, Attributes::FACTORY, true>> factory() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetFactoryAttribute, Attributes::FACTORY, true>>(std::move(*this));
  }
  template <int Value>
  MembersAdder<C, UpdateAttribute<Attributes, int, SetCellDepthAttribute, Attributes::CELL_DEPTH, Value>> cell_depth() {
    return MembersAdder<C, UpdateAttribute<Attributes, int, SetCellDepthAttribute, Attributes::CELL_DEPTH, Value>>(std::move(*this));
  }
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetBindInstanceAttribute, Attributes::BIND_INSTANCE, true>> bind_instance() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetBindInstanceAttribute, Attributes::BIND_INSTANCE, true>>(std::move(*this));
  }
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetVoidResultAttribute, Attributes::VOID_RESULT, true>> void_result() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetVoidResultAttribute, Attributes::VOID_RESULT, true>>(std::move(*this));
  }
  template <int N_OPTIONAL_ARGS=-1, typename... Args>
  MembersAdder<C, SetDefaultArgsAttribute<N_OPTIONAL_ARGS, Attributes, Args...>> default_args(Args... args) {
    using NewAttributes = SetDefaultArgsAttribute<N_OPTIONAL_ARGS, Attributes, Args...>;
    NewAttributes::default_args.reset(new std::tuple<Args...>(args...));
    return MembersAdder<C, NewAttributes>(std::move(*this));
  }

  template <int MIN_ARGS, typename... Args>
  MembersAdder<C> ctor();

  template <typename... Args>
  MembersAdder<C> ctor() {
    return ctor<0, Args...>();
  }

  // Add factory method based on specified ctor
  template <int MIN_ARGS, typename... Args>
  MembersAdder<C> ctor_method(std::string name);

  template <typename... Args>
  MembersAdder<C> ctor_method(std::string name) {
    return ctor_method<0, Args...>(name);
  }

  // Add const method with return value
  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, R(C::* method)(Args...) const);

  // Add const method with no return value
  template <typename... Args>
  MembersAdder<C> method(std::string name, void(C::* method)(Args...) const);

  // Add non-const method with return value
  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, R(C::* method)(Args...));

  // Add non-const method with no return value
  template <typename... Args>
  MembersAdder<C> method(std::string name, void(C::* method)(Args...));

  // Add free function as method
  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, std::function<R(Args...)> func);

  // Add free function as method
  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, R(* func)(Args...));

  // Add assignment operator as a method (defaults to copy assignment)
  template <typename Arg=const C&>
  MembersAdder<C> assignment_method(std::string name) {
    return method<C&, Arg>(name, &C::operator=);
  }

  // Add property with const getter() method
  template <typename T>
  MembersAdder<C> property(std::string name, T(C::* getter)() const);

  // Add property with getter() method that returns a read-only value
  template <typename T, MX_OVERLOAD_IF(is_pure_data_type<T>::value && !((std::is_reference<T>::value || is_all_pointer<T>::value) && !std::is_const<dereference_all_t<T>>::value))>
  MembersAdder<C> property(std::string name, T(C::* getter)());

  // Add property with getter() method that returns a read and writeable value
  template <typename T, MX_OVERLOAD_IF(is_pure_data_type<T>::value && (std::is_reference<T>::value || is_all_pointer<T>::value) && !std::is_const<dereference_all_t<T>>::value)>
  MembersAdder<C> property(std::string name, T(C::* getter)());

  // Add data member
  template <typename T, MX_OVERLOAD_IF(is_pure_data_type<T>::value && !std::is_function<T>::value)>
  MembersAdder<C> property(std::string name, T C::* data_member);

  // Add const data member as read-only
  template <typename T, MX_OVERLOAD_IF(!std::is_function<T>::value)>
  MembersAdder<C> property(std::string name, const T C::* data_member);

  // Add const pointer data member as read-only
  template <typename T>
  MembersAdder<C> property(std::string name, const T* C::* data_member);

  // Add const data member reference as read-only
  template <typename T>
  MembersAdder<C> property_ref_(std::string name, const T* data_member);

  // Add data member reference
  template <typename T, MX_OVERLOAD_IF(is_pure_data_type<T>::value)>
  MembersAdder<C> property_ref_(std::string name, T* data_member);

  // Add property with getter() and setter() methods
  template <typename T1, typename T2>
  MembersAdder<C> property(std::string name, T1(C::* getter)() const, void(C::* setter)(T2));

  // Add property with non-const getter() and setter() methods
  template <typename T1, typename T2>
  MembersAdder<C> property(std::string name, T1(C::* getter)(), void(C::* setter)(T2));

  // Add mexClass type data member
  template <typename T, MX_OVERLOAD_IF(!is_pure_data_type<T>::value && !std::is_function<T>::value)>
  MembersAdder<C> property(std::string name, T C::* data_member);

  // Add mexClass type property with non-const getter() method
  template <typename T, MX_OVERLOAD_IF(!is_pure_data_type<T>::value)>
  MembersAdder<C> property(std::string name, T(C::* getter)());

  // Add non-const mexClass type data member reference
  template <typename T, MX_OVERLOAD_IF(!is_pure_data_type<T>::value)>
  MembersAdder<C> property_ref_(std::string name, T* data_member);

  // Add static data member for pure data types
  template <typename T, MX_OVERLOAD_IF(is_pure_data_type<T>::value && !std::is_function<T>::value)>
  MembersAdder<C> property(std::string name, T* static_member);

  // Add static data member for mexClass types
  template <typename T, MX_OVERLOAD_IF(!is_pure_data_type<T>::value && !std::is_function<T>::value)>
  MembersAdder<C> property(std::string name, T* static_member);

  template <typename T>
  MembersAdder<C> property(std::string name, const T* static_member);

  // Add base class
  template <typename BaseClass>
  MembersAdder<C> base_class();
 
  // Add a mexClass type member
  template <typename Type, MX_OVERLOAD_IF(!is_pure_data_type<Type>::value)>
  MembersAdder<C> type_member(std::string name);

  // These are used by packages

  // Add class
  template <typename T>
  void class_(std::string name) {
    type_member<T>(name);
  }

  // Add function
  template <typename R=void, typename... Args>
  void function_(std::string name, R(& func)(Args...)) {
    method(name, func);
  }

  // Add object
  template <typename T>
  void object_(std::string name, T&& obj) {
    property(name, &std::forward<T>(obj));
  }

  // Add package
  template <typename T>
  void package_(std::string name);
};


template <typename C, typename Attributes>
class MembersAdder<C, Attributes, BindInstanceSpecialization> : public MembersAdderParams<C> {
  using MembersAdderParams<C>::attr_;
  using MembersAdderParams<C>::obj_;
  using MembersAdderParams<C>::is_const_object;
  using MembersAdderParams<C>::nonconst_obj;

 public:
  // Move constructor
  MembersAdder(MembersAdderParams<C>&& p)
      : MembersAdderParams<C>(std::move(p))
  {}

  // Exposure attribute functions
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetHiddenAttribute, Attributes::HIDDEN, true>> hidden() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetHiddenAttribute, Attributes::HIDDEN, true>>(std::move(*this));
  }
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetFactoryAttribute, Attributes::FACTORY, true>> factory() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetFactoryAttribute, Attributes::FACTORY, true>>(std::move(*this));
  }
  template <int Value>
  MembersAdder<C, UpdateAttribute<Attributes, int, SetCellDepthAttribute, Attributes::CELL_DEPTH, Value>> cell_depth() {
    return MembersAdder<C, UpdateAttribute<Attributes, int, SetCellDepthAttribute, Attributes::CELL_DEPTH, Value>>(std::move(*this));
  }
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetVoidResultAttribute, Attributes::VOID_RESULT, true>> void_result() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetVoidResultAttribute, Attributes::VOID_RESULT, true>>(std::move(*this));
  }
  template <int N_OPTIONAL_ARGS=-1, typename... Args>
  MembersAdder<C, SetDefaultArgsAttribute<N_OPTIONAL_ARGS, Attributes, Args...>> default_args(Args... args) {
    using NewAttributes = SetDefaultArgsAttribute<N_OPTIONAL_ARGS, Attributes, Args...>;
    NewAttributes::default_args.reset(new std::tuple<Args...>(args...));
    return MembersAdder<C, NewAttributes>(std::move(*this));
  }

  // Add method from free function that accepts a const object reference
  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, std::function<R(const C&, Args...)> func);

  template <typename... Args>
  MembersAdder<C> method(std::string name, std::function<void(const C&, Args...)> func);

  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, R(* func)(const C&, Args...));

  // Add method from free function that accepts a non-const object reference
  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, std::function<R(C&, Args...)> func);

  template <typename... Args>
  MembersAdder<C> method(std::string name, std::function<void(C&, Args...)> func);

  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, R(* func)(C&, Args...));

  // Add method from free function that accepts a const object pointer
  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, std::function<R(const C*, Args...)> func);

  template <typename... Args>
  MembersAdder<C> method(std::string name, std::function<void(const C*, Args...)> func);

  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, R(* func)(const C*, Args...));

  // Add method from free function that accepts a non-const object pointer
  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, std::function<R(C*, Args...)> func);

  template <typename... Args>
  MembersAdder<C> method(std::string name, std::function<void(C*, Args...)> func);

  template <typename R, typename... Args>
  MembersAdder<C> method(std::string name, R(* func)(C*, Args...));

  // Add property with getter() function that accepts a const object reference
  template <typename R, MX_OVERLOAD_IF(is_pure_data_type<R>::value && !((std::is_reference<R>::value || is_all_pointer<R>::value) && !std::is_const<dereference_all_t<R>>::value))>
  MembersAdder<C> property(std::string name, std::function<R(const C&)> func);

  // Add property with getter() function that accepts a non-const object reference
  template <typename R, MX_OVERLOAD_IF(is_pure_data_type<R>::value && (std::is_reference<R>::value || is_all_pointer<R>::value) && !std::is_const<dereference_all_t<R>>::value)>
  MembersAdder<C> property(std::string name, std::function<R(C&)> func);

  // Add property with getter() and setter() functions
  template <typename R>
  MembersAdder<C> property(std::string name, std::function<R(const C&)> getter_func, std::function<void(C&, R)> setter_func);

  // Add mexClass type property with getter() function that accepts a const object reference
  template <typename R, MX_OVERLOAD_IF(!is_pure_data_type<R>::value)>
  MembersAdder<C> property(std::string name, std::function<R(const C&)> func);

  // Add mexClass type property with getter() function that accepts a non-const object reference
  template <typename R, MX_OVERLOAD_IF(!is_pure_data_type<R>::value)>
  MembersAdder<C> property(std::string name, std::function<R(C&)> func);
};


template <typename C, typename Attributes>
class MembersAdder<C, Attributes, ReadOnlySpecialization> : public MembersAdderParams<C> {
  using MembersAdderParams<C>::attr_;
  using MembersAdderParams<C>::obj_;
  using MembersAdderParams<C>::is_const_object;
  using MembersAdderParams<C>::nonconst_obj;

 public:
  // Move constructor
  MembersAdder(MembersAdderParams<C>&& p)
      : MembersAdderParams<C>(std::move(p))
  {}

  // Exposure attribute functions
  MembersAdder<C, UpdateAttribute<Attributes, bool, SetHiddenAttribute, Attributes::HIDDEN, true>> hidden() {
    return MembersAdder<C, UpdateAttribute<Attributes, bool, SetHiddenAttribute, Attributes::HIDDEN, true>>(std::move(*this));
  }
  template <int Value>
  MembersAdder<C, UpdateAttribute<Attributes, int, SetCellDepthAttribute, Attributes::CELL_DEPTH, Value>> cell_depth() {
    return MembersAdder<C, UpdateAttribute<Attributes, int, SetCellDepthAttribute, Attributes::CELL_DEPTH, Value>>(std::move(*this));
  }

  // Add property with const getter() method
  template <typename T>
  MembersAdder<C> property(std::string name, T(C::* getter)() const);

  // Add property with getter() method
  template <typename T>
  MembersAdder<C> property(std::string name, T(C::* getter)());

  // Add data member (const or non-const)
  template <typename T, MX_OVERLOAD_IF(!std::is_function<T>::value)>
  MembersAdder<C> property(std::string name, T C::* data_member);

  // Add data member reference (const or non-const)
  template <typename T>
  MembersAdder<C> property_ref_(std::string name, T* data_member);

  // Add static data member
  template <typename T, MX_OVERLOAD_IF(!std::is_function<T>::value)>
  MembersAdder<C> property(std::string name, T* static_member);
};

} // namespace mex_pack

