#pragma once

#include "mx_sfinae_helpers.h"
#include "mx_traits.h"
#include <memory>
#include <type_traits>

// A collection of useful type traits

namespace mex_pack {

// Since C++17 not supported yet, use this instead
template <bool B>
using bool_const = std::integral_constant<bool, B>;


template <typename T>
struct is_smart_pointer : std::false_type {
};
template <typename T, typename Deleter>
struct is_smart_pointer<std::unique_ptr<T, Deleter>> : std::true_type {
};
template <typename T>
struct is_smart_pointer<std::shared_ptr<T>> : std::true_type {
};
template <typename T>
struct is_smart_pointer<std::weak_ptr<T>> : std::true_type {
};


// Enhanced version of std::is_pointer to include STL smart pointers
template <typename T, typename = void>
struct is_all_pointer : std::false_type {
};
template <typename T>
struct is_all_pointer<T, MX_USE_IF(std::is_pointer<T>::value)> : std::true_type {
};
template <typename T>
struct is_all_pointer<T, MX_USE_IF(is_smart_pointer<T>::value)> : std::true_type {
};


// Metafunction to identify if type is mxArray* or not
template <typename T> struct is_mxarray_type : std::false_type {};
template <> struct is_mxarray_type<mxArray*> : std::true_type {};
template <> struct is_mxarray_type<const mxArray*> : std::true_type {};
// sometimes CV qualifiers are stripped off and pointers are dereferenced when calling this, so this will catch it
template <> struct is_mxarray_type<mxArray> : std::true_type {};


// If the passed in value is a pointer, it dereferences it, otherwise it just returns it unchanged.
// Includes smart pointers.
template <typename T, MX_OVERLOAD_IF(!is_all_pointer<mx_decay_t<T>>::value || is_mxarray_type<T>::value)>
auto dereference(T&& v) -> decltype(std::forward<T>(v)) {
  return std::forward<T>(v);
}
template <typename T, MX_OVERLOAD_IF(is_all_pointer<mx_decay_t<T>>::value && !is_mxarray_type<T>::value)>
auto dereference(T&& v) -> decltype(*std::forward<T>(v)) {
  return *std::forward<T>(v);
}

// Returns type that a reference is bound to or a pointer points to.
template <typename T, typename=void>
struct dereference_type {
  using type = typename std::remove_reference<T>::type;
};
template <typename T>
struct dereference_type<T, MX_USE_IF(std::is_pointer<mx_decay_t<T>>::value)> {
  using type = typename std::remove_reference<decltype(*std::declval<T>())>::type;
};
template <typename T>
using dereference_t = typename dereference_type<T>::type;

// Returns type that a reference is bound to or a pointer points to.
// Includes smart pointers
template <typename T, typename=void>
struct dereference_all_type {
  using type = typename std::remove_reference<T>::type;
};
template <typename T>
struct dereference_all_type<T, MX_USE_IF(is_all_pointer<mx_decay_t<T>>::value)> {
  using type = typename std::remove_reference<decltype(*std::declval<T>())>::type;
};
template <typename T>
using dereference_all_t = typename dereference_all_type<T>::type;


// Indicates if T is pure data type reference (pointer or non-const reference)
template <typename T, bool = mx_is_pure_data_type<dereference_t<T>>::value &&
                             ((std::is_reference<T>::value && !std::is_const<typename std::remove_reference<T>::type>::value) ||
                              (std::is_pointer<T>::value && !mx_is_pure_data_type<T>::value))>
struct is_pure_data_type_reference : std::false_type {};

template <typename T>
struct is_pure_data_type_reference<T, true> : std::true_type {};

/*==============================================================================
Similar metafunction to mx_is_pure_data_type<>, but also includes mxArray
types, and allows for pointers.
==============================================================================*/
template <typename T, typename = void>
struct is_pure_data_type : std::false_type {};

template <typename T>
struct is_pure_data_type<T, MX_USE_IF(mx_is_pure_data_type<dereference_t<T>>::value)>
    : std:: true_type
{};

template <typename T>
struct is_pure_data_type<T, MX_USE_IF(is_mxarray_type<T>::value)>
    : std:: true_type
{};


// Indicates if value is a null pointer
template <typename T, MX_OVERLOAD_IF(is_all_pointer<mx_decay_t<T>>::value && !is_mxarray_type<T>::value)>
bool is_null_pointer(T&& val) {return !bool(val);}

template <typename T, MX_OVERLOAD_IF(!(is_all_pointer<mx_decay_t<T>>::value && !is_mxarray_type<T>::value))>
bool is_null_pointer(T&&) {return false;}


// Extracts a specific Arg from an Arg parameter pack given an index
template <std::size_t ArgNum, typename Arg0, typename...Args>
struct extract_arg {
  using type = typename extract_arg<ArgNum-1, Args...>::type;
};

template <typename Arg0, typename...Args>
struct extract_arg<0, Arg0, Args...> {
  using type = Arg0;
};

} // namespace mex_pack

