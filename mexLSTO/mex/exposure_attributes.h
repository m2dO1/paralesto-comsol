#pragma once

#include <tuple>
#include <memory>


namespace mex_pack {

// SetAttribute classes set a specific attribute value
template <bool V, typename A> struct SetReadOnlyAttribute : A {static constexpr bool READ_ONLY = V;}; // Make read only
template <bool V, typename A> struct SetHiddenAttribute : A {static constexpr bool HIDDEN = V;};      // Make hidden in Matlab (but still accessible)
template <bool V, typename A> struct SetFactoryAttribute : A {static constexpr bool FACTORY = V;};    // Makes return value from method/function a managed object from Matlab
template <int V, typename A> struct SetCellDepthAttribute : A {static constexpr int CELL_DEPTH = V;}; // Convert arrays to cell arrays in Matlab
template <bool V, typename A> struct SetBindInstanceAttribute : A {static constexpr bool BIND_INSTANCE = V;}; // Specifies that first argument of function should be bound to class instance
template <bool V, typename A> struct SetVoidResultAttribute : A {static constexpr bool VOID_RESULT = V;}; // Make return type of function call void, effectively ignoring it

// Special SetAttribute class that stores run-time default argument values as a tuple
template <int V, typename A, typename... DefaultArgs>
struct SetDefaultArgsAttribute : A {
  static std::unique_ptr<std::tuple<DefaultArgs...>> default_args;
  static constexpr std::size_t N_DEFAULT_ARGS = sizeof...(DefaultArgs);
  static constexpr int N_OPTIONAL_ARGS = V == -1 ? N_DEFAULT_ARGS : V;
  static constexpr int MAX_ARGS(std::size_t NUM_ARGS) {return NUM_ARGS - (N_DEFAULT_ARGS - N_OPTIONAL_ARGS);}
};
template <int V, typename A, typename... DefaultArgs>
std::unique_ptr<std::tuple<DefaultArgs...>> SetDefaultArgsAttribute<V, A, DefaultArgs...>::default_args;

// Specialization for no default arguments which always must be initialized by default
template <int V, typename A>
struct SetDefaultArgsAttribute<V, A> : A {
  static std::unique_ptr<std::tuple<>> default_args;
  static constexpr std::size_t N_DEFAULT_ARGS = 0;
  static constexpr int N_OPTIONAL_ARGS = V == -1 ? N_DEFAULT_ARGS : V;
  static constexpr int MAX_ARGS(std::size_t NUM_ARGS) {return NUM_ARGS - (N_DEFAULT_ARGS - N_OPTIONAL_ARGS);}
};
template <int V, typename A>
std::unique_ptr<std::tuple<>> SetDefaultArgsAttribute<V, A>::default_args = std::unique_ptr<std::tuple<>>(new std::tuple<>);


struct EmptyAttributes{};
using DefaultAttributes = SetDefaultArgsAttribute<-1,
                          SetVoidResultAttribute<false,
                          SetBindInstanceAttribute<false, 
                          SetCellDepthAttribute<0, 
                          SetFactoryAttribute<false, 
                          SetHiddenAttribute<false, 
                          SetReadOnlyAttribute<false, 
                          EmptyAttributes>>>>>>>;

} // namespace mex_pack

