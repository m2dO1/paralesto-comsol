#include "mex_class_objects.h"
#include "mex_data.h"


namespace mex_pack {

std::deque<std::unique_ptr<ClassMembers>> Objects::handles_;
std::deque<unsigned> Objects::deleted_handles_;

const Objects::UniqueID Objects::unique_id_;

unsigned Objects::Add(ClassMembers* obj) {
  unsigned handle;
  if (deleted_handles_.empty()) {
    handles_.emplace_back(obj);
    handle = handles_.size() - 1;
  } else {
    handle = deleted_handles_.back();
    handles_[handle].reset(obj);
    deleted_handles_.pop_back();
  }
  return handle;
}

void Objects::Remove(unsigned handle, unsigned long long unique_id) {
  if (unique_id == unique_id_.value() || unique_id == 0) {
    if (CheckValid(handle, true)) {
      handles_[handle].reset();
      if (handle == handles_.size() - 1) {
        handles_.pop_back();
      } else {
        deleted_handles_.push_back(handle);
      }
    }
  }
}

bool Objects::CheckValid(unsigned handle, bool just_report) {
  bool valid = false;
  if (handle < handles_.size()) {
    valid = bool(handles_[handle]);
  }
  if (!valid && !just_report) mexErrMsgTxt((std::string("Invalid mexClass handle: ") + std::to_string(handle)).c_str());
  return valid;
}

mxArray* Objects::GetMexClassParams(unsigned handle, std::string matlab_class) {
  const auto& obj = Objects::Get(handle);
  mxArray* pm = mxCreateCellMatrix(9, 1);
  mxSetCell(pm, 0, mxConvert(unique_id_.value()));
  mxSetCell(pm, 1, mxConvert(matlab_class));
  mxSetCell(pm, 2, mxConvert(handle));
  mxSetCell(pm, 3, mxConvert(obj.GetMethodNames()));
  mxSetCell(pm, 4, mxConvert(obj.GetPropertyNames(ClassMembers::PropertyType::pure_data)));
  mxSetCell(pm, 5, mxConvert(obj.GetPropertyNames(ClassMembers::PropertyType::fixed_mex_class_object)));
  mxSetCell(pm, 6, mxConvert(obj.GetPropertyNames(ClassMembers::PropertyType::dynamic_mex_class_object)));
  mxSetCell(pm, 7, mxConvert(obj.GetPropertyNames(ClassMembers::PropertyType::type_member)));
  mxSetCell(pm, 8, mxConvert(obj.GetOrderedMemberNames()));
  return pm;
}

} // namespace mex_pack

