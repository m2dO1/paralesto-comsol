#pragma once

#include "mex_type_traits.h"


namespace mex_pack {
  // Metafunction that indicates if type has a mexClass definition or not.
  // Note that this works by checking if calling AddMexClassMembers() with
  // the desired type is valid or not.  But this does have a limitation for
  // templates, because this metafunction won't force an explicit template
  // instantiation of AddMexClassMembers().  Typically this isn't done unless
  // that type is being exported to Matlab in the mexPackage definition.

  // Primary template
  template <typename T, typename = bool_const<true>, bool = mx_is_element_type<T>::value || is_mxarray_type<T>::value>
  struct is_mex_class_type
      : std::false_type
  {};

  // Identify T as an array of mexClass types
  template <typename T, typename = void>
  struct is_mex_class_array_type : std::false_type {};
}


// Add support for mexClass types with mxConvert()

// Overload for mexClass types
template <typename T, MX_OVERLOAD_IF(mex_pack::is_mex_class_type<mex_pack::dereference_all_t<T>>::value)>
mxArray* mxConvert(T&& c_obj);

// Overload for mexClass array types.  Only supported when passed by reference.
// If passed by value (rvalue), the mexClass elements will be passed to mxConvert()
// by reference, which will cause ClassInstance to make a reference of the element
// instead of a copy which it should, since this array is only temporary.
template <typename T, MX_OVERLOAD_IF(mex_pack::is_mex_class_array_type<T>::value)>
mxArray* mxConvert(T&& c_obj);

// Overload for mexClass type non-const references
template <typename T, MX_OVERLOAD_IF(mex_pack::is_mex_class_type<mex_pack::dereference_t<T>>::value && (!std::is_const<mex_pack::dereference_t<T>>::value && (std::is_pointer<T>::value || std::is_reference<T>::value)))>
T mxExtract(const mxArray* pm);

// Overload for all other mexClass types
template <typename T, MX_OVERLOAD_IF(mex_pack::is_mex_class_type<mex_pack::dereference_t<T>>::value && !(!std::is_const<mex_pack::dereference_t<T>>::value && (std::is_pointer<T>::value || std::is_reference<T>::value)))>
T mxExtract(const mxArray* pm);

// Overload for pure data type non-const references
template <typename T, MX_OVERLOAD_IF(mex_pack::is_pure_data_type_reference<T>::value)>
T mxExtract(const mxArray* pm);

