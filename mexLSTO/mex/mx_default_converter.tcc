#pragma once

#include "mx_default_converter.h"


template <typename T>
mxArray* MxDefaultConverter::convert(T&& c_obj) {
  return mxConvert(std::forward<T>(c_obj));
}

