#pragma once

#include "mex_output_args.h"

/*
mexGetterValue is similar to mexOutputArgs, but uses a different converter
class, GetterValueConverter instead of mxDefaultConverter, which allows reading
of pure data type pointers (including smart pointers), and handles null
pointers as an empty object.
*/

namespace mex_pack {

struct GetterValueConverter {
  template <typename T>
  static mxArray* convert(T&& c_obj);

  template <int CELL_DEPTH, typename T>
  static mxArray* cell_convert(T&& c_obj);
};

using mexGetterValue = mexOutputArgsTemplate<GetterValueConverter>;

} // namespace mex_pack

