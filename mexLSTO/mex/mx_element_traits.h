#pragma once

// Do not include mex.h or matrix.h in here, nor should any of the header files
// included in here include them.

#include "mx_numeric_proxy.h"
#include "mx_class_id.h"
#include "mx_sfinae_helpers.h"
#include <complex>
#include <type_traits>


/*==============================================================================
Class Template:  mx_element_traits

Description:
------------
All C types that are intended to be used as array elements must have an
implementation of mx_element_traits defined for it.  Any C type can be
supported, as long as it can map to an equivalent mxArray element type, which
includes not only numerical types, but also logical types and even structures.

Properties:
  is_complex : Set to true for complex types.  Only needed for numerical types.

  class_id : The equivalent mxClassID that this C type should map to.  This is
  only used when converting the C type to Matlab.  Structure types should always
  specify mxSTRUCT_CLASS.

Functions:
----------
One or two static methods need to be defined, one for converting from the Matlab
type to the C type called convert_to_c(), and another for converting from the C
type to the Matlab type called convert_to_matlab().  You can choose to implement
only one of the methods if that's the only conversion you will ever use.
The function declarations are slightly different for numerical/logical
types vs structure types.

Numerical/Logical Types:
  // Convert Matlab to C
  template <typename U, typename C_iter>
  static void convert_to_c(const mx_numeric_proxy<U>& mx_proxy, C_iter c_obj);

  // Convert C to Matlab
  template <typename U>
  static void convert_to_matlab(const T& c_obj, mx_numeric_proxy<U>& mx_proxy);

Structure Types:
  // Convert Matlab to C
  template <typename C_iter>
  static void convert_to_c(const mxConstStruct& mx_proxy, C_iter c_obj);

  // Convert C to Matlab
  static void convert_to_matlab(const T& c_obj, mxStruct& mx_proxy);

Custom Conversions:
------------------
If the user wants to provide a custom (overloaded) implementation of mxConvert
for a particular type, then simply provide a specialization of mx_element_traits
for this type that sets the class_id = mxUNKNOWN_CLASS.  Nothing else needs to
be provided in the specialization.

==============================================================================*/

// Base class for mx_element_traits of non-element types.
struct mx_not_element_type {};

// Primary template.  Only used for unsupported element types.
template <typename T, typename = void>
struct mx_element_traits : mx_not_element_type {
};

// Metatfunction that returns an indication of the type being an element type or not.
template <typename T, bool = std::is_base_of<mx_not_element_type, mx_element_traits<mx_value_t<T>>>::value>
struct mx_is_element_type : std::false_type {};
template <typename T>
struct mx_is_element_type<T, false>
    : std:: true_type {};

// Metafunction that retuns an indication if the element type must be used in a cell array or not.
// Assumes false if the 'use_cell_array' property doesn't exist
template <typename T, typename = void>
struct mx_element_use_cell_array {
  static constexpr bool value = false;
};
template <typename T>
struct mx_element_use_cell_array<T, mx_void_t<decltype(T::use_cell_array)>> {
  static constexpr bool value = T::use_cell_array;
};

// Base class for real value C element types.
template <typename T>
struct mx_element_traits_real {
  static constexpr bool is_complex = false;

  static constexpr mxClassID_ class_id = mxClassID_::mxDOUBLE_CLASS;

  template <typename U, typename C_iter>
  static void convert_to_c(const mx_numeric_proxy<U>& mx_proxy, C_iter c_obj) {
#ifdef _MSC_VER
#pragma warning(disable:4244)
#endif
    *c_obj = mx_proxy.real();
#ifdef _MSC_VER
#pragma warning(default:4244)
#endif
  }

  template <typename U>
  static void convert_to_matlab(const T& c_obj, mx_numeric_proxy<U>& mx_proxy) {
    mx_proxy.real(c_obj);
  }
};

// Implementation for bool type.
template <>
struct mx_element_traits<bool, void> : mx_element_traits_real<bool> {
  static constexpr mxClassID_ class_id = mxClassID_::mxLOGICAL_CLASS;
};

// Implementation for float type.
template <>
struct mx_element_traits<float, void> : mx_element_traits_real<float> {
  static constexpr mxClassID_ class_id = mxClassID_::mxDOUBLE_CLASS;
};

// Implementation for double type.
template <>
struct mx_element_traits<double, void> : mx_element_traits_real<double> {
  static constexpr mxClassID_ class_id = mxClassID_::mxDOUBLE_CLASS;
};

// Implementation for integer types.
template <typename T>
struct mx_element_traits<T, MX_USE_IF(std::is_integral<T>::value)>
    : mx_element_traits_real<T>
{
  // Use double type unless it is a 64-bit integer
  static constexpr mxClassID_ class_id = 
      (std::is_signed<T>::value && sizeof(T) > 4) ? mxClassID_::mxINT64_CLASS :
      (std::is_unsigned<T>::value && sizeof(T) > 4) ? mxClassID_::mxUINT64_CLASS :
      mxClassID_::mxDOUBLE_CLASS;
};

// Base class for std::complex<T>-like classes
template <typename T, mxClassID_ ID>
struct mx_element_traits_complex {
  static constexpr bool is_complex = true;

  static constexpr mxClassID_ class_id = ID;

  template <typename U, typename C_iter>
  static void convert_to_c(const mx_numeric_proxy<U>& mx_proxy, C_iter c_obj) {
    *c_obj = T(mx_proxy.real(), mx_proxy.imag());
  }

  template <typename U>
  static void convert_to_matlab(const T& c_obj, mx_numeric_proxy<U>& mx_proxy) {
    mx_proxy.real(c_obj.real());
    mx_proxy.imag(c_obj.imag());
  }
};

// Implementation for std;:complex<T>
template <typename T>
struct mx_element_traits<std::complex<T>, void>
    : mx_element_traits_complex<std::complex<T>, mx_element_traits<T>::class_id> {
};

// Implementation for enum types
template <typename EnumType>
struct mx_element_traits<EnumType, MX_USE_IF(std::is_enum<EnumType>::value)> 
    : mx_element_traits_real<EnumType>
{
  template <typename U, typename C_iter>
  static void convert_to_c(const mx_numeric_proxy<U>& mx_proxy, C_iter c_obj) {
    *c_obj = static_cast<EnumType>(static_cast<int>(mx_proxy.real()));
  }
};

// Add support for struct types

// provide dummy in case no structs are defined in the build
struct DummyStructType__;
void ConvertMxStruct(DummyStructType__*, mx_type_match<DummyStructType__>);

// checks if type is a struct type or not
template <typename T, typename = void>
struct is_mx_struct_type
    : std::false_type
{};
template <typename T>
struct is_mx_struct_type<T, mx_void_t<decltype(ConvertMxStruct(std::declval<T*>(), mx_type_match<T>{}))>>
    : std::true_type
{};

// forward declaration
class mxStruct;
class mxConstStruct;

template <typename T>
struct mx_element_traits<T, MX_USE_IF(is_mx_struct_type<T>::value)> {
  static constexpr mxClassID_ class_id = mxClassID_::mxSTRUCT_CLASS;

  template <typename C_iter>
  static void convert_to_c(const mxConstStruct& mx_proxy, C_iter c_obj);

  static void convert_to_matlab(const T& c_obj, mxStruct& mx_proxy);
};


// forward declarations
class mx_const_cell_proxy;

// Add support for std::string types
template <>
struct mx_element_traits<std::string, void> {
  static constexpr bool is_complex = false;

  static constexpr bool use_cell_array = true;

  static constexpr mxClassID_ class_id = mxClassID_::mxCHAR_CLASS;

  template <typename C_iter>
  static void convert_to_c(const mx_const_cell_proxy& mx_proxy, C_iter c_obj);

  template <typename U>
  static void convert_to_matlab(const std::string& c_obj, U& mx_proxy);
};

// Add support for const char* types
template <>
struct mx_element_traits<const char*, void> {
  static constexpr bool is_complex = false;

  static constexpr bool use_cell_array = true;

  static constexpr mxClassID_ class_id = mxClassID_::mxCHAR_CLASS;

  template <typename U>
  static void convert_to_matlab(const char* c_obj, U& mx_proxy);
};


// Add support for std::tuple
template <typename ... Types>
struct mx_element_traits<std::tuple<Types...>, void> {
  static constexpr bool is_complex = false;

  static constexpr bool use_cell_array = true;

  static constexpr mxClassID_ class_id = mxClassID_::mxCELL_CLASS;

  template <typename T, typename U>
  static void convert_to_matlab(T&& c_obj, U& mx_proxy);
};

// Add support for std::pair
template <typename T1, typename T2>
struct mx_element_traits<std::pair<T1, T2>, void> {
  static constexpr bool is_complex = false;

  static constexpr bool use_cell_array = true;

  static constexpr mxClassID_ class_id = mxClassID_::mxCELL_CLASS;

  template <typename T, typename U>
  static void convert_to_matlab(T&& c_obj, U& mx_proxy);
};

