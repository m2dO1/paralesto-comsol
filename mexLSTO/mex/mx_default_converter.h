#pragma once

/*
A converter is a class with a convert() method, which can be called to convert
any C++ object to an mxArray*.  This is the default converter which simply calls
mxConvert().  But one can define different converters that override the behavior
of the convert() function and possibly do additional things before eventually
calling mxConvert().
*/

struct MxDefaultConverter {
  template <typename T>
  static mxArray* convert(T&& c_obj);
};

