#pragma once

#include "mex_class_members.h"
#include "mex_class_members_adder.h"
#include "mex_class_objects.h"
#include "mex_ref_arg.h"
#include <string>

/*
The MexPackage class is the base class for all packages.  A package exposes
classes (types), functions, and objects to Matlab.  Every mexPackage compiled
mex library contains a top level package that the user defines with the
MEX_PACKAGE macro, what immediate classes, functions, and objects can be seen in
the mex library.  But the user can also create his/her own packages as well that
reside under the top level package, effectively creating a hierarchy of
packages.  This can be done for organizational purposes, but another benefit to
doing this is because these additional packages can be defined as templates,
allowing one to group classes, functions, and objects together under a common
template argument, making it easy to template instantiate the package template
with different template arguments.
Technically, a package is a class derived from the MexPackage base class, and
has a constructor that defines which classes, functions, objects, and packages
to expose.  There always exists a MainMexPackage class which is created by
default, and the MEX_PACKAGE macro simply provides an easy way to define the
constructor for this.
Creating an additional package means defining a new class derived from
MexPackage, and in the constructor you would use class_(), function_(),
object_(), or package_() to add items to the package.  There's no need to call
the base class (MexPackage) constructor explicitly.

Below is an example of how to create a custom package:

struct MyPackage : MexPackage {
  MyPackage() {
    package_<AnotherPackage>("AnotherPackage"); // include another package, AnotherPackage
    class_<MyClass>("MyClass");                 // include a class, MyClass
    function_("MyFunction", MyFunction);        // include a function, MyFunction()
    object_("my_object", my_object);            // include an object, my_object
  }
};

You can include this new package in the top level package like this:

MEX_PACKAGE {
  package_<MyPackage>("MyPackage");
  ...                                 // include additional items
}

*/

class MexPackage : public mex_pack::ClassMembers, public mex_pack::MembersAdder<MexPackage> {

 protected:
  // Ctor only use for classes derived from this to create package classes
  MexPackage()
      : mex_pack::ClassMembers(typeid(MexPackage))
      , mex_pack::MembersAdder<MexPackage>(this)
  {
    hide_type_info_ = true;
  }
};


struct MainMexPackageBase : MexPackage {
  MainMexPackageBase() {
    class_<mex_pack::RefArg>("mexRefArg");
  }
};


// This is the main mex package that is always loaded at startup.
struct MainMexPackage : MainMexPackageBase {
  // Constructor is defined by the user with the MEX_PACKAGE macro
  MainMexPackage();
};

