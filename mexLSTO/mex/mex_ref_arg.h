#pragma once

#include "mex_class.h"
#include "mex_class_members.h"
#include "mx_array_persistent_ptr.h"
#include "mex_type_traits.h"
#include "demangle.h"
#include <memory>

/*
The RefArg class allows for pass-by-reference of pure data types as input and
output arguments of Matlab calls to C/C++ functions.  It is exposed to Matlab as
a mexClass type.  For C/C++ functions that have reference/pointer input
arguments, using RefArg from Matlab allows one to create C storage of specific
types that can then be passed by reference or pointer to the C/C++ function
argument.  Matlab can then access the C storage from the RefArg object's 'value'
property.  For functions that return non-const references or pointers, these
can be returned as RefArg objects to Matlab to allow it access to the data the
reference or pointer refers to.
*/

namespace mex_pack {

// Abstract base class for holders of reference data
class Holder {
 public:
  virtual ~Holder() = default;

  virtual mxArray* convert() const = 0;

  virtual void set(const mxArray* pm) = 0;

  virtual std::string get_type_name() const {return std::string("void");}
};

// Holds the non-const data reference
template <typename T>
class RefHolder : public Holder {
  T& data_ref_;
  std::string type_name_;

 public:
  RefHolder(T& data)
      : data_ref_(data)
      , type_name_(demangle(typeid(T).name()))
  {}

  mxArray* convert() const override;

  void set(const mxArray* pm) override;

  std::string get_type_name() const override {return type_name_;}

  T& get() {return data_ref_;}

  const T& get() const {return data_ref_;}
};

// Holds the const data reference
template <typename T>
class ConstRefHolder : public Holder {
  const T& data_ref_;
  std::string type_name_;

 public:
  ConstRefHolder(const T& data)
      : data_ref_(data)
      , type_name_(demangle(typeid(T).name()))
  {}

  mxArray* convert() const override;

  void set(const mxArray*) override;

  std::string get_type_name() const override {return type_name_;}

  const T& get() const {return data_ref_;}
};

// Holds the actual data
template <typename T>
class DataHolder : public RefHolder<T> {
  T data_;

 public:
  DataHolder() : RefHolder<T>(data_) {}

  DataHolder(const mxArray* pm);
};

template <typename T>
class SmartPtrConstHolder : public Holder {
 protected:
  const T smart_ptr_;
  std::string type_name_;

 public:
  SmartPtrConstHolder(T&& smart_ptr)
      : smart_ptr_(std::move(smart_ptr))
      , type_name_(demangle(typeid(T).name()))
  {}

  mxArray* convert() const override;

  void set(const mxArray* pm) override;

  std::string get_type_name() const override {return type_name_;}

  const T& get() const {return *smart_ptr_;}
};

template <typename T>
class SmartPtrHolder : public SmartPtrConstHolder<T> {
 public:
  using SmartPtrConstHolder<T>::SmartPtrConstHolder;

  void set(const mxArray* pm) override;

  T& get() {return *SmartPtrConstHolder<T>::smart_ptr_;}
};


class RefArg {
  mutable std::unique_ptr<Holder> ref_data;
  mxArrayPersistentPtr pm_;

  // Initialize with const smart pointer
  template <typename T, MX_OVERLOAD_IF(is_smart_pointer<T>::value)>
  RefArg(T&& smart_ptr, std::true_type)
      : ref_data(new SmartPtrConstHolder<T>(std::forward<T>(smart_ptr)))
      , is_const_(true)
  {}

  // Initialize with non-const smart pointer
  template <typename T, MX_OVERLOAD_IF(is_smart_pointer<T>::value)>
  RefArg(T&& smart_ptr, std::false_type)
      : ref_data(new SmartPtrHolder<T>(std::forward<T>(smart_ptr)))
      , is_const_(false)
  {}

 public:
  bool is_const_ = false;

  // Allow move construction since there should only be one owner of the reference
  RefArg(RefArg&&) = default;

  // These constructors are only used from Matlab for passing to function input arguments.

  // Uninitialized
  RefArg() = default;

  // Seed with value from Matlab, but without reference data
  RefArg(const mxArray* pm) : pm_(pm) {}

  // These constructors are only used from within C++ to return function values.
  // Don't define RefArg(T& data) ctor as that will get confused with the actual
  // copy constructor which is needed by ClassInstance ctor.

  // Initialize with non-const pointer
  template <typename T>
  RefArg(T* data) : ref_data(new RefHolder<T>(*data)) {}

  // Initialize with const pointer
  template <typename T>
  RefArg(const T* data)
      : ref_data(new ConstRefHolder<T>(*data))
      , is_const_(true)
  {}

  // Initialize with smart pointer
  template <typename T, MX_OVERLOAD_IF(is_smart_pointer<T>::value)>
  RefArg(T&& smart_ptr)
      : RefArg(std::forward<T>(smart_ptr), bool_const<std::is_const<typename T::element_type>::value>{})
  {}

  std::string get_type_name() const {
    if (ref_data)
      return ref_data->get_type_name();
    else
      return std::string("void");
  }

  static ClassMembers* GetTypeObject();

  // Get reference data
  template <typename T, MX_OVERLOAD_IF(!std::is_const<T>::value)>
  T& extract();

  // Get const reference data
  template <typename T, MX_OVERLOAD_IF(std::is_const<T>::value)>
  T& extract() const;

  // Get Matlab value reference data
  mxArray* get_value() const {
    mxArray* ret_value;
    if (ref_data) {
      ret_value = ref_data->convert();
    } else if (pm_) {
      ret_value = pm_.copy();
    } else {
      // Return an empty matrix to indicate it is uninitialized.
      ret_value = mxCreateDoubleMatrix(0, 0, mxREAL);
    }
    return ret_value;
  }

  // Set value of reference data
  void set_value(const mxArray* pm) {
    if (ref_data) {
      ref_data->set(pm);
    } else {
      pm_.reset(pm);
    }
  }
};

} // namespace mex_pack


MEX_CLASS(mex_pack::RefArg) {
  def(self)
    .ctor<>()
    .ctor<const mxArray*>()
    .property("CPP_TYPE_", &mex_pack::RefArg::get_type_name)
    .property("value", &mex_pack::RefArg::get_value, &mex_pack::RefArg::set_value)
  ;
}

