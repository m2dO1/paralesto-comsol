#pragma once

#include "mx_struct.h"
#include "mx_convert.h"
#include <cassert>


template <typename T>
mxConstStruct::operator T() const {
  mxAssert(mxa_, "");
  return mxExtract<T>(mxa_);
}

template <typename T>
void mxConstStruct::convert_field(const char* field_name, T* c_obj) const {
  if (is_field(field_name)) mxSafeExtract<T>((*this)[field_name], c_obj);
}

template <typename T, MX_OVERLOAD_IF_DEF(!std::is_convertible<T, mxArray*>::value)>
mxStruct& mxStruct::operator=(const T& val) {
  *this = mxConvert(val);
  return *this;
}

template <typename T, MX_OVERLOAD_IF_DEF(!std::is_convertible<T, mxArray*>::value)>
mxStruct& mxStruct::operator=(T& val) {
  *this = mxConvert(val);
  return *this;
}

template <typename T>
mxStruct::operator T() const {
  mxAssert(mxa_, "");
  return mxExtract<T>(mxa_);
}

