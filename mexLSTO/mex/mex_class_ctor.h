#pragma once

#include "mx_sfinae_helpers.h"


class mexInputArgs;

namespace mex_pack {

// If default arguments are specified
template <typename T, typename Attributes, int MIN_ARGS_OVERRIDE, typename... Args, MX_OVERLOAD_IF(Attributes::N_DEFAULT_ARGS > 0)>
T* CreateFromArgTypes(const mexInputArgs& prhs);

// If no default arguments specified
template <typename T, typename Attributes, int MIN_ARGS_OVERRIDE, typename... Args, MX_OVERLOAD_IF(Attributes::N_DEFAULT_ARGS == 0)>
T* CreateFromArgTypes(const mexInputArgs& prhs);

} // namespace mex_pack


