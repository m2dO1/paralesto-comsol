#pragma once

#include "mex_input_args.h"
#include "mx_data.h"
#include "mx_convert.h"


template <typename T>
mxData& mexInputArgs::Data::with_default(const T& val) {
  if (!mxa_) {
    mxa_ = mxConvert(val);
  } else if (mxIsEmpty(mxa_)) {
    mxa_ = mxConvert(val);
  }
  return *this;
}

