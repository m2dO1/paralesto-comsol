#pragma once

#include "mx_convert_tuple.h"


template <std::size_t I, typename Converter>
template <typename T>
void MxConvertTupleElements<I, Converter>::convert(mxArray* pm, T&& c_obj) {
  mxSetCell(pm, I, Converter::convert(std::get<I>(std::forward<T>(c_obj))));
  MxConvertTupleElements<I-1, Converter>::convert(pm, std::forward<T>(c_obj));
}

template <typename Converter>
template <typename T>
void MxConvertTupleElements<0, Converter>::convert(mxArray* pm, T&& c_obj) {
  mxSetCell(pm, 0, Converter::convert(std::get<0>(std::forward<T>(c_obj))));
}

template <typename Converter, typename T>
mxArray* mxConvertTuple(T&& c_obj) {
  constexpr auto c_obj_size = std::tuple_size<mx_decay_t<T>>::value;
  auto pm = mxCreateCellMatrix(c_obj_size, 1);
  MxConvertTupleElements<c_obj_size-1, Converter>::convert(pm, std::forward<T>(c_obj));
  return pm;
}

template <typename Converter, typename T>
mxArray* mxConvertPair(T&& c_obj) {
  constexpr auto c_obj_size = 2;
  auto pm = mxCreateCellMatrix(c_obj_size, 1);
  MxConvertTupleElements<c_obj_size-1, Converter>::convert(pm, std::forward<T>(c_obj));
  return pm;
}

