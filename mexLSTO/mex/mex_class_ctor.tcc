#pragma once

#include "mex_class_ctor.h"
#include "mex_input_args.h"
#include "mx_convert.h"
#include "mex_type_traits.h"
#include "mex_index_sequence.h"


namespace mex_pack {

template <typename T, typename... Args>
constexpr int find_min_ctor_args(mex_index_sequence<>) {
  return std::is_constructible<T>::value ? 0 : 1;
};
template <typename T, typename... Args, std::size_t... I>
constexpr int find_min_ctor_args(mex_index_sequence<I...>) {
  return std::is_constructible<T, typename extract_arg<I, Args...>::type...>::value ? find_min_ctor_args<T, Args...>(mex_make_index_sequence<sizeof...(I) - 1>()) : int(sizeof...(I) + 1);
};

// Function that returns the mandatory number of constructor arguments given
// the constructor argument types.
template <typename T, typename... Args>
constexpr int min_ctor_args() {
  return find_min_ctor_args<T, Args...>(mex_make_index_sequence<sizeof...(Args)>());
}


// If number of input arguments exceeds max allowed
template <int MAX_ARGS, typename T, typename... Args, std::size_t... I, typename... DefaultArgs, std::size_t... Idef, MX_OVERLOAD_IF(sizeof...(I) > MAX_ARGS)>
T* call_ctor(const mexInputArgs&, mex_index_sequence<I...>, const std::tuple<DefaultArgs...>&, mex_index_sequence<Idef...>) {
  return nullptr;
}

// With input arguments, no default arguments specified
//template <int MAX_ARGS, typename T, typename... Args, std::size_t... I, typename... DefaultArgs, MX_OVERLOAD_IF(sizeof...(I) >= min_ctor_args<T, Args...>() && sizeof...(I) > 0 && sizeof...(I) <= MAX_ARGS)>
template <int MAX_ARGS, typename T, typename... Args, std::size_t... I, typename... DefaultArgs, MX_OVERLOAD_IF(sizeof...(I) <= MAX_ARGS)>
T* call_ctor(const mexInputArgs& prhs, mex_index_sequence<I...>, const std::tuple<DefaultArgs...>& default_args, mex_index_sequence<>) {
  if (prhs.size() == sizeof...(I)) {
    return new T(mxExtract<typename extract_arg<I, Args...>::type>(prhs[I])...);
  } else {
    constexpr std::size_t NEW_N_INPUT_ARGS = sizeof...(I) + 1;
    return call_ctor<MAX_ARGS, T, Args...>(prhs, mex_make_index_sequence<NEW_N_INPUT_ARGS>(), default_args, mex_make_index_sequence<0>());
  }
}

// No input arguments, with default arguments
template <int MAX_ARGS, typename T, typename... Args, typename... DefaultArgs, std::size_t... Idef, MX_OVERLOAD_IF(sizeof...(Idef) > 0)>
T* call_ctor(const mexInputArgs& prhs, mex_index_sequence<>, const std::tuple<DefaultArgs...>& default_args, mex_index_sequence<Idef...>) {
  if (prhs.size() == 0) {
    return new T(std::get<Idef>(default_args)...);
  } else {
    constexpr std::size_t NEW_N_INPUT_ARGS = 1;
    return call_ctor<MAX_ARGS, T, Args...>(prhs, mex_make_index_sequence<NEW_N_INPUT_ARGS>(), default_args, mex_make_index_sequence<sizeof...(Args) - NEW_N_INPUT_ARGS>());
  }
}

// With input and default arguments
template <int MAX_ARGS, typename T, typename... Args, std::size_t... I, typename... DefaultArgs, std::size_t... Idef, MX_OVERLOAD_IF(sizeof...(I) <= MAX_ARGS && sizeof...(I) < sizeof...(Args) && sizeof...(I) > 0)>
T* call_ctor(const mexInputArgs& prhs, mex_index_sequence<I...>, const std::tuple<DefaultArgs...>& default_args, mex_index_sequence<Idef...>) {
  if (prhs.size() == sizeof...(I)) {
    return new T(mxExtract<typename extract_arg<I, Args...>::type>(prhs[I])..., std::get<Idef + sizeof...(DefaultArgs) + sizeof...(I) - sizeof...(Args)>(default_args)...);
  } else {
    constexpr std::size_t NEW_N_INPUT_ARGS = sizeof...(I) + 1;
    return call_ctor<MAX_ARGS, T, Args...>(prhs, mex_make_index_sequence<NEW_N_INPUT_ARGS>(), default_args, mex_make_index_sequence<sizeof...(Args) - NEW_N_INPUT_ARGS>());
  }
}


// If default arguments are specified
template <typename T, typename Attributes, int MIN_ARGS_OVERRIDE, typename... Args, MX_OVERLOAD_IF_DEF(Attributes::N_DEFAULT_ARGS > 0)>
T* CreateFromArgTypes(const mexInputArgs& prhs) {
  static_assert(Attributes::N_DEFAULT_ARGS <= sizeof...(Args), "Too many default args specified.");

  constexpr int MIN_ARGS_DEFAULT = sizeof...(Args) - Attributes::N_DEFAULT_ARGS;
  constexpr int MIN_ARGS = MIN_ARGS_DEFAULT < MIN_ARGS_OVERRIDE ? MIN_ARGS_OVERRIDE : MIN_ARGS_DEFAULT;
  return call_ctor<Attributes::MAX_ARGS(sizeof...(Args)), T, Args...>(prhs, mex_make_index_sequence<MIN_ARGS>(), *Attributes::default_args, mex_make_index_sequence<Attributes::N_DEFAULT_ARGS>());
}

// If no default arguments specified
template <typename T, typename Attributes, int MIN_ARGS_OVERRIDE, typename... Args, MX_OVERLOAD_IF_DEF(Attributes::N_DEFAULT_ARGS == 0)>
T* CreateFromArgTypes(const mexInputArgs& prhs) {
  constexpr int MIN_ARGS_DEFAULT = min_ctor_args<T, Args...>();
  constexpr int MIN_ARGS = MIN_ARGS_DEFAULT < MIN_ARGS_OVERRIDE ? MIN_ARGS_OVERRIDE : MIN_ARGS_DEFAULT;
  return call_ctor<sizeof...(Args), T, Args...>(prhs, mex_make_index_sequence<MIN_ARGS>(), std::tuple<>(), mex_make_index_sequence<0>());
}

} // namespace mex_pack

