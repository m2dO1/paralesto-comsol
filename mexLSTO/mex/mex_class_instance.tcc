#include "mex_class_instance.h"
#include "mex_class_type.h"


namespace mex_pack {

template <typename T>
T& GetManagedObject(ManagedObject managed_obj) {
  auto managed_obj_holder = dynamic_cast<ManagedObjectHolder<T>*>(managed_obj.get());
  if (!managed_obj_holder) mexErrMsgTxt("Could not find managed object of that type.");
  return *(managed_obj_holder->get());
}


template <typename T>
void ClassInstance<T>::Initialize() {
  AddTypeNameProperty();
  AddPropertyGetter<bool>("READ_ONLY_", [&](mexGetterValue::Data data) {data = is_const_;}, true);
  AddMexClassMembers(&object_referenced_, MembersAdderBase(this, true));
  AddMexClassMembers(&object_referenced_, MembersAdderBase(this, false));
  AddMexClassMembersExtended(&object_referenced_, MembersAdderBase(this, false));
}

template <typename T>
ClassMembers* ClassInstance<T>::GetType() const {
  return new ClassType<T>;
}

} // namespace mex_pack

