#pragma once

#include "mex_input_args.h"
#include "mex_output_args.h"
#include "mex_return_value.h"
#include "mex_getter_value.h"
#include "exposure_attributes.h"
#include "mex_type_traits.h"
#include "demangle.h"
#include <deque>
#include <functional>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <utility>
#include <typeindex>


namespace mex_pack {

/*==============================================================================
Class: ClassMembers

Description:
------------
ClassMembers collects the class member information for both class type objects
and class instance objects, and stores them as function handles that can be
called later from Matlab to retrieve.  ClassMembers is populated by a
MembersAdder object.
ClassMembers applies only to mexClass types.
ClassMembers is the base class for ClassType, which extends ClassMembers to
include type info to create a mexClass type object.
ClassMembers is the base class for ClasInstance, which extends ClassMembers to
include an object instance to create a mexClass instance object.
==============================================================================*/

class ClassMembers {
 public:
  enum class PropertyType {pure_data, fixed_mex_class_object, dynamic_mex_class_object, type_member};

 private:
  // Collection of member access functions
  std::unordered_map<std::string, std::pair<std::function<void(mexGetterValue::Data)>, PropertyType>> property_getters_;
  std::unordered_map<std::string, std::pair<std::function<void(mexInputArgs::Data)>, PropertyType>> property_setters_;
  std::unordered_map<std::string, std::function<void(mexReturnValue, mexInputArgs)>> methods_;

  // Used to keep track of the order that properties are specifed in the mexClass
  // definition so that it is preserved in Matlab
  std::deque<std::string> ordered_member_names_;

 public:
  ClassMembers(const std::type_info& typeinfo, bool is_const=false)
      : type_info_(typeinfo)
      , is_const_(is_const)
  {}

  virtual ~ClassMembers() = 0;

  std::type_index type_info_;
  bool hide_type_info_ = false;
  const bool is_const_ = false;

  // Get referenced object
  virtual const void* GetObject() const {
    mexErrMsgTxt("Illegal call to ClassMembers::GetObject()");
    return nullptr;
  }

  // Get referenced object, and upcast to the given type if necessary
  virtual const void* GetUpcastableObject(const std::type_index&) const {
    mexErrMsgTxt("Illegal call to ClassMembers::GetUpcastableObject()");
    return nullptr;
  }

  virtual ClassMembers* GetType() const {
    mexErrMsgTxt("Illegal call to ClassMembers::GetType()");
    return nullptr;
  }

  virtual ClassMembers* CreateNewObject(mexInputArgs) const {
    mexErrMsgTxt("Illegal call to ClassMembers::CreateNewObject()");
    return nullptr;
  }

  auto GetOrderedMemberNames() const -> const decltype(ordered_member_names_)& {
    return ordered_member_names_;
  }

  std::unordered_set<std::string> GetMethodNames() const;

  std::unordered_set<std::string> GetPropertyNames(PropertyType ptype) const;

  bool CallMethod(std::string name, mexOutputArgs plhs, mexInputArgs prhs);

  template <typename T>
  void AddPropertyGetter(std::string name, const std::function<void(mexGetterValue::Data)>& getter, bool hidden=false);

  template <typename T>
  void AddTypeMember(std::string name, const std::function<void(mexGetterValue::Data)>& getter, bool hidden=false);

  void AddPropertySetter(std::string name, const std::function<void(mexInputArgs::Data)>& setter);

  bool GetProperty(std::string name, mexGetterValue::Data out);

  bool SetProperty(std::string name, mexInputArgs::Data in);

  void AddMethod(std::string name, const std::function<void(mexReturnValue, mexInputArgs)>& method, bool hidden=false);

  // Add method with Matlab argument format
  template <typename Attributes>
  void AddMethod(const std::string& name, const std::function<void(mexOutputArgs, mexInputArgs)>& func);

  // Add method with Matlab argument format
  template <typename Attributes>
  void AddMethod(const std::string& name, const std::function<void(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])>& func);

  // Add method with arbitrary format
  template <typename Attributes=DefaultAttributes, typename R, typename... Args>
  void AddMethod(const std::string& name, const std::function<R(Args...)>& func);

 private:
  void AddPropertyName(std::string name);

 protected:
  void AddTypeNameProperty() {
    if (!hide_type_info_) {
      AddPropertyGetter<std::string>("CPP_TYPE_", [&](mexGetterValue::Data data) {data = demangle(type_info_.name());});
    }
  }
};

} // namespace mex_pack

