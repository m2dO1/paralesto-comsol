//
// Copyright 2023 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef OPTIMIZE_IPOPT_H
#define OPTIMIZE_IPOPT_H

#include <assert.h>
#include <math.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <functional>
#include <iosfwd>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "coin-or/IpTNLP.hpp"

using namespace Ipopt;

/*! \file optimize_ipopt.h
    \brief A file that has the class for optimization
*/

namespace para_opt {

/*! \class OptimizeIpopt
    \brief A class that is a wrapper for the optimization
*/
class OptimizeIpopt : public TNLP {
 public:
  int nDesVar;                                   //!< number of design variables
  int nCons;                                     //!< number of constraints
  std::vector<double> &z;                        //!< design variable
  std::vector<double> &z_up;                     //!< upper bounds of the design variables
  std::vector<double> &z_lo;                     //!< lower bounds of the design variables
  std::vector<double> &objFunGrad;               //!< objective function gradient
  std::vector<std::vector<double>> &conFunGrad;  //!< constraint function gradient
  std::vector<double> conMaxVals;                //!< Constraint maximum values
  double gamma = 0.5;                            //!< scaling factor for constraints

  //! Constructor
  /*! \param z
        Design variable

      \param z_lo
        Lower bounds of the design variables

      \param z_up
        Upper bounds of the design variables

      \param objFunGrad
        Objective function gradient

      \param conFunGrad
        Constraint function gradient

      \param conMaxVals
        Constraint maximum values
  */
  OptimizeIpopt (std::vector<double> &z, std::vector<double> &z_lo, std::vector<double> &z_up,
                 std::vector<double> &objFunGrad, std::vector<std::vector<double>> &conFunGrad,
                 std::vector<double> conMaxVals);

  // Destructor
  virtual ~OptimizeIpopt ();

  // Method to scale constraints
  void ScaleConstraints ();

  // The following methods are Overloaded from Ipopt::TNLP class - DO NOT CHANGE THE NAME OF THE
  // METHODS

  // Method to return some info about the nlp
  virtual bool get_nlp_info (Ipopt::Index &n,             // number of x
                             Ipopt::Index &m,             // number of g
                             Ipopt::Index &nnz_jac_g,     // number of non zero elements of J
                             Ipopt::Index &nnz_h_lag,     // number of non zero elements of H
                             IndexStyleEnum &index_style  // style of the indexing (C starts from 0)
  );

  // Method to return the number of non-linear variables
  virtual Ipopt::Index get_number_of_nonlinear_variables ();

  // Method to return the bounds for the nlp
  virtual bool get_bounds_info (Ipopt::Index n,  // number of x
                                Number *x_l,     // lower bound for x
                                Number *x_u,     // upper bound for x
                                Ipopt::Index m,  // number of g
                                Number *g_l,     // lower bound for g
                                Number *g_u      // upper bound for g
  );

  // Method to return the starting point for the algorithm
  virtual bool get_starting_point (
      Ipopt::Index n,    // number of x
      bool init_x,       // if TRUE this method must provide an initial value for x
      Number *x,         // initial value for x
      bool init_z,       // if TRUE this method must provide an initial value for z
      Number *z_l,       // lower bound for z
      Number *z_u,       // upper bound for z
      Ipopt::Index m,    // number of g
      bool init_lambda,  // if TRUE this method must provide an initial value for lambda
      Number *lambda     // initial value for lambda
  );

  // Method to return the value of the objective function f
  virtual bool eval_f (Ipopt::Index n,   // number of x
                       const Number *x,  // value of x
                       bool new_x,  // FALSE if any evaluation method (eval_*) was previously called
                                    // with the same values in x
                       Number &obj_value  // value of f
  );

  // Method to return the value of the gradient of the objective function
  virtual bool eval_grad_f (Ipopt::Index n,   // number of x
                            const Number *x,  // value of x
                            bool new_x,  // FALSE if any evaluation method (eval_*) was previously
                                         // called with the same values in x
                            Number *grad_f  // value of df
  );

  // Method to return the value of the constraints
  virtual bool eval_g (Ipopt::Index n,   // number of x
                       const Number *x,  // value of x
                       bool new_x,  // FALSE if any evaluation method (eval_*) was previously called
                                    // with the same values in x
                       Ipopt::Index m,  // number of g
                       Number *g        // value of g
  );

  /* Method to return:
      1) The structure of the constraint's jacobian if "values" is NULL
      2) The values of the constraint's jacobian if "values" is not NULL
  */
  virtual bool eval_jac_g (
      Ipopt::Index n,   // number of x
      const Number *x,  // value of x
      bool new_x,  // FALSE if any evaluation method (eval_*) was previously called with the same
                   // values in x
      Ipopt::Index m,          // number of g
      Ipopt::Index nnz_jac_g,  // number of non zero elements of J
      Ipopt::Index *iRow,  // first call: vector of row indices of the non zero elements of J, later
                           // calls: NULL
      Ipopt::Index *jCol,  // first call: vector of column indices of the non zero elements of J,
                           // later calls: NULL
      Number *values       // first call: NULL, later calls: values of the non zero elements of J
  );

  // This method is called when the algorithm is complete so the TNLP can store/write the solution
  virtual void finalize_solution (
      SolverReturn status,              // status of the algorithm
      Ipopt::Index n,                   // number of x
      const Number *x,                  // optimal value of x
      const Number *z_l,                // final lower value of z
      const Number *z_u,                // final upper bound of z
      Ipopt::Index m,                   // number of g
      const Number *g,                  // final values of g
      const Number *lambda,             // final values lambda
      Number obj_value,                 // residual of f
      const IpoptData *ip_data,         // provided for expert users - not my case I think
      IpoptCalculatedQuantities *ip_cp  // provided for expert users - not my case I think
  );

 private:
  // Method to block default compiler methods
  OptimizeIpopt (const OptimizeIpopt &);

  OptimizeIpopt &operator= (const OptimizeIpopt &);
};
}  // namespace para_opt

#endif