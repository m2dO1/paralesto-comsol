//
// Copyright 2020 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef LEVELSETWRAPPER_H
#define LEVELSETWRAPPER_H

#include <omp.h>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>
#include <string>

#include "eigen3/Eigen/Dense"

#include "boundary.h"
#include "input_output.h"
#include "level_set.h"
#include "mesh.h"

/*! \file level_set_wrapper.h
    \brief A file that contains the class for wrapping the level set method
    of the topology optimization framework into a single object.
*/

namespace lsm_2d {
/*! \class LevelSetWrapper
    \brief Wrapper for all the level set functionality into a single object
*/
class LevelSetWrapper {
 public:
  using GridPtr = std::shared_ptr<Mesh>;
  using LevelSetPtr = std::shared_ptr<LevelSet>;
  using BoundaryPtr = std::shared_ptr<Boundary>;
  using InputOutputPtr = std::shared_ptr<InputOutput>;
  using BlobPtr = std::shared_ptr<Hole>;

  //! Default constructor
  LevelSetWrapper ();

  //! Destructor
  ~LevelSetWrapper ();
  
  //! Create an initial circular void
    /*! \param x
            double. X coordinate of the origin of the circle

        \param y
            double. Y coordinate of the origin of the circle

        \param r
            double. Outer radius
    */
    void CreateInitialCircleVoids (double x, double y, double r);

    //! Create a circular void maintained throughout the optimization
    /*! \param x
            double. X coordinate of the origin of the circle

        \param y
            double. Y coordinate of the origin of the circle

        \param r
            double. Outer radius
    */
    void CreateCircleVoids (double x, double y, double r);

    //! Create a circular non-design domain maintained throughout the optimization
    /*! \param x
            double. X coordinate of the origin of the circle

        \param y
            double. Y coordinate of the origin of the circle

        \param r
            double. Outer radius
    */
    void CreateCircleDomains (double x, double y, double r);
    
    //! Create an initial cuboid hole
    /*! \param x
            double. X coordinate of the origin of the cuboid

        \param y
            double. Y coordinate of the origin of the cuboid

        \param hx
            double. Half-width of the cuboid in the x direction

        \param hy
            double. Half-width of the cuboid in the y direction
    */
    void CreateInitialCuboidVoids (double x, double y, double hx, double hy);

    //! Create a cuboid hole maintained throughout the optimization
    /*! \param x
            double. X coordinate of the origin of the cuboid

        \param y
            double. Y coordinate of the origin of the cuboid

        \param hx
            double. Half-width of the cuboid in the x direction

        \param hy
            double. Half-width of the cuboid in the y direction
    */
    void CreateCuboidVoids (double x, double y, double hx, double hy);

    //! Create a cuboid non-design domain maintained throughout the optimization
    /*! \param x
            double. X coordinate of the origin of the cuboid

        \param y
            double. Y coordinate of the origin of the cuboid

        \param hx
            double. Half-width of the cuboid in the x direction

        \param hy
            double. Half-width of the cuboid in the y direction
    */
    void CreateCuboidDomains (double x, double y, double hx, double hy);

    //! Create level-set boundary in a region of the domain
    /*! \param x
            double. X coordinate of the origin of the cuboid

        \param y
            double. Y coordinate of the origin of the cuboid

        \param hx
            double. Half-width of the cuboid in the x direction

        \param hy
            double. Half-width of the cuboid in the y direction
    */
    void CreateLevelSetBoundary (double x, double y, double hx, double hy);

  //! Calculate the element densities (area fractions) of the level set mesh
  /*! \param is_print
        Specifies whether progress statements for the method will be printed
        during runtime. Useful for debugging.
  */
  std::vector<double> CalculateElementDensities (bool is_print = false);

  //! Map element sensitvities to boundary points
  /*! Use this method for the objective element sensitivites and each of the
      constraint element sensitivities

      \param elem_sens
        The partial derivative of the objective or constraint function with
        respect to the density of each element of the level set mesh (df/drho).

      \param is_print
        Prints out progress statements during method runtime. Useful for
        debugging.

      \note If you need the boundary point sensitivities for a volume
      constraint use the MapVolumeSensitivities function instead.
  */
  std::vector<double> MapSensitivities (std::vector<double> elem_sens, bool is_print = false);

  //! Return boundary point volume sensitivities
  std::vector<double> MapVolumeSensitivities ();

  //! Updates the level set via advection
  /*! \param bpoint_velocity
        Vector of velocities for each of the boundary points

      \param is_print
        Prints out progress statements during method runtime. Useful for
        debugging.
  */
  void Update (std::vector<double> bpoint_velocity, bool is_print = false);

  //! Get volume represented by the level set
  /*! CalculateElementDensities should be called before using this method
   */
  double GetVolume ();

  //! Get the number of boundary points that help define the discrete boundary
  /*! CalculateElementDensities should be called before using this method
   */
  int GetNumberBoundaryPoints ();

  //! Get the limits for the movement of each boundary point
  void GetLimits ();

  //! Writes element densities to a text file (.txt)
  /*! \param iteration
        Iteration number of the topology optimization cycle.

      \param file_path
        Directory to save the file to.

      \param file_name
        Name of the text file.

      \todo TODO(carolina): Move to wrapper_io class?
  */
  void WriteElementDensitiesToTxt (int iter = 0, std::string file_path = "",
                                   std::string file_name = "dens");

  //! Writes stl file of the level set boundary for the current iteration
  /*! \param iteration
        Iteration number of the topology optimization cycle.

      \param file_path
        Directory to save the file to.

      \param file_name
        Name of the text file.

      \todo TODO(carolina): Move to wrapper_io class?
  */
  void WriteVtk (int iter = 0, std::string file_path = "", std::string file_name = "opt_");

  //! Writes vtk file of the boundary points
  /*! \param BoundaryValues
        Vector of boundary values to be written to the vtk file
      
      \param curr_iter
        Iteration number of the topology optimization cycle.

      \param file_path
        Directory to save the file to.
      
      \param file_name
        Name of the text file.
  */
  void WriteBoundaryVtk (std::vector<std::vector<double>> BoundaryValues, int curr_iter,
                         std::string file_path, std::string file_name);
  
  //! Get the signed distance values of the level-set mesh
  /*! \return
        Vector of signed distance values.
  */
  std::vector<double> GetSignedDistanceValues ();

  //! Helper function for setting up the grid, level set, and boundary
  void SetUp ();

  //! Helper function for least squares interpolation of boundary sensitivities
  /*! \param bPoint
        Reference to the vector of boundary sensitivities

      \param hWidth
        Half-width of square region to sample points from for interpolation
  */
  std::vector<std::vector<int>> GetNearbyCellsInfo (std::vector<double> bPoint, int hWidth);

  //! Function to check if an index of a cell is in bound
  /*! \param i
        Index of the cell in the x direction

      \param j
        Index of the cell in the y direction
  */
  bool IsCellInBounds (int i, int j);

  //! Least squares interpolation of boundary sensitivities
  /*! \param boundary_sens
        Reference to the vector of boundary sensitivities

      \param element_sens
        Vector of sensitivities with respect to element densities

      \param half_width
        Half-width of square region to sample points from for interpolation

      \param weighted_vol_frac
  */
  void LeastSquareInterpolateBoundarySensitivities (std::vector<double> &boundary_sens,
                                                    std::vector<double> element_sens,
                                                    int half_width = 2, int weighted_vol_frac = 1);

  //! Discrete adjoint calculation of boundary sensitivities
  /*! \param boundary_sens
        Reference to the vector of boundary sensitivities

      \param element_sens
        Vector of sensitivities with respect to element densities
  */
  void DiscreteAdjointBoundarySensitivities (std::vector<double> &boundary_sens,
                                             std::vector<double> element_sens);

  //! Indicates which scheme should be used for mapping the element
  //! sensitivities to the boundary points.
  /*! 0 indicates least squares interpolation.
      1 indicates discrete adjoint.
  */
  int map_flag;

  double perturbation;  //!< Size of perturbation for sensitivity mapping

  double move_limit; //!< maximum distance that a node can move in a single iteration

  /*! \name Dimensionality*/
  ///\{
  int nelx;  //!< number of elements in the x direction
  int nely;  //!< number of elements in the y direction
  ///\}

  /*! \name Pointers to level set objects*/
  ///\{
  GridPtr grid_ptr;           //!< A pointer to the discrete grid for the level set
  LevelSetPtr level_set_ptr;  //!< A pointer to the level set
  BoundaryPtr boundary_ptr;   //!< A pointer to the discretized boundary
  InputOutputPtr io_ptr;      //!< A pointer to the input/output object
  ///\}

  /*! \name Initialization of voids*/
  ///\{
  std::vector<BlobPtr> initialVoids;   //!< vector of pointers to the initial holes
  std::vector<BlobPtr> domainVoids;    //!< vector of pointers to the domain holes
  std::vector<BlobPtr> fixedBlobs;     //!< vector of pointers to the fixed holes
  std::vector<BlobPtr> boundaryBlobs;  //!< vector of pointers to the level-set boundaries
  ///\}

  //! \name Boundary points limits
  ///\{
  std::vector<double> upper_lim, lower_lim;
  ///\}
};

}  // namespace lsm_2d
#endif

//######################################

                // MEX //

//######################################

#include "mex_class.h"

MEX_CLASS(lsm_2d::LevelSetWrapper) {
    def(self)
        .ctor()
        .mex_method(CreateInitialCircleVoids)
        .mex_method(CreateCircleVoids)
        .mex_method(CreateCircleDomains)
        .mex_method(CreateInitialCuboidVoids)
        .mex_method(CreateCuboidVoids)
        .mex_method(CreateCuboidDomains)
        .mex_method(CreateLevelSetBoundary)
	      .mex_method(CalculateElementDensities)
        .mex_method(MapSensitivities)
        .mex_method(MapVolumeSensitivities)
        .mex_method(Update)
        .mex_method(GetVolume)
        .mex_method(GetNumberBoundaryPoints)
        .mex_method(GetLimits)
        .mex_method(WriteElementDensitiesToTxt)
        .mex_method(WriteVtk)
        .mex_method(WriteBoundaryVtk)
        .mex_method(GetSignedDistanceValues)
        .mex_method(SetUp)
        .mex_property(nelx)
        .mex_property(nely)
        .mex_property(map_flag)
        .mex_property(perturbation)
        .mex_property(move_limit)
        .mex_property(upper_lim)
        .mex_property(lower_lim)
    ;
}
