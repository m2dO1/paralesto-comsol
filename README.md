# Level-set Topology optimization with COMSOL
Note that this code has only been tested for Ubuntu 22.04 and Windows 11.

## Software dependencies
### C++ dependencies
1. [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) 3.4.0
2. [NLopt](https://nlopt.readthedocs.io/en/latest/) 2.7.1 (optional, Linux only)
3. [GLPK](https://www.gnu.org/software/glpk/) 5.0 (optional, Linux only)

### COMSOL Multiphysics
1. [COMSOL Multiphysics](https://www.comsol.com/) 6.2
2. [LiveLink for MATLAB](https://www.comsol.com/livelink-for-matlab)
3. [Optimization Module](https://www.comsol.com/optimization-module)

### MATLAB
1. [MATLAB](https://www.mathworks.com/products/matlab.html) 2023.b
2. [mexPackage](https://www.mathworks.com/matlabcentral/fileexchange/78655-mexpackage) 1.13
4. [Optimization Toolbox](https://it.mathworks.com/products/optimization.html)
3. [C++ Compiler](https://it.mathworks.com/support/requirements/supported-compilers.html)

We recommend using [MinGW-w64](https://it.mathworks.com/matlabcentral/fileexchange/52848-matlab-support-for-mingw-w64-c-c-fortran-compiler) on Windows and [GCC](https://gcc.gnu.org/) on Linux. A list with all the compatible compilers is available [here](https://it.mathworks.com/support/requirements/supported-compilers.html).

Note that for ease of installation Eigen and mexPackage are included in the mexLSTO folder

## Instructions for installing C++ dependencies

### NLopt (optional, Linux only)
1. To clone the repository:

        git clone https://github.com/stevengj/nlopt.git

2. Navigate to the nlopt folder and follow the installation guide provided here: https://nlopt.readthedocs.io/en/latest/NLopt_Installation/
   Note that the installtion requires `cmake`, which can be installed on linux ubuntu using `sudo apt install cmake`

### GLPK (optional, Linux only)
1. The tarbell can be downloaded from one of the many mirrors at:

   https://www.gnu.org/prep/ftp.html

2. Once extracted, the file 'INSTALL' contains instructions for installation.


## Instructions to in install the software
1. Open MATLAB and run the `setup.m` script script from its location


## Instructions for running an example
1. Start COMSOL LiveLink for MATLAB. On a Windows machine, click on the `COMSOL Multiphysics with MATLAB` shortcut icon. On a Linux machine, open a terminal and use the command below

        comsol mphserver matlab

    Alternatively, instructions on how to connect MATLAB to a COMSOL server can be found [here](https://doc.comsol.com/5.6/doc/com.comsol.help.llmatlab/llmatlab_ug_start.5.05.html).

2. Add the helpers-matlab-files and mexLSTO folders and subfolders to the path. Alternatively, run the `setup.m` script
        
3. Navigate to the folder with the example

4. Run the example in MATLAB by clicking on 'RUN'

For convenience, two templates for general optimization problems (one for 2D and the other for 3D) are also provided in the `templates` folder


## Viewing optimization results
Optimization results are exported to stl (for 3D) and vtk (for 2D) files. We recommend using [Paraview](https://www.paraview.org/) for visualization, though other applications can be used as well. The optimized results are also saved as mph files to be opened with COMSOL Multiphysics.

## Citation
To cite this package, please cite the following:

A.T.R. Guibert, J. Hyun, A. Neofytou, and H.A. Kim. "Facilitating Multidisciplinary Collaboration through a Versatile Level-Set Topology Optimization Framework via COMSOL Multiphysics", (2024)

## Contact
If you have any questions, create an issue or email m2dolab@ucsd.edu.
