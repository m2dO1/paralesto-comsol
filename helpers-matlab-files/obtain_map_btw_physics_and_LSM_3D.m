function [physics_to_LSM_map, LSM_to_physics_map] = obtain_map_btw_physics_and_LSM_3D(XYZ_physics)

    % Copyright 2023 H Alicia Kim
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %     http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.

    % ========================================================================
    % 3D CASE
    % 
    % This MATLAB code is aimed to produce the "map" of the elements between 
    % the physics solver and LSM interface. Therefore, as inputs, the centroid
    % coordinates of finite elements in physics solver as well as the specified
    % sorting direction (e.g., x,y,z or z,y,x or z,x,y, etc.) will be used.
    % Also, the outputs are "physics_to_LSM_map" and "LSM_to_physics_map".
    %
    % In the main LSTO code,
    % 1) When assigning the element densities from LSM interface to physics 
    %    solver, the "LSM_to_physics_map" can be used as,
    %    -> element_densities_physics = element_densities_LSM(LSM_to_physics_map)
    %
    % 2) When passing the element sensitivities from physics solver to LSM
    %    interface, the "physics_to_LSM_map" can be used as,
    %    -> element_sensitivities_LSM = element_sensitivities_physics(physics_to_LSM_maps)
    % ========================================================================

    XYZ_physics = round(XYZ_physics,5,"significant"); % Very important to have a correct significant
   
    %% Obtain the "physics_to_LSM_map"  
    % Sort the element centroids in physics domain to map those in LSM domain
    % It is assumed that the order of element numbering in LSM domain is followed 
    % by X (1st column) -> Y (2nd column) -> Z (3rd column)
    
    % ------------------------------------------------------------------------
    % X (1st column) -> Y (2nd column) -> Z (3rd column)
    % ------------------------------------------------------------------------
    [XYZ_LSM_by_X, physics_to_LSM_map_by_X] = sortrows(XYZ_physics,1);
    [XYZ_LSM_by_XY, physics_to_LSM_map_by_XY] = sortrows(XYZ_LSM_by_X,2);
    [XYZ_LSM_by_XYZ, physics_to_LSM_map_by_XYZ] = sortrows(XYZ_LSM_by_XY,3);
    % ------------------------------------------------------------------------

    XYZ_LSM = XYZ_LSM_by_XYZ;
    physics_to_LSM_map = physics_to_LSM_map_by_X(physics_to_LSM_map_by_XY(physics_to_LSM_map_by_XYZ));

    % Check if the sorting is correct or not
    if (isequal(XYZ_physics(physics_to_LSM_map,:),XYZ_LSM) == 1)
        disp('XYZ_physics(physics_to_LSM_map,:) == XYZ_LSM');
    else
        disp('XYZ_physics(physics_to_LSM_map,:) ~= XYZ_LSM');
    end

    %% Obtain the "LSM_to_physics_map"
    [~, LSM_to_physics_map] = sort(physics_to_LSM_map);

    % Check if the sorting is correct or not
    if (isequal(XYZ_LSM(LSM_to_physics_map,:),XYZ_physics) == 1)
        disp('XYZ_LSM(LSM_to_physics_map,:) == XYZ_physics');    
    else
        disp('XYZ_LSM(LSM_to_physics_map,:) ~= XYZ_physics');
    end
end