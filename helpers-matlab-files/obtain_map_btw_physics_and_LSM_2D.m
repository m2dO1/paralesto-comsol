function [physics_to_LSM_map, LSM_to_physics_map] = obtain_map_btw_physics_and_LSM_2D(XY_physics)

    % Copyright 2023 H Alicia Kim
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %     http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.

    % ========================================================================
    % 2D CASE
    % 
    % This MATLAB code is aimed to produce the "map" of the elements between 
    % the physics solver and LSM interface. Therefore, as inputs, the centroid
    % coordinates of finite elements in physics solver as well as the specified
    % sorting direction (e.g., x,y or y,x) will be used.
    % Also, the outputs are "physics_to_LSM_map" and "LSM_to_physics_map".
    %
    % In the main LSTO code,
    % 1) When assigning the element densities from LSM interface to physics 
    %    solver, the "LSM_to_physics_map" can be used as,
    %    -> element_densities_physics = element_densities_LSM(LSM_to_physics_map)
    %
    % 2) When passing the element sensitivities from physics solver to LSM
    %    interface, the "physics_to_LSM_map" can be used as,
    %    -> element_sensitivities_LSM = element_sensitivities_physics(physics_to_LSM_maps)
    % ========================================================================

    XY_physics = round(XY_physics,5,"significant"); % Very important to have a correct significant
   
    %% Obtain the "physics_to_LSM_map"  
    % Sort the element centroids in physics domain to map those in LSM domain
    % It is assumed that the order of element numbering in LSM domain is followed 
    % by X (1st column) -> Y (2nd column)
    
    % ------------------------------------------------------------------------
    % X (1st column) -> Y (2nd column)
    % ------------------------------------------------------------------------
    [XY_LSM_by_X, physics_to_LSM_map_by_X] = sortrows(XY_physics,1);
    [XY_LSM_by_XY, physics_to_LSM_map_by_XY] = sortrows(XY_LSM_by_X,2);
    % ------------------------------------------------------------------------

    XY_LSM = XY_LSM_by_XY;
    physics_to_LSM_map = physics_to_LSM_map_by_X(physics_to_LSM_map_by_XY);

    % Check if the sorting is correct or not
    if (isequal(XY_physics(physics_to_LSM_map,:),XY_LSM))
        disp('XY_physics(physics_to_LSM_map,:) == XY_LSM');
    else
        disp('XY_physics(physics_to_LSM_map,:) ~= XY_LSM');
    end

    %% Obtain the "LSM_to_physics_map"
    [~, LSM_to_physics_map] = sort(physics_to_LSM_map);

    % Check if the sorting is correct or not
    if (isequal(XY_LSM(LSM_to_physics_map,:),XY_physics))
        disp('XY_LSM(LSM_to_physics_map,:) == XY_physics');    
    else
        disp('XY_LSM(LSM_to_physics_map,:) ~= XY_physics');
    end
end