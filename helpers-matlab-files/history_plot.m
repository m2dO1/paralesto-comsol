function history_plot(Obj_History,Vol_History,legend_str)

    % Copyright 2023 H Alicia Kim
    %
    % Licensed under the Apache License, Version 2.0 (the "License");
    % you may not use this file except in compliance with the License.
    % You may obtain a copy of the License at
    %
    %     http://www.apache.org/licenses/LICENSE-2.0
    %
    % Unless required by applicable law or agreed to in writing, software
    % distributed under the License is distributed on an "AS IS" BASIS,
    % WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    % See the License for the specific language governing permissions and
    % limitations under the License.

    FontSize = 17;

    % For objective function
    yyaxis left;
    plot(1:size(Obj_History, 1),Obj_History,'linewidth',1.5);
    Axes = gca;
    Axes.FontSize = FontSize;
    Axes.YColor = [0 0.4470 0.7410];
    xlabel('Iteration','fontsize',FontSize);
    ylabel('J','fontsize',FontSize);

    % For volume
    yyaxis right;
    plot(1:numel(Vol_History),Vol_History,'linewidth',1.5);
    Axes = gca;
    Axes.FontSize = FontSize;
    Axes.YColor = [0.8500 0.3250 0.0980];
    xlabel('Iteration','fontsize',FontSize);
    ylabel('Volume (%)','fontsize',FontSize);

    legend(legend_str,'location','east')

    set(gca, 'FontSize', FontSize)
