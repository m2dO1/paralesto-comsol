function x = optimize(f, A, cons, cons_curr, lb, ub, do_normalize)

    % linprog EXITFLAG values
    %{
     3  linprog converged to a solution X with poor constraint feasibility.
     1  linprog converged to a solution X.
     0  Maximum number of iterations reached.
    -2  No feasible point found.
    -3  Problem is unbounded.
    -4  NaN value encountered during execution of algorithm.
    -5  Both primal and dual problems are infeasible.
    -7  Magnitude of search direction became too small; no further
        progress can be made. The problem is ill-posed or badly
        conditioned.
    -9  linprog lost feasibility probably due to ill-conditioned matrix.
    %}

    % Inputs
    if nargin == 6
        do_normalize = false; % default false: no scaling
    end

    % Options
    algorithm = "dual-simplex"; % "dual-simplex" or "interior-point"
    options = optimoptions("linprog", "Algorithm", algorithm, ...
                           "Display", "none");

    % Equality constraints
    Aeq = [];
    beq = [];

    % Constraint distances
    b = cons - cons_curr;

    % Normalize objective gradient
    if do_normalize
        max_abs_grad = max(abs(f));
        if max_abs_grad > 0
            f = f / max_abs_grad;
        end
    end

    % Scale constraints
    scaling = 0.5;
    for ii = 1:length(b) % loop over constraints
        
        % Normalize constraint gradient
        if do_normalize
            max_abs_grad = max(abs(A(ii, :)));
            if max_abs_grad > 0
                A(ii, :) = A(ii, :) / max_abs_grad;
                b(ii) = b(ii) / max_abs_grad;
            end
        end

        % Check if the constraint is not yet satisfied
        if (b(ii) < 0)
            
            % This is the lowest achievable variation
            min_variation = sum(min(lb.*(A(ii, :).'), ub.*(A(ii, :).')));

            % Scale the variation
            min_variation = min_variation * scaling;

            % Relax the constraint if it cannot be satisfied
            b(ii) = max(b(ii), min_variation);
        end
    end

    
    % Solve
    if size(b) == 1     
        % One constraint
        [x, ~, exitflag] = linprog(f, A.', b, Aeq, beq, lb, ub, options);
    else 
        % Multiple constraints
        [x, ~, exitflag] = linprog(f, A, b, Aeq, beq, lb, ub, options);
    end
    
    if exitflag ~= 1
        disp(['WARNING: linprog exit code: ', num2str(exitflag)])
    end

end







