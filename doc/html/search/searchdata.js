var indexSectionsWithContent =
{
  0: "abcdefghijklmnoprstuvwxyz~",
  1: "bcefghilmnopstv",
  2: "lp",
  3: "bcdfghilmor",
  4: "abcdefghiklmnoprstuvw~",
  5: "abcdefghijklmnoprstuvwxyz",
  6: "bgil",
  7: "efn",
  8: "bcfimnot",
  9: "lmr",
  10: "lt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros",
  10: "Pages"
};

